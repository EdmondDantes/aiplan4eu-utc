(define (problem huddersfield)
(:domain urbantraffic)
(:objects
wrac1 wrbc1 wrcc1 wrdc1 wrec1 wrfc1 - junction
hsac3_c_wrac1 wrac1_z_hsac1 wrac1_y_wrbc1 wrac1_m_stand stand_f_wrac1 wrac1_n_firth firth_d_wrac1 wrac1_x_wrbc1 wrbc1_a_wrac1 wrbc1_b_wrcc1 wrbc1_r_silve silve_w_wrbc1 wrbc1_s_somer somer_v_wrbc1 wrbc1_b_wrac1 wrcc1_z_wrbc1 wrcc1_x_wrdc1 smith_c_wrcc1 wrcc1_w_wrdc1 wrdc1_a_wrcc1 wrdc1_b_wrec1 wrdc1_q_abnor wrdc1_l_absou abnor_v_wrdc1 wrec1_z_wrdc1 wrec1_y_wrfc1 oldwa_c_wrec1 wrec1_y_wrdc1 oldwa_d_wrec1 wakef_z_wrfc1 wrfc1_a_wrec1 wrfc1_r_broad broad_x_wrfc1 wrfc1_t_wakef outside - link
wrac1_stage1 wrac1_stage2 wrac1_stage3 wrac1_stage4 wrbc1_stage1 wrbc1_stage2 wrbc1_stage3 wrbc1_stage4 wrbc1_stage5 wrcc1_stage1 wrcc1_stage2 wrcc1_stage3 wrcc1_stage4 wrcc1_stage5 wrcc1_stage6 wrdc1_stage1 wrdc1_stage2 wrdc1_stage3 wrdc1_stage4 wrec1_stage1 wrec1_stage2 wrec1_stage3 wrec1_stage4 wrfc1_stage1 wrfc1_stage2 wrfc1_stage3 fake - stage
)
(:init

(controllable wrac1)
(controllable wrbc1)
(controllable wrcc1)
(controllable wrdc1)
(controllable wrec1)
(controllable wrfc1)

(= (capacity outside) 100000.0)
(= (capacity hsac3_c_wrac1)   100000.0)
(= (capacity wrac1_z_hsac1)   100000.0)
(= (capacity wrac1_y_wrbc1)   55.5)
(= (capacity wrac1_m_stand)   100000.0)
(= (capacity stand_f_wrac1)   100000.0)
(= (capacity wrac1_n_firth)   100000.0)
(= (capacity firth_d_wrac1)   100000.0)
(= (capacity wrac1_x_wrbc1)   24.5)
(= (capacity wrbc1_a_wrac1)   85.0)
(= (capacity wrbc1_b_wrcc1)   30.66)
(= (capacity wrbc1_r_silve)  100000.0)
(= (capacity silve_w_wrbc1)  100000.0)
(= (capacity wrbc1_s_somer)  100000.0)
(= (capacity somer_v_wrbc1)  100000.0)
(= (capacity wrbc1_b_wrac1)  12.0)
(= (capacity wrcc1_z_wrbc1)  56.0)
(= (capacity wrcc1_x_wrdc1)  93.3)
(= (capacity smith_c_wrcc1)  100000.0)
(= (capacity wrdc1_a_wrcc1)  42.0)
(= (capacity wrdc1_b_wrec1)  26.67)
(= (capacity wrdc1_q_abnor)  100000.0)
(= (capacity wrcc1_w_wrdc1)   5.0)
(= (capacity wrdc1_l_absou)   100000.0)
(= (capacity abnor_v_wrdc1)   100000.0)
(= (capacity wrec1_z_wrdc1)   28.67)
(= (capacity wrec1_y_wrfc1)   13.0)
(= (capacity oldwa_c_wrec1)   100000.0)
(= (capacity wrec1_y_wrdc1)   6.33)
(= (capacity oldwa_d_wrec1)   100000.0)
(= (capacity wakef_z_wrfc1)   100000.0)
(= (capacity wrfc1_a_wrec1)   17.4)
(= (capacity wrfc1_r_broad)   100000.0)
(= (capacity broad_x_wrfc1)   100000.0)
(= (capacity wrfc1_t_wakef)   100000.0)


(contains wrac1 wrac1_stage1)
(contains wrac1 wrac1_stage2)
(contains wrac1 wrac1_stage3)
(contains wrac1 wrac1_stage4)
(contains wrbc1 wrbc1_stage1)
(contains wrbc1 wrbc1_stage2)
(contains wrbc1 wrbc1_stage3)
(contains wrbc1 wrbc1_stage4)
(contains wrbc1 wrbc1_stage5)
(contains wrcc1 wrcc1_stage1)
(contains wrcc1 wrcc1_stage2)
(contains wrcc1 wrcc1_stage3)
(contains wrcc1 wrcc1_stage4)
(contains wrcc1 wrcc1_stage5)
(contains wrcc1 wrcc1_stage6)
(contains wrdc1 wrdc1_stage1)
(contains wrdc1 wrdc1_stage2)
(contains wrdc1 wrdc1_stage3)
(contains wrdc1 wrdc1_stage4)
(contains wrec1 wrec1_stage1)
(contains wrec1 wrec1_stage2)
(contains wrec1 wrec1_stage3)
(contains wrec1 wrec1_stage4)
(contains wrfc1 wrfc1_stage1)
(contains wrfc1 wrfc1_stage2)
(contains wrfc1 wrfc1_stage3)

(= (interlimit wrac1_stage1 )5)
(= (interlimit wrac1_stage2 )6)
(= (interlimit wrac1_stage3 )5)
(= (interlimit wrac1_stage4 )5)

(= (interlimit wrbc1_stage1 )6)
(= (interlimit wrbc1_stage2 )12)
(= (interlimit wrbc1_stage3 )6)
(= (interlimit wrbc1_stage4 )7)
(= (interlimit wrbc1_stage5 )12)

(= (interlimit wrcc1_stage1 )6)
(= (interlimit wrcc1_stage2 )8)
(= (interlimit wrcc1_stage3 )8)
(= (interlimit wrcc1_stage4 )11)
(= (interlimit wrcc1_stage5 )3)
(= (interlimit wrcc1_stage6 )2)

(= (interlimit wrdc1_stage1 )8)
(= (interlimit wrdc1_stage2 )8)
(= (interlimit wrdc1_stage3 )6)
(= (interlimit wrdc1_stage4 )7)

(= (interlimit wrec1_stage1 )3)
(= (interlimit wrec1_stage2 )7)
(= (interlimit wrec1_stage3 )7)
(= (interlimit wrec1_stage4 )7)

(= (interlimit wrfc1_stage1 )5)
(= (interlimit wrfc1_stage2 )9)
(= (interlimit wrfc1_stage3 )8)

(next wrac1_stage1 wrac1_stage2)
(next wrac1_stage2 wrac1_stage3)
(next wrac1_stage3 wrac1_stage4)
(next wrac1_stage4 wrac1_stage1)

(next wrbc1_stage1 wrbc1_stage2)
(next wrbc1_stage2 wrbc1_stage3)
(next wrbc1_stage3 wrbc1_stage4)
(next wrbc1_stage4 wrbc1_stage5)
(next wrbc1_stage5 wrbc1_stage1)

(next wrcc1_stage1 wrcc1_stage2)
(next wrcc1_stage2 wrcc1_stage3)
(next wrcc1_stage3 wrcc1_stage4)
(next wrcc1_stage4 wrcc1_stage5)
(next wrcc1_stage5 wrcc1_stage6)
(next wrcc1_stage6 wrcc1_stage1)

(next wrdc1_stage1 wrdc1_stage2)
(next wrdc1_stage2 wrdc1_stage3)
(next wrdc1_stage3 wrdc1_stage4)
(next wrdc1_stage4 wrdc1_stage1)

(next wrec1_stage1 wrec1_stage2)
(next wrec1_stage2 wrec1_stage3)
(next wrec1_stage3 wrec1_stage4)
(next wrec1_stage4 wrec1_stage1)

(next wrfc1_stage1 wrfc1_stage2)
(next wrfc1_stage2 wrfc1_stage3)
(next wrfc1_stage3 wrfc1_stage1)

(= (mingreentime wrac1_stage1 ) 10 )
(= (maxgreentime wrac1_stage1 ) 120 )

(= (mingreentime wrac1_stage2 ) 9 )
(= (maxgreentime wrac1_stage2 ) 120 )

(= (mingreentime wrac1_stage3 ) 8 )
(= (maxgreentime wrac1_stage3 ) 120 )

(= (mingreentime wrac1_stage4 ) 3 )
(= (maxgreentime wrac1_stage4 ) 120 )

(= (mingreentime wrbc1_stage1 ) 7 )
(= (maxgreentime wrbc1_stage1 ) 120 )

(= (mingreentime wrbc1_stage2 ) 5 )
(= (maxgreentime wrbc1_stage2 ) 120 )

(= (mingreentime wrbc1_stage3 ) 10 )
(= (maxgreentime wrbc1_stage3 ) 120 )

(= (mingreentime wrbc1_stage4 ) 5 )
(= (maxgreentime wrbc1_stage4 ) 120 )

(= (mingreentime wrbc1_stage5 ) 2 )
(= (maxgreentime wrbc1_stage5 ) 120 )

(= (mingreentime wrcc1_stage1 ) 7 )
(= (maxgreentime wrcc1_stage1 ) 120 )

(= (mingreentime wrcc1_stage2 ) 5 )
(= (maxgreentime wrcc1_stage2 ) 120 )

(= (mingreentime wrcc1_stage3 ) 5 )
(= (maxgreentime wrcc1_stage3 ) 120 )

(= (mingreentime wrcc1_stage4 ) 4 )
(= (maxgreentime wrcc1_stage4 ) 120 )

(= (mingreentime wrcc1_stage5 ) 7 )
(= (maxgreentime wrcc1_stage5 ) 120 )

(= (mingreentime wrcc1_stage6 ) 2 )
(= (maxgreentime wrcc1_stage6 ) 120 )

(= (mingreentime wrdc1_stage1 ) 7 )
(= (maxgreentime wrdc1_stage1 ) 120 )

(= (mingreentime wrdc1_stage2 ) 7 )
(= (maxgreentime wrdc1_stage2 ) 120 )

(= (mingreentime wrdc1_stage3 ) 7 )
(= (maxgreentime wrdc1_stage3 ) 120 )

(= (mingreentime wrdc1_stage4 ) 7 )
(= (maxgreentime wrdc1_stage4 ) 120 )

(= (mingreentime wrec1_stage1 ) 10 )
(= (maxgreentime wrec1_stage1 ) 120 )

(= (mingreentime wrec1_stage2 ) 2 )
(= (maxgreentime wrec1_stage2 ) 120 )

(= (mingreentime wrec1_stage3 ) 6 )
(= (maxgreentime wrec1_stage3 ) 120 )

(= (mingreentime wrec1_stage4 ) 6 )
(= (maxgreentime wrec1_stage4 ) 120 )

(= (mingreentime wrfc1_stage1 ) 10 )
(= (maxgreentime wrfc1_stage1 ) 120 )

(= (mingreentime wrfc1_stage2 ) 4 )
(= (maxgreentime wrfc1_stage2 ) 120 )

(= (mingreentime wrfc1_stage3 ) 7 )
(= (maxgreentime wrfc1_stage3 ) 120 )



(= (defaultgreentime wrac1_stage1) 45)
(= (defaultgreentime wrac1_stage2) 14)
(= (defaultgreentime wrac1_stage3) 19)
(= (defaultgreentime wrac1_stage4) 10)
(= (defaultgreentime wrbc1_stage1) 47)
(= (defaultgreentime wrbc1_stage2) 15)
(= (defaultgreentime wrbc1_stage3) 26)
(= (defaultgreentime wrbc1_stage4) 7)
(= (defaultgreentime wrbc1_stage5) 7)
(= (defaultgreentime wrcc1_stage1) 50)
(= (defaultgreentime wrcc1_stage2) 7)
(= (defaultgreentime wrcc1_stage3) 20)
(= (defaultgreentime wrcc1_stage4) 7)
(= (defaultgreentime wrcc1_stage5) 18)
(= (defaultgreentime wrcc1_stage6) 7)
(= (defaultgreentime wrdc1_stage1) 41)
(= (defaultgreentime wrdc1_stage2) 17)
(= (defaultgreentime wrdc1_stage3) 15)
(= (defaultgreentime wrdc1_stage4) 15)
(= (defaultgreentime wrec1_stage1) 72)
(= (defaultgreentime wrec1_stage2) 7)
(= (defaultgreentime wrec1_stage3) 16)
(= (defaultgreentime wrec1_stage4) 7)
(= (defaultgreentime wrfc1_stage1) 67)
(= (defaultgreentime wrfc1_stage2) 9)
(= (defaultgreentime wrfc1_stage3) 12)


(= (counter abnor_v_wrdc1)  0.0)
(= (counter broad_x_wrfc1)   0.0)
(= (counter firth_d_wrac1)   0.0)
(= (counter hsac3_c_wrac1)   0.0)
(= (counter oldwa_c_wrec1)   0.0)
(= (counter oldwa_d_wrec1)   0.0)
(= (counter silve_w_wrbc1)  0.0)
(= (counter smith_c_wrcc1)  0.0)
(= (counter somer_v_wrbc1)  0.0)
(= (counter stand_f_wrac1)   0.0)
(= (counter wakef_z_wrfc1)   0.0)
(= (counter wrac1_m_stand)   0.0)
(= (counter wrac1_n_firth)  0.0)
(= (counter wrac1_x_wrbc1)   0.0)
(= (counter wrac1_y_wrbc1)   0.0)
(= (counter wrac1_z_hsac1)   0.0)
(= (counter wrbc1_a_wrac1)   0.0)
(= (counter wrbc1_b_wrac1)  0.0)
(= (counter wrbc1_b_wrcc1)   0.0)
(= (counter wrbc1_r_silve)  0.0)
(= (counter wrbc1_s_somer)  0.0)
(= (counter wrcc1_w_wrdc1)  0.0)
(= (counter wrcc1_x_wrdc1)  0.0)
(= (counter wrcc1_z_wrbc1)  0.0)
(= (counter wrdc1_a_wrcc1)  0.0)
(= (counter wrdc1_b_wrec1)  0.0)
(= (counter wrdc1_l_absou)  0.0)
(= (counter wrdc1_q_abnor)  0.0)
(= (counter wrec1_y_wrdc1)   0.0)
(= (counter wrec1_y_wrfc1)   0.0)
(= (counter wrec1_z_wrdc1)  0.0)
(= (counter wrfc1_a_wrec1)   0.0)
(= (counter wrfc1_r_broad)   0.0)
(= (counter wrfc1_t_wakef)   0.0)
(= (cycletime wrac1) 88)
(= (cycletime wrbc1) 102)
(= (cycletime wrcc1) 109)
(= (cycletime wrdc1) 88)
(= (cycletime wrec1) 102)
(= (cycletime wrfc1) 102)
(= (granularity) 10.0)
(= (maxcycletime wrac1) 200)
(= (maxcycletime wrbc1) 200)
(= (maxcycletime wrcc1) 200)
(= (maxcycletime wrdc1) 200)
(= (maxcycletime wrec1) 200)
(= (maxcycletime wrfc1) 200)
(= (mincycletime wrac1) 50)
(= (mincycletime wrbc1) 50)
(= (mincycletime wrcc1) 50)
(= (mincycletime wrdc1) 50)
(= (mincycletime wrec1) 50)
(= (mincycletime wrfc1) 50)
(= (time) 0)




(= (turnrate wrac1_stage1 hsac3_c_wrac1 wrac1_x_wrbc1) 0.294)
(= (turnrate wrac1_stage1 hsac3_c_wrac1 wrac1_m_stand) 0.178)
(= (turnrate wrac1_stage1 hsac3_c_wrac1 wrac1_y_wrbc1) 0.704)
(= (turnrate wrac1_stage1 wrbc1_a_wrac1 wrac1_z_hsac1) 0.844)
(= (turnrate wrac1_stage1 wrbc1_a_wrac1 wrac1_n_firth) 0.097)
(= (turnrate wrac1_stage2 wrbc1_a_wrac1 wrac1_z_hsac1) 0.844)
(= (turnrate wrac1_stage2 wrbc1_a_wrac1 wrac1_n_firth) 0.097)
(= (turnrate wrac1_stage2 wrbc1_b_wrac1 wrac1_m_stand) 0.412)
(= (turnrate wrbc1_stage1 wrac1_y_wrbc1 wrbc1_b_wrcc1) 0.592)
(= (turnrate wrbc1_stage1 wrac1_y_wrbc1 wrbc1_r_silve) 0.114)
(= (turnrate wrbc1_stage1 wrcc1_z_wrbc1 wrbc1_a_wrac1) 0.904)
(= (turnrate wrbc1_stage1 wrcc1_z_wrbc1 wrbc1_b_wrac1) 0.22)
(= (turnrate wrbc1_stage1 wrcc1_z_wrbc1 wrbc1_r_silve) 0.026)
(= (turnrate wrbc1_stage1 wrcc1_z_wrbc1 wrbc1_s_somer) 0.027)
(= (turnrate wrbc1_stage2 silve_w_wrbc1 wrbc1_a_wrac1) 0.182)
(= (turnrate wrbc1_stage2 silve_w_wrbc1 wrbc1_b_wrac1) 0.068)
(= (turnrate wrbc1_stage2 silve_w_wrbc1 wrbc1_b_wrcc1) 0.093)
(= (turnrate wrbc1_stage2 silve_w_wrbc1 wrbc1_s_somer) 0.07)
(= (turnrate wrbc1_stage3 wrac1_y_wrbc1 wrbc1_b_wrcc1) 0.592)
(= (turnrate wrbc1_stage3 wrac1_y_wrbc1 wrbc1_r_silve) 0.114)
(= (turnrate wrbc1_stage3 wrac1_x_wrbc1 wrbc1_s_somer) 0.412)
(= (turnrate wrbc1_stage3 somer_v_wrbc1 wrbc1_a_wrac1) 0.478)
(= (turnrate wrbc1_stage3 somer_v_wrbc1 wrbc1_b_wrac1) 0.111)
(= (turnrate wrbc1_stage4 wrac1_x_wrbc1 wrbc1_s_somer) 0.412)
(= (turnrate wrbc1_stage4 somer_v_wrbc1 wrbc1_a_wrac1) 0.478)
(= (turnrate wrbc1_stage4 somer_v_wrbc1 wrbc1_b_wrac1) 0.111)
(= (turnrate wrbc1_stage5 wrac1_y_wrbc1 wrbc1_b_wrcc1) 0.592)
(= (turnrate wrbc1_stage5 wrac1_y_wrbc1 wrbc1_r_silve) 0.114)
(= (turnrate wrbc1_stage5 wrac1_x_wrbc1 wrbc1_s_somer) 0.412)
(= (turnrate wrcc1_stage1 wrbc1_b_wrcc1 wrcc1_x_wrdc1) 0.467)
(= (turnrate wrcc1_stage1 wrbc1_b_wrcc1 wrcc1_w_wrdc1) 0.003)
(= (turnrate wrcc1_stage1 wrdc1_a_wrcc1 wrcc1_z_wrbc1) 0.824)
(= (turnrate wrcc1_stage2 wrdc1_a_wrcc1 wrcc1_z_wrbc1) 0.824)
(= (turnrate wrcc1_stage3 wrbc1_b_wrcc1 wrcc1_x_wrdc1) 0.467)
(= (turnrate wrcc1_stage3 wrbc1_b_wrcc1 wrcc1_w_wrdc1) 0.003)
(= (turnrate wrcc1_stage4 smith_c_wrcc1 wrcc1_z_wrbc1) 0.001)
(= (turnrate wrcc1_stage4 smith_c_wrcc1 wrcc1_x_wrdc1) 0.468)
(= (turnrate wrcc1_stage4 smith_c_wrcc1 wrcc1_w_wrdc1) 0.001)
(= (turnrate wrcc1_stage5 smith_c_wrcc1 wrcc1_z_wrbc1) 0.001)
(= (turnrate wrcc1_stage5 smith_c_wrcc1 wrcc1_x_wrdc1) 0.468)
(= (turnrate wrcc1_stage5 smith_c_wrcc1 wrcc1_w_wrdc1) 0.001)
(= (turnrate wrdc1_stage1 wrcc1_x_wrdc1 wrdc1_b_wrec1) 0.821)
(= (turnrate wrdc1_stage1 wrcc1_x_wrdc1 wrdc1_l_absou) 0.002)
(= (turnrate wrdc1_stage1 wrec1_z_wrdc1 wrdc1_a_wrcc1) 0.867)
(= (turnrate wrdc1_stage1 wrec1_z_wrdc1 wrdc1_q_abnor) 0.133)
(= (turnrate wrdc1_stage2 wrec1_z_wrdc1 wrdc1_a_wrcc1) 0.867)
(= (turnrate wrdc1_stage2 wrec1_z_wrdc1 wrdc1_q_abnor) 0.133)
(= (turnrate wrdc1_stage2 wrec1_y_wrdc1 wrdc1_l_absou) 0.353)
(= (turnrate wrdc1_stage3 abnor_v_wrdc1 wrdc1_a_wrcc1) 0.283)
(= (turnrate wrdc1_stage3 abnor_v_wrdc1 wrdc1_b_wrec1) 0.215)
(= (turnrate wrdc1_stage3 abnor_v_wrdc1 wrdc1_l_absou) 0.03)
(= (turnrate wrdc1_stage4 wrcc1_x_wrdc1 wrdc1_b_wrec1) 0.821)
(= (turnrate wrdc1_stage4 wrcc1_x_wrdc1 wrdc1_l_absou) 0.002)
(= (turnrate wrdc1_stage4 wrcc1_w_wrdc1 wrdc1_q_abnor) 0.353)
(= (turnrate wrec1_stage1 wrfc1_a_wrec1 wrec1_z_wrdc1) 0.774)
(= (turnrate wrec1_stage1 wrfc1_a_wrec1 wrec1_y_wrdc1) 0.049)
(= (turnrate wrec1_stage1 wrdc1_b_wrec1 wrec1_y_wrfc1) 0.824)
(= (turnrate wrec1_stage2 wrdc1_b_wrec1 wrec1_y_wrfc1) 0.824)
(= (turnrate wrec1_stage3 oldwa_c_wrec1 wrec1_z_wrdc1) 0.332)
(= (turnrate wrec1_stage3 oldwa_c_wrec1 wrec1_y_wrdc1) 0.021)
(= (turnrate wrec1_stage3 oldwa_d_wrec1 wrec1_y_wrfc1) 0.353)
(= (turnrate wrec1_stage4 wrfc1_a_wrec1 wrec1_z_wrdc1) 0.774)
(= (turnrate wrec1_stage4 wrfc1_a_wrec1 wrec1_y_wrdc1) 0.049)
(= (turnrate wrec1_stage4 oldwa_d_wrec1 wrec1_y_wrfc1) 0.353)
(= (turnrate wrfc1_stage1 wakef_z_wrfc1 wrfc1_a_wrec1) 0.765)
(= (turnrate wrfc1_stage1 wrec1_y_wrfc1 wrfc1_r_broad) 0.249)
(= (turnrate wrfc1_stage1 wrec1_y_wrfc1 wrfc1_t_wakef) 0.575)
(= (turnrate wrfc1_stage2 wrec1_y_wrfc1 wrfc1_r_broad) 0.249)
(= (turnrate wrfc1_stage2 wrec1_y_wrfc1 wrfc1_t_wakef) 0.575)
(= (turnrate wrfc1_stage3 broad_x_wrfc1 wrfc1_a_wrec1) 0.586)
(= (turnrate wrfc1_stage3 broad_x_wrfc1 wrfc1_t_wakef) 0.002)
(= (turnrate wrac1_stage3 firth_d_wrac1 wrac1_z_hsac1) 0.118)
(= (turnrate wrac1_stage3 firth_d_wrac1 wrac1_m_stand) 0.294)
(= (turnrate wrac1_stage4 firth_d_wrac1 wrac1_z_hsac1) 0.057)
(= (turnrate wrac1_stage4 firth_d_wrac1 wrac1_y_wrbc1) 0.165)
(= (turnrate wrac1_stage4 firth_d_wrac1 wrac1_x_wrbc1) 0.049)
(= (turnrate wrac1_stage4 firth_d_wrac1 wrac1_m_stand) 0.141)
(= (turnrate wrac1_stage2 stand_f_wrac1 wrac1_y_wrbc1) 0.372)
(= (turnrate wrac1_stage2 stand_f_wrac1 wrac1_x_wrbc1) 0.154)
(= (turnrate wrac1_stage3 stand_f_wrac1 wrac1_n_firth) 0.339)
(= (turnrate wrac1_stage3 stand_f_wrac1 wrac1_y_wrbc1) 0.159)
(= (turnrate wrac1_stage3 stand_f_wrac1 wrac1_x_wrbc1) 0.089)



























(= (turnrate fake outside abnor_v_wrdc1) 0.0736)


(= (turnrate fake outside broad_x_wrfc1) 0.1798)


(= (turnrate fake outside firth_d_wrac1) 0.0686)


(= (turnrate fake outside hsac3_c_wrac1) 0.3376)


(= (turnrate fake outside oldwa_c_wrec1) 0.0064)


(= (turnrate fake outside oldwa_d_wrec1) 0.02)


(= (turnrate fake outside silve_w_wrbc1) 0.0546)


(= (turnrate fake outside smith_c_wrcc1) 0.0685)


(= (turnrate fake outside somer_v_wrbc1) 0.0774)


(= (turnrate fake outside stand_f_wrac1) 0.2258)


(= (turnrate fake outside wakef_z_wrfc1) 0.2116)

(= (min_occ wrac1_y_wrbc1) 15.866)
(= (min_occ wrac1_x_wrbc1) 2.318)
(= (min_occ wrbc1_a_wrac1) 23.0795)
(= (min_occ wrbc1_b_wrcc1) 7.4091)
(= (min_occ wrbc1_b_wrac1) 0.8363)
(= (min_occ wrcc1_z_wrbc1) 14.741)
(= (min_occ wrcc1_x_wrdc1) 32.5227)
(= (min_occ wrcc1_w_wrdc1) 0.1893)
(= (min_occ wrdc1_a_wrcc1) 12.7607)
(= (min_occ wrdc1_b_wrec1) 9.3493)
(= (min_occ wrec1_z_wrdc1) 8.7141)
(= (min_occ wrec1_y_wrfc1) 4.0929)
(= (min_occ wrec1_y_wrdc1) 0.062)
(= (min_occ wrfc1_a_wrec1) 0.1)
(= (min_occ hsac3_c_wrac1) 14.8544)
(= (min_occ stand_f_wrac1) 9.7546)
(= (min_occ firth_d_wrac1) 1.5185)
(= (min_occ silve_w_wrbc1) 0.5971)
(= (min_occ somer_v_wrbc1) 3.4366)
(= (min_occ smith_c_wrcc1) 0.7743)
(= (min_occ abnor_v_wrdc1) 8.7337)
(= (min_occ oldwa_c_wrec1) 0.0631)
(= (min_occ oldwa_d_wrec1) 0.2133)
(= (min_occ wakef_z_wrfc1) 4.0627)
(= (min_occ broad_x_wrfc1) 1.9663)
(= (min_occ outside) 1.0)

(= (occupancy hsac3_c_wrac1) 20.59)
(= (occupancy wrac1_y_wrbc1) 15.87)
(= (occupancy stand_f_wrac1) 16.56)
(= (occupancy firth_d_wrac1) 5.09)
(= (occupancy wrac1_x_wrbc1) 6.59)
(= (occupancy wrbc1_a_wrac1) 23.28)
(= (occupancy wrbc1_b_wrcc1) 9.78)
(= (occupancy silve_w_wrbc1) 5.02)
(= (occupancy somer_v_wrbc1) 6.76)
(= (occupancy wrbc1_b_wrac1) 2.75)
(= (occupancy wrcc1_z_wrbc1) 23.28)
(= (occupancy wrcc1_x_wrdc1) 80.01)
(= (occupancy smith_c_wrcc1) 5.12)
(= (occupancy wrcc1_w_wrdc1) 0.25)
(= (occupancy wrdc1_a_wrcc1) 12.76)
(= (occupancy wrdc1_b_wrec1) 26.67)
(= (occupancy abnor_v_wrdc1) 12.19)
(= (occupancy wrec1_z_wrdc1) 8.71)
(= (occupancy wrec1_y_wrfc1) 4.09)
(= (occupancy oldwa_c_wrec1) 0.15)
(= (occupancy wrec1_y_wrdc1) 0.42)
(= (occupancy oldwa_d_wrec1) 0.49)
(= (occupancy wakef_z_wrfc1) 4.06)
(= (occupancy wrfc1_a_wrec1) 0.88)
(= (occupancy broad_x_wrfc1) 9.09)
(= (occupancy wrac1_z_hsac1) 0.0)
(= (occupancy wrac1_m_stand) 0.0)
(= (occupancy wrac1_n_firth) 0.0)
(= (occupancy wrbc1_r_silve) 0.0)
(= (occupancy wrbc1_s_somer) 0.0)
(= (occupancy wrdc1_q_abnor) 0.0)
(= (occupancy wrdc1_l_absou) 0.0)
(= (occupancy wrfc1_r_broad) 0.0)
(= (occupancy wrfc1_t_wakef) 0.0)
(= (occupancy outside) 50000.0)

(active wrac1_stage2)
(= (greentime wrac1) 13)
(= (intertime wrac1) 0)
(= (greentime wrbc1) 0)
(= (intertime wrbc1) 1)
(inter wrbc1_stage1)
(active wrcc1_stage1)
(= (greentime wrcc1) 39)
(= (intertime wrcc1) 0)
(active wrdc1_stage1)
(= (greentime wrdc1) 15)
(= (intertime wrdc1) 0)
(active wrec1_stage1)
(= (greentime wrec1) 15)
(= (intertime wrec1) 0)
(active wrfc1_stage1)
(= (greentime wrfc1) 27)
(= (intertime wrfc1) 0)
(active fake)

)
(:goal
  (and
      (>= (counter wrbc1_b_wrcc1) 500) (>= (counter wrac1_y_wrbc1) 500) (<= (time) 10000000000)
  )
 )
)
