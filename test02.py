import sys

from re                         import findall
from os                         import system
from os.path                    import basename, join, isfile
from unified_planning.io        import PDDLReader, PDDLWriter
from unified_planning.shortcuts import *

up.shortcuts.get_environment().credits_stream = None

# generated through SIPP
DOMAIN_FILENAME    = "INPUT_UPF/domain-cycleTime.pddl"
PROBLEM_FILENAME   = "INPUT_UPF/scenario=A_goal=1.pddl"

SCHEMA             = "POLY" # Polynomial Compilation
OUTPUT_PATH        = "OUTPUT_COMPILATION"

PYTHON_COMPILER_COMMAND = "/usr/bin/time -f '%e' python3 compiler[improvedGrounder]/pddlplus_compiler.py --domain_path {} --problem_path {} --schema {} --output_path {}"

def main():
    print ("Testing the Scenario 1 - Goal A")
    print ("- Input Domain Filename : {}".format(DOMAIN_FILENAME))
    print ("- Input Problem Filename: {}\n".format(PROBLEM_FILENAME))

    # Call the compiler from PDDL+ to PDDL2.1
    comp_dom_filename, comp_prob_filename = compile_from_pddlplus_to_numeric(
        DOMAIN_FILENAME, PROBLEM_FILENAME, SCHEMA, OUTPUT_PATH)

    # Parsing the compiled PDDL2.1 problem with UPF
    parsed_problem = parse_numeric_problem(
        comp_dom_filename, comp_prob_filename)

    print (parsed_problem)
    exit(1)

    # Calling the ENHSP solver provided by UPF over the PDDL2.1 representation
    call_ENHSP(parsed_problem)


def compile_from_pddlplus_to_numeric(dom_filename, prob_filename, schema, output_path):
    print ("Compiling into PDDL2.1")
    preprocess_pddlplus_encoding(dom_filename, prob_filename)
    command = PYTHON_COMPILER_COMMAND.format(
        dom_filename, 
        prob_filename,
        schema,
        output_path)
    print ("Command: {}".format(command))
    system(command)

    domain_name        = basename(dom_filename)
    problem_name       = basename(prob_filename)
    comp_dom_filename  = join(OUTPUT_PATH, "compiled_{}".format(domain_name))
    comp_prob_filename = join(OUTPUT_PATH, "compiled_{}".format(problem_name))
    # Testing the compilation outcome
    assert isfile(comp_dom_filename)
    assert isfile(comp_prob_filename)
    print ("Done!")
    preprocess_numeric_encoding(comp_dom_filename, comp_prob_filename)
    return comp_dom_filename, comp_prob_filename


def preprocess_numeric_encoding(comp_dom_filename, comp_prob_filename):
    # Useless for our purposes and unparsable by UPF
    INCR_TOTTIME = "(increase \n\t\t\t\t(total-time) (delta) )"
    METRIC       = "(:metric minimize (total-time))"
    I_TOTTIME    = "(= \n\t\t\t(total-time) 0.0 )"
    comp_domain_str  = readfile(comp_dom_filename)
    assert INCR_TOTTIME in comp_domain_str
    comp_domain_str  = comp_domain_str.replace(INCR_TOTTIME, "")
    writefile(comp_dom_filename, comp_domain_str)
    comp_problem_str = readfile(comp_prob_filename)
    assert METRIC in comp_problem_str
    comp_problem_str = comp_problem_str.replace(METRIC, "")
    assert I_TOTTIME in comp_problem_str
    comp_problem_str = comp_problem_str.replace(I_TOTTIME, "")
    writefile(comp_prob_filename, comp_problem_str)


def preprocess_pddlplus_encoding(domain_filename, problem_filename):
    # Useless and unmanageble for the parser
    HANDLE_TIME = "(:process handle_time\n:parameters()\n:effect (and (increase (time) (* #t 1 )))\n)"
    domain_str = readfile(domain_filename)
    domain_str = domain_str.replace(HANDLE_TIME, "")
    writefile(domain_filename, domain_str)
    # Predicates "interflow" are not part of the domain
    problem_str = readfile(problem_filename)
    interflows  = findall(r"\(interflow[^\)]+\)", problem_str)
    for inter in interflows:
        problem_str = problem_str.replace(inter, "")
    writefile(problem_filename, problem_str)


def parse_numeric_problem(domain_filename, problem_filename):
    print ("Parsing the numeric problem")
    reader  = PDDLReader()
    problem = reader.parse_problem(domain_filename, problem_filename)
    return problem


def call_ENHSP(parsed_problem):
    with OneshotPlanner(name="enhsp") as planner:
        result = planner.solve(parsed_problem)
        for r in (result.log_messages):
            print (r)
        if result.status == up.engines.PlanGenerationResultStatus.SOLVED_SATISFICING:
            print("ENHSP returned: %s" % result.plan)
        else:
            print("No plan found.")


def readfile(path):
    with open(path, "r") as fout:
        return fout.read()


def writefile(path, txt):
    with open(path, "w") as fin:
        fin.write(txt)



if __name__ == "__main__":
    main()