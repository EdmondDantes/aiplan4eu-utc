(define (domain grounded-urbantraffic)
(:requirements :fluents :adl :typing)
(:predicates
	(controllable__wrac1)
	(controllable__wrbc1)
	(controllable__wrcc1)
	(controllable__wrdc1)
	(controllable__wrec1)
	(controllable__wrfc1)
	(contains__wrac1__wrac1_stage1)
	(contains__wrac1__wrac1_stage2)
	(contains__wrac1__wrac1_stage3)
	(contains__wrac1__wrac1_stage4)
	(contains__wrbc1__wrbc1_stage1)
	(contains__wrbc1__wrbc1_stage2)
	(contains__wrbc1__wrbc1_stage3)
	(contains__wrbc1__wrbc1_stage4)
	(contains__wrbc1__wrbc1_stage5)
	(contains__wrcc1__wrcc1_stage1)
	(contains__wrcc1__wrcc1_stage2)
	(contains__wrcc1__wrcc1_stage3)
	(contains__wrcc1__wrcc1_stage4)
	(contains__wrcc1__wrcc1_stage5)
	(contains__wrcc1__wrcc1_stage6)
	(contains__wrdc1__wrdc1_stage1)
	(contains__wrdc1__wrdc1_stage2)
	(contains__wrdc1__wrdc1_stage3)
	(contains__wrdc1__wrdc1_stage4)
	(contains__wrec1__wrec1_stage1)
	(contains__wrec1__wrec1_stage2)
	(contains__wrec1__wrec1_stage3)
	(contains__wrec1__wrec1_stage4)
	(contains__wrfc1__wrfc1_stage1)
	(contains__wrfc1__wrfc1_stage2)
	(contains__wrfc1__wrfc1_stage3)
	(next__wrac1_stage1__wrac1_stage2)
	(next__wrac1_stage2__wrac1_stage3)
	(next__wrac1_stage3__wrac1_stage4)
	(next__wrac1_stage4__wrac1_stage1)
	(next__wrbc1_stage1__wrbc1_stage2)
	(next__wrbc1_stage2__wrbc1_stage3)
	(next__wrbc1_stage3__wrbc1_stage4)
	(next__wrbc1_stage4__wrbc1_stage5)
	(next__wrbc1_stage5__wrbc1_stage1)
	(next__wrcc1_stage1__wrcc1_stage2)
	(next__wrcc1_stage2__wrcc1_stage3)
	(next__wrcc1_stage3__wrcc1_stage4)
	(next__wrcc1_stage4__wrcc1_stage5)
	(next__wrcc1_stage5__wrcc1_stage6)
	(next__wrcc1_stage6__wrcc1_stage1)
	(next__wrdc1_stage1__wrdc1_stage2)
	(next__wrdc1_stage2__wrdc1_stage3)
	(next__wrdc1_stage3__wrdc1_stage4)
	(next__wrdc1_stage4__wrdc1_stage1)
	(next__wrec1_stage1__wrec1_stage2)
	(next__wrec1_stage2__wrec1_stage3)
	(next__wrec1_stage3__wrec1_stage4)
	(next__wrec1_stage4__wrec1_stage1)
	(next__wrfc1_stage1__wrfc1_stage2)
	(next__wrfc1_stage2__wrfc1_stage3)
	(next__wrfc1_stage3__wrfc1_stage1)
	(active__wrac1_stage2)
	(inter__wrbc1_stage1)
	(active__wrcc1_stage1)
	(active__wrdc1_stage1)
	(active__wrec1_stage1)
	(active__wrfc1_stage1)
	(active__fake)
	(active__wrac1_stage1)
	(trigger__wrac1)
	(active__wrac1_stage3)
	(active__wrac1_stage4)
	(active__wrbc1_stage1)
	(trigger__wrbc1)
	(active__wrbc1_stage2)
	(active__wrbc1_stage3)
	(active__wrbc1_stage4)
	(active__wrbc1_stage5)
	(trigger__wrcc1)
	(active__wrcc1_stage2)
	(active__wrcc1_stage3)
	(active__wrcc1_stage4)
	(active__wrcc1_stage5)
	(active__wrcc1_stage6)
	(trigger__wrdc1)
	(active__wrdc1_stage2)
	(active__wrdc1_stage3)
	(active__wrdc1_stage4)
	(trigger__wrec1)
	(active__wrec1_stage2)
	(active__wrec1_stage3)
	(active__wrec1_stage4)
	(trigger__wrfc1)
	(active__wrfc1_stage2)
	(active__wrfc1_stage3)
	(inter__wrac1_stage1)
	(inter__wrac1_stage2)
	(inter__wrac1_stage3)
	(inter__wrac1_stage4)
	(inter__wrbc1_stage2)
	(inter__wrbc1_stage3)
	(inter__wrbc1_stage4)
	(inter__wrbc1_stage5)
	(inter__wrcc1_stage1)
	(inter__wrcc1_stage2)
	(inter__wrcc1_stage3)
	(inter__wrcc1_stage4)
	(inter__wrcc1_stage5)
	(inter__wrcc1_stage6)
	(inter__wrdc1_stage1)
	(inter__wrdc1_stage2)
	(inter__wrdc1_stage3)
	(inter__wrdc1_stage4)
	(inter__wrec1_stage1)
	(inter__wrec1_stage2)
	(inter__wrec1_stage3)
	(inter__wrec1_stage4)
	(inter__wrfc1_stage1)
	(inter__wrfc1_stage2)
	(inter__wrfc1_stage3)
	(inter__fake)
	(force-events)
	(fired--defgreenreached__wrac1_stage1__wrac1)
	(fired--defgreenreached__wrac1_stage2__wrac1)
	(fired--defgreenreached__wrac1_stage3__wrac1)
	(fired--defgreenreached__wrac1_stage4__wrac1)
	(fired--defgreenreached__wrbc1_stage1__wrbc1)
	(fired--defgreenreached__wrbc1_stage2__wrbc1)
	(fired--defgreenreached__wrbc1_stage3__wrbc1)
	(fired--defgreenreached__wrbc1_stage4__wrbc1)
	(fired--defgreenreached__wrbc1_stage5__wrbc1)
	(fired--defgreenreached__wrcc1_stage1__wrcc1)
	(fired--defgreenreached__wrcc1_stage2__wrcc1)
	(fired--defgreenreached__wrcc1_stage3__wrcc1)
	(fired--defgreenreached__wrcc1_stage4__wrcc1)
	(fired--defgreenreached__wrcc1_stage5__wrcc1)
	(fired--defgreenreached__wrcc1_stage6__wrcc1)
	(fired--defgreenreached__wrdc1_stage1__wrdc1)
	(fired--defgreenreached__wrdc1_stage2__wrdc1)
	(fired--defgreenreached__wrdc1_stage3__wrdc1)
	(fired--defgreenreached__wrdc1_stage4__wrdc1)
	(fired--defgreenreached__wrec1_stage1__wrec1)
	(fired--defgreenreached__wrec1_stage2__wrec1)
	(fired--defgreenreached__wrec1_stage3__wrec1)
	(fired--defgreenreached__wrec1_stage4__wrec1)
	(fired--defgreenreached__wrfc1_stage1__wrfc1)
	(fired--defgreenreached__wrfc1_stage2__wrfc1)
	(fired--defgreenreached__wrfc1_stage3__wrfc1)
	(fired--trigger-inter__wrac1_stage1__wrac1)
	(fired--trigger-inter__wrac1_stage2__wrac1)
	(fired--trigger-inter__wrac1_stage3__wrac1)
	(fired--trigger-inter__wrac1_stage4__wrac1)
	(fired--trigger-inter__wrbc1_stage1__wrbc1)
	(fired--trigger-inter__wrbc1_stage2__wrbc1)
	(fired--trigger-inter__wrbc1_stage3__wrbc1)
	(fired--trigger-inter__wrbc1_stage4__wrbc1)
	(fired--trigger-inter__wrbc1_stage5__wrbc1)
	(fired--trigger-inter__wrcc1_stage1__wrcc1)
	(fired--trigger-inter__wrcc1_stage2__wrcc1)
	(fired--trigger-inter__wrcc1_stage3__wrcc1)
	(fired--trigger-inter__wrcc1_stage4__wrcc1)
	(fired--trigger-inter__wrcc1_stage5__wrcc1)
	(fired--trigger-inter__wrcc1_stage6__wrcc1)
	(fired--trigger-inter__wrdc1_stage1__wrdc1)
	(fired--trigger-inter__wrdc1_stage2__wrdc1)
	(fired--trigger-inter__wrdc1_stage3__wrdc1)
	(fired--trigger-inter__wrdc1_stage4__wrdc1)
	(fired--trigger-inter__wrec1_stage1__wrec1)
	(fired--trigger-inter__wrec1_stage2__wrec1)
	(fired--trigger-inter__wrec1_stage3__wrec1)
	(fired--trigger-inter__wrec1_stage4__wrec1)
	(fired--trigger-inter__wrfc1_stage1__wrfc1)
	(fired--trigger-inter__wrfc1_stage2__wrfc1)
	(fired--trigger-inter__wrfc1_stage3__wrfc1)
	(fired--trigger-change__wrac1_stage1__wrac1_stage2__wrac1)
	(fired--trigger-change__wrac1_stage2__wrac1_stage3__wrac1)
	(fired--trigger-change__wrac1_stage3__wrac1_stage4__wrac1)
	(fired--trigger-change__wrac1_stage4__wrac1_stage1__wrac1)
	(fired--trigger-change__wrbc1_stage1__wrbc1_stage2__wrbc1)
	(fired--trigger-change__wrbc1_stage2__wrbc1_stage3__wrbc1)
	(fired--trigger-change__wrbc1_stage3__wrbc1_stage4__wrbc1)
	(fired--trigger-change__wrbc1_stage4__wrbc1_stage5__wrbc1)
	(fired--trigger-change__wrbc1_stage5__wrbc1_stage1__wrbc1)
	(fired--trigger-change__wrcc1_stage1__wrcc1_stage2__wrcc1)
	(fired--trigger-change__wrcc1_stage2__wrcc1_stage3__wrcc1)
	(fired--trigger-change__wrcc1_stage3__wrcc1_stage4__wrcc1)
	(fired--trigger-change__wrcc1_stage4__wrcc1_stage5__wrcc1)
	(fired--trigger-change__wrcc1_stage5__wrcc1_stage6__wrcc1)
	(fired--trigger-change__wrcc1_stage6__wrcc1_stage1__wrcc1)
	(fired--trigger-change__wrdc1_stage1__wrdc1_stage2__wrdc1)
	(fired--trigger-change__wrdc1_stage2__wrdc1_stage3__wrdc1)
	(fired--trigger-change__wrdc1_stage3__wrdc1_stage4__wrdc1)
	(fired--trigger-change__wrdc1_stage4__wrdc1_stage1__wrdc1)
	(fired--trigger-change__wrec1_stage1__wrec1_stage2__wrec1)
	(fired--trigger-change__wrec1_stage2__wrec1_stage3__wrec1)
	(fired--trigger-change__wrec1_stage3__wrec1_stage4__wrec1)
	(fired--trigger-change__wrec1_stage4__wrec1_stage1__wrec1)
	(fired--trigger-change__wrfc1_stage1__wrfc1_stage2__wrfc1)
	(fired--trigger-change__wrfc1_stage2__wrfc1_stage3__wrfc1)
	(fired--trigger-change__wrfc1_stage3__wrfc1_stage1__wrfc1)
	(pause)
	(done--0)
	(done--1)
	(done--2)
	(done--3)
	(done--4)
	(done--5)
	(done--6)
	(done--7)
	(done--8)
	(done--9)
	(done--10)
	(done--11)
	(done--12)
	(done--13)
	(done--14)
	(done--15)
	(done--16)
	(done--17)
	(done--18)
	(done--19)
	(done--20)
	(done--21)
	(done--22)
	(done--23)
	(done--24)
	(done--25)
	(done--26)
	(done--27)
	(done--28)
	(done--29)
	(done--30)
	(done--31)
	(done--32)
	(done--33)
	(done--34)
	(done--35)
	(done--36)
	(done--37)
	(done--38)
	(done--39)
	(done--40)
	(done--41)
	(done--42)
	(done--43)
	(done--44)
	(done--45)
	(done--46)
	(done--47)
	(done--48)
	(done--49)
	(done--50)
	(done--51)
	(done--52)
	(done--53)
	(done--54)
	(done--55)
	(done--56)
	(done--57)
	(done--58)
	(done--59)
	(done--60)
	(done--61)
	(done--62)
	(done--63)
	(done--64)
	(done--65)
	(done--66)
	(done--67)
	(done--68)
	(done--69)
	(done--70)
	(done--71)
	(done--72)
	(done--73)
	(done--74)
	(done--75)
	(done--76)
	(done--77)
	(done--78)
	(done--79)
	(done--80)
	(done--81)
	(done--82)
	(done--83)
	(done--84)
	(done--85)
	(done--86)
	(done--87)
	(done--88)
	(done--89)
	(done--90)
	(done--91)
	(done--92)
	(done--93)
	(done--94)
	(done--95)
	(done--96)
	(done--97)
	(done--98)
	(done--99)
	(done--100)
	(done--101)
	(done--102)
	(done--103)
	(done--104)
	(done--105)
	(done--106)
	(done--107)
	(done--108)
	(done--109)
	(done--110)
	(done--111)
	(done--112)
	(done--113)
	(done--114)
	(done--115)
	(done--116)
	(done--117)
	(done--118)
	(done--119)
	(done--120)
	(done--121)
	(done--122)
	(done--123)
	(done--124)
	(done--125)
	(done--126)
	(done--127)
	(done--128)
	(done--129)
	(done--130)
	(done--131)
	(done--132)
	(done--133)
	(done--134)
	(done--135)
	(done--136)
	(done--137)
	(done--138)
	(done--139)
	(done--140)
	(done--141)
	(done--142)
	(done--143)
	(done--144)
	(done--145)
	(done--146)
	(done--147)
	(done--148)
	(done--149)
	(done--150)
	(done--151)
	(done--152)
	(done--153)
	(done--154)
	(done--155)
	(done--156)
	(done--157)
	(done--158)
	(done--159)
	(done--160)
	(done--161)
	(done--162)
	(done--163)
	(done--164)
	(done--165)
	(done--166)
	(done--167)
	(done--168)
	(done--169)
	(done--170)
	(done--171)
	(done--172)
	(done--173)
	(done--174)
	(done--175)
	(done--176)
	(done--177)
	(done--178)
	(done--179)
	(done--180)
	(done--181)
	(done--182)
	(done--183)
	(done--184)
	(done--185)
	(done--186)
	(done--187)
	(done--188)
	(done--189)
	(done--190)
	(done--191)
	(done--192)
	(done--193)
	(done--194)
	(done--195)
	(done--196)
	(done--197)
	(done--198)
	(done--199)
	(done--200)
	(done--201)
	(done--202)
	(done--203)
	(done--204)
	(done--205)
	(done--206)
	(done--207)
	(done--208)
	(done--209)
	(done--210)
	(done--211)
	(done--212)
	(done--213)
	(done--214)
	(done--215)
	(done--216)
	(done--217)
	(done--218)
	(done--219)
	(done--220)
	(done--221)
	(done--222)
	(done--223)
	(done--224)
	(done--225)
	(done--226)
	(done--227)
	(done--228)
	(done--229)
	(done--230)
	(done--231)
	(done--232)
	(done--233)
	(done--234)
	(done--235)
	(done--236)
	(done--237)
	(done--238)
	(done--239)
	(done--240)
	(done--241)
	(done--242)
	(done--243)
	(done--244)
	(done--245)
	(done--246)
	(done--247)
	(done--248)
	(done--249)
	(done--250)
	(done--251)
	(done--252)
	(done--253)
	(done--254)
	(done--255)
	(done--256)
	(done--257)
	(done--258)
	(done--259)
	(done--260)
	(done--261)
	(done--262)
	(done--263)
	(done--264)
	(done--265)
	(done--266)
	(done--267)
	(done--268)
	(done--269)
	(done--270)
	(done--271)
	(done--272)
	(done--273)
	(done--274)
	(done--275)
	(done--276)
	(done--277)
	(done--278)
	(done--279)
	(done--280)
	(done--281)
	(done--282)
	(done--283)
	(done--284)
	(done--285)
	(done--286)
	(done--287)
	(done--288)
	(done--289)
	(done--290)
	(done--291)
	(done--292)
	(done--293)
	(done--294)
	(done--295)
	(done--296)
	(done--297)
	(done--298)
	(done--299)
	(done--300)
	(done--301)
	(done--302)
	(done--303)
	(done--304)
	(done--305)
	(done--306)
	(done--307)
	(done--308)
	(done--309)
	(done--310)
	(done--311)
	(done--312)
	(done--313)
	(done--314)
	(done--315)
	(done--316)
	(done--317)
	(done--318)
	(done--319)
	(done--320)
	(done--321)
	(done--322)
	(done--323)
	(done--324)
	(done--325)
	(done--326)
	(done--327)
	(done--328)
	(done--329)
	(done--330)
	(done--331)
)

(:functions
	(capacity__outside)
	(capacity__hsac3_c_wrac1)
	(capacity__wrac1_z_hsac1)
	(capacity__wrac1_y_wrbc1)
	(capacity__wrac1_m_stand)
	(capacity__stand_f_wrac1)
	(capacity__wrac1_n_firth)
	(capacity__firth_d_wrac1)
	(capacity__wrac1_x_wrbc1)
	(capacity__wrbc1_a_wrac1)
	(capacity__wrbc1_b_wrcc1)
	(capacity__wrbc1_r_silve)
	(capacity__silve_w_wrbc1)
	(capacity__wrbc1_s_somer)
	(capacity__somer_v_wrbc1)
	(capacity__wrbc1_b_wrac1)
	(capacity__wrcc1_z_wrbc1)
	(capacity__wrcc1_x_wrdc1)
	(capacity__smith_c_wrcc1)
	(capacity__wrdc1_a_wrcc1)
	(capacity__wrdc1_b_wrec1)
	(capacity__wrdc1_q_abnor)
	(capacity__wrcc1_w_wrdc1)
	(capacity__wrdc1_l_absou)
	(capacity__abnor_v_wrdc1)
	(capacity__wrec1_z_wrdc1)
	(capacity__wrec1_y_wrfc1)
	(capacity__oldwa_c_wrec1)
	(capacity__wrec1_y_wrdc1)
	(capacity__oldwa_d_wrec1)
	(capacity__wakef_z_wrfc1)
	(capacity__wrfc1_a_wrec1)
	(capacity__wrfc1_r_broad)
	(capacity__broad_x_wrfc1)
	(capacity__wrfc1_t_wakef)
	(interlimit__wrac1_stage1)
	(interlimit__wrac1_stage2)
	(interlimit__wrac1_stage3)
	(interlimit__wrac1_stage4)
	(interlimit__wrbc1_stage1)
	(interlimit__wrbc1_stage2)
	(interlimit__wrbc1_stage3)
	(interlimit__wrbc1_stage4)
	(interlimit__wrbc1_stage5)
	(interlimit__wrcc1_stage1)
	(interlimit__wrcc1_stage2)
	(interlimit__wrcc1_stage3)
	(interlimit__wrcc1_stage4)
	(interlimit__wrcc1_stage5)
	(interlimit__wrcc1_stage6)
	(interlimit__wrdc1_stage1)
	(interlimit__wrdc1_stage2)
	(interlimit__wrdc1_stage3)
	(interlimit__wrdc1_stage4)
	(interlimit__wrec1_stage1)
	(interlimit__wrec1_stage2)
	(interlimit__wrec1_stage3)
	(interlimit__wrec1_stage4)
	(interlimit__wrfc1_stage1)
	(interlimit__wrfc1_stage2)
	(interlimit__wrfc1_stage3)
	(mingreentime__wrac1_stage1)
	(maxgreentime__wrac1_stage1)
	(mingreentime__wrac1_stage2)
	(maxgreentime__wrac1_stage2)
	(mingreentime__wrac1_stage3)
	(maxgreentime__wrac1_stage3)
	(mingreentime__wrac1_stage4)
	(maxgreentime__wrac1_stage4)
	(mingreentime__wrbc1_stage1)
	(maxgreentime__wrbc1_stage1)
	(mingreentime__wrbc1_stage2)
	(maxgreentime__wrbc1_stage2)
	(mingreentime__wrbc1_stage3)
	(maxgreentime__wrbc1_stage3)
	(mingreentime__wrbc1_stage4)
	(maxgreentime__wrbc1_stage4)
	(mingreentime__wrbc1_stage5)
	(maxgreentime__wrbc1_stage5)
	(mingreentime__wrcc1_stage1)
	(maxgreentime__wrcc1_stage1)
	(mingreentime__wrcc1_stage2)
	(maxgreentime__wrcc1_stage2)
	(mingreentime__wrcc1_stage3)
	(maxgreentime__wrcc1_stage3)
	(mingreentime__wrcc1_stage4)
	(maxgreentime__wrcc1_stage4)
	(mingreentime__wrcc1_stage5)
	(maxgreentime__wrcc1_stage5)
	(mingreentime__wrcc1_stage6)
	(maxgreentime__wrcc1_stage6)
	(mingreentime__wrdc1_stage1)
	(maxgreentime__wrdc1_stage1)
	(mingreentime__wrdc1_stage2)
	(maxgreentime__wrdc1_stage2)
	(mingreentime__wrdc1_stage3)
	(maxgreentime__wrdc1_stage3)
	(mingreentime__wrdc1_stage4)
	(maxgreentime__wrdc1_stage4)
	(mingreentime__wrec1_stage1)
	(maxgreentime__wrec1_stage1)
	(mingreentime__wrec1_stage2)
	(maxgreentime__wrec1_stage2)
	(mingreentime__wrec1_stage3)
	(maxgreentime__wrec1_stage3)
	(mingreentime__wrec1_stage4)
	(maxgreentime__wrec1_stage4)
	(mingreentime__wrfc1_stage1)
	(maxgreentime__wrfc1_stage1)
	(mingreentime__wrfc1_stage2)
	(maxgreentime__wrfc1_stage2)
	(mingreentime__wrfc1_stage3)
	(maxgreentime__wrfc1_stage3)
	(defaultgreentime__wrac1_stage1)
	(defaultgreentime__wrac1_stage2)
	(defaultgreentime__wrac1_stage3)
	(defaultgreentime__wrac1_stage4)
	(defaultgreentime__wrbc1_stage1)
	(defaultgreentime__wrbc1_stage2)
	(defaultgreentime__wrbc1_stage3)
	(defaultgreentime__wrbc1_stage4)
	(defaultgreentime__wrbc1_stage5)
	(defaultgreentime__wrcc1_stage1)
	(defaultgreentime__wrcc1_stage2)
	(defaultgreentime__wrcc1_stage3)
	(defaultgreentime__wrcc1_stage4)
	(defaultgreentime__wrcc1_stage5)
	(defaultgreentime__wrcc1_stage6)
	(defaultgreentime__wrdc1_stage1)
	(defaultgreentime__wrdc1_stage2)
	(defaultgreentime__wrdc1_stage3)
	(defaultgreentime__wrdc1_stage4)
	(defaultgreentime__wrec1_stage1)
	(defaultgreentime__wrec1_stage2)
	(defaultgreentime__wrec1_stage3)
	(defaultgreentime__wrec1_stage4)
	(defaultgreentime__wrfc1_stage1)
	(defaultgreentime__wrfc1_stage2)
	(defaultgreentime__wrfc1_stage3)
	(counter__abnor_v_wrdc1)
	(counter__broad_x_wrfc1)
	(counter__firth_d_wrac1)
	(counter__hsac3_c_wrac1)
	(counter__oldwa_c_wrec1)
	(counter__oldwa_d_wrec1)
	(counter__silve_w_wrbc1)
	(counter__smith_c_wrcc1)
	(counter__somer_v_wrbc1)
	(counter__stand_f_wrac1)
	(counter__wakef_z_wrfc1)
	(counter__wrac1_m_stand)
	(counter__wrac1_n_firth)
	(counter__wrac1_x_wrbc1)
	(counter__wrac1_y_wrbc1)
	(counter__wrac1_z_hsac1)
	(counter__wrbc1_a_wrac1)
	(counter__wrbc1_b_wrac1)
	(counter__wrbc1_b_wrcc1)
	(counter__wrbc1_r_silve)
	(counter__wrbc1_s_somer)
	(counter__wrcc1_w_wrdc1)
	(counter__wrcc1_x_wrdc1)
	(counter__wrcc1_z_wrbc1)
	(counter__wrdc1_a_wrcc1)
	(counter__wrdc1_b_wrec1)
	(counter__wrdc1_l_absou)
	(counter__wrdc1_q_abnor)
	(counter__wrec1_y_wrdc1)
	(counter__wrec1_y_wrfc1)
	(counter__wrec1_z_wrdc1)
	(counter__wrfc1_a_wrec1)
	(counter__wrfc1_r_broad)
	(counter__wrfc1_t_wakef)
	(cycletime__wrac1)
	(cycletime__wrbc1)
	(cycletime__wrcc1)
	(cycletime__wrdc1)
	(cycletime__wrec1)
	(cycletime__wrfc1)
	(granularity)
	(maxcycletime__wrac1)
	(maxcycletime__wrbc1)
	(maxcycletime__wrcc1)
	(maxcycletime__wrdc1)
	(maxcycletime__wrec1)
	(maxcycletime__wrfc1)
	(mincycletime__wrac1)
	(mincycletime__wrbc1)
	(mincycletime__wrcc1)
	(mincycletime__wrdc1)
	(mincycletime__wrec1)
	(mincycletime__wrfc1)
	(time)
	(turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1)
	(turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand)
	(turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1)
	(turnrate__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1)
	(turnrate__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth)
	(turnrate__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1)
	(turnrate__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth)
	(turnrate__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand)
	(turnrate__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1)
	(turnrate__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve)
	(turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1)
	(turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1)
	(turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve)
	(turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer)
	(turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1)
	(turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1)
	(turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1)
	(turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer)
	(turnrate__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1)
	(turnrate__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve)
	(turnrate__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer)
	(turnrate__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1)
	(turnrate__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1)
	(turnrate__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer)
	(turnrate__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1)
	(turnrate__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1)
	(turnrate__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1)
	(turnrate__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve)
	(turnrate__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer)
	(turnrate__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1)
	(turnrate__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1)
	(turnrate__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1)
	(turnrate__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1)
	(turnrate__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1)
	(turnrate__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1)
	(turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1)
	(turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1)
	(turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1)
	(turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1)
	(turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1)
	(turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1)
	(turnrate__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1)
	(turnrate__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou)
	(turnrate__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1)
	(turnrate__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor)
	(turnrate__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1)
	(turnrate__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor)
	(turnrate__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou)
	(turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1)
	(turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1)
	(turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou)
	(turnrate__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1)
	(turnrate__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou)
	(turnrate__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor)
	(turnrate__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1)
	(turnrate__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1)
	(turnrate__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1)
	(turnrate__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1)
	(turnrate__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1)
	(turnrate__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1)
	(turnrate__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1)
	(turnrate__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1)
	(turnrate__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1)
	(turnrate__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1)
	(turnrate__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1)
	(turnrate__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad)
	(turnrate__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef)
	(turnrate__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad)
	(turnrate__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef)
	(turnrate__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1)
	(turnrate__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef)
	(turnrate__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1)
	(turnrate__wrac1_stage3__firth_d_wrac1__wrac1_m_stand)
	(turnrate__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1)
	(turnrate__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1)
	(turnrate__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1)
	(turnrate__wrac1_stage4__firth_d_wrac1__wrac1_m_stand)
	(turnrate__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1)
	(turnrate__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1)
	(turnrate__wrac1_stage3__stand_f_wrac1__wrac1_n_firth)
	(turnrate__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1)
	(turnrate__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1)
	(turnrate__fake__outside__abnor_v_wrdc1)
	(turnrate__fake__outside__broad_x_wrfc1)
	(turnrate__fake__outside__firth_d_wrac1)
	(turnrate__fake__outside__hsac3_c_wrac1)
	(turnrate__fake__outside__oldwa_c_wrec1)
	(turnrate__fake__outside__oldwa_d_wrec1)
	(turnrate__fake__outside__silve_w_wrbc1)
	(turnrate__fake__outside__smith_c_wrcc1)
	(turnrate__fake__outside__somer_v_wrbc1)
	(turnrate__fake__outside__stand_f_wrac1)
	(turnrate__fake__outside__wakef_z_wrfc1)
	(min_occ__wrac1_y_wrbc1)
	(min_occ__wrac1_x_wrbc1)
	(min_occ__wrbc1_a_wrac1)
	(min_occ__wrbc1_b_wrcc1)
	(min_occ__wrbc1_b_wrac1)
	(min_occ__wrcc1_z_wrbc1)
	(min_occ__wrcc1_x_wrdc1)
	(min_occ__wrcc1_w_wrdc1)
	(min_occ__wrdc1_a_wrcc1)
	(min_occ__wrdc1_b_wrec1)
	(min_occ__wrec1_z_wrdc1)
	(min_occ__wrec1_y_wrfc1)
	(min_occ__wrec1_y_wrdc1)
	(min_occ__wrfc1_a_wrec1)
	(min_occ__hsac3_c_wrac1)
	(min_occ__stand_f_wrac1)
	(min_occ__firth_d_wrac1)
	(min_occ__silve_w_wrbc1)
	(min_occ__somer_v_wrbc1)
	(min_occ__smith_c_wrcc1)
	(min_occ__abnor_v_wrdc1)
	(min_occ__oldwa_c_wrec1)
	(min_occ__oldwa_d_wrec1)
	(min_occ__wakef_z_wrfc1)
	(min_occ__broad_x_wrfc1)
	(min_occ__outside)
	(occupancy__hsac3_c_wrac1)
	(occupancy__wrac1_y_wrbc1)
	(occupancy__stand_f_wrac1)
	(occupancy__firth_d_wrac1)
	(occupancy__wrac1_x_wrbc1)
	(occupancy__wrbc1_a_wrac1)
	(occupancy__wrbc1_b_wrcc1)
	(occupancy__silve_w_wrbc1)
	(occupancy__somer_v_wrbc1)
	(occupancy__wrbc1_b_wrac1)
	(occupancy__wrcc1_z_wrbc1)
	(occupancy__wrcc1_x_wrdc1)
	(occupancy__smith_c_wrcc1)
	(occupancy__wrcc1_w_wrdc1)
	(occupancy__wrdc1_a_wrcc1)
	(occupancy__wrdc1_b_wrec1)
	(occupancy__abnor_v_wrdc1)
	(occupancy__wrec1_z_wrdc1)
	(occupancy__wrec1_y_wrfc1)
	(occupancy__oldwa_c_wrec1)
	(occupancy__wrec1_y_wrdc1)
	(occupancy__oldwa_d_wrec1)
	(occupancy__wakef_z_wrfc1)
	(occupancy__wrfc1_a_wrec1)
	(occupancy__broad_x_wrfc1)
	(occupancy__wrac1_z_hsac1)
	(occupancy__wrac1_m_stand)
	(occupancy__wrac1_n_firth)
	(occupancy__wrbc1_r_silve)
	(occupancy__wrbc1_s_somer)
	(occupancy__wrdc1_q_abnor)
	(occupancy__wrdc1_l_absou)
	(occupancy__wrfc1_r_broad)
	(occupancy__wrfc1_t_wakef)
	(occupancy__outside)
	(greentime__wrac1)
	(intertime__wrac1)
	(greentime__wrbc1)
	(intertime__wrbc1)
	(greentime__wrcc1)
	(intertime__wrcc1)
	(greentime__wrdc1)
	(intertime__wrdc1)
	(greentime__wrec1)
	(intertime__wrec1)
	(greentime__wrfc1)
	(intertime__wrfc1)
	(defaultgreentime__fake)
	(delta)
	(copy--capacity__outside)
	(copy--capacity__hsac3_c_wrac1)
	(copy--capacity__wrac1_z_hsac1)
	(copy--capacity__wrac1_y_wrbc1)
	(copy--capacity__wrac1_m_stand)
	(copy--capacity__stand_f_wrac1)
	(copy--capacity__wrac1_n_firth)
	(copy--capacity__firth_d_wrac1)
	(copy--capacity__wrac1_x_wrbc1)
	(copy--capacity__wrbc1_a_wrac1)
	(copy--capacity__wrbc1_b_wrcc1)
	(copy--capacity__wrbc1_r_silve)
	(copy--capacity__silve_w_wrbc1)
	(copy--capacity__wrbc1_s_somer)
	(copy--capacity__somer_v_wrbc1)
	(copy--capacity__wrbc1_b_wrac1)
	(copy--capacity__wrcc1_z_wrbc1)
	(copy--capacity__wrcc1_x_wrdc1)
	(copy--capacity__smith_c_wrcc1)
	(copy--capacity__wrdc1_a_wrcc1)
	(copy--capacity__wrdc1_b_wrec1)
	(copy--capacity__wrdc1_q_abnor)
	(copy--capacity__wrcc1_w_wrdc1)
	(copy--capacity__wrdc1_l_absou)
	(copy--capacity__abnor_v_wrdc1)
	(copy--capacity__wrec1_z_wrdc1)
	(copy--capacity__wrec1_y_wrfc1)
	(copy--capacity__oldwa_c_wrec1)
	(copy--capacity__wrec1_y_wrdc1)
	(copy--capacity__oldwa_d_wrec1)
	(copy--capacity__wakef_z_wrfc1)
	(copy--capacity__wrfc1_a_wrec1)
	(copy--capacity__wrfc1_r_broad)
	(copy--capacity__broad_x_wrfc1)
	(copy--capacity__wrfc1_t_wakef)
	(copy--interlimit__wrac1_stage1)
	(copy--interlimit__wrac1_stage2)
	(copy--interlimit__wrac1_stage3)
	(copy--interlimit__wrac1_stage4)
	(copy--interlimit__wrbc1_stage1)
	(copy--interlimit__wrbc1_stage2)
	(copy--interlimit__wrbc1_stage3)
	(copy--interlimit__wrbc1_stage4)
	(copy--interlimit__wrbc1_stage5)
	(copy--interlimit__wrcc1_stage1)
	(copy--interlimit__wrcc1_stage2)
	(copy--interlimit__wrcc1_stage3)
	(copy--interlimit__wrcc1_stage4)
	(copy--interlimit__wrcc1_stage5)
	(copy--interlimit__wrcc1_stage6)
	(copy--interlimit__wrdc1_stage1)
	(copy--interlimit__wrdc1_stage2)
	(copy--interlimit__wrdc1_stage3)
	(copy--interlimit__wrdc1_stage4)
	(copy--interlimit__wrec1_stage1)
	(copy--interlimit__wrec1_stage2)
	(copy--interlimit__wrec1_stage3)
	(copy--interlimit__wrec1_stage4)
	(copy--interlimit__wrfc1_stage1)
	(copy--interlimit__wrfc1_stage2)
	(copy--interlimit__wrfc1_stage3)
	(copy--mingreentime__wrac1_stage1)
	(copy--maxgreentime__wrac1_stage1)
	(copy--mingreentime__wrac1_stage2)
	(copy--maxgreentime__wrac1_stage2)
	(copy--mingreentime__wrac1_stage3)
	(copy--maxgreentime__wrac1_stage3)
	(copy--mingreentime__wrac1_stage4)
	(copy--maxgreentime__wrac1_stage4)
	(copy--mingreentime__wrbc1_stage1)
	(copy--maxgreentime__wrbc1_stage1)
	(copy--mingreentime__wrbc1_stage2)
	(copy--maxgreentime__wrbc1_stage2)
	(copy--mingreentime__wrbc1_stage3)
	(copy--maxgreentime__wrbc1_stage3)
	(copy--mingreentime__wrbc1_stage4)
	(copy--maxgreentime__wrbc1_stage4)
	(copy--mingreentime__wrbc1_stage5)
	(copy--maxgreentime__wrbc1_stage5)
	(copy--mingreentime__wrcc1_stage1)
	(copy--maxgreentime__wrcc1_stage1)
	(copy--mingreentime__wrcc1_stage2)
	(copy--maxgreentime__wrcc1_stage2)
	(copy--mingreentime__wrcc1_stage3)
	(copy--maxgreentime__wrcc1_stage3)
	(copy--mingreentime__wrcc1_stage4)
	(copy--maxgreentime__wrcc1_stage4)
	(copy--mingreentime__wrcc1_stage5)
	(copy--maxgreentime__wrcc1_stage5)
	(copy--mingreentime__wrcc1_stage6)
	(copy--maxgreentime__wrcc1_stage6)
	(copy--mingreentime__wrdc1_stage1)
	(copy--maxgreentime__wrdc1_stage1)
	(copy--mingreentime__wrdc1_stage2)
	(copy--maxgreentime__wrdc1_stage2)
	(copy--mingreentime__wrdc1_stage3)
	(copy--maxgreentime__wrdc1_stage3)
	(copy--mingreentime__wrdc1_stage4)
	(copy--maxgreentime__wrdc1_stage4)
	(copy--mingreentime__wrec1_stage1)
	(copy--maxgreentime__wrec1_stage1)
	(copy--mingreentime__wrec1_stage2)
	(copy--maxgreentime__wrec1_stage2)
	(copy--mingreentime__wrec1_stage3)
	(copy--maxgreentime__wrec1_stage3)
	(copy--mingreentime__wrec1_stage4)
	(copy--maxgreentime__wrec1_stage4)
	(copy--mingreentime__wrfc1_stage1)
	(copy--maxgreentime__wrfc1_stage1)
	(copy--mingreentime__wrfc1_stage2)
	(copy--maxgreentime__wrfc1_stage2)
	(copy--mingreentime__wrfc1_stage3)
	(copy--maxgreentime__wrfc1_stage3)
	(copy--defaultgreentime__wrac1_stage1)
	(copy--defaultgreentime__wrac1_stage2)
	(copy--defaultgreentime__wrac1_stage3)
	(copy--defaultgreentime__wrac1_stage4)
	(copy--defaultgreentime__wrbc1_stage1)
	(copy--defaultgreentime__wrbc1_stage2)
	(copy--defaultgreentime__wrbc1_stage3)
	(copy--defaultgreentime__wrbc1_stage4)
	(copy--defaultgreentime__wrbc1_stage5)
	(copy--defaultgreentime__wrcc1_stage1)
	(copy--defaultgreentime__wrcc1_stage2)
	(copy--defaultgreentime__wrcc1_stage3)
	(copy--defaultgreentime__wrcc1_stage4)
	(copy--defaultgreentime__wrcc1_stage5)
	(copy--defaultgreentime__wrcc1_stage6)
	(copy--defaultgreentime__wrdc1_stage1)
	(copy--defaultgreentime__wrdc1_stage2)
	(copy--defaultgreentime__wrdc1_stage3)
	(copy--defaultgreentime__wrdc1_stage4)
	(copy--defaultgreentime__wrec1_stage1)
	(copy--defaultgreentime__wrec1_stage2)
	(copy--defaultgreentime__wrec1_stage3)
	(copy--defaultgreentime__wrec1_stage4)
	(copy--defaultgreentime__wrfc1_stage1)
	(copy--defaultgreentime__wrfc1_stage2)
	(copy--defaultgreentime__wrfc1_stage3)
	(copy--counter__abnor_v_wrdc1)
	(copy--counter__broad_x_wrfc1)
	(copy--counter__firth_d_wrac1)
	(copy--counter__hsac3_c_wrac1)
	(copy--counter__oldwa_c_wrec1)
	(copy--counter__oldwa_d_wrec1)
	(copy--counter__silve_w_wrbc1)
	(copy--counter__smith_c_wrcc1)
	(copy--counter__somer_v_wrbc1)
	(copy--counter__stand_f_wrac1)
	(copy--counter__wakef_z_wrfc1)
	(copy--counter__wrac1_m_stand)
	(copy--counter__wrac1_n_firth)
	(copy--counter__wrac1_x_wrbc1)
	(copy--counter__wrac1_y_wrbc1)
	(copy--counter__wrac1_z_hsac1)
	(copy--counter__wrbc1_a_wrac1)
	(copy--counter__wrbc1_b_wrac1)
	(copy--counter__wrbc1_b_wrcc1)
	(copy--counter__wrbc1_r_silve)
	(copy--counter__wrbc1_s_somer)
	(copy--counter__wrcc1_w_wrdc1)
	(copy--counter__wrcc1_x_wrdc1)
	(copy--counter__wrcc1_z_wrbc1)
	(copy--counter__wrdc1_a_wrcc1)
	(copy--counter__wrdc1_b_wrec1)
	(copy--counter__wrdc1_l_absou)
	(copy--counter__wrdc1_q_abnor)
	(copy--counter__wrec1_y_wrdc1)
	(copy--counter__wrec1_y_wrfc1)
	(copy--counter__wrec1_z_wrdc1)
	(copy--counter__wrfc1_a_wrec1)
	(copy--counter__wrfc1_r_broad)
	(copy--counter__wrfc1_t_wakef)
	(copy--cycletime__wrac1)
	(copy--cycletime__wrbc1)
	(copy--cycletime__wrcc1)
	(copy--cycletime__wrdc1)
	(copy--cycletime__wrec1)
	(copy--cycletime__wrfc1)
	(copy--granularity)
	(copy--maxcycletime__wrac1)
	(copy--maxcycletime__wrbc1)
	(copy--maxcycletime__wrcc1)
	(copy--maxcycletime__wrdc1)
	(copy--maxcycletime__wrec1)
	(copy--maxcycletime__wrfc1)
	(copy--mincycletime__wrac1)
	(copy--mincycletime__wrbc1)
	(copy--mincycletime__wrcc1)
	(copy--mincycletime__wrdc1)
	(copy--mincycletime__wrec1)
	(copy--mincycletime__wrfc1)
	(copy--time)
	(copy--turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1)
	(copy--turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand)
	(copy--turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1)
	(copy--turnrate__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1)
	(copy--turnrate__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth)
	(copy--turnrate__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1)
	(copy--turnrate__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth)
	(copy--turnrate__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand)
	(copy--turnrate__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1)
	(copy--turnrate__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve)
	(copy--turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1)
	(copy--turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1)
	(copy--turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve)
	(copy--turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer)
	(copy--turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1)
	(copy--turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1)
	(copy--turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1)
	(copy--turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer)
	(copy--turnrate__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1)
	(copy--turnrate__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve)
	(copy--turnrate__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer)
	(copy--turnrate__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1)
	(copy--turnrate__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1)
	(copy--turnrate__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer)
	(copy--turnrate__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1)
	(copy--turnrate__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1)
	(copy--turnrate__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1)
	(copy--turnrate__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve)
	(copy--turnrate__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer)
	(copy--turnrate__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1)
	(copy--turnrate__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1)
	(copy--turnrate__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1)
	(copy--turnrate__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1)
	(copy--turnrate__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1)
	(copy--turnrate__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1)
	(copy--turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1)
	(copy--turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1)
	(copy--turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1)
	(copy--turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1)
	(copy--turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1)
	(copy--turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1)
	(copy--turnrate__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1)
	(copy--turnrate__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou)
	(copy--turnrate__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1)
	(copy--turnrate__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor)
	(copy--turnrate__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1)
	(copy--turnrate__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor)
	(copy--turnrate__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou)
	(copy--turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1)
	(copy--turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1)
	(copy--turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou)
	(copy--turnrate__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1)
	(copy--turnrate__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou)
	(copy--turnrate__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor)
	(copy--turnrate__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1)
	(copy--turnrate__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1)
	(copy--turnrate__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1)
	(copy--turnrate__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1)
	(copy--turnrate__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1)
	(copy--turnrate__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1)
	(copy--turnrate__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1)
	(copy--turnrate__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1)
	(copy--turnrate__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1)
	(copy--turnrate__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1)
	(copy--turnrate__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1)
	(copy--turnrate__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad)
	(copy--turnrate__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef)
	(copy--turnrate__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad)
	(copy--turnrate__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef)
	(copy--turnrate__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1)
	(copy--turnrate__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef)
	(copy--turnrate__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1)
	(copy--turnrate__wrac1_stage3__firth_d_wrac1__wrac1_m_stand)
	(copy--turnrate__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1)
	(copy--turnrate__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1)
	(copy--turnrate__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1)
	(copy--turnrate__wrac1_stage4__firth_d_wrac1__wrac1_m_stand)
	(copy--turnrate__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1)
	(copy--turnrate__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1)
	(copy--turnrate__wrac1_stage3__stand_f_wrac1__wrac1_n_firth)
	(copy--turnrate__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1)
	(copy--turnrate__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1)
	(copy--turnrate__fake__outside__abnor_v_wrdc1)
	(copy--turnrate__fake__outside__broad_x_wrfc1)
	(copy--turnrate__fake__outside__firth_d_wrac1)
	(copy--turnrate__fake__outside__hsac3_c_wrac1)
	(copy--turnrate__fake__outside__oldwa_c_wrec1)
	(copy--turnrate__fake__outside__oldwa_d_wrec1)
	(copy--turnrate__fake__outside__silve_w_wrbc1)
	(copy--turnrate__fake__outside__smith_c_wrcc1)
	(copy--turnrate__fake__outside__somer_v_wrbc1)
	(copy--turnrate__fake__outside__stand_f_wrac1)
	(copy--turnrate__fake__outside__wakef_z_wrfc1)
	(copy--min_occ__wrac1_y_wrbc1)
	(copy--min_occ__wrac1_x_wrbc1)
	(copy--min_occ__wrbc1_a_wrac1)
	(copy--min_occ__wrbc1_b_wrcc1)
	(copy--min_occ__wrbc1_b_wrac1)
	(copy--min_occ__wrcc1_z_wrbc1)
	(copy--min_occ__wrcc1_x_wrdc1)
	(copy--min_occ__wrcc1_w_wrdc1)
	(copy--min_occ__wrdc1_a_wrcc1)
	(copy--min_occ__wrdc1_b_wrec1)
	(copy--min_occ__wrec1_z_wrdc1)
	(copy--min_occ__wrec1_y_wrfc1)
	(copy--min_occ__wrec1_y_wrdc1)
	(copy--min_occ__wrfc1_a_wrec1)
	(copy--min_occ__hsac3_c_wrac1)
	(copy--min_occ__stand_f_wrac1)
	(copy--min_occ__firth_d_wrac1)
	(copy--min_occ__silve_w_wrbc1)
	(copy--min_occ__somer_v_wrbc1)
	(copy--min_occ__smith_c_wrcc1)
	(copy--min_occ__abnor_v_wrdc1)
	(copy--min_occ__oldwa_c_wrec1)
	(copy--min_occ__oldwa_d_wrec1)
	(copy--min_occ__wakef_z_wrfc1)
	(copy--min_occ__broad_x_wrfc1)
	(copy--min_occ__outside)
	(copy--occupancy__hsac3_c_wrac1)
	(copy--occupancy__wrac1_y_wrbc1)
	(copy--occupancy__stand_f_wrac1)
	(copy--occupancy__firth_d_wrac1)
	(copy--occupancy__wrac1_x_wrbc1)
	(copy--occupancy__wrbc1_a_wrac1)
	(copy--occupancy__wrbc1_b_wrcc1)
	(copy--occupancy__silve_w_wrbc1)
	(copy--occupancy__somer_v_wrbc1)
	(copy--occupancy__wrbc1_b_wrac1)
	(copy--occupancy__wrcc1_z_wrbc1)
	(copy--occupancy__wrcc1_x_wrdc1)
	(copy--occupancy__smith_c_wrcc1)
	(copy--occupancy__wrcc1_w_wrdc1)
	(copy--occupancy__wrdc1_a_wrcc1)
	(copy--occupancy__wrdc1_b_wrec1)
	(copy--occupancy__abnor_v_wrdc1)
	(copy--occupancy__wrec1_z_wrdc1)
	(copy--occupancy__wrec1_y_wrfc1)
	(copy--occupancy__oldwa_c_wrec1)
	(copy--occupancy__wrec1_y_wrdc1)
	(copy--occupancy__oldwa_d_wrec1)
	(copy--occupancy__wakef_z_wrfc1)
	(copy--occupancy__wrfc1_a_wrec1)
	(copy--occupancy__broad_x_wrfc1)
	(copy--occupancy__wrac1_z_hsac1)
	(copy--occupancy__wrac1_m_stand)
	(copy--occupancy__wrac1_n_firth)
	(copy--occupancy__wrbc1_r_silve)
	(copy--occupancy__wrbc1_s_somer)
	(copy--occupancy__wrdc1_q_abnor)
	(copy--occupancy__wrdc1_l_absou)
	(copy--occupancy__wrfc1_r_broad)
	(copy--occupancy__wrfc1_t_wakef)
	(copy--occupancy__outside)
	(copy--greentime__wrac1)
	(copy--intertime__wrac1)
	(copy--greentime__wrbc1)
	(copy--intertime__wrbc1)
	(copy--greentime__wrcc1)
	(copy--intertime__wrcc1)
	(copy--greentime__wrdc1)
	(copy--intertime__wrdc1)
	(copy--greentime__wrec1)
	(copy--intertime__wrec1)
	(copy--greentime__wrfc1)
	(copy--intertime__wrfc1)
	(copy--defaultgreentime__fake)
	(copy--delta)
)

(:action extendStage__wrac1_stage1__wrac1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrac1_stage1) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrac1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrac1_stage1) 10.0 ) 
			(increase 
				(cycletime__wrac1) 10.0 ) (force-events) )
)

(:action extendStage__wrac1_stage2__wrac1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrac1_stage2) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrac1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrac1_stage2) 10.0 ) 
			(increase 
				(cycletime__wrac1) 10.0 ) (force-events) )
)

(:action extendStage__wrac1_stage3__wrac1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrac1_stage3) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrac1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrac1_stage3) 10.0 ) 
			(increase 
				(cycletime__wrac1) 10.0 ) (force-events) )
)

(:action extendStage__wrac1_stage4__wrac1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrac1_stage4) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrac1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrac1_stage4) 10.0 ) 
			(increase 
				(cycletime__wrac1) 10.0 ) (force-events) )
)

(:action extendStage__wrbc1_stage1__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrbc1_stage1) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrbc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrbc1_stage1) 10.0 ) 
			(increase 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action extendStage__wrbc1_stage2__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrbc1_stage2) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrbc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrbc1_stage2) 10.0 ) 
			(increase 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action extendStage__wrbc1_stage3__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrbc1_stage3) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrbc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrbc1_stage3) 10.0 ) 
			(increase 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action extendStage__wrbc1_stage4__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrbc1_stage4) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrbc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrbc1_stage4) 10.0 ) 
			(increase 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action extendStage__wrbc1_stage5__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrbc1_stage5) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrbc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrbc1_stage5) 10.0 ) 
			(increase 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action extendStage__wrcc1_stage1__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrcc1_stage1) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrcc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrcc1_stage1) 10.0 ) 
			(increase 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action extendStage__wrcc1_stage2__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrcc1_stage2) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrcc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrcc1_stage2) 10.0 ) 
			(increase 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action extendStage__wrcc1_stage3__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrcc1_stage3) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrcc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrcc1_stage3) 10.0 ) 
			(increase 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action extendStage__wrcc1_stage4__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrcc1_stage4) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrcc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrcc1_stage4) 10.0 ) 
			(increase 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action extendStage__wrcc1_stage5__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrcc1_stage5) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrcc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrcc1_stage5) 10.0 ) 
			(increase 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action extendStage__wrcc1_stage6__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrcc1_stage6) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrcc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrcc1_stage6) 10.0 ) 
			(increase 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action extendStage__wrdc1_stage1__wrdc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrdc1_stage1) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrdc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrdc1_stage1) 10.0 ) 
			(increase 
				(cycletime__wrdc1) 10.0 ) (force-events) )
)

(:action extendStage__wrdc1_stage2__wrdc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrdc1_stage2) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrdc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrdc1_stage2) 10.0 ) 
			(increase 
				(cycletime__wrdc1) 10.0 ) (force-events) )
)

(:action extendStage__wrdc1_stage3__wrdc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrdc1_stage3) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrdc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrdc1_stage3) 10.0 ) 
			(increase 
				(cycletime__wrdc1) 10.0 ) (force-events) )
)

(:action extendStage__wrdc1_stage4__wrdc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrdc1_stage4) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrdc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrdc1_stage4) 10.0 ) 
			(increase 
				(cycletime__wrdc1) 10.0 ) (force-events) )
)

(:action extendStage__wrec1_stage1__wrec1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrec1_stage1) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrec1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrec1_stage1) 10.0 ) 
			(increase 
				(cycletime__wrec1) 10.0 ) (force-events) )
)

(:action extendStage__wrec1_stage2__wrec1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrec1_stage2) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrec1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrec1_stage2) 10.0 ) 
			(increase 
				(cycletime__wrec1) 10.0 ) (force-events) )
)

(:action extendStage__wrec1_stage3__wrec1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrec1_stage3) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrec1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrec1_stage3) 10.0 ) 
			(increase 
				(cycletime__wrec1) 10.0 ) (force-events) )
)

(:action extendStage__wrec1_stage4__wrec1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrec1_stage4) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrec1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrec1_stage4) 10.0 ) 
			(increase 
				(cycletime__wrec1) 10.0 ) (force-events) )
)

(:action extendStage__wrfc1_stage1__wrfc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrfc1_stage1) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrfc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrfc1_stage1) 10.0 ) 
			(increase 
				(cycletime__wrfc1) 10.0 ) (force-events) )
)

(:action extendStage__wrfc1_stage2__wrfc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrfc1_stage2) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrfc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrfc1_stage2) 10.0 ) 
			(increase 
				(cycletime__wrfc1) 10.0 ) (force-events) )
)

(:action extendStage__wrfc1_stage3__wrfc1
 :parameters()
 :precondition 
	(and 
		
			(< 
				
					(+ 
						(defaultgreentime__wrfc1_stage3) 10.0 ) 120 ) 
			(< 
				
					(+ 
						(cycletime__wrfc1) 10.0 ) 200 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(defaultgreentime__wrfc1_stage3) 10.0 ) 
			(increase 
				(cycletime__wrfc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrac1_stage1__wrac1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrac1_stage1) 10.0 ) 10 ) 
			(> 
				
					(- 
						(cycletime__wrac1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrac1_stage1) 10.0 ) 
			(decrease 
				(cycletime__wrac1) 10.0 ) (force-events) )
)

(:action reduceStage__wrac1_stage2__wrac1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrac1_stage2) 10.0 ) 9 ) 
			(> 
				
					(- 
						(cycletime__wrac1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrac1_stage2) 10.0 ) 
			(decrease 
				(cycletime__wrac1) 10.0 ) (force-events) )
)

(:action reduceStage__wrac1_stage3__wrac1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrac1_stage3) 10.0 ) 8 ) 
			(> 
				
					(- 
						(cycletime__wrac1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrac1_stage3) 10.0 ) 
			(decrease 
				(cycletime__wrac1) 10.0 ) (force-events) )
)

(:action reduceStage__wrac1_stage4__wrac1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrac1_stage4) 10.0 ) 3 ) 
			(> 
				
					(- 
						(cycletime__wrac1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrac1_stage4) 10.0 ) 
			(decrease 
				(cycletime__wrac1) 10.0 ) (force-events) )
)

(:action reduceStage__wrbc1_stage1__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrbc1_stage1) 10.0 ) 7 ) 
			(> 
				
					(- 
						(cycletime__wrbc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrbc1_stage1) 10.0 ) 
			(decrease 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrbc1_stage2__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrbc1_stage2) 10.0 ) 5 ) 
			(> 
				
					(- 
						(cycletime__wrbc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrbc1_stage2) 10.0 ) 
			(decrease 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrbc1_stage3__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrbc1_stage3) 10.0 ) 10 ) 
			(> 
				
					(- 
						(cycletime__wrbc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrbc1_stage3) 10.0 ) 
			(decrease 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrbc1_stage4__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrbc1_stage4) 10.0 ) 5 ) 
			(> 
				
					(- 
						(cycletime__wrbc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrbc1_stage4) 10.0 ) 
			(decrease 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrbc1_stage5__wrbc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrbc1_stage5) 10.0 ) 2 ) 
			(> 
				
					(- 
						(cycletime__wrbc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrbc1_stage5) 10.0 ) 
			(decrease 
				(cycletime__wrbc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrcc1_stage1__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrcc1_stage1) 10.0 ) 7 ) 
			(> 
				
					(- 
						(cycletime__wrcc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrcc1_stage1) 10.0 ) 
			(decrease 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrcc1_stage2__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrcc1_stage2) 10.0 ) 5 ) 
			(> 
				
					(- 
						(cycletime__wrcc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrcc1_stage2) 10.0 ) 
			(decrease 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrcc1_stage3__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrcc1_stage3) 10.0 ) 5 ) 
			(> 
				
					(- 
						(cycletime__wrcc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrcc1_stage3) 10.0 ) 
			(decrease 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrcc1_stage4__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrcc1_stage4) 10.0 ) 4 ) 
			(> 
				
					(- 
						(cycletime__wrcc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrcc1_stage4) 10.0 ) 
			(decrease 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrcc1_stage5__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrcc1_stage5) 10.0 ) 7 ) 
			(> 
				
					(- 
						(cycletime__wrcc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrcc1_stage5) 10.0 ) 
			(decrease 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrcc1_stage6__wrcc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrcc1_stage6) 10.0 ) 2 ) 
			(> 
				
					(- 
						(cycletime__wrcc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrcc1_stage6) 10.0 ) 
			(decrease 
				(cycletime__wrcc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrdc1_stage1__wrdc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrdc1_stage1) 10.0 ) 7 ) 
			(> 
				
					(- 
						(cycletime__wrdc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrdc1_stage1) 10.0 ) 
			(decrease 
				(cycletime__wrdc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrdc1_stage2__wrdc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrdc1_stage2) 10.0 ) 7 ) 
			(> 
				
					(- 
						(cycletime__wrdc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrdc1_stage2) 10.0 ) 
			(decrease 
				(cycletime__wrdc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrdc1_stage3__wrdc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrdc1_stage3) 10.0 ) 7 ) 
			(> 
				
					(- 
						(cycletime__wrdc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrdc1_stage3) 10.0 ) 
			(decrease 
				(cycletime__wrdc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrdc1_stage4__wrdc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrdc1_stage4) 10.0 ) 7 ) 
			(> 
				
					(- 
						(cycletime__wrdc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrdc1_stage4) 10.0 ) 
			(decrease 
				(cycletime__wrdc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrec1_stage1__wrec1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrec1_stage1) 10.0 ) 10 ) 
			(> 
				
					(- 
						(cycletime__wrec1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrec1_stage1) 10.0 ) 
			(decrease 
				(cycletime__wrec1) 10.0 ) (force-events) )
)

(:action reduceStage__wrec1_stage2__wrec1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrec1_stage2) 10.0 ) 2 ) 
			(> 
				
					(- 
						(cycletime__wrec1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrec1_stage2) 10.0 ) 
			(decrease 
				(cycletime__wrec1) 10.0 ) (force-events) )
)

(:action reduceStage__wrec1_stage3__wrec1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrec1_stage3) 10.0 ) 6 ) 
			(> 
				
					(- 
						(cycletime__wrec1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrec1_stage3) 10.0 ) 
			(decrease 
				(cycletime__wrec1) 10.0 ) (force-events) )
)

(:action reduceStage__wrec1_stage4__wrec1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrec1_stage4) 10.0 ) 6 ) 
			(> 
				
					(- 
						(cycletime__wrec1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrec1_stage4) 10.0 ) 
			(decrease 
				(cycletime__wrec1) 10.0 ) (force-events) )
)

(:action reduceStage__wrfc1_stage1__wrfc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrfc1_stage1) 10.0 ) 10 ) 
			(> 
				
					(- 
						(cycletime__wrfc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrfc1_stage1) 10.0 ) 
			(decrease 
				(cycletime__wrfc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrfc1_stage2__wrfc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrfc1_stage2) 10.0 ) 4 ) 
			(> 
				
					(- 
						(cycletime__wrfc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrfc1_stage2) 10.0 ) 
			(decrease 
				(cycletime__wrfc1) 10.0 ) (force-events) )
)

(:action reduceStage__wrfc1_stage3__wrfc1
 :parameters()
 :precondition 
	(and 
		
			(> 
				
					(- 
						(defaultgreentime__wrfc1_stage3) 10.0 ) 7 ) 
			(> 
				
					(- 
						(cycletime__wrfc1) 10.0 ) 50 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(decrease 
				(defaultgreentime__wrfc1_stage3) 10.0 ) 
			(decrease 
				(cycletime__wrfc1) 10.0 ) (force-events) )
)

(:action simulate-events
 :parameters()
 :precondition 
	(and (force-events))
 :effect 
	(and 
		
			
				(when 
					
						(and 
							(active__wrac1_stage1) 
								(>= 
									(greentime__wrac1) (defaultgreentime__wrac1_stage1) ) ) (fired--defgreenreached__wrac1_stage1__wrac1) ) 
				(when 
					
						(and 
							(active__wrac1_stage2) 
								(>= 
									(greentime__wrac1) (defaultgreentime__wrac1_stage2) ) ) (fired--defgreenreached__wrac1_stage2__wrac1) ) 
				(when 
					
						(and 
							(active__wrac1_stage3) 
								(>= 
									(greentime__wrac1) (defaultgreentime__wrac1_stage3) ) ) (fired--defgreenreached__wrac1_stage3__wrac1) ) 
				(when 
					
						(and 
							(active__wrac1_stage4) 
								(>= 
									(greentime__wrac1) (defaultgreentime__wrac1_stage4) ) ) (fired--defgreenreached__wrac1_stage4__wrac1) ) 
				(when 
					
						(and 
							(active__wrbc1_stage1) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage1) ) ) (fired--defgreenreached__wrbc1_stage1__wrbc1) ) 
				(when 
					
						(and 
							(active__wrbc1_stage2) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage2) ) ) (fired--defgreenreached__wrbc1_stage2__wrbc1) ) 
				(when 
					
						(and 
							(active__wrbc1_stage3) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage3) ) ) (fired--defgreenreached__wrbc1_stage3__wrbc1) ) 
				(when 
					
						(and 
							(active__wrbc1_stage4) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage4) ) ) (fired--defgreenreached__wrbc1_stage4__wrbc1) ) 
				(when 
					
						(and 
							(active__wrbc1_stage5) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage5) ) ) (fired--defgreenreached__wrbc1_stage5__wrbc1) ) 
				(when 
					
						(and 
							(active__wrcc1_stage1) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage1) ) ) (fired--defgreenreached__wrcc1_stage1__wrcc1) ) 
				(when 
					
						(and 
							(active__wrcc1_stage2) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage2) ) ) (fired--defgreenreached__wrcc1_stage2__wrcc1) ) 
				(when 
					
						(and 
							(active__wrcc1_stage3) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage3) ) ) (fired--defgreenreached__wrcc1_stage3__wrcc1) ) 
				(when 
					
						(and 
							(active__wrcc1_stage4) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage4) ) ) (fired--defgreenreached__wrcc1_stage4__wrcc1) ) 
				(when 
					
						(and 
							(active__wrcc1_stage5) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage5) ) ) (fired--defgreenreached__wrcc1_stage5__wrcc1) ) 
				(when 
					
						(and 
							(active__wrcc1_stage6) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage6) ) ) (fired--defgreenreached__wrcc1_stage6__wrcc1) ) 
				(when 
					
						(and 
							(active__wrdc1_stage1) 
								(>= 
									(greentime__wrdc1) (defaultgreentime__wrdc1_stage1) ) ) (fired--defgreenreached__wrdc1_stage1__wrdc1) ) 
				(when 
					
						(and 
							(active__wrdc1_stage2) 
								(>= 
									(greentime__wrdc1) (defaultgreentime__wrdc1_stage2) ) ) (fired--defgreenreached__wrdc1_stage2__wrdc1) ) 
				(when 
					
						(and 
							(active__wrdc1_stage3) 
								(>= 
									(greentime__wrdc1) (defaultgreentime__wrdc1_stage3) ) ) (fired--defgreenreached__wrdc1_stage3__wrdc1) ) 
				(when 
					
						(and 
							(active__wrdc1_stage4) 
								(>= 
									(greentime__wrdc1) (defaultgreentime__wrdc1_stage4) ) ) (fired--defgreenreached__wrdc1_stage4__wrdc1) ) 
				(when 
					
						(and 
							(active__wrec1_stage1) 
								(>= 
									(greentime__wrec1) (defaultgreentime__wrec1_stage1) ) ) (fired--defgreenreached__wrec1_stage1__wrec1) ) 
				(when 
					
						(and 
							(active__wrec1_stage2) 
								(>= 
									(greentime__wrec1) (defaultgreentime__wrec1_stage2) ) ) (fired--defgreenreached__wrec1_stage2__wrec1) ) 
				(when 
					
						(and 
							(active__wrec1_stage3) 
								(>= 
									(greentime__wrec1) (defaultgreentime__wrec1_stage3) ) ) (fired--defgreenreached__wrec1_stage3__wrec1) ) 
				(when 
					
						(and 
							(active__wrec1_stage4) 
								(>= 
									(greentime__wrec1) (defaultgreentime__wrec1_stage4) ) ) (fired--defgreenreached__wrec1_stage4__wrec1) ) 
				(when 
					
						(and 
							(active__wrfc1_stage1) 
								(>= 
									(greentime__wrfc1) (defaultgreentime__wrfc1_stage1) ) ) (fired--defgreenreached__wrfc1_stage1__wrfc1) ) 
				(when 
					
						(and 
							(active__wrfc1_stage2) 
								(>= 
									(greentime__wrfc1) (defaultgreentime__wrfc1_stage2) ) ) (fired--defgreenreached__wrfc1_stage2__wrfc1) ) 
				(when 
					
						(and 
							(active__wrfc1_stage3) 
								(>= 
									(greentime__wrfc1) (defaultgreentime__wrfc1_stage3) ) ) (fired--defgreenreached__wrfc1_stage3__wrfc1) ) 
				(when 
					
						(and 
							(trigger__wrac1) (active__wrac1_stage1) ) (fired--trigger-inter__wrac1_stage1__wrac1) ) 
				(when 
					
						(and 
							(trigger__wrac1) (active__wrac1_stage2) ) (fired--trigger-inter__wrac1_stage2__wrac1) ) 
				(when 
					
						(and 
							(trigger__wrac1) (active__wrac1_stage3) ) (fired--trigger-inter__wrac1_stage3__wrac1) ) 
				(when 
					
						(and 
							(trigger__wrac1) (active__wrac1_stage4) ) (fired--trigger-inter__wrac1_stage4__wrac1) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage1) ) (fired--trigger-inter__wrbc1_stage1__wrbc1) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage2) ) (fired--trigger-inter__wrbc1_stage2__wrbc1) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage3) ) (fired--trigger-inter__wrbc1_stage3__wrbc1) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage4) ) (fired--trigger-inter__wrbc1_stage4__wrbc1) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage5) ) (fired--trigger-inter__wrbc1_stage5__wrbc1) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage1) ) (fired--trigger-inter__wrcc1_stage1__wrcc1) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage2) ) (fired--trigger-inter__wrcc1_stage2__wrcc1) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage3) ) (fired--trigger-inter__wrcc1_stage3__wrcc1) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage4) ) (fired--trigger-inter__wrcc1_stage4__wrcc1) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage5) ) (fired--trigger-inter__wrcc1_stage5__wrcc1) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage6) ) (fired--trigger-inter__wrcc1_stage6__wrcc1) ) 
				(when 
					
						(and 
							(trigger__wrdc1) (active__wrdc1_stage1) ) (fired--trigger-inter__wrdc1_stage1__wrdc1) ) 
				(when 
					
						(and 
							(trigger__wrdc1) (active__wrdc1_stage2) ) (fired--trigger-inter__wrdc1_stage2__wrdc1) ) 
				(when 
					
						(and 
							(trigger__wrdc1) (active__wrdc1_stage3) ) (fired--trigger-inter__wrdc1_stage3__wrdc1) ) 
				(when 
					
						(and 
							(trigger__wrdc1) (active__wrdc1_stage4) ) (fired--trigger-inter__wrdc1_stage4__wrdc1) ) 
				(when 
					
						(and 
							(trigger__wrec1) (active__wrec1_stage1) ) (fired--trigger-inter__wrec1_stage1__wrec1) ) 
				(when 
					
						(and 
							(trigger__wrec1) (active__wrec1_stage2) ) (fired--trigger-inter__wrec1_stage2__wrec1) ) 
				(when 
					
						(and 
							(trigger__wrec1) (active__wrec1_stage3) ) (fired--trigger-inter__wrec1_stage3__wrec1) ) 
				(when 
					
						(and 
							(trigger__wrec1) (active__wrec1_stage4) ) (fired--trigger-inter__wrec1_stage4__wrec1) ) 
				(when 
					
						(and 
							(trigger__wrfc1) (active__wrfc1_stage1) ) (fired--trigger-inter__wrfc1_stage1__wrfc1) ) 
				(when 
					
						(and 
							(trigger__wrfc1) (active__wrfc1_stage2) ) (fired--trigger-inter__wrfc1_stage2__wrfc1) ) 
				(when 
					
						(and 
							(trigger__wrfc1) (active__wrfc1_stage3) ) (fired--trigger-inter__wrfc1_stage3__wrfc1) ) 
				(when 
					
						(and 
							(inter__wrac1_stage1) 
								(>= 
									(intertime__wrac1) 
										(- 
											5 0.1 ) ) ) (fired--trigger-change__wrac1_stage1__wrac1_stage2__wrac1) ) 
				(when 
					
						(and 
							(inter__wrac1_stage2) 
								(>= 
									(intertime__wrac1) 
										(- 
											6 0.1 ) ) ) (fired--trigger-change__wrac1_stage2__wrac1_stage3__wrac1) ) 
				(when 
					
						(and 
							(inter__wrac1_stage3) 
								(>= 
									(intertime__wrac1) 
										(- 
											5 0.1 ) ) ) (fired--trigger-change__wrac1_stage3__wrac1_stage4__wrac1) ) 
				(when 
					
						(and 
							(inter__wrac1_stage4) 
								(>= 
									(intertime__wrac1) 
										(- 
											5 0.1 ) ) ) (fired--trigger-change__wrac1_stage4__wrac1_stage1__wrac1) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage1) 
								(>= 
									(intertime__wrbc1) 
										(- 
											6 0.1 ) ) ) (fired--trigger-change__wrbc1_stage1__wrbc1_stage2__wrbc1) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage2) 
								(>= 
									(intertime__wrbc1) 
										(- 
											12 0.1 ) ) ) (fired--trigger-change__wrbc1_stage2__wrbc1_stage3__wrbc1) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage3) 
								(>= 
									(intertime__wrbc1) 
										(- 
											6 0.1 ) ) ) (fired--trigger-change__wrbc1_stage3__wrbc1_stage4__wrbc1) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage4) 
								(>= 
									(intertime__wrbc1) 
										(- 
											7 0.1 ) ) ) (fired--trigger-change__wrbc1_stage4__wrbc1_stage5__wrbc1) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage5) 
								(>= 
									(intertime__wrbc1) 
										(- 
											12 0.1 ) ) ) (fired--trigger-change__wrbc1_stage5__wrbc1_stage1__wrbc1) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage1) 
								(>= 
									(intertime__wrcc1) 
										(- 
											6 0.1 ) ) ) (fired--trigger-change__wrcc1_stage1__wrcc1_stage2__wrcc1) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage2) 
								(>= 
									(intertime__wrcc1) 
										(- 
											8 0.1 ) ) ) (fired--trigger-change__wrcc1_stage2__wrcc1_stage3__wrcc1) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage3) 
								(>= 
									(intertime__wrcc1) 
										(- 
											8 0.1 ) ) ) (fired--trigger-change__wrcc1_stage3__wrcc1_stage4__wrcc1) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage4) 
								(>= 
									(intertime__wrcc1) 
										(- 
											11 0.1 ) ) ) (fired--trigger-change__wrcc1_stage4__wrcc1_stage5__wrcc1) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage5) 
								(>= 
									(intertime__wrcc1) 
										(- 
											3 0.1 ) ) ) (fired--trigger-change__wrcc1_stage5__wrcc1_stage6__wrcc1) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage6) 
								(>= 
									(intertime__wrcc1) 
										(- 
											2 0.1 ) ) ) (fired--trigger-change__wrcc1_stage6__wrcc1_stage1__wrcc1) ) 
				(when 
					
						(and 
							(inter__wrdc1_stage1) 
								(>= 
									(intertime__wrdc1) 
										(- 
											8 0.1 ) ) ) (fired--trigger-change__wrdc1_stage1__wrdc1_stage2__wrdc1) ) 
				(when 
					
						(and 
							(inter__wrdc1_stage2) 
								(>= 
									(intertime__wrdc1) 
										(- 
											8 0.1 ) ) ) (fired--trigger-change__wrdc1_stage2__wrdc1_stage3__wrdc1) ) 
				(when 
					
						(and 
							(inter__wrdc1_stage3) 
								(>= 
									(intertime__wrdc1) 
										(- 
											6 0.1 ) ) ) (fired--trigger-change__wrdc1_stage3__wrdc1_stage4__wrdc1) ) 
				(when 
					
						(and 
							(inter__wrdc1_stage4) 
								(>= 
									(intertime__wrdc1) 
										(- 
											7 0.1 ) ) ) (fired--trigger-change__wrdc1_stage4__wrdc1_stage1__wrdc1) ) 
				(when 
					
						(and 
							(inter__wrec1_stage1) 
								(>= 
									(intertime__wrec1) 
										(- 
											3 0.1 ) ) ) (fired--trigger-change__wrec1_stage1__wrec1_stage2__wrec1) ) 
				(when 
					
						(and 
							(inter__wrec1_stage2) 
								(>= 
									(intertime__wrec1) 
										(- 
											7 0.1 ) ) ) (fired--trigger-change__wrec1_stage2__wrec1_stage3__wrec1) ) 
				(when 
					
						(and 
							(inter__wrec1_stage3) 
								(>= 
									(intertime__wrec1) 
										(- 
											7 0.1 ) ) ) (fired--trigger-change__wrec1_stage3__wrec1_stage4__wrec1) ) 
				(when 
					
						(and 
							(inter__wrec1_stage4) 
								(>= 
									(intertime__wrec1) 
										(- 
											7 0.1 ) ) ) (fired--trigger-change__wrec1_stage4__wrec1_stage1__wrec1) ) 
				(when 
					
						(and 
							(inter__wrfc1_stage1) 
								(>= 
									(intertime__wrfc1) 
										(- 
											5 0.1 ) ) ) (fired--trigger-change__wrfc1_stage1__wrfc1_stage2__wrfc1) ) 
				(when 
					
						(and 
							(inter__wrfc1_stage2) 
								(>= 
									(intertime__wrfc1) 
										(- 
											9 0.1 ) ) ) (fired--trigger-change__wrfc1_stage2__wrfc1_stage3__wrfc1) ) 
				(when 
					
						(and 
							(inter__wrfc1_stage3) 
								(>= 
									(intertime__wrfc1) 
										(- 
											8 0.1 ) ) ) (fired--trigger-change__wrfc1_stage3__wrfc1_stage1__wrfc1) )  
			
				(when 
					
						(and 
							
								(or 
									
										(not 
											(and 
												(active__wrac1_stage1) 
													(>= 
														(greentime__wrac1) (defaultgreentime__wrac1_stage1) ) )) (fired--defgreenreached__wrac1_stage1__wrac1) ) 
								(or 
									
										(not 
											(and 
												(active__wrac1_stage2) 
													(>= 
														(greentime__wrac1) (defaultgreentime__wrac1_stage2) ) )) (fired--defgreenreached__wrac1_stage2__wrac1) ) 
								(or 
									
										(not 
											(and 
												(active__wrac1_stage3) 
													(>= 
														(greentime__wrac1) (defaultgreentime__wrac1_stage3) ) )) (fired--defgreenreached__wrac1_stage3__wrac1) ) 
								(or 
									
										(not 
											(and 
												(active__wrac1_stage4) 
													(>= 
														(greentime__wrac1) (defaultgreentime__wrac1_stage4) ) )) (fired--defgreenreached__wrac1_stage4__wrac1) ) 
								(or 
									
										(not 
											(and 
												(active__wrbc1_stage1) 
													(>= 
														(greentime__wrbc1) (defaultgreentime__wrbc1_stage1) ) )) (fired--defgreenreached__wrbc1_stage1__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrbc1_stage2) 
													(>= 
														(greentime__wrbc1) (defaultgreentime__wrbc1_stage2) ) )) (fired--defgreenreached__wrbc1_stage2__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrbc1_stage3) 
													(>= 
														(greentime__wrbc1) (defaultgreentime__wrbc1_stage3) ) )) (fired--defgreenreached__wrbc1_stage3__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrbc1_stage4) 
													(>= 
														(greentime__wrbc1) (defaultgreentime__wrbc1_stage4) ) )) (fired--defgreenreached__wrbc1_stage4__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrbc1_stage5) 
													(>= 
														(greentime__wrbc1) (defaultgreentime__wrbc1_stage5) ) )) (fired--defgreenreached__wrbc1_stage5__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrcc1_stage1) 
													(>= 
														(greentime__wrcc1) (defaultgreentime__wrcc1_stage1) ) )) (fired--defgreenreached__wrcc1_stage1__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrcc1_stage2) 
													(>= 
														(greentime__wrcc1) (defaultgreentime__wrcc1_stage2) ) )) (fired--defgreenreached__wrcc1_stage2__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrcc1_stage3) 
													(>= 
														(greentime__wrcc1) (defaultgreentime__wrcc1_stage3) ) )) (fired--defgreenreached__wrcc1_stage3__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrcc1_stage4) 
													(>= 
														(greentime__wrcc1) (defaultgreentime__wrcc1_stage4) ) )) (fired--defgreenreached__wrcc1_stage4__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrcc1_stage5) 
													(>= 
														(greentime__wrcc1) (defaultgreentime__wrcc1_stage5) ) )) (fired--defgreenreached__wrcc1_stage5__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrcc1_stage6) 
													(>= 
														(greentime__wrcc1) (defaultgreentime__wrcc1_stage6) ) )) (fired--defgreenreached__wrcc1_stage6__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrdc1_stage1) 
													(>= 
														(greentime__wrdc1) (defaultgreentime__wrdc1_stage1) ) )) (fired--defgreenreached__wrdc1_stage1__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrdc1_stage2) 
													(>= 
														(greentime__wrdc1) (defaultgreentime__wrdc1_stage2) ) )) (fired--defgreenreached__wrdc1_stage2__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrdc1_stage3) 
													(>= 
														(greentime__wrdc1) (defaultgreentime__wrdc1_stage3) ) )) (fired--defgreenreached__wrdc1_stage3__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrdc1_stage4) 
													(>= 
														(greentime__wrdc1) (defaultgreentime__wrdc1_stage4) ) )) (fired--defgreenreached__wrdc1_stage4__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrec1_stage1) 
													(>= 
														(greentime__wrec1) (defaultgreentime__wrec1_stage1) ) )) (fired--defgreenreached__wrec1_stage1__wrec1) ) 
								(or 
									
										(not 
											(and 
												(active__wrec1_stage2) 
													(>= 
														(greentime__wrec1) (defaultgreentime__wrec1_stage2) ) )) (fired--defgreenreached__wrec1_stage2__wrec1) ) 
								(or 
									
										(not 
											(and 
												(active__wrec1_stage3) 
													(>= 
														(greentime__wrec1) (defaultgreentime__wrec1_stage3) ) )) (fired--defgreenreached__wrec1_stage3__wrec1) ) 
								(or 
									
										(not 
											(and 
												(active__wrec1_stage4) 
													(>= 
														(greentime__wrec1) (defaultgreentime__wrec1_stage4) ) )) (fired--defgreenreached__wrec1_stage4__wrec1) ) 
								(or 
									
										(not 
											(and 
												(active__wrfc1_stage1) 
													(>= 
														(greentime__wrfc1) (defaultgreentime__wrfc1_stage1) ) )) (fired--defgreenreached__wrfc1_stage1__wrfc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrfc1_stage2) 
													(>= 
														(greentime__wrfc1) (defaultgreentime__wrfc1_stage2) ) )) (fired--defgreenreached__wrfc1_stage2__wrfc1) ) 
								(or 
									
										(not 
											(and 
												(active__wrfc1_stage3) 
													(>= 
														(greentime__wrfc1) (defaultgreentime__wrfc1_stage3) ) )) (fired--defgreenreached__wrfc1_stage3__wrfc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrac1) (active__wrac1_stage1) )) (fired--trigger-inter__wrac1_stage1__wrac1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrac1) (active__wrac1_stage2) )) (fired--trigger-inter__wrac1_stage2__wrac1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrac1) (active__wrac1_stage3) )) (fired--trigger-inter__wrac1_stage3__wrac1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrac1) (active__wrac1_stage4) )) (fired--trigger-inter__wrac1_stage4__wrac1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrbc1) (active__wrbc1_stage1) )) (fired--trigger-inter__wrbc1_stage1__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrbc1) (active__wrbc1_stage2) )) (fired--trigger-inter__wrbc1_stage2__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrbc1) (active__wrbc1_stage3) )) (fired--trigger-inter__wrbc1_stage3__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrbc1) (active__wrbc1_stage4) )) (fired--trigger-inter__wrbc1_stage4__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrbc1) (active__wrbc1_stage5) )) (fired--trigger-inter__wrbc1_stage5__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrcc1) (active__wrcc1_stage1) )) (fired--trigger-inter__wrcc1_stage1__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrcc1) (active__wrcc1_stage2) )) (fired--trigger-inter__wrcc1_stage2__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrcc1) (active__wrcc1_stage3) )) (fired--trigger-inter__wrcc1_stage3__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrcc1) (active__wrcc1_stage4) )) (fired--trigger-inter__wrcc1_stage4__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrcc1) (active__wrcc1_stage5) )) (fired--trigger-inter__wrcc1_stage5__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrcc1) (active__wrcc1_stage6) )) (fired--trigger-inter__wrcc1_stage6__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrdc1) (active__wrdc1_stage1) )) (fired--trigger-inter__wrdc1_stage1__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrdc1) (active__wrdc1_stage2) )) (fired--trigger-inter__wrdc1_stage2__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrdc1) (active__wrdc1_stage3) )) (fired--trigger-inter__wrdc1_stage3__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrdc1) (active__wrdc1_stage4) )) (fired--trigger-inter__wrdc1_stage4__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrec1) (active__wrec1_stage1) )) (fired--trigger-inter__wrec1_stage1__wrec1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrec1) (active__wrec1_stage2) )) (fired--trigger-inter__wrec1_stage2__wrec1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrec1) (active__wrec1_stage3) )) (fired--trigger-inter__wrec1_stage3__wrec1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrec1) (active__wrec1_stage4) )) (fired--trigger-inter__wrec1_stage4__wrec1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrfc1) (active__wrfc1_stage1) )) (fired--trigger-inter__wrfc1_stage1__wrfc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrfc1) (active__wrfc1_stage2) )) (fired--trigger-inter__wrfc1_stage2__wrfc1) ) 
								(or 
									
										(not 
											(and 
												(trigger__wrfc1) (active__wrfc1_stage3) )) (fired--trigger-inter__wrfc1_stage3__wrfc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrac1_stage1) 
													(>= 
														(intertime__wrac1) 
															(- 
																5 0.1 ) ) )) (fired--trigger-change__wrac1_stage1__wrac1_stage2__wrac1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrac1_stage2) 
													(>= 
														(intertime__wrac1) 
															(- 
																6 0.1 ) ) )) (fired--trigger-change__wrac1_stage2__wrac1_stage3__wrac1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrac1_stage3) 
													(>= 
														(intertime__wrac1) 
															(- 
																5 0.1 ) ) )) (fired--trigger-change__wrac1_stage3__wrac1_stage4__wrac1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrac1_stage4) 
													(>= 
														(intertime__wrac1) 
															(- 
																5 0.1 ) ) )) (fired--trigger-change__wrac1_stage4__wrac1_stage1__wrac1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrbc1_stage1) 
													(>= 
														(intertime__wrbc1) 
															(- 
																6 0.1 ) ) )) (fired--trigger-change__wrbc1_stage1__wrbc1_stage2__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrbc1_stage2) 
													(>= 
														(intertime__wrbc1) 
															(- 
																12 0.1 ) ) )) (fired--trigger-change__wrbc1_stage2__wrbc1_stage3__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrbc1_stage3) 
													(>= 
														(intertime__wrbc1) 
															(- 
																6 0.1 ) ) )) (fired--trigger-change__wrbc1_stage3__wrbc1_stage4__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrbc1_stage4) 
													(>= 
														(intertime__wrbc1) 
															(- 
																7 0.1 ) ) )) (fired--trigger-change__wrbc1_stage4__wrbc1_stage5__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrbc1_stage5) 
													(>= 
														(intertime__wrbc1) 
															(- 
																12 0.1 ) ) )) (fired--trigger-change__wrbc1_stage5__wrbc1_stage1__wrbc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrcc1_stage1) 
													(>= 
														(intertime__wrcc1) 
															(- 
																6 0.1 ) ) )) (fired--trigger-change__wrcc1_stage1__wrcc1_stage2__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrcc1_stage2) 
													(>= 
														(intertime__wrcc1) 
															(- 
																8 0.1 ) ) )) (fired--trigger-change__wrcc1_stage2__wrcc1_stage3__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrcc1_stage3) 
													(>= 
														(intertime__wrcc1) 
															(- 
																8 0.1 ) ) )) (fired--trigger-change__wrcc1_stage3__wrcc1_stage4__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrcc1_stage4) 
													(>= 
														(intertime__wrcc1) 
															(- 
																11 0.1 ) ) )) (fired--trigger-change__wrcc1_stage4__wrcc1_stage5__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrcc1_stage5) 
													(>= 
														(intertime__wrcc1) 
															(- 
																3 0.1 ) ) )) (fired--trigger-change__wrcc1_stage5__wrcc1_stage6__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrcc1_stage6) 
													(>= 
														(intertime__wrcc1) 
															(- 
																2 0.1 ) ) )) (fired--trigger-change__wrcc1_stage6__wrcc1_stage1__wrcc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrdc1_stage1) 
													(>= 
														(intertime__wrdc1) 
															(- 
																8 0.1 ) ) )) (fired--trigger-change__wrdc1_stage1__wrdc1_stage2__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrdc1_stage2) 
													(>= 
														(intertime__wrdc1) 
															(- 
																8 0.1 ) ) )) (fired--trigger-change__wrdc1_stage2__wrdc1_stage3__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrdc1_stage3) 
													(>= 
														(intertime__wrdc1) 
															(- 
																6 0.1 ) ) )) (fired--trigger-change__wrdc1_stage3__wrdc1_stage4__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrdc1_stage4) 
													(>= 
														(intertime__wrdc1) 
															(- 
																7 0.1 ) ) )) (fired--trigger-change__wrdc1_stage4__wrdc1_stage1__wrdc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrec1_stage1) 
													(>= 
														(intertime__wrec1) 
															(- 
																3 0.1 ) ) )) (fired--trigger-change__wrec1_stage1__wrec1_stage2__wrec1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrec1_stage2) 
													(>= 
														(intertime__wrec1) 
															(- 
																7 0.1 ) ) )) (fired--trigger-change__wrec1_stage2__wrec1_stage3__wrec1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrec1_stage3) 
													(>= 
														(intertime__wrec1) 
															(- 
																7 0.1 ) ) )) (fired--trigger-change__wrec1_stage3__wrec1_stage4__wrec1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrec1_stage4) 
													(>= 
														(intertime__wrec1) 
															(- 
																7 0.1 ) ) )) (fired--trigger-change__wrec1_stage4__wrec1_stage1__wrec1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrfc1_stage1) 
													(>= 
														(intertime__wrfc1) 
															(- 
																5 0.1 ) ) )) (fired--trigger-change__wrfc1_stage1__wrfc1_stage2__wrfc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrfc1_stage2) 
													(>= 
														(intertime__wrfc1) 
															(- 
																9 0.1 ) ) )) (fired--trigger-change__wrfc1_stage2__wrfc1_stage3__wrfc1) ) 
								(or 
									
										(not 
											(and 
												(inter__wrfc1_stage3) 
													(>= 
														(intertime__wrfc1) 
															(- 
																8 0.1 ) ) )) (fired--trigger-change__wrfc1_stage3__wrfc1_stage1__wrfc1) ) ) 
						(and 
							
								(not (force-events)) 
								(not (fired--defgreenreached__wrac1_stage1__wrac1)) 
								(not (fired--defgreenreached__wrac1_stage2__wrac1)) 
								(not (fired--defgreenreached__wrac1_stage3__wrac1)) 
								(not (fired--defgreenreached__wrac1_stage4__wrac1)) 
								(not (fired--defgreenreached__wrbc1_stage1__wrbc1)) 
								(not (fired--defgreenreached__wrbc1_stage2__wrbc1)) 
								(not (fired--defgreenreached__wrbc1_stage3__wrbc1)) 
								(not (fired--defgreenreached__wrbc1_stage4__wrbc1)) 
								(not (fired--defgreenreached__wrbc1_stage5__wrbc1)) 
								(not (fired--defgreenreached__wrcc1_stage1__wrcc1)) 
								(not (fired--defgreenreached__wrcc1_stage2__wrcc1)) 
								(not (fired--defgreenreached__wrcc1_stage3__wrcc1)) 
								(not (fired--defgreenreached__wrcc1_stage4__wrcc1)) 
								(not (fired--defgreenreached__wrcc1_stage5__wrcc1)) 
								(not (fired--defgreenreached__wrcc1_stage6__wrcc1)) 
								(not (fired--defgreenreached__wrdc1_stage1__wrdc1)) 
								(not (fired--defgreenreached__wrdc1_stage2__wrdc1)) 
								(not (fired--defgreenreached__wrdc1_stage3__wrdc1)) 
								(not (fired--defgreenreached__wrdc1_stage4__wrdc1)) 
								(not (fired--defgreenreached__wrec1_stage1__wrec1)) 
								(not (fired--defgreenreached__wrec1_stage2__wrec1)) 
								(not (fired--defgreenreached__wrec1_stage3__wrec1)) 
								(not (fired--defgreenreached__wrec1_stage4__wrec1)) 
								(not (fired--defgreenreached__wrfc1_stage1__wrfc1)) 
								(not (fired--defgreenreached__wrfc1_stage2__wrfc1)) 
								(not (fired--defgreenreached__wrfc1_stage3__wrfc1)) 
								(not (fired--trigger-inter__wrac1_stage1__wrac1)) 
								(not (fired--trigger-inter__wrac1_stage2__wrac1)) 
								(not (fired--trigger-inter__wrac1_stage3__wrac1)) 
								(not (fired--trigger-inter__wrac1_stage4__wrac1)) 
								(not (fired--trigger-inter__wrbc1_stage1__wrbc1)) 
								(not (fired--trigger-inter__wrbc1_stage2__wrbc1)) 
								(not (fired--trigger-inter__wrbc1_stage3__wrbc1)) 
								(not (fired--trigger-inter__wrbc1_stage4__wrbc1)) 
								(not (fired--trigger-inter__wrbc1_stage5__wrbc1)) 
								(not (fired--trigger-inter__wrcc1_stage1__wrcc1)) 
								(not (fired--trigger-inter__wrcc1_stage2__wrcc1)) 
								(not (fired--trigger-inter__wrcc1_stage3__wrcc1)) 
								(not (fired--trigger-inter__wrcc1_stage4__wrcc1)) 
								(not (fired--trigger-inter__wrcc1_stage5__wrcc1)) 
								(not (fired--trigger-inter__wrcc1_stage6__wrcc1)) 
								(not (fired--trigger-inter__wrdc1_stage1__wrdc1)) 
								(not (fired--trigger-inter__wrdc1_stage2__wrdc1)) 
								(not (fired--trigger-inter__wrdc1_stage3__wrdc1)) 
								(not (fired--trigger-inter__wrdc1_stage4__wrdc1)) 
								(not (fired--trigger-inter__wrec1_stage1__wrec1)) 
								(not (fired--trigger-inter__wrec1_stage2__wrec1)) 
								(not (fired--trigger-inter__wrec1_stage3__wrec1)) 
								(not (fired--trigger-inter__wrec1_stage4__wrec1)) 
								(not (fired--trigger-inter__wrfc1_stage1__wrfc1)) 
								(not (fired--trigger-inter__wrfc1_stage2__wrfc1)) 
								(not (fired--trigger-inter__wrfc1_stage3__wrfc1)) 
								(not (fired--trigger-change__wrac1_stage1__wrac1_stage2__wrac1)) 
								(not (fired--trigger-change__wrac1_stage2__wrac1_stage3__wrac1)) 
								(not (fired--trigger-change__wrac1_stage3__wrac1_stage4__wrac1)) 
								(not (fired--trigger-change__wrac1_stage4__wrac1_stage1__wrac1)) 
								(not (fired--trigger-change__wrbc1_stage1__wrbc1_stage2__wrbc1)) 
								(not (fired--trigger-change__wrbc1_stage2__wrbc1_stage3__wrbc1)) 
								(not (fired--trigger-change__wrbc1_stage3__wrbc1_stage4__wrbc1)) 
								(not (fired--trigger-change__wrbc1_stage4__wrbc1_stage5__wrbc1)) 
								(not (fired--trigger-change__wrbc1_stage5__wrbc1_stage1__wrbc1)) 
								(not (fired--trigger-change__wrcc1_stage1__wrcc1_stage2__wrcc1)) 
								(not (fired--trigger-change__wrcc1_stage2__wrcc1_stage3__wrcc1)) 
								(not (fired--trigger-change__wrcc1_stage3__wrcc1_stage4__wrcc1)) 
								(not (fired--trigger-change__wrcc1_stage4__wrcc1_stage5__wrcc1)) 
								(not (fired--trigger-change__wrcc1_stage5__wrcc1_stage6__wrcc1)) 
								(not (fired--trigger-change__wrcc1_stage6__wrcc1_stage1__wrcc1)) 
								(not (fired--trigger-change__wrdc1_stage1__wrdc1_stage2__wrdc1)) 
								(not (fired--trigger-change__wrdc1_stage2__wrdc1_stage3__wrdc1)) 
								(not (fired--trigger-change__wrdc1_stage3__wrdc1_stage4__wrdc1)) 
								(not (fired--trigger-change__wrdc1_stage4__wrdc1_stage1__wrdc1)) 
								(not (fired--trigger-change__wrec1_stage1__wrec1_stage2__wrec1)) 
								(not (fired--trigger-change__wrec1_stage2__wrec1_stage3__wrec1)) 
								(not (fired--trigger-change__wrec1_stage3__wrec1_stage4__wrec1)) 
								(not (fired--trigger-change__wrec1_stage4__wrec1_stage1__wrec1)) 
								(not (fired--trigger-change__wrfc1_stage1__wrfc1_stage2__wrfc1)) 
								(not (fired--trigger-change__wrfc1_stage2__wrfc1_stage3__wrfc1)) 
								(not (fired--trigger-change__wrfc1_stage3__wrfc1_stage1__wrfc1)) ) )  
			
				(when 
					
						(and 
							(active__wrac1_stage1) 
								(>= 
									(greentime__wrac1) (defaultgreentime__wrac1_stage1) ) ) 
						(and 
							(trigger__wrac1) ) ) 
				(when 
					
						(and 
							(active__wrac1_stage2) 
								(>= 
									(greentime__wrac1) (defaultgreentime__wrac1_stage2) ) ) 
						(and 
							(trigger__wrac1) ) ) 
				(when 
					
						(and 
							(active__wrac1_stage3) 
								(>= 
									(greentime__wrac1) (defaultgreentime__wrac1_stage3) ) ) 
						(and 
							(trigger__wrac1) ) ) 
				(when 
					
						(and 
							(active__wrac1_stage4) 
								(>= 
									(greentime__wrac1) (defaultgreentime__wrac1_stage4) ) ) 
						(and 
							(trigger__wrac1) ) ) 
				(when 
					
						(and 
							(active__wrbc1_stage1) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage1) ) ) 
						(and 
							(trigger__wrbc1) ) ) 
				(when 
					
						(and 
							(active__wrbc1_stage2) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage2) ) ) 
						(and 
							(trigger__wrbc1) ) ) 
				(when 
					
						(and 
							(active__wrbc1_stage3) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage3) ) ) 
						(and 
							(trigger__wrbc1) ) ) 
				(when 
					
						(and 
							(active__wrbc1_stage4) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage4) ) ) 
						(and 
							(trigger__wrbc1) ) ) 
				(when 
					
						(and 
							(active__wrbc1_stage5) 
								(>= 
									(greentime__wrbc1) (defaultgreentime__wrbc1_stage5) ) ) 
						(and 
							(trigger__wrbc1) ) ) 
				(when 
					
						(and 
							(active__wrcc1_stage1) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage1) ) ) 
						(and 
							(trigger__wrcc1) ) ) 
				(when 
					
						(and 
							(active__wrcc1_stage2) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage2) ) ) 
						(and 
							(trigger__wrcc1) ) ) 
				(when 
					
						(and 
							(active__wrcc1_stage3) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage3) ) ) 
						(and 
							(trigger__wrcc1) ) ) 
				(when 
					
						(and 
							(active__wrcc1_stage4) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage4) ) ) 
						(and 
							(trigger__wrcc1) ) ) 
				(when 
					
						(and 
							(active__wrcc1_stage5) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage5) ) ) 
						(and 
							(trigger__wrcc1) ) ) 
				(when 
					
						(and 
							(active__wrcc1_stage6) 
								(>= 
									(greentime__wrcc1) (defaultgreentime__wrcc1_stage6) ) ) 
						(and 
							(trigger__wrcc1) ) ) 
				(when 
					
						(and 
							(active__wrdc1_stage1) 
								(>= 
									(greentime__wrdc1) (defaultgreentime__wrdc1_stage1) ) ) 
						(and 
							(trigger__wrdc1) ) ) 
				(when 
					
						(and 
							(active__wrdc1_stage2) 
								(>= 
									(greentime__wrdc1) (defaultgreentime__wrdc1_stage2) ) ) 
						(and 
							(trigger__wrdc1) ) ) 
				(when 
					
						(and 
							(active__wrdc1_stage3) 
								(>= 
									(greentime__wrdc1) (defaultgreentime__wrdc1_stage3) ) ) 
						(and 
							(trigger__wrdc1) ) ) 
				(when 
					
						(and 
							(active__wrdc1_stage4) 
								(>= 
									(greentime__wrdc1) (defaultgreentime__wrdc1_stage4) ) ) 
						(and 
							(trigger__wrdc1) ) ) 
				(when 
					
						(and 
							(active__wrec1_stage1) 
								(>= 
									(greentime__wrec1) (defaultgreentime__wrec1_stage1) ) ) 
						(and 
							(trigger__wrec1) ) ) 
				(when 
					
						(and 
							(active__wrec1_stage2) 
								(>= 
									(greentime__wrec1) (defaultgreentime__wrec1_stage2) ) ) 
						(and 
							(trigger__wrec1) ) ) 
				(when 
					
						(and 
							(active__wrec1_stage3) 
								(>= 
									(greentime__wrec1) (defaultgreentime__wrec1_stage3) ) ) 
						(and 
							(trigger__wrec1) ) ) 
				(when 
					
						(and 
							(active__wrec1_stage4) 
								(>= 
									(greentime__wrec1) (defaultgreentime__wrec1_stage4) ) ) 
						(and 
							(trigger__wrec1) ) ) 
				(when 
					
						(and 
							(active__wrfc1_stage1) 
								(>= 
									(greentime__wrfc1) (defaultgreentime__wrfc1_stage1) ) ) 
						(and 
							(trigger__wrfc1) ) ) 
				(when 
					
						(and 
							(active__wrfc1_stage2) 
								(>= 
									(greentime__wrfc1) (defaultgreentime__wrfc1_stage2) ) ) 
						(and 
							(trigger__wrfc1) ) ) 
				(when 
					
						(and 
							(active__wrfc1_stage3) 
								(>= 
									(greentime__wrfc1) (defaultgreentime__wrfc1_stage3) ) ) 
						(and 
							(trigger__wrfc1) ) ) 
				(when 
					
						(and 
							(trigger__wrac1) (active__wrac1_stage1) ) 
						(and 
							
								(not 
									(trigger__wrac1) ) 
								(not 
									(active__wrac1_stage1) ) (inter__wrac1_stage1) 
								(assign 
									(greentime__wrac1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrac1) (active__wrac1_stage2) ) 
						(and 
							
								(not 
									(trigger__wrac1) ) 
								(not 
									(active__wrac1_stage2) ) (inter__wrac1_stage2) 
								(assign 
									(greentime__wrac1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrac1) (active__wrac1_stage3) ) 
						(and 
							
								(not 
									(trigger__wrac1) ) 
								(not 
									(active__wrac1_stage3) ) (inter__wrac1_stage3) 
								(assign 
									(greentime__wrac1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrac1) (active__wrac1_stage4) ) 
						(and 
							
								(not 
									(trigger__wrac1) ) 
								(not 
									(active__wrac1_stage4) ) (inter__wrac1_stage4) 
								(assign 
									(greentime__wrac1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage1) ) 
						(and 
							
								(not 
									(trigger__wrbc1) ) 
								(not 
									(active__wrbc1_stage1) ) (inter__wrbc1_stage1) 
								(assign 
									(greentime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage2) ) 
						(and 
							
								(not 
									(trigger__wrbc1) ) 
								(not 
									(active__wrbc1_stage2) ) (inter__wrbc1_stage2) 
								(assign 
									(greentime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage3) ) 
						(and 
							
								(not 
									(trigger__wrbc1) ) 
								(not 
									(active__wrbc1_stage3) ) (inter__wrbc1_stage3) 
								(assign 
									(greentime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage4) ) 
						(and 
							
								(not 
									(trigger__wrbc1) ) 
								(not 
									(active__wrbc1_stage4) ) (inter__wrbc1_stage4) 
								(assign 
									(greentime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrbc1) (active__wrbc1_stage5) ) 
						(and 
							
								(not 
									(trigger__wrbc1) ) 
								(not 
									(active__wrbc1_stage5) ) (inter__wrbc1_stage5) 
								(assign 
									(greentime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage1) ) 
						(and 
							
								(not 
									(trigger__wrcc1) ) 
								(not 
									(active__wrcc1_stage1) ) (inter__wrcc1_stage1) 
								(assign 
									(greentime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage2) ) 
						(and 
							
								(not 
									(trigger__wrcc1) ) 
								(not 
									(active__wrcc1_stage2) ) (inter__wrcc1_stage2) 
								(assign 
									(greentime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage3) ) 
						(and 
							
								(not 
									(trigger__wrcc1) ) 
								(not 
									(active__wrcc1_stage3) ) (inter__wrcc1_stage3) 
								(assign 
									(greentime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage4) ) 
						(and 
							
								(not 
									(trigger__wrcc1) ) 
								(not 
									(active__wrcc1_stage4) ) (inter__wrcc1_stage4) 
								(assign 
									(greentime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage5) ) 
						(and 
							
								(not 
									(trigger__wrcc1) ) 
								(not 
									(active__wrcc1_stage5) ) (inter__wrcc1_stage5) 
								(assign 
									(greentime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrcc1) (active__wrcc1_stage6) ) 
						(and 
							
								(not 
									(trigger__wrcc1) ) 
								(not 
									(active__wrcc1_stage6) ) (inter__wrcc1_stage6) 
								(assign 
									(greentime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrdc1) (active__wrdc1_stage1) ) 
						(and 
							
								(not 
									(trigger__wrdc1) ) 
								(not 
									(active__wrdc1_stage1) ) (inter__wrdc1_stage1) 
								(assign 
									(greentime__wrdc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrdc1) (active__wrdc1_stage2) ) 
						(and 
							
								(not 
									(trigger__wrdc1) ) 
								(not 
									(active__wrdc1_stage2) ) (inter__wrdc1_stage2) 
								(assign 
									(greentime__wrdc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrdc1) (active__wrdc1_stage3) ) 
						(and 
							
								(not 
									(trigger__wrdc1) ) 
								(not 
									(active__wrdc1_stage3) ) (inter__wrdc1_stage3) 
								(assign 
									(greentime__wrdc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrdc1) (active__wrdc1_stage4) ) 
						(and 
							
								(not 
									(trigger__wrdc1) ) 
								(not 
									(active__wrdc1_stage4) ) (inter__wrdc1_stage4) 
								(assign 
									(greentime__wrdc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrec1) (active__wrec1_stage1) ) 
						(and 
							
								(not 
									(trigger__wrec1) ) 
								(not 
									(active__wrec1_stage1) ) (inter__wrec1_stage1) 
								(assign 
									(greentime__wrec1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrec1) (active__wrec1_stage2) ) 
						(and 
							
								(not 
									(trigger__wrec1) ) 
								(not 
									(active__wrec1_stage2) ) (inter__wrec1_stage2) 
								(assign 
									(greentime__wrec1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrec1) (active__wrec1_stage3) ) 
						(and 
							
								(not 
									(trigger__wrec1) ) 
								(not 
									(active__wrec1_stage3) ) (inter__wrec1_stage3) 
								(assign 
									(greentime__wrec1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrec1) (active__wrec1_stage4) ) 
						(and 
							
								(not 
									(trigger__wrec1) ) 
								(not 
									(active__wrec1_stage4) ) (inter__wrec1_stage4) 
								(assign 
									(greentime__wrec1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrfc1) (active__wrfc1_stage1) ) 
						(and 
							
								(not 
									(trigger__wrfc1) ) 
								(not 
									(active__wrfc1_stage1) ) (inter__wrfc1_stage1) 
								(assign 
									(greentime__wrfc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrfc1) (active__wrfc1_stage2) ) 
						(and 
							
								(not 
									(trigger__wrfc1) ) 
								(not 
									(active__wrfc1_stage2) ) (inter__wrfc1_stage2) 
								(assign 
									(greentime__wrfc1) 0 ) ) ) 
				(when 
					
						(and 
							(trigger__wrfc1) (active__wrfc1_stage3) ) 
						(and 
							
								(not 
									(trigger__wrfc1) ) 
								(not 
									(active__wrfc1_stage3) ) (inter__wrfc1_stage3) 
								(assign 
									(greentime__wrfc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrac1_stage1) 
								(>= 
									(intertime__wrac1) 
										(- 
											5 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrac1_stage1) ) (active__wrac1_stage2) 
								(assign 
									(intertime__wrac1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrac1_stage2) 
								(>= 
									(intertime__wrac1) 
										(- 
											6 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrac1_stage2) ) (active__wrac1_stage3) 
								(assign 
									(intertime__wrac1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrac1_stage3) 
								(>= 
									(intertime__wrac1) 
										(- 
											5 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrac1_stage3) ) (active__wrac1_stage4) 
								(assign 
									(intertime__wrac1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrac1_stage4) 
								(>= 
									(intertime__wrac1) 
										(- 
											5 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrac1_stage4) ) (active__wrac1_stage1) 
								(assign 
									(intertime__wrac1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage1) 
								(>= 
									(intertime__wrbc1) 
										(- 
											6 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrbc1_stage1) ) (active__wrbc1_stage2) 
								(assign 
									(intertime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage2) 
								(>= 
									(intertime__wrbc1) 
										(- 
											12 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrbc1_stage2) ) (active__wrbc1_stage3) 
								(assign 
									(intertime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage3) 
								(>= 
									(intertime__wrbc1) 
										(- 
											6 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrbc1_stage3) ) (active__wrbc1_stage4) 
								(assign 
									(intertime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage4) 
								(>= 
									(intertime__wrbc1) 
										(- 
											7 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrbc1_stage4) ) (active__wrbc1_stage5) 
								(assign 
									(intertime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrbc1_stage5) 
								(>= 
									(intertime__wrbc1) 
										(- 
											12 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrbc1_stage5) ) (active__wrbc1_stage1) 
								(assign 
									(intertime__wrbc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage1) 
								(>= 
									(intertime__wrcc1) 
										(- 
											6 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrcc1_stage1) ) (active__wrcc1_stage2) 
								(assign 
									(intertime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage2) 
								(>= 
									(intertime__wrcc1) 
										(- 
											8 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrcc1_stage2) ) (active__wrcc1_stage3) 
								(assign 
									(intertime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage3) 
								(>= 
									(intertime__wrcc1) 
										(- 
											8 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrcc1_stage3) ) (active__wrcc1_stage4) 
								(assign 
									(intertime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage4) 
								(>= 
									(intertime__wrcc1) 
										(- 
											11 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrcc1_stage4) ) (active__wrcc1_stage5) 
								(assign 
									(intertime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage5) 
								(>= 
									(intertime__wrcc1) 
										(- 
											3 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrcc1_stage5) ) (active__wrcc1_stage6) 
								(assign 
									(intertime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrcc1_stage6) 
								(>= 
									(intertime__wrcc1) 
										(- 
											2 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrcc1_stage6) ) (active__wrcc1_stage1) 
								(assign 
									(intertime__wrcc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrdc1_stage1) 
								(>= 
									(intertime__wrdc1) 
										(- 
											8 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrdc1_stage1) ) (active__wrdc1_stage2) 
								(assign 
									(intertime__wrdc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrdc1_stage2) 
								(>= 
									(intertime__wrdc1) 
										(- 
											8 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrdc1_stage2) ) (active__wrdc1_stage3) 
								(assign 
									(intertime__wrdc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrdc1_stage3) 
								(>= 
									(intertime__wrdc1) 
										(- 
											6 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrdc1_stage3) ) (active__wrdc1_stage4) 
								(assign 
									(intertime__wrdc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrdc1_stage4) 
								(>= 
									(intertime__wrdc1) 
										(- 
											7 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrdc1_stage4) ) (active__wrdc1_stage1) 
								(assign 
									(intertime__wrdc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrec1_stage1) 
								(>= 
									(intertime__wrec1) 
										(- 
											3 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrec1_stage1) ) (active__wrec1_stage2) 
								(assign 
									(intertime__wrec1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrec1_stage2) 
								(>= 
									(intertime__wrec1) 
										(- 
											7 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrec1_stage2) ) (active__wrec1_stage3) 
								(assign 
									(intertime__wrec1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrec1_stage3) 
								(>= 
									(intertime__wrec1) 
										(- 
											7 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrec1_stage3) ) (active__wrec1_stage4) 
								(assign 
									(intertime__wrec1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrec1_stage4) 
								(>= 
									(intertime__wrec1) 
										(- 
											7 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrec1_stage4) ) (active__wrec1_stage1) 
								(assign 
									(intertime__wrec1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrfc1_stage1) 
								(>= 
									(intertime__wrfc1) 
										(- 
											5 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrfc1_stage1) ) (active__wrfc1_stage2) 
								(assign 
									(intertime__wrfc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrfc1_stage2) 
								(>= 
									(intertime__wrfc1) 
										(- 
											9 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrfc1_stage2) ) (active__wrfc1_stage3) 
								(assign 
									(intertime__wrfc1) 0 ) ) ) 
				(when 
					
						(and 
							(inter__wrfc1_stage3) 
								(>= 
									(intertime__wrfc1) 
										(- 
											8 0.1 ) ) ) 
						(and 
							
								(not 
									(inter__wrfc1_stage3) ) (active__wrfc1_stage1) 
								(assign 
									(intertime__wrfc1) 0 ) ) )  )
)

(:action start-simulate-processes
 :parameters()
 :precondition 
	(and 
		
			(not (pause)) 
			(not (force-events)) )
 :effect 
	(and 
		
			(assign 
				(copy--defaultgreentime__wrac1_stage1) (defaultgreentime__wrac1_stage1) ) 
			(assign 
				(copy--defaultgreentime__wrac1_stage2) (defaultgreentime__wrac1_stage2) ) 
			(assign 
				(copy--defaultgreentime__wrac1_stage3) (defaultgreentime__wrac1_stage3) ) 
			(assign 
				(copy--defaultgreentime__wrac1_stage4) (defaultgreentime__wrac1_stage4) ) 
			(assign 
				(copy--defaultgreentime__wrbc1_stage1) (defaultgreentime__wrbc1_stage1) ) 
			(assign 
				(copy--defaultgreentime__wrbc1_stage2) (defaultgreentime__wrbc1_stage2) ) 
			(assign 
				(copy--defaultgreentime__wrbc1_stage3) (defaultgreentime__wrbc1_stage3) ) 
			(assign 
				(copy--defaultgreentime__wrbc1_stage4) (defaultgreentime__wrbc1_stage4) ) 
			(assign 
				(copy--defaultgreentime__wrbc1_stage5) (defaultgreentime__wrbc1_stage5) ) 
			(assign 
				(copy--defaultgreentime__wrcc1_stage1) (defaultgreentime__wrcc1_stage1) ) 
			(assign 
				(copy--defaultgreentime__wrcc1_stage2) (defaultgreentime__wrcc1_stage2) ) 
			(assign 
				(copy--defaultgreentime__wrcc1_stage3) (defaultgreentime__wrcc1_stage3) ) 
			(assign 
				(copy--defaultgreentime__wrcc1_stage4) (defaultgreentime__wrcc1_stage4) ) 
			(assign 
				(copy--defaultgreentime__wrcc1_stage5) (defaultgreentime__wrcc1_stage5) ) 
			(assign 
				(copy--defaultgreentime__wrcc1_stage6) (defaultgreentime__wrcc1_stage6) ) 
			(assign 
				(copy--defaultgreentime__wrdc1_stage1) (defaultgreentime__wrdc1_stage1) ) 
			(assign 
				(copy--defaultgreentime__wrdc1_stage2) (defaultgreentime__wrdc1_stage2) ) 
			(assign 
				(copy--defaultgreentime__wrdc1_stage3) (defaultgreentime__wrdc1_stage3) ) 
			(assign 
				(copy--defaultgreentime__wrdc1_stage4) (defaultgreentime__wrdc1_stage4) ) 
			(assign 
				(copy--defaultgreentime__wrec1_stage1) (defaultgreentime__wrec1_stage1) ) 
			(assign 
				(copy--defaultgreentime__wrec1_stage2) (defaultgreentime__wrec1_stage2) ) 
			(assign 
				(copy--defaultgreentime__wrec1_stage3) (defaultgreentime__wrec1_stage3) ) 
			(assign 
				(copy--defaultgreentime__wrec1_stage4) (defaultgreentime__wrec1_stage4) ) 
			(assign 
				(copy--defaultgreentime__wrfc1_stage1) (defaultgreentime__wrfc1_stage1) ) 
			(assign 
				(copy--defaultgreentime__wrfc1_stage2) (defaultgreentime__wrfc1_stage2) ) 
			(assign 
				(copy--defaultgreentime__wrfc1_stage3) (defaultgreentime__wrfc1_stage3) ) 
			(assign 
				(copy--counter__abnor_v_wrdc1) (counter__abnor_v_wrdc1) ) 
			(assign 
				(copy--counter__broad_x_wrfc1) (counter__broad_x_wrfc1) ) 
			(assign 
				(copy--counter__firth_d_wrac1) (counter__firth_d_wrac1) ) 
			(assign 
				(copy--counter__hsac3_c_wrac1) (counter__hsac3_c_wrac1) ) 
			(assign 
				(copy--counter__oldwa_c_wrec1) (counter__oldwa_c_wrec1) ) 
			(assign 
				(copy--counter__oldwa_d_wrec1) (counter__oldwa_d_wrec1) ) 
			(assign 
				(copy--counter__silve_w_wrbc1) (counter__silve_w_wrbc1) ) 
			(assign 
				(copy--counter__smith_c_wrcc1) (counter__smith_c_wrcc1) ) 
			(assign 
				(copy--counter__somer_v_wrbc1) (counter__somer_v_wrbc1) ) 
			(assign 
				(copy--counter__stand_f_wrac1) (counter__stand_f_wrac1) ) 
			(assign 
				(copy--counter__wakef_z_wrfc1) (counter__wakef_z_wrfc1) ) 
			(assign 
				(copy--counter__wrac1_m_stand) (counter__wrac1_m_stand) ) 
			(assign 
				(copy--counter__wrac1_n_firth) (counter__wrac1_n_firth) ) 
			(assign 
				(copy--counter__wrac1_x_wrbc1) (counter__wrac1_x_wrbc1) ) 
			(assign 
				(copy--counter__wrac1_y_wrbc1) (counter__wrac1_y_wrbc1) ) 
			(assign 
				(copy--counter__wrac1_z_hsac1) (counter__wrac1_z_hsac1) ) 
			(assign 
				(copy--counter__wrbc1_a_wrac1) (counter__wrbc1_a_wrac1) ) 
			(assign 
				(copy--counter__wrbc1_b_wrac1) (counter__wrbc1_b_wrac1) ) 
			(assign 
				(copy--counter__wrbc1_b_wrcc1) (counter__wrbc1_b_wrcc1) ) 
			(assign 
				(copy--counter__wrbc1_r_silve) (counter__wrbc1_r_silve) ) 
			(assign 
				(copy--counter__wrbc1_s_somer) (counter__wrbc1_s_somer) ) 
			(assign 
				(copy--counter__wrcc1_w_wrdc1) (counter__wrcc1_w_wrdc1) ) 
			(assign 
				(copy--counter__wrcc1_x_wrdc1) (counter__wrcc1_x_wrdc1) ) 
			(assign 
				(copy--counter__wrcc1_z_wrbc1) (counter__wrcc1_z_wrbc1) ) 
			(assign 
				(copy--counter__wrdc1_a_wrcc1) (counter__wrdc1_a_wrcc1) ) 
			(assign 
				(copy--counter__wrdc1_b_wrec1) (counter__wrdc1_b_wrec1) ) 
			(assign 
				(copy--counter__wrdc1_l_absou) (counter__wrdc1_l_absou) ) 
			(assign 
				(copy--counter__wrdc1_q_abnor) (counter__wrdc1_q_abnor) ) 
			(assign 
				(copy--counter__wrec1_y_wrdc1) (counter__wrec1_y_wrdc1) ) 
			(assign 
				(copy--counter__wrec1_y_wrfc1) (counter__wrec1_y_wrfc1) ) 
			(assign 
				(copy--counter__wrec1_z_wrdc1) (counter__wrec1_z_wrdc1) ) 
			(assign 
				(copy--counter__wrfc1_a_wrec1) (counter__wrfc1_a_wrec1) ) 
			(assign 
				(copy--counter__wrfc1_r_broad) (counter__wrfc1_r_broad) ) 
			(assign 
				(copy--counter__wrfc1_t_wakef) (counter__wrfc1_t_wakef) ) 
			(assign 
				(copy--occupancy__hsac3_c_wrac1) (occupancy__hsac3_c_wrac1) ) 
			(assign 
				(copy--occupancy__wrac1_y_wrbc1) (occupancy__wrac1_y_wrbc1) ) 
			(assign 
				(copy--occupancy__stand_f_wrac1) (occupancy__stand_f_wrac1) ) 
			(assign 
				(copy--occupancy__firth_d_wrac1) (occupancy__firth_d_wrac1) ) 
			(assign 
				(copy--occupancy__wrac1_x_wrbc1) (occupancy__wrac1_x_wrbc1) ) 
			(assign 
				(copy--occupancy__wrbc1_a_wrac1) (occupancy__wrbc1_a_wrac1) ) 
			(assign 
				(copy--occupancy__wrbc1_b_wrcc1) (occupancy__wrbc1_b_wrcc1) ) 
			(assign 
				(copy--occupancy__silve_w_wrbc1) (occupancy__silve_w_wrbc1) ) 
			(assign 
				(copy--occupancy__somer_v_wrbc1) (occupancy__somer_v_wrbc1) ) 
			(assign 
				(copy--occupancy__wrbc1_b_wrac1) (occupancy__wrbc1_b_wrac1) ) 
			(assign 
				(copy--occupancy__wrcc1_z_wrbc1) (occupancy__wrcc1_z_wrbc1) ) 
			(assign 
				(copy--occupancy__wrcc1_x_wrdc1) (occupancy__wrcc1_x_wrdc1) ) 
			(assign 
				(copy--occupancy__smith_c_wrcc1) (occupancy__smith_c_wrcc1) ) 
			(assign 
				(copy--occupancy__wrcc1_w_wrdc1) (occupancy__wrcc1_w_wrdc1) ) 
			(assign 
				(copy--occupancy__wrdc1_a_wrcc1) (occupancy__wrdc1_a_wrcc1) ) 
			(assign 
				(copy--occupancy__wrdc1_b_wrec1) (occupancy__wrdc1_b_wrec1) ) 
			(assign 
				(copy--occupancy__abnor_v_wrdc1) (occupancy__abnor_v_wrdc1) ) 
			(assign 
				(copy--occupancy__wrec1_z_wrdc1) (occupancy__wrec1_z_wrdc1) ) 
			(assign 
				(copy--occupancy__wrec1_y_wrfc1) (occupancy__wrec1_y_wrfc1) ) 
			(assign 
				(copy--occupancy__oldwa_c_wrec1) (occupancy__oldwa_c_wrec1) ) 
			(assign 
				(copy--occupancy__wrec1_y_wrdc1) (occupancy__wrec1_y_wrdc1) ) 
			(assign 
				(copy--occupancy__oldwa_d_wrec1) (occupancy__oldwa_d_wrec1) ) 
			(assign 
				(copy--occupancy__wakef_z_wrfc1) (occupancy__wakef_z_wrfc1) ) 
			(assign 
				(copy--occupancy__wrfc1_a_wrec1) (occupancy__wrfc1_a_wrec1) ) 
			(assign 
				(copy--occupancy__broad_x_wrfc1) (occupancy__broad_x_wrfc1) ) 
			(assign 
				(copy--occupancy__wrac1_z_hsac1) (occupancy__wrac1_z_hsac1) ) 
			(assign 
				(copy--occupancy__wrac1_m_stand) (occupancy__wrac1_m_stand) ) 
			(assign 
				(copy--occupancy__wrac1_n_firth) (occupancy__wrac1_n_firth) ) 
			(assign 
				(copy--occupancy__wrbc1_r_silve) (occupancy__wrbc1_r_silve) ) 
			(assign 
				(copy--occupancy__wrbc1_s_somer) (occupancy__wrbc1_s_somer) ) 
			(assign 
				(copy--occupancy__wrdc1_q_abnor) (occupancy__wrdc1_q_abnor) ) 
			(assign 
				(copy--occupancy__wrdc1_l_absou) (occupancy__wrdc1_l_absou) ) 
			(assign 
				(copy--occupancy__wrfc1_r_broad) (occupancy__wrfc1_r_broad) ) 
			(assign 
				(copy--occupancy__wrfc1_t_wakef) (occupancy__wrfc1_t_wakef) ) 
			(assign 
				(copy--occupancy__outside) (occupancy__outside) ) 
			(assign 
				(copy--greentime__wrac1) (greentime__wrac1) ) 
			(assign 
				(copy--intertime__wrac1) (intertime__wrac1) ) 
			(assign 
				(copy--greentime__wrbc1) (greentime__wrbc1) ) 
			(assign 
				(copy--intertime__wrbc1) (intertime__wrbc1) ) 
			(assign 
				(copy--greentime__wrcc1) (greentime__wrcc1) ) 
			(assign 
				(copy--intertime__wrcc1) (intertime__wrcc1) ) 
			(assign 
				(copy--greentime__wrdc1) (greentime__wrdc1) ) 
			(assign 
				(copy--intertime__wrdc1) (intertime__wrdc1) ) 
			(assign 
				(copy--greentime__wrec1) (greentime__wrec1) ) 
			(assign 
				(copy--intertime__wrec1) (intertime__wrec1) ) 
			(assign 
				(copy--greentime__wrfc1) (greentime__wrfc1) ) 
			(assign 
				(copy--intertime__wrfc1) (intertime__wrfc1) ) (pause) (done--0) 
			 )
)

(:action simulate--positive--keepgreen__wrac1_stage1__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(< 
						(copy--greentime__wrac1) (copy--defaultgreentime__wrac1_stage1) ) ) (done--0) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrac1) 
					(* 
						(delta) 1 ) ) 
			(not (done--0)) (done--1) )
)

(:action simulate--negative--keepgreen__wrac1_stage1__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(< 
							(copy--greentime__wrac1) (copy--defaultgreentime__wrac1_stage1) ) )) (done--0) )
 :effect 
	(and 
		
			(not (done--0)) (done--1) )
)

(:action simulate--positive--keepgreen__wrac1_stage2__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(< 
						(copy--greentime__wrac1) (copy--defaultgreentime__wrac1_stage2) ) ) (done--1) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrac1) 
					(* 
						(delta) 1 ) ) 
			(not (done--1)) (done--2) )
)

(:action simulate--negative--keepgreen__wrac1_stage2__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(< 
							(copy--greentime__wrac1) (copy--defaultgreentime__wrac1_stage2) ) )) (done--1) )
 :effect 
	(and 
		
			(not (done--1)) (done--2) )
)

(:action simulate--positive--keepgreen__wrac1_stage3__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(< 
						(copy--greentime__wrac1) (copy--defaultgreentime__wrac1_stage3) ) ) (done--2) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrac1) 
					(* 
						(delta) 1 ) ) 
			(not (done--2)) (done--3) )
)

(:action simulate--negative--keepgreen__wrac1_stage3__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(< 
							(copy--greentime__wrac1) (copy--defaultgreentime__wrac1_stage3) ) )) (done--2) )
 :effect 
	(and 
		
			(not (done--2)) (done--3) )
)

(:action simulate--positive--keepgreen__wrac1_stage4__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(< 
						(copy--greentime__wrac1) (copy--defaultgreentime__wrac1_stage4) ) ) (done--3) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrac1) 
					(* 
						(delta) 1 ) ) 
			(not (done--3)) (done--4) )
)

(:action simulate--negative--keepgreen__wrac1_stage4__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(< 
							(copy--greentime__wrac1) (copy--defaultgreentime__wrac1_stage4) ) )) (done--3) )
 :effect 
	(and 
		
			(not (done--3)) (done--4) )
)

(:action simulate--positive--keepgreen__wrbc1_stage1__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(< 
						(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage1) ) ) (done--4) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--4)) (done--5) )
)

(:action simulate--negative--keepgreen__wrbc1_stage1__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(< 
							(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage1) ) )) (done--4) )
 :effect 
	(and 
		
			(not (done--4)) (done--5) )
)

(:action simulate--positive--keepgreen__wrbc1_stage2__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(< 
						(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage2) ) ) (done--5) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--5)) (done--6) )
)

(:action simulate--negative--keepgreen__wrbc1_stage2__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(< 
							(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage2) ) )) (done--5) )
 :effect 
	(and 
		
			(not (done--5)) (done--6) )
)

(:action simulate--positive--keepgreen__wrbc1_stage3__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(< 
						(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage3) ) ) (done--6) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--6)) (done--7) )
)

(:action simulate--negative--keepgreen__wrbc1_stage3__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(< 
							(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage3) ) )) (done--6) )
 :effect 
	(and 
		
			(not (done--6)) (done--7) )
)

(:action simulate--positive--keepgreen__wrbc1_stage4__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(< 
						(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage4) ) ) (done--7) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--7)) (done--8) )
)

(:action simulate--negative--keepgreen__wrbc1_stage4__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(< 
							(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage4) ) )) (done--7) )
 :effect 
	(and 
		
			(not (done--7)) (done--8) )
)

(:action simulate--positive--keepgreen__wrbc1_stage5__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(< 
						(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage5) ) ) (done--8) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--8)) (done--9) )
)

(:action simulate--negative--keepgreen__wrbc1_stage5__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(< 
							(copy--greentime__wrbc1) (copy--defaultgreentime__wrbc1_stage5) ) )) (done--8) )
 :effect 
	(and 
		
			(not (done--8)) (done--9) )
)

(:action simulate--positive--keepgreen__wrcc1_stage1__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(< 
						(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage1) ) ) (done--9) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--9)) (done--10) )
)

(:action simulate--negative--keepgreen__wrcc1_stage1__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(< 
							(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage1) ) )) (done--9) )
 :effect 
	(and 
		
			(not (done--9)) (done--10) )
)

(:action simulate--positive--keepgreen__wrcc1_stage2__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage2) 
					(< 
						(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage2) ) ) (done--10) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--10)) (done--11) )
)

(:action simulate--negative--keepgreen__wrcc1_stage2__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage2) 
						(< 
							(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage2) ) )) (done--10) )
 :effect 
	(and 
		
			(not (done--10)) (done--11) )
)

(:action simulate--positive--keepgreen__wrcc1_stage3__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage3) 
					(< 
						(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage3) ) ) (done--11) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--11)) (done--12) )
)

(:action simulate--negative--keepgreen__wrcc1_stage3__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage3) 
						(< 
							(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage3) ) )) (done--11) )
 :effect 
	(and 
		
			(not (done--11)) (done--12) )
)

(:action simulate--positive--keepgreen__wrcc1_stage4__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(< 
						(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage4) ) ) (done--12) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--12)) (done--13) )
)

(:action simulate--negative--keepgreen__wrcc1_stage4__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(< 
							(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage4) ) )) (done--12) )
 :effect 
	(and 
		
			(not (done--12)) (done--13) )
)

(:action simulate--positive--keepgreen__wrcc1_stage5__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(< 
						(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage5) ) ) (done--13) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--13)) (done--14) )
)

(:action simulate--negative--keepgreen__wrcc1_stage5__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(< 
							(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage5) ) )) (done--13) )
 :effect 
	(and 
		
			(not (done--13)) (done--14) )
)

(:action simulate--positive--keepgreen__wrcc1_stage6__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage6) 
					(< 
						(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage6) ) ) (done--14) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--14)) (done--15) )
)

(:action simulate--negative--keepgreen__wrcc1_stage6__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage6) 
						(< 
							(copy--greentime__wrcc1) (copy--defaultgreentime__wrcc1_stage6) ) )) (done--14) )
 :effect 
	(and 
		
			(not (done--14)) (done--15) )
)

(:action simulate--positive--keepgreen__wrdc1_stage1__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(< 
						(copy--greentime__wrdc1) (copy--defaultgreentime__wrdc1_stage1) ) ) (done--15) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrdc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--15)) (done--16) )
)

(:action simulate--negative--keepgreen__wrdc1_stage1__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(< 
							(copy--greentime__wrdc1) (copy--defaultgreentime__wrdc1_stage1) ) )) (done--15) )
 :effect 
	(and 
		
			(not (done--15)) (done--16) )
)

(:action simulate--positive--keepgreen__wrdc1_stage2__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(< 
						(copy--greentime__wrdc1) (copy--defaultgreentime__wrdc1_stage2) ) ) (done--16) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrdc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--16)) (done--17) )
)

(:action simulate--negative--keepgreen__wrdc1_stage2__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(< 
							(copy--greentime__wrdc1) (copy--defaultgreentime__wrdc1_stage2) ) )) (done--16) )
 :effect 
	(and 
		
			(not (done--16)) (done--17) )
)

(:action simulate--positive--keepgreen__wrdc1_stage3__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(< 
						(copy--greentime__wrdc1) (copy--defaultgreentime__wrdc1_stage3) ) ) (done--17) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrdc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--17)) (done--18) )
)

(:action simulate--negative--keepgreen__wrdc1_stage3__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(< 
							(copy--greentime__wrdc1) (copy--defaultgreentime__wrdc1_stage3) ) )) (done--17) )
 :effect 
	(and 
		
			(not (done--17)) (done--18) )
)

(:action simulate--positive--keepgreen__wrdc1_stage4__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(< 
						(copy--greentime__wrdc1) (copy--defaultgreentime__wrdc1_stage4) ) ) (done--18) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrdc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--18)) (done--19) )
)

(:action simulate--negative--keepgreen__wrdc1_stage4__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(< 
							(copy--greentime__wrdc1) (copy--defaultgreentime__wrdc1_stage4) ) )) (done--18) )
 :effect 
	(and 
		
			(not (done--18)) (done--19) )
)

(:action simulate--positive--keepgreen__wrec1_stage1__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(< 
						(copy--greentime__wrec1) (copy--defaultgreentime__wrec1_stage1) ) ) (done--19) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrec1) 
					(* 
						(delta) 1 ) ) 
			(not (done--19)) (done--20) )
)

(:action simulate--negative--keepgreen__wrec1_stage1__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(< 
							(copy--greentime__wrec1) (copy--defaultgreentime__wrec1_stage1) ) )) (done--19) )
 :effect 
	(and 
		
			(not (done--19)) (done--20) )
)

(:action simulate--positive--keepgreen__wrec1_stage2__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage2) 
					(< 
						(copy--greentime__wrec1) (copy--defaultgreentime__wrec1_stage2) ) ) (done--20) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrec1) 
					(* 
						(delta) 1 ) ) 
			(not (done--20)) (done--21) )
)

(:action simulate--negative--keepgreen__wrec1_stage2__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage2) 
						(< 
							(copy--greentime__wrec1) (copy--defaultgreentime__wrec1_stage2) ) )) (done--20) )
 :effect 
	(and 
		
			(not (done--20)) (done--21) )
)

(:action simulate--positive--keepgreen__wrec1_stage3__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(< 
						(copy--greentime__wrec1) (copy--defaultgreentime__wrec1_stage3) ) ) (done--21) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrec1) 
					(* 
						(delta) 1 ) ) 
			(not (done--21)) (done--22) )
)

(:action simulate--negative--keepgreen__wrec1_stage3__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(< 
							(copy--greentime__wrec1) (copy--defaultgreentime__wrec1_stage3) ) )) (done--21) )
 :effect 
	(and 
		
			(not (done--21)) (done--22) )
)

(:action simulate--positive--keepgreen__wrec1_stage4__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(< 
						(copy--greentime__wrec1) (copy--defaultgreentime__wrec1_stage4) ) ) (done--22) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrec1) 
					(* 
						(delta) 1 ) ) 
			(not (done--22)) (done--23) )
)

(:action simulate--negative--keepgreen__wrec1_stage4__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(< 
							(copy--greentime__wrec1) (copy--defaultgreentime__wrec1_stage4) ) )) (done--22) )
 :effect 
	(and 
		
			(not (done--22)) (done--23) )
)

(:action simulate--positive--keepgreen__wrfc1_stage1__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(< 
						(copy--greentime__wrfc1) (copy--defaultgreentime__wrfc1_stage1) ) ) (done--23) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrfc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--23)) (done--24) )
)

(:action simulate--negative--keepgreen__wrfc1_stage1__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(< 
							(copy--greentime__wrfc1) (copy--defaultgreentime__wrfc1_stage1) ) )) (done--23) )
 :effect 
	(and 
		
			(not (done--23)) (done--24) )
)

(:action simulate--positive--keepgreen__wrfc1_stage2__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage2) 
					(< 
						(copy--greentime__wrfc1) (copy--defaultgreentime__wrfc1_stage2) ) ) (done--24) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrfc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--24)) (done--25) )
)

(:action simulate--negative--keepgreen__wrfc1_stage2__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage2) 
						(< 
							(copy--greentime__wrfc1) (copy--defaultgreentime__wrfc1_stage2) ) )) (done--24) )
 :effect 
	(and 
		
			(not (done--24)) (done--25) )
)

(:action simulate--positive--keepgreen__wrfc1_stage3__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage3) 
					(< 
						(copy--greentime__wrfc1) (copy--defaultgreentime__wrfc1_stage3) ) ) (done--25) )
 :effect 
	(and 
		
			(increase 
				(greentime__wrfc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--25)) (done--26) )
)

(:action simulate--negative--keepgreen__wrfc1_stage3__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage3) 
						(< 
							(copy--greentime__wrfc1) (copy--defaultgreentime__wrfc1_stage3) ) )) (done--25) )
 :effect 
	(and 
		
			(not (done--25)) (done--26) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
					(> 
						0.704 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--26) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.704 ) ) 
			(not (done--26)) (done--27) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
						(> 
							0.704 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--26) )
 :effect 
	(and 
		
			(not (done--26)) (done--27) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
					(> 
						0.704 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--27) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__hsac3_c_wrac1) 
					(* 
						(delta) 0.704 ) ) 
			(not (done--27)) (done--28) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
						(> 
							0.704 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--27) )
 :effect 
	(and 
		
			(not (done--27)) (done--28) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
					(> 
						0.704 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--28) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_y_wrbc1) 
					(* 
						(delta) 0.704 ) ) 
			(not (done--28)) (done--29) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
						(> 
							0.704 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--28) )
 :effect 
	(and 
		
			(not (done--28)) (done--29) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
					(> 
						0.178 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--29) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_m_stand) 
					(* 
						(delta) 0.178 ) ) 
			(not (done--29)) (done--30) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
						(> 
							0.178 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--29) )
 :effect 
	(and 
		
			(not (done--29)) (done--30) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
					(> 
						0.178 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--30) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__hsac3_c_wrac1) 
					(* 
						(delta) 0.178 ) ) 
			(not (done--30)) (done--31) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
						(> 
							0.178 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--30) )
 :effect 
	(and 
		
			(not (done--30)) (done--31) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
					(> 
						0.178 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--31) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_m_stand) 
					(* 
						(delta) 0.178 ) ) 
			(not (done--31)) (done--32) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
						(> 
							0.178 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--31) )
 :effect 
	(and 
		
			(not (done--31)) (done--32) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
					(> 
						0.294 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--32) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_x_wrbc1) 
					(* 
						(delta) 0.294 ) ) 
			(not (done--32)) (done--33) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
						(> 
							0.294 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--32) )
 :effect 
	(and 
		
			(not (done--32)) (done--33) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
					(> 
						0.294 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--33) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__hsac3_c_wrac1) 
					(* 
						(delta) 0.294 ) ) 
			(not (done--33)) (done--34) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
						(> 
							0.294 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--33) )
 :effect 
	(and 
		
			(not (done--33)) (done--34) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
					(> 
						0.294 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--34) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_x_wrbc1) 
					(* 
						(delta) 0.294 ) ) 
			(not (done--34)) (done--35) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__hsac3_c_wrac1) 0.0 ) 
						(> 
							0.294 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--34) )
 :effect 
	(and 
		
			(not (done--34)) (done--35) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.844 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--35) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_z_hsac1) 
					(* 
						(delta) 0.844 ) ) 
			(not (done--35)) (done--36) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.844 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--35) )
 :effect 
	(and 
		
			(not (done--35)) (done--36) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.844 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--36) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrbc1_a_wrac1) 
					(* 
						(delta) 0.844 ) ) 
			(not (done--36)) (done--37) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.844 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--36) )
 :effect 
	(and 
		
			(not (done--36)) (done--37) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.844 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--37) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_z_hsac1) 
					(* 
						(delta) 0.844 ) ) 
			(not (done--37)) (done--38) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.844 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--37) )
 :effect 
	(and 
		
			(not (done--37)) (done--38) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.097 0.0 ) 
					(< 
						(copy--occupancy__wrac1_n_firth) 100000.0 ) ) (done--38) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_n_firth) 
					(* 
						(delta) 0.097 ) ) 
			(not (done--38)) (done--39) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.097 0.0 ) 
						(< 
							(copy--occupancy__wrac1_n_firth) 100000.0 ) )) (done--38) )
 :effect 
	(and 
		
			(not (done--38)) (done--39) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.097 0.0 ) 
					(< 
						(copy--occupancy__wrac1_n_firth) 100000.0 ) ) (done--39) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrbc1_a_wrac1) 
					(* 
						(delta) 0.097 ) ) 
			(not (done--39)) (done--40) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.097 0.0 ) 
						(< 
							(copy--occupancy__wrac1_n_firth) 100000.0 ) )) (done--39) )
 :effect 
	(and 
		
			(not (done--39)) (done--40) )
)

(:action simulate--positive--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage1) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.097 0.0 ) 
					(< 
						(copy--occupancy__wrac1_n_firth) 100000.0 ) ) (done--40) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_n_firth) 
					(* 
						(delta) 0.097 ) ) 
			(not (done--40)) (done--41) )
)

(:action simulate--negative--flowrun_green__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage1) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.097 0.0 ) 
						(< 
							(copy--occupancy__wrac1_n_firth) 100000.0 ) )) (done--40) )
 :effect 
	(and 
		
			(not (done--40)) (done--41) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.372 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--41) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.372 ) ) 
			(not (done--41)) (done--42) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.372 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--41) )
 :effect 
	(and 
		
			(not (done--41)) (done--42) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.372 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--42) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__stand_f_wrac1) 
					(* 
						(delta) 0.372 ) ) 
			(not (done--42)) (done--43) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.372 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--42) )
 :effect 
	(and 
		
			(not (done--42)) (done--43) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.372 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--43) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_y_wrbc1) 
					(* 
						(delta) 0.372 ) ) 
			(not (done--43)) (done--44) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.372 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--43) )
 :effect 
	(and 
		
			(not (done--43)) (done--44) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.154 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--44) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_x_wrbc1) 
					(* 
						(delta) 0.154 ) ) 
			(not (done--44)) (done--45) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.154 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--44) )
 :effect 
	(and 
		
			(not (done--44)) (done--45) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.154 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--45) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__stand_f_wrac1) 
					(* 
						(delta) 0.154 ) ) 
			(not (done--45)) (done--46) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.154 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--45) )
 :effect 
	(and 
		
			(not (done--45)) (done--46) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.154 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--46) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_x_wrbc1) 
					(* 
						(delta) 0.154 ) ) 
			(not (done--46)) (done--47) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.154 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--46) )
 :effect 
	(and 
		
			(not (done--46)) (done--47) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.844 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--47) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_z_hsac1) 
					(* 
						(delta) 0.844 ) ) 
			(not (done--47)) (done--48) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.844 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--47) )
 :effect 
	(and 
		
			(not (done--47)) (done--48) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.844 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--48) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrbc1_a_wrac1) 
					(* 
						(delta) 0.844 ) ) 
			(not (done--48)) (done--49) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.844 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--48) )
 :effect 
	(and 
		
			(not (done--48)) (done--49) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.844 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--49) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_z_hsac1) 
					(* 
						(delta) 0.844 ) ) 
			(not (done--49)) (done--50) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.844 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--49) )
 :effect 
	(and 
		
			(not (done--49)) (done--50) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.097 0.0 ) 
					(< 
						(copy--occupancy__wrac1_n_firth) 100000.0 ) ) (done--50) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_n_firth) 
					(* 
						(delta) 0.097 ) ) 
			(not (done--50)) (done--51) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.097 0.0 ) 
						(< 
							(copy--occupancy__wrac1_n_firth) 100000.0 ) )) (done--50) )
 :effect 
	(and 
		
			(not (done--50)) (done--51) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.097 0.0 ) 
					(< 
						(copy--occupancy__wrac1_n_firth) 100000.0 ) ) (done--51) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrbc1_a_wrac1) 
					(* 
						(delta) 0.097 ) ) 
			(not (done--51)) (done--52) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.097 0.0 ) 
						(< 
							(copy--occupancy__wrac1_n_firth) 100000.0 ) )) (done--51) )
 :effect 
	(and 
		
			(not (done--51)) (done--52) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
					(> 
						0.097 0.0 ) 
					(< 
						(copy--occupancy__wrac1_n_firth) 100000.0 ) ) (done--52) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_n_firth) 
					(* 
						(delta) 0.097 ) ) 
			(not (done--52)) (done--53) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__wrbc1_a_wrac1) 0.0 ) 
						(> 
							0.097 0.0 ) 
						(< 
							(copy--occupancy__wrac1_n_firth) 100000.0 ) )) (done--52) )
 :effect 
	(and 
		
			(not (done--52)) (done--53) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__wrbc1_b_wrac1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--53) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_m_stand) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--53)) (done--54) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__wrbc1_b_wrac1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--53) )
 :effect 
	(and 
		
			(not (done--53)) (done--54) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__wrbc1_b_wrac1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--54) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrbc1_b_wrac1) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--54)) (done--55) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__wrbc1_b_wrac1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--54) )
 :effect 
	(and 
		
			(not (done--54)) (done--55) )
)

(:action simulate--positive--flowrun_green__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage2) 
					(> 
						(copy--occupancy__wrbc1_b_wrac1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--55) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_m_stand) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--55)) (done--56) )
)

(:action simulate--negative--flowrun_green__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage2) 
						(> 
							(copy--occupancy__wrbc1_b_wrac1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--55) )
 :effect 
	(and 
		
			(not (done--55)) (done--56) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.159 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--56) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.159 ) ) 
			(not (done--56)) (done--57) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.159 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--56) )
 :effect 
	(and 
		
			(not (done--56)) (done--57) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.159 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--57) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__stand_f_wrac1) 
					(* 
						(delta) 0.159 ) ) 
			(not (done--57)) (done--58) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.159 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--57) )
 :effect 
	(and 
		
			(not (done--57)) (done--58) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.159 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--58) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_y_wrbc1) 
					(* 
						(delta) 0.159 ) ) 
			(not (done--58)) (done--59) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.159 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--58) )
 :effect 
	(and 
		
			(not (done--58)) (done--59) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_n_firth--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.339 0.0 ) 
					(< 
						(copy--occupancy__wrac1_n_firth) 100000.0 ) ) (done--59) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_n_firth) 
					(* 
						(delta) 0.339 ) ) 
			(not (done--59)) (done--60) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_n_firth--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.339 0.0 ) 
						(< 
							(copy--occupancy__wrac1_n_firth) 100000.0 ) )) (done--59) )
 :effect 
	(and 
		
			(not (done--59)) (done--60) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_n_firth--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.339 0.0 ) 
					(< 
						(copy--occupancy__wrac1_n_firth) 100000.0 ) ) (done--60) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__stand_f_wrac1) 
					(* 
						(delta) 0.339 ) ) 
			(not (done--60)) (done--61) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_n_firth--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.339 0.0 ) 
						(< 
							(copy--occupancy__wrac1_n_firth) 100000.0 ) )) (done--60) )
 :effect 
	(and 
		
			(not (done--60)) (done--61) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_n_firth--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.339 0.0 ) 
					(< 
						(copy--occupancy__wrac1_n_firth) 100000.0 ) ) (done--61) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_n_firth) 
					(* 
						(delta) 0.339 ) ) 
			(not (done--61)) (done--62) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_n_firth--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.339 0.0 ) 
						(< 
							(copy--occupancy__wrac1_n_firth) 100000.0 ) )) (done--61) )
 :effect 
	(and 
		
			(not (done--61)) (done--62) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.089 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--62) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_x_wrbc1) 
					(* 
						(delta) 0.089 ) ) 
			(not (done--62)) (done--63) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.089 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--62) )
 :effect 
	(and 
		
			(not (done--62)) (done--63) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.089 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--63) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__stand_f_wrac1) 
					(* 
						(delta) 0.089 ) ) 
			(not (done--63)) (done--64) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.089 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--63) )
 :effect 
	(and 
		
			(not (done--63)) (done--64) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__stand_f_wrac1) 0.0 ) 
					(> 
						0.089 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--64) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_x_wrbc1) 
					(* 
						(delta) 0.089 ) ) 
			(not (done--64)) (done--65) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__stand_f_wrac1) 0.0 ) 
						(> 
							0.089 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--64) )
 :effect 
	(and 
		
			(not (done--64)) (done--65) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.118 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--65) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_z_hsac1) 
					(* 
						(delta) 0.118 ) ) 
			(not (done--65)) (done--66) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.118 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--65) )
 :effect 
	(and 
		
			(not (done--65)) (done--66) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.118 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--66) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__firth_d_wrac1) 
					(* 
						(delta) 0.118 ) ) 
			(not (done--66)) (done--67) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.118 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--66) )
 :effect 
	(and 
		
			(not (done--66)) (done--67) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.118 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--67) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_z_hsac1) 
					(* 
						(delta) 0.118 ) ) 
			(not (done--67)) (done--68) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.118 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--67) )
 :effect 
	(and 
		
			(not (done--67)) (done--68) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_m_stand--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.294 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--68) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_m_stand) 
					(* 
						(delta) 0.294 ) ) 
			(not (done--68)) (done--69) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_m_stand--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.294 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--68) )
 :effect 
	(and 
		
			(not (done--68)) (done--69) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_m_stand--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.294 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--69) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__firth_d_wrac1) 
					(* 
						(delta) 0.294 ) ) 
			(not (done--69)) (done--70) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_m_stand--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.294 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--69) )
 :effect 
	(and 
		
			(not (done--69)) (done--70) )
)

(:action simulate--positive--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_m_stand--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage3) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.294 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--70) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_m_stand) 
					(* 
						(delta) 0.294 ) ) 
			(not (done--70)) (done--71) )
)

(:action simulate--negative--flowrun_green__wrac1_stage3__firth_d_wrac1__wrac1_m_stand--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage3) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.294 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--70) )
 :effect 
	(and 
		
			(not (done--70)) (done--71) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.057 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--71) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_z_hsac1) 
					(* 
						(delta) 0.057 ) ) 
			(not (done--71)) (done--72) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.057 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--71) )
 :effect 
	(and 
		
			(not (done--71)) (done--72) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.057 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--72) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__firth_d_wrac1) 
					(* 
						(delta) 0.057 ) ) 
			(not (done--72)) (done--73) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.057 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--72) )
 :effect 
	(and 
		
			(not (done--72)) (done--73) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.057 0.0 ) 
					(< 
						(copy--occupancy__wrac1_z_hsac1) 100000.0 ) ) (done--73) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_z_hsac1) 
					(* 
						(delta) 0.057 ) ) 
			(not (done--73)) (done--74) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.057 0.0 ) 
						(< 
							(copy--occupancy__wrac1_z_hsac1) 100000.0 ) )) (done--73) )
 :effect 
	(and 
		
			(not (done--73)) (done--74) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.165 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--74) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.165 ) ) 
			(not (done--74)) (done--75) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.165 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--74) )
 :effect 
	(and 
		
			(not (done--74)) (done--75) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.165 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--75) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__firth_d_wrac1) 
					(* 
						(delta) 0.165 ) ) 
			(not (done--75)) (done--76) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.165 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--75) )
 :effect 
	(and 
		
			(not (done--75)) (done--76) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.165 0.0 ) 
					(< 
						(copy--occupancy__wrac1_y_wrbc1) 55.5 ) ) (done--76) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_y_wrbc1) 
					(* 
						(delta) 0.165 ) ) 
			(not (done--76)) (done--77) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.165 0.0 ) 
						(< 
							(copy--occupancy__wrac1_y_wrbc1) 55.5 ) )) (done--76) )
 :effect 
	(and 
		
			(not (done--76)) (done--77) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_m_stand--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.141 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--77) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_m_stand) 
					(* 
						(delta) 0.141 ) ) 
			(not (done--77)) (done--78) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_m_stand--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.141 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--77) )
 :effect 
	(and 
		
			(not (done--77)) (done--78) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_m_stand--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.141 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--78) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__firth_d_wrac1) 
					(* 
						(delta) 0.141 ) ) 
			(not (done--78)) (done--79) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_m_stand--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.141 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--78) )
 :effect 
	(and 
		
			(not (done--78)) (done--79) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_m_stand--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.141 0.0 ) 
					(< 
						(copy--occupancy__wrac1_m_stand) 100000.0 ) ) (done--79) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_m_stand) 
					(* 
						(delta) 0.141 ) ) 
			(not (done--79)) (done--80) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_m_stand--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.141 0.0 ) 
						(< 
							(copy--occupancy__wrac1_m_stand) 100000.0 ) )) (done--79) )
 :effect 
	(and 
		
			(not (done--79)) (done--80) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.049 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--80) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrac1_x_wrbc1) 
					(* 
						(delta) 0.049 ) ) 
			(not (done--80)) (done--81) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.049 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--80) )
 :effect 
	(and 
		
			(not (done--80)) (done--81) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.049 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--81) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__firth_d_wrac1) 
					(* 
						(delta) 0.049 ) ) 
			(not (done--81)) (done--82) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.049 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--81) )
 :effect 
	(and 
		
			(not (done--81)) (done--82) )
)

(:action simulate--positive--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrac1_stage4) 
					(> 
						(copy--occupancy__firth_d_wrac1) 0.0 ) 
					(> 
						0.049 0.0 ) 
					(< 
						(copy--occupancy__wrac1_x_wrbc1) 24.5 ) ) (done--82) )
 :effect 
	(and 
		
			(increase 
				(counter__wrac1_x_wrbc1) 
					(* 
						(delta) 0.049 ) ) 
			(not (done--82)) (done--83) )
)

(:action simulate--negative--flowrun_green__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrac1_stage4) 
						(> 
							(copy--occupancy__firth_d_wrac1) 0.0 ) 
						(> 
							0.049 0.0 ) 
						(< 
							(copy--occupancy__wrac1_x_wrbc1) 24.5 ) )) (done--82) )
 :effect 
	(and 
		
			(not (done--82)) (done--83) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.592 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--83) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.592 ) ) 
			(not (done--83)) (done--84) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.592 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--83) )
 :effect 
	(and 
		
			(not (done--83)) (done--84) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.592 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--84) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.592 ) ) 
			(not (done--84)) (done--85) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.592 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--84) )
 :effect 
	(and 
		
			(not (done--84)) (done--85) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.592 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--85) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.592 ) ) 
			(not (done--85)) (done--86) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.592 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--85) )
 :effect 
	(and 
		
			(not (done--85)) (done--86) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.114 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--86) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_r_silve) 
					(* 
						(delta) 0.114 ) ) 
			(not (done--86)) (done--87) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.114 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--86) )
 :effect 
	(and 
		
			(not (done--86)) (done--87) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.114 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--87) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.114 ) ) 
			(not (done--87)) (done--88) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.114 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--87) )
 :effect 
	(and 
		
			(not (done--87)) (done--88) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.114 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--88) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_r_silve) 
					(* 
						(delta) 0.114 ) ) 
			(not (done--88)) (done--89) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.114 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--88) )
 :effect 
	(and 
		
			(not (done--88)) (done--89) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.904 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--89) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_a_wrac1) 
					(* 
						(delta) 0.904 ) ) 
			(not (done--89)) (done--90) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.904 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--89) )
 :effect 
	(and 
		
			(not (done--89)) (done--90) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.904 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--90) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.904 ) ) 
			(not (done--90)) (done--91) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.904 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--90) )
 :effect 
	(and 
		
			(not (done--90)) (done--91) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.904 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--91) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_a_wrac1) 
					(* 
						(delta) 0.904 ) ) 
			(not (done--91)) (done--92) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.904 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--91) )
 :effect 
	(and 
		
			(not (done--91)) (done--92) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.026 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--92) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_r_silve) 
					(* 
						(delta) 0.026 ) ) 
			(not (done--92)) (done--93) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.026 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--92) )
 :effect 
	(and 
		
			(not (done--92)) (done--93) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.026 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--93) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.026 ) ) 
			(not (done--93)) (done--94) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.026 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--93) )
 :effect 
	(and 
		
			(not (done--93)) (done--94) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.026 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--94) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_r_silve) 
					(* 
						(delta) 0.026 ) ) 
			(not (done--94)) (done--95) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.026 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--94) )
 :effect 
	(and 
		
			(not (done--94)) (done--95) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.027 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--95) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_s_somer) 
					(* 
						(delta) 0.027 ) ) 
			(not (done--95)) (done--96) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.027 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--95) )
 :effect 
	(and 
		
			(not (done--95)) (done--96) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.027 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--96) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.027 ) ) 
			(not (done--96)) (done--97) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.027 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--96) )
 :effect 
	(and 
		
			(not (done--96)) (done--97) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.027 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--97) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_s_somer) 
					(* 
						(delta) 0.027 ) ) 
			(not (done--97)) (done--98) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.027 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--97) )
 :effect 
	(and 
		
			(not (done--97)) (done--98) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.22 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--98) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_b_wrac1) 
					(* 
						(delta) 0.22 ) ) 
			(not (done--98)) (done--99) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.22 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--98) )
 :effect 
	(and 
		
			(not (done--98)) (done--99) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.22 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--99) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.22 ) ) 
			(not (done--99)) (done--100) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.22 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--99) )
 :effect 
	(and 
		
			(not (done--99)) (done--100) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
					(> 
						0.22 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--100) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_b_wrac1) 
					(* 
						(delta) 0.22 ) ) 
			(not (done--100)) (done--101) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_z_wrbc1) 0.0 ) 
						(> 
							0.22 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--100) )
 :effect 
	(and 
		
			(not (done--100)) (done--101) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.182 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--101) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_a_wrac1) 
					(* 
						(delta) 0.182 ) ) 
			(not (done--101)) (done--102) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.182 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--101) )
 :effect 
	(and 
		
			(not (done--101)) (done--102) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.182 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--102) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__silve_w_wrbc1) 
					(* 
						(delta) 0.182 ) ) 
			(not (done--102)) (done--103) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.182 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--102) )
 :effect 
	(and 
		
			(not (done--102)) (done--103) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.182 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--103) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_a_wrac1) 
					(* 
						(delta) 0.182 ) ) 
			(not (done--103)) (done--104) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.182 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--103) )
 :effect 
	(and 
		
			(not (done--103)) (done--104) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.093 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--104) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.093 ) ) 
			(not (done--104)) (done--105) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.093 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--104) )
 :effect 
	(and 
		
			(not (done--104)) (done--105) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.093 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--105) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__silve_w_wrbc1) 
					(* 
						(delta) 0.093 ) ) 
			(not (done--105)) (done--106) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.093 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--105) )
 :effect 
	(and 
		
			(not (done--105)) (done--106) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.093 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--106) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.093 ) ) 
			(not (done--106)) (done--107) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.093 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--106) )
 :effect 
	(and 
		
			(not (done--106)) (done--107) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.07 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--107) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_s_somer) 
					(* 
						(delta) 0.07 ) ) 
			(not (done--107)) (done--108) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.07 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--107) )
 :effect 
	(and 
		
			(not (done--107)) (done--108) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.07 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--108) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__silve_w_wrbc1) 
					(* 
						(delta) 0.07 ) ) 
			(not (done--108)) (done--109) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.07 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--108) )
 :effect 
	(and 
		
			(not (done--108)) (done--109) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.07 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--109) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_s_somer) 
					(* 
						(delta) 0.07 ) ) 
			(not (done--109)) (done--110) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.07 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--109) )
 :effect 
	(and 
		
			(not (done--109)) (done--110) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.068 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--110) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_b_wrac1) 
					(* 
						(delta) 0.068 ) ) 
			(not (done--110)) (done--111) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.068 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--110) )
 :effect 
	(and 
		
			(not (done--110)) (done--111) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.068 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--111) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__silve_w_wrbc1) 
					(* 
						(delta) 0.068 ) ) 
			(not (done--111)) (done--112) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.068 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--111) )
 :effect 
	(and 
		
			(not (done--111)) (done--112) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage2) 
					(> 
						(copy--occupancy__silve_w_wrbc1) 0.0 ) 
					(> 
						0.068 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--112) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_b_wrac1) 
					(* 
						(delta) 0.068 ) ) 
			(not (done--112)) (done--113) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage2) 
						(> 
							(copy--occupancy__silve_w_wrbc1) 0.0 ) 
						(> 
							0.068 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--112) )
 :effect 
	(and 
		
			(not (done--112)) (done--113) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.592 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--113) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.592 ) ) 
			(not (done--113)) (done--114) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.592 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--113) )
 :effect 
	(and 
		
			(not (done--113)) (done--114) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.592 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--114) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.592 ) ) 
			(not (done--114)) (done--115) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.592 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--114) )
 :effect 
	(and 
		
			(not (done--114)) (done--115) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.592 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--115) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.592 ) ) 
			(not (done--115)) (done--116) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.592 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--115) )
 :effect 
	(and 
		
			(not (done--115)) (done--116) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.114 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--116) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_r_silve) 
					(* 
						(delta) 0.114 ) ) 
			(not (done--116)) (done--117) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.114 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--116) )
 :effect 
	(and 
		
			(not (done--116)) (done--117) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.114 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--117) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.114 ) ) 
			(not (done--117)) (done--118) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.114 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--117) )
 :effect 
	(and 
		
			(not (done--117)) (done--118) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.114 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--118) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_r_silve) 
					(* 
						(delta) 0.114 ) ) 
			(not (done--118)) (done--119) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.114 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--118) )
 :effect 
	(and 
		
			(not (done--118)) (done--119) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--119) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_s_somer) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--119)) (done--120) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--119) )
 :effect 
	(and 
		
			(not (done--119)) (done--120) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--120) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrac1_x_wrbc1) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--120)) (done--121) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--120) )
 :effect 
	(and 
		
			(not (done--120)) (done--121) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--121) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_s_somer) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--121)) (done--122) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--121) )
 :effect 
	(and 
		
			(not (done--121)) (done--122) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.478 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--122) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_a_wrac1) 
					(* 
						(delta) 0.478 ) ) 
			(not (done--122)) (done--123) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.478 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--122) )
 :effect 
	(and 
		
			(not (done--122)) (done--123) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.478 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--123) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__somer_v_wrbc1) 
					(* 
						(delta) 0.478 ) ) 
			(not (done--123)) (done--124) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.478 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--123) )
 :effect 
	(and 
		
			(not (done--123)) (done--124) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.478 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--124) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_a_wrac1) 
					(* 
						(delta) 0.478 ) ) 
			(not (done--124)) (done--125) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.478 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--124) )
 :effect 
	(and 
		
			(not (done--124)) (done--125) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.111 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--125) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_b_wrac1) 
					(* 
						(delta) 0.111 ) ) 
			(not (done--125)) (done--126) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.111 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--125) )
 :effect 
	(and 
		
			(not (done--125)) (done--126) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.111 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--126) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__somer_v_wrbc1) 
					(* 
						(delta) 0.111 ) ) 
			(not (done--126)) (done--127) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.111 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--126) )
 :effect 
	(and 
		
			(not (done--126)) (done--127) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage3) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.111 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--127) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_b_wrac1) 
					(* 
						(delta) 0.111 ) ) 
			(not (done--127)) (done--128) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage3) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.111 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--127) )
 :effect 
	(and 
		
			(not (done--127)) (done--128) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(> 
						(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--128) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_s_somer) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--128)) (done--129) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(> 
							(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--128) )
 :effect 
	(and 
		
			(not (done--128)) (done--129) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(> 
						(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--129) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrac1_x_wrbc1) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--129)) (done--130) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(> 
							(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--129) )
 :effect 
	(and 
		
			(not (done--129)) (done--130) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(> 
						(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--130) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_s_somer) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--130)) (done--131) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(> 
							(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--130) )
 :effect 
	(and 
		
			(not (done--130)) (done--131) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.478 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--131) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_a_wrac1) 
					(* 
						(delta) 0.478 ) ) 
			(not (done--131)) (done--132) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.478 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--131) )
 :effect 
	(and 
		
			(not (done--131)) (done--132) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.478 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--132) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__somer_v_wrbc1) 
					(* 
						(delta) 0.478 ) ) 
			(not (done--132)) (done--133) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.478 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--132) )
 :effect 
	(and 
		
			(not (done--132)) (done--133) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.478 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_a_wrac1) 85.0 ) ) (done--133) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_a_wrac1) 
					(* 
						(delta) 0.478 ) ) 
			(not (done--133)) (done--134) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.478 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_a_wrac1) 85.0 ) )) (done--133) )
 :effect 
	(and 
		
			(not (done--133)) (done--134) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.111 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--134) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_b_wrac1) 
					(* 
						(delta) 0.111 ) ) 
			(not (done--134)) (done--135) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.111 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--134) )
 :effect 
	(and 
		
			(not (done--134)) (done--135) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.111 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--135) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__somer_v_wrbc1) 
					(* 
						(delta) 0.111 ) ) 
			(not (done--135)) (done--136) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.111 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--135) )
 :effect 
	(and 
		
			(not (done--135)) (done--136) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage4) 
					(> 
						(copy--occupancy__somer_v_wrbc1) 0.0 ) 
					(> 
						0.111 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrac1) 12.0 ) ) (done--136) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_b_wrac1) 
					(* 
						(delta) 0.111 ) ) 
			(not (done--136)) (done--137) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage4) 
						(> 
							(copy--occupancy__somer_v_wrbc1) 0.0 ) 
						(> 
							0.111 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrac1) 12.0 ) )) (done--136) )
 :effect 
	(and 
		
			(not (done--136)) (done--137) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.592 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--137) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.592 ) ) 
			(not (done--137)) (done--138) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.592 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--137) )
 :effect 
	(and 
		
			(not (done--137)) (done--138) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.592 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--138) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.592 ) ) 
			(not (done--138)) (done--139) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.592 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--138) )
 :effect 
	(and 
		
			(not (done--138)) (done--139) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.592 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) ) (done--139) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.592 ) ) 
			(not (done--139)) (done--140) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.592 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_b_wrcc1) 30.66 ) )) (done--139) )
 :effect 
	(and 
		
			(not (done--139)) (done--140) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.114 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--140) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_r_silve) 
					(* 
						(delta) 0.114 ) ) 
			(not (done--140)) (done--141) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.114 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--140) )
 :effect 
	(and 
		
			(not (done--140)) (done--141) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.114 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--141) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrac1_y_wrbc1) 
					(* 
						(delta) 0.114 ) ) 
			(not (done--141)) (done--142) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.114 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--141) )
 :effect 
	(and 
		
			(not (done--141)) (done--142) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(> 
						(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
					(> 
						0.114 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_r_silve) 100000.0 ) ) (done--142) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_r_silve) 
					(* 
						(delta) 0.114 ) ) 
			(not (done--142)) (done--143) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(> 
							(copy--occupancy__wrac1_y_wrbc1) 0.0 ) 
						(> 
							0.114 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_r_silve) 100000.0 ) )) (done--142) )
 :effect 
	(and 
		
			(not (done--142)) (done--143) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(> 
						(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--143) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrbc1_s_somer) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--143)) (done--144) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(> 
							(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--143) )
 :effect 
	(and 
		
			(not (done--143)) (done--144) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(> 
						(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--144) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrac1_x_wrbc1) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--144)) (done--145) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(> 
							(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--144) )
 :effect 
	(and 
		
			(not (done--144)) (done--145) )
)

(:action simulate--positive--flowrun_green__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrbc1_stage5) 
					(> 
						(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
					(> 
						0.412 0.0 ) 
					(< 
						(copy--occupancy__wrbc1_s_somer) 100000.0 ) ) (done--145) )
 :effect 
	(and 
		
			(increase 
				(counter__wrbc1_s_somer) 
					(* 
						(delta) 0.412 ) ) 
			(not (done--145)) (done--146) )
)

(:action simulate--negative--flowrun_green__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrbc1_stage5) 
						(> 
							(copy--occupancy__wrac1_x_wrbc1) 0.0 ) 
						(> 
							0.412 0.0 ) 
						(< 
							(copy--occupancy__wrbc1_s_somer) 100000.0 ) )) (done--145) )
 :effect 
	(and 
		
			(not (done--145)) (done--146) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.467 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--146) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.467 ) ) 
			(not (done--146)) (done--147) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.467 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--146) )
 :effect 
	(and 
		
			(not (done--146)) (done--147) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.467 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--147) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.467 ) ) 
			(not (done--147)) (done--148) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.467 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--147) )
 :effect 
	(and 
		
			(not (done--147)) (done--148) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.467 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--148) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.467 ) ) 
			(not (done--148)) (done--149) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.467 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--148) )
 :effect 
	(and 
		
			(not (done--148)) (done--149) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.003 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--149) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_w_wrdc1) 
					(* 
						(delta) 0.003 ) ) 
			(not (done--149)) (done--150) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.003 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--149) )
 :effect 
	(and 
		
			(not (done--149)) (done--150) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.003 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--150) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.003 ) ) 
			(not (done--150)) (done--151) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.003 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--150) )
 :effect 
	(and 
		
			(not (done--150)) (done--151) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.003 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--151) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_w_wrdc1) 
					(* 
						(delta) 0.003 ) ) 
			(not (done--151)) (done--152) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.003 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--151) )
 :effect 
	(and 
		
			(not (done--151)) (done--152) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(> 
						(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--152) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--152)) (done--153) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(> 
							(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--152) )
 :effect 
	(and 
		
			(not (done--152)) (done--153) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(> 
						(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--153) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrdc1_a_wrcc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--153)) (done--154) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(> 
							(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--153) )
 :effect 
	(and 
		
			(not (done--153)) (done--154) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage1) 
					(> 
						(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--154) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--154)) (done--155) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage1) 
						(> 
							(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--154) )
 :effect 
	(and 
		
			(not (done--154)) (done--155) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage2) 
					(> 
						(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--155) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--155)) (done--156) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage2) 
						(> 
							(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--155) )
 :effect 
	(and 
		
			(not (done--155)) (done--156) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage2) 
					(> 
						(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--156) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrdc1_a_wrcc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--156)) (done--157) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage2) 
						(> 
							(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--156) )
 :effect 
	(and 
		
			(not (done--156)) (done--157) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage2) 
					(> 
						(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--157) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--157)) (done--158) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage2) 
						(> 
							(copy--occupancy__wrdc1_a_wrcc1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--157) )
 :effect 
	(and 
		
			(not (done--157)) (done--158) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage3) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.467 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--158) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.467 ) ) 
			(not (done--158)) (done--159) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage3) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.467 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--158) )
 :effect 
	(and 
		
			(not (done--158)) (done--159) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage3) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.467 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--159) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.467 ) ) 
			(not (done--159)) (done--160) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage3) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.467 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--159) )
 :effect 
	(and 
		
			(not (done--159)) (done--160) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage3) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.467 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--160) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.467 ) ) 
			(not (done--160)) (done--161) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage3) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.467 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--160) )
 :effect 
	(and 
		
			(not (done--160)) (done--161) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage3) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.003 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--161) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_w_wrdc1) 
					(* 
						(delta) 0.003 ) ) 
			(not (done--161)) (done--162) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage3) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.003 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--161) )
 :effect 
	(and 
		
			(not (done--161)) (done--162) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage3) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.003 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--162) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrbc1_b_wrcc1) 
					(* 
						(delta) 0.003 ) ) 
			(not (done--162)) (done--163) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage3) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.003 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--162) )
 :effect 
	(and 
		
			(not (done--162)) (done--163) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage3) 
					(> 
						(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
					(> 
						0.003 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--163) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_w_wrdc1) 
					(* 
						(delta) 0.003 ) ) 
			(not (done--163)) (done--164) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage3) 
						(> 
							(copy--occupancy__wrbc1_b_wrcc1) 0.0 ) 
						(> 
							0.003 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--163) )
 :effect 
	(and 
		
			(not (done--163)) (done--164) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--164) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--164)) (done--165) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--164) )
 :effect 
	(and 
		
			(not (done--164)) (done--165) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--165) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__smith_c_wrcc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--165)) (done--166) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--165) )
 :effect 
	(and 
		
			(not (done--165)) (done--166) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--166) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--166)) (done--167) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--166) )
 :effect 
	(and 
		
			(not (done--166)) (done--167) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.468 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--167) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.468 ) ) 
			(not (done--167)) (done--168) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.468 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--167) )
 :effect 
	(and 
		
			(not (done--167)) (done--168) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.468 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--168) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__smith_c_wrcc1) 
					(* 
						(delta) 0.468 ) ) 
			(not (done--168)) (done--169) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.468 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--168) )
 :effect 
	(and 
		
			(not (done--168)) (done--169) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.468 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--169) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.468 ) ) 
			(not (done--169)) (done--170) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.468 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--169) )
 :effect 
	(and 
		
			(not (done--169)) (done--170) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--170) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_w_wrdc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--170)) (done--171) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--170) )
 :effect 
	(and 
		
			(not (done--170)) (done--171) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--171) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__smith_c_wrcc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--171)) (done--172) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--171) )
 :effect 
	(and 
		
			(not (done--171)) (done--172) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage4) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--172) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_w_wrdc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--172)) (done--173) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage4) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--172) )
 :effect 
	(and 
		
			(not (done--172)) (done--173) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--173) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--173)) (done--174) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--173) )
 :effect 
	(and 
		
			(not (done--173)) (done--174) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--174) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__smith_c_wrcc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--174)) (done--175) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--174) )
 :effect 
	(and 
		
			(not (done--174)) (done--175) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) ) (done--175) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_z_wrbc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--175)) (done--176) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_z_wrbc1) 56.0 ) )) (done--175) )
 :effect 
	(and 
		
			(not (done--175)) (done--176) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.468 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--176) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.468 ) ) 
			(not (done--176)) (done--177) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.468 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--176) )
 :effect 
	(and 
		
			(not (done--176)) (done--177) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.468 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--177) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__smith_c_wrcc1) 
					(* 
						(delta) 0.468 ) ) 
			(not (done--177)) (done--178) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.468 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--177) )
 :effect 
	(and 
		
			(not (done--177)) (done--178) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.468 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) ) (done--178) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.468 ) ) 
			(not (done--178)) (done--179) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.468 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_x_wrdc1) 93.3 ) )) (done--178) )
 :effect 
	(and 
		
			(not (done--178)) (done--179) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--179) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrcc1_w_wrdc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--179)) (done--180) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--179) )
 :effect 
	(and 
		
			(not (done--179)) (done--180) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--180) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__smith_c_wrcc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--180)) (done--181) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--180) )
 :effect 
	(and 
		
			(not (done--180)) (done--181) )
)

(:action simulate--positive--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrcc1_stage5) 
					(> 
						(copy--occupancy__smith_c_wrcc1) 0.0 ) 
					(> 
						0.001 0.0 ) 
					(< 
						(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) ) (done--181) )
 :effect 
	(and 
		
			(increase 
				(counter__wrcc1_w_wrdc1) 
					(* 
						(delta) 0.001 ) ) 
			(not (done--181)) (done--182) )
)

(:action simulate--negative--flowrun_green__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrcc1_stage5) 
						(> 
							(copy--occupancy__smith_c_wrcc1) 0.0 ) 
						(> 
							0.001 0.0 ) 
						(< 
							(copy--occupancy__wrcc1_w_wrdc1) 5.0 ) )) (done--181) )
 :effect 
	(and 
		
			(not (done--181)) (done--182) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.821 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_b_wrec1) 26.67 ) ) (done--182) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_b_wrec1) 
					(* 
						(delta) 0.821 ) ) 
			(not (done--182)) (done--183) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.821 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_b_wrec1) 26.67 ) )) (done--182) )
 :effect 
	(and 
		
			(not (done--182)) (done--183) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.821 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_b_wrec1) 26.67 ) ) (done--183) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.821 ) ) 
			(not (done--183)) (done--184) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.821 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_b_wrec1) 26.67 ) )) (done--183) )
 :effect 
	(and 
		
			(not (done--183)) (done--184) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.821 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_b_wrec1) 26.67 ) ) (done--184) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_b_wrec1) 
					(* 
						(delta) 0.821 ) ) 
			(not (done--184)) (done--185) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.821 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_b_wrec1) 26.67 ) )) (done--184) )
 :effect 
	(and 
		
			(not (done--184)) (done--185) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.002 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--185) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_l_absou) 
					(* 
						(delta) 0.002 ) ) 
			(not (done--185)) (done--186) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.002 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--185) )
 :effect 
	(and 
		
			(not (done--185)) (done--186) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.002 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--186) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.002 ) ) 
			(not (done--186)) (done--187) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.002 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--186) )
 :effect 
	(and 
		
			(not (done--186)) (done--187) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.002 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--187) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_l_absou) 
					(* 
						(delta) 0.002 ) ) 
			(not (done--187)) (done--188) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.002 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--187) )
 :effect 
	(and 
		
			(not (done--187)) (done--188) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.867 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) ) (done--188) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_a_wrcc1) 
					(* 
						(delta) 0.867 ) ) 
			(not (done--188)) (done--189) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.867 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) )) (done--188) )
 :effect 
	(and 
		
			(not (done--188)) (done--189) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.867 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) ) (done--189) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrec1_z_wrdc1) 
					(* 
						(delta) 0.867 ) ) 
			(not (done--189)) (done--190) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.867 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) )) (done--189) )
 :effect 
	(and 
		
			(not (done--189)) (done--190) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.867 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) ) (done--190) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_a_wrcc1) 
					(* 
						(delta) 0.867 ) ) 
			(not (done--190)) (done--191) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.867 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) )) (done--190) )
 :effect 
	(and 
		
			(not (done--190)) (done--191) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.133 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_q_abnor) 100000.0 ) ) (done--191) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_q_abnor) 
					(* 
						(delta) 0.133 ) ) 
			(not (done--191)) (done--192) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.133 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_q_abnor) 100000.0 ) )) (done--191) )
 :effect 
	(and 
		
			(not (done--191)) (done--192) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.133 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_q_abnor) 100000.0 ) ) (done--192) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrec1_z_wrdc1) 
					(* 
						(delta) 0.133 ) ) 
			(not (done--192)) (done--193) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.133 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_q_abnor) 100000.0 ) )) (done--192) )
 :effect 
	(and 
		
			(not (done--192)) (done--193) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage1) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.133 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_q_abnor) 100000.0 ) ) (done--193) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_q_abnor) 
					(* 
						(delta) 0.133 ) ) 
			(not (done--193)) (done--194) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage1) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.133 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_q_abnor) 100000.0 ) )) (done--193) )
 :effect 
	(and 
		
			(not (done--193)) (done--194) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.867 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) ) (done--194) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_a_wrcc1) 
					(* 
						(delta) 0.867 ) ) 
			(not (done--194)) (done--195) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.867 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) )) (done--194) )
 :effect 
	(and 
		
			(not (done--194)) (done--195) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.867 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) ) (done--195) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrec1_z_wrdc1) 
					(* 
						(delta) 0.867 ) ) 
			(not (done--195)) (done--196) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.867 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) )) (done--195) )
 :effect 
	(and 
		
			(not (done--195)) (done--196) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.867 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) ) (done--196) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_a_wrcc1) 
					(* 
						(delta) 0.867 ) ) 
			(not (done--196)) (done--197) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.867 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) )) (done--196) )
 :effect 
	(and 
		
			(not (done--196)) (done--197) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.133 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_q_abnor) 100000.0 ) ) (done--197) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_q_abnor) 
					(* 
						(delta) 0.133 ) ) 
			(not (done--197)) (done--198) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.133 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_q_abnor) 100000.0 ) )) (done--197) )
 :effect 
	(and 
		
			(not (done--197)) (done--198) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.133 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_q_abnor) 100000.0 ) ) (done--198) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrec1_z_wrdc1) 
					(* 
						(delta) 0.133 ) ) 
			(not (done--198)) (done--199) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.133 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_q_abnor) 100000.0 ) )) (done--198) )
 :effect 
	(and 
		
			(not (done--198)) (done--199) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(> 
						(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
					(> 
						0.133 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_q_abnor) 100000.0 ) ) (done--199) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_q_abnor) 
					(* 
						(delta) 0.133 ) ) 
			(not (done--199)) (done--200) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(> 
							(copy--occupancy__wrec1_z_wrdc1) 0.0 ) 
						(> 
							0.133 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_q_abnor) 100000.0 ) )) (done--199) )
 :effect 
	(and 
		
			(not (done--199)) (done--200) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(> 
						(copy--occupancy__wrec1_y_wrdc1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--200) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_l_absou) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--200)) (done--201) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(> 
							(copy--occupancy__wrec1_y_wrdc1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--200) )
 :effect 
	(and 
		
			(not (done--200)) (done--201) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(> 
						(copy--occupancy__wrec1_y_wrdc1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--201) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrec1_y_wrdc1) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--201)) (done--202) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(> 
							(copy--occupancy__wrec1_y_wrdc1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--201) )
 :effect 
	(and 
		
			(not (done--201)) (done--202) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage2) 
					(> 
						(copy--occupancy__wrec1_y_wrdc1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--202) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_l_absou) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--202)) (done--203) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage2) 
						(> 
							(copy--occupancy__wrec1_y_wrdc1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--202) )
 :effect 
	(and 
		
			(not (done--202)) (done--203) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(> 
						(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
					(> 
						0.283 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) ) (done--203) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_a_wrcc1) 
					(* 
						(delta) 0.283 ) ) 
			(not (done--203)) (done--204) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(> 
							(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
						(> 
							0.283 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) )) (done--203) )
 :effect 
	(and 
		
			(not (done--203)) (done--204) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(> 
						(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
					(> 
						0.283 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) ) (done--204) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__abnor_v_wrdc1) 
					(* 
						(delta) 0.283 ) ) 
			(not (done--204)) (done--205) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(> 
							(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
						(> 
							0.283 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) )) (done--204) )
 :effect 
	(and 
		
			(not (done--204)) (done--205) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(> 
						(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
					(> 
						0.283 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) ) (done--205) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_a_wrcc1) 
					(* 
						(delta) 0.283 ) ) 
			(not (done--205)) (done--206) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(> 
							(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
						(> 
							0.283 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_a_wrcc1) 42.0 ) )) (done--205) )
 :effect 
	(and 
		
			(not (done--205)) (done--206) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(> 
						(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
					(> 
						0.215 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_b_wrec1) 26.67 ) ) (done--206) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_b_wrec1) 
					(* 
						(delta) 0.215 ) ) 
			(not (done--206)) (done--207) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(> 
							(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
						(> 
							0.215 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_b_wrec1) 26.67 ) )) (done--206) )
 :effect 
	(and 
		
			(not (done--206)) (done--207) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(> 
						(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
					(> 
						0.215 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_b_wrec1) 26.67 ) ) (done--207) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__abnor_v_wrdc1) 
					(* 
						(delta) 0.215 ) ) 
			(not (done--207)) (done--208) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(> 
							(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
						(> 
							0.215 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_b_wrec1) 26.67 ) )) (done--207) )
 :effect 
	(and 
		
			(not (done--207)) (done--208) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(> 
						(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
					(> 
						0.215 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_b_wrec1) 26.67 ) ) (done--208) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_b_wrec1) 
					(* 
						(delta) 0.215 ) ) 
			(not (done--208)) (done--209) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(> 
							(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
						(> 
							0.215 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_b_wrec1) 26.67 ) )) (done--208) )
 :effect 
	(and 
		
			(not (done--208)) (done--209) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(> 
						(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
					(> 
						0.03 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--209) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_l_absou) 
					(* 
						(delta) 0.03 ) ) 
			(not (done--209)) (done--210) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(> 
							(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
						(> 
							0.03 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--209) )
 :effect 
	(and 
		
			(not (done--209)) (done--210) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(> 
						(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
					(> 
						0.03 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--210) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__abnor_v_wrdc1) 
					(* 
						(delta) 0.03 ) ) 
			(not (done--210)) (done--211) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(> 
							(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
						(> 
							0.03 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--210) )
 :effect 
	(and 
		
			(not (done--210)) (done--211) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage3) 
					(> 
						(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
					(> 
						0.03 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--211) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_l_absou) 
					(* 
						(delta) 0.03 ) ) 
			(not (done--211)) (done--212) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage3) 
						(> 
							(copy--occupancy__abnor_v_wrdc1) 0.0 ) 
						(> 
							0.03 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--211) )
 :effect 
	(and 
		
			(not (done--211)) (done--212) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.821 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_b_wrec1) 26.67 ) ) (done--212) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_b_wrec1) 
					(* 
						(delta) 0.821 ) ) 
			(not (done--212)) (done--213) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.821 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_b_wrec1) 26.67 ) )) (done--212) )
 :effect 
	(and 
		
			(not (done--212)) (done--213) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.821 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_b_wrec1) 26.67 ) ) (done--213) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.821 ) ) 
			(not (done--213)) (done--214) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.821 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_b_wrec1) 26.67 ) )) (done--213) )
 :effect 
	(and 
		
			(not (done--213)) (done--214) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.821 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_b_wrec1) 26.67 ) ) (done--214) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_b_wrec1) 
					(* 
						(delta) 0.821 ) ) 
			(not (done--214)) (done--215) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.821 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_b_wrec1) 26.67 ) )) (done--214) )
 :effect 
	(and 
		
			(not (done--214)) (done--215) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.002 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--215) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_l_absou) 
					(* 
						(delta) 0.002 ) ) 
			(not (done--215)) (done--216) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.002 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--215) )
 :effect 
	(and 
		
			(not (done--215)) (done--216) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.002 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--216) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrcc1_x_wrdc1) 
					(* 
						(delta) 0.002 ) ) 
			(not (done--216)) (done--217) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.002 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--216) )
 :effect 
	(and 
		
			(not (done--216)) (done--217) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(> 
						(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
					(> 
						0.002 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_l_absou) 100000.0 ) ) (done--217) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_l_absou) 
					(* 
						(delta) 0.002 ) ) 
			(not (done--217)) (done--218) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(> 
							(copy--occupancy__wrcc1_x_wrdc1) 0.0 ) 
						(> 
							0.002 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_l_absou) 100000.0 ) )) (done--217) )
 :effect 
	(and 
		
			(not (done--217)) (done--218) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(> 
						(copy--occupancy__wrcc1_w_wrdc1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_q_abnor) 100000.0 ) ) (done--218) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrdc1_q_abnor) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--218)) (done--219) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(> 
							(copy--occupancy__wrcc1_w_wrdc1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_q_abnor) 100000.0 ) )) (done--218) )
 :effect 
	(and 
		
			(not (done--218)) (done--219) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(> 
						(copy--occupancy__wrcc1_w_wrdc1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_q_abnor) 100000.0 ) ) (done--219) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrcc1_w_wrdc1) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--219)) (done--220) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(> 
							(copy--occupancy__wrcc1_w_wrdc1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_q_abnor) 100000.0 ) )) (done--219) )
 :effect 
	(and 
		
			(not (done--219)) (done--220) )
)

(:action simulate--positive--flowrun_green__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrdc1_stage4) 
					(> 
						(copy--occupancy__wrcc1_w_wrdc1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrdc1_q_abnor) 100000.0 ) ) (done--220) )
 :effect 
	(and 
		
			(increase 
				(counter__wrdc1_q_abnor) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--220)) (done--221) )
)

(:action simulate--negative--flowrun_green__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrdc1_stage4) 
						(> 
							(copy--occupancy__wrcc1_w_wrdc1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrdc1_q_abnor) 100000.0 ) )) (done--220) )
 :effect 
	(and 
		
			(not (done--220)) (done--221) )
)

(:action simulate--positive--flowrun_green__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(> 
						(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--221) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_y_wrfc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--221)) (done--222) )
)

(:action simulate--negative--flowrun_green__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(> 
							(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--221) )
 :effect 
	(and 
		
			(not (done--221)) (done--222) )
)

(:action simulate--positive--flowrun_green__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(> 
						(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--222) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrdc1_b_wrec1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--222)) (done--223) )
)

(:action simulate--negative--flowrun_green__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(> 
							(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--222) )
 :effect 
	(and 
		
			(not (done--222)) (done--223) )
)

(:action simulate--positive--flowrun_green__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(> 
						(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--223) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_y_wrfc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--223)) (done--224) )
)

(:action simulate--negative--flowrun_green__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(> 
							(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--223) )
 :effect 
	(and 
		
			(not (done--223)) (done--224) )
)

(:action simulate--positive--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.774 0.0 ) 
					(< 
						(copy--occupancy__wrec1_z_wrdc1) 28.67 ) ) (done--224) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_z_wrdc1) 
					(* 
						(delta) 0.774 ) ) 
			(not (done--224)) (done--225) )
)

(:action simulate--negative--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.774 0.0 ) 
						(< 
							(copy--occupancy__wrec1_z_wrdc1) 28.67 ) )) (done--224) )
 :effect 
	(and 
		
			(not (done--224)) (done--225) )
)

(:action simulate--positive--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.774 0.0 ) 
					(< 
						(copy--occupancy__wrec1_z_wrdc1) 28.67 ) ) (done--225) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrfc1_a_wrec1) 
					(* 
						(delta) 0.774 ) ) 
			(not (done--225)) (done--226) )
)

(:action simulate--negative--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.774 0.0 ) 
						(< 
							(copy--occupancy__wrec1_z_wrdc1) 28.67 ) )) (done--225) )
 :effect 
	(and 
		
			(not (done--225)) (done--226) )
)

(:action simulate--positive--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.774 0.0 ) 
					(< 
						(copy--occupancy__wrec1_z_wrdc1) 28.67 ) ) (done--226) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_z_wrdc1) 
					(* 
						(delta) 0.774 ) ) 
			(not (done--226)) (done--227) )
)

(:action simulate--negative--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.774 0.0 ) 
						(< 
							(copy--occupancy__wrec1_z_wrdc1) 28.67 ) )) (done--226) )
 :effect 
	(and 
		
			(not (done--226)) (done--227) )
)

(:action simulate--positive--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.049 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrdc1) 6.33 ) ) (done--227) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_y_wrdc1) 
					(* 
						(delta) 0.049 ) ) 
			(not (done--227)) (done--228) )
)

(:action simulate--negative--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.049 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrdc1) 6.33 ) )) (done--227) )
 :effect 
	(and 
		
			(not (done--227)) (done--228) )
)

(:action simulate--positive--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.049 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrdc1) 6.33 ) ) (done--228) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrfc1_a_wrec1) 
					(* 
						(delta) 0.049 ) ) 
			(not (done--228)) (done--229) )
)

(:action simulate--negative--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.049 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrdc1) 6.33 ) )) (done--228) )
 :effect 
	(and 
		
			(not (done--228)) (done--229) )
)

(:action simulate--positive--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage1) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.049 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrdc1) 6.33 ) ) (done--229) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_y_wrdc1) 
					(* 
						(delta) 0.049 ) ) 
			(not (done--229)) (done--230) )
)

(:action simulate--negative--flowrun_green__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage1) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.049 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrdc1) 6.33 ) )) (done--229) )
 :effect 
	(and 
		
			(not (done--229)) (done--230) )
)

(:action simulate--positive--flowrun_green__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage2) 
					(> 
						(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--230) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_y_wrfc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--230)) (done--231) )
)

(:action simulate--negative--flowrun_green__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage2) 
						(> 
							(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--230) )
 :effect 
	(and 
		
			(not (done--230)) (done--231) )
)

(:action simulate--positive--flowrun_green__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage2) 
					(> 
						(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--231) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrdc1_b_wrec1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--231)) (done--232) )
)

(:action simulate--negative--flowrun_green__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage2) 
						(> 
							(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--231) )
 :effect 
	(and 
		
			(not (done--231)) (done--232) )
)

(:action simulate--positive--flowrun_green__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage2) 
					(> 
						(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
					(> 
						0.824 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--232) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_y_wrfc1) 
					(* 
						(delta) 0.824 ) ) 
			(not (done--232)) (done--233) )
)

(:action simulate--negative--flowrun_green__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage2) 
						(> 
							(copy--occupancy__wrdc1_b_wrec1) 0.0 ) 
						(> 
							0.824 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--232) )
 :effect 
	(and 
		
			(not (done--232)) (done--233) )
)

(:action simulate--positive--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(> 
						(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
					(> 
						0.332 0.0 ) 
					(< 
						(copy--occupancy__wrec1_z_wrdc1) 28.67 ) ) (done--233) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_z_wrdc1) 
					(* 
						(delta) 0.332 ) ) 
			(not (done--233)) (done--234) )
)

(:action simulate--negative--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(> 
							(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
						(> 
							0.332 0.0 ) 
						(< 
							(copy--occupancy__wrec1_z_wrdc1) 28.67 ) )) (done--233) )
 :effect 
	(and 
		
			(not (done--233)) (done--234) )
)

(:action simulate--positive--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(> 
						(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
					(> 
						0.332 0.0 ) 
					(< 
						(copy--occupancy__wrec1_z_wrdc1) 28.67 ) ) (done--234) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__oldwa_c_wrec1) 
					(* 
						(delta) 0.332 ) ) 
			(not (done--234)) (done--235) )
)

(:action simulate--negative--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(> 
							(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
						(> 
							0.332 0.0 ) 
						(< 
							(copy--occupancy__wrec1_z_wrdc1) 28.67 ) )) (done--234) )
 :effect 
	(and 
		
			(not (done--234)) (done--235) )
)

(:action simulate--positive--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(> 
						(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
					(> 
						0.332 0.0 ) 
					(< 
						(copy--occupancy__wrec1_z_wrdc1) 28.67 ) ) (done--235) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_z_wrdc1) 
					(* 
						(delta) 0.332 ) ) 
			(not (done--235)) (done--236) )
)

(:action simulate--negative--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(> 
							(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
						(> 
							0.332 0.0 ) 
						(< 
							(copy--occupancy__wrec1_z_wrdc1) 28.67 ) )) (done--235) )
 :effect 
	(and 
		
			(not (done--235)) (done--236) )
)

(:action simulate--positive--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(> 
						(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
					(> 
						0.021 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrdc1) 6.33 ) ) (done--236) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_y_wrdc1) 
					(* 
						(delta) 0.021 ) ) 
			(not (done--236)) (done--237) )
)

(:action simulate--negative--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(> 
							(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
						(> 
							0.021 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrdc1) 6.33 ) )) (done--236) )
 :effect 
	(and 
		
			(not (done--236)) (done--237) )
)

(:action simulate--positive--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(> 
						(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
					(> 
						0.021 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrdc1) 6.33 ) ) (done--237) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__oldwa_c_wrec1) 
					(* 
						(delta) 0.021 ) ) 
			(not (done--237)) (done--238) )
)

(:action simulate--negative--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(> 
							(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
						(> 
							0.021 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrdc1) 6.33 ) )) (done--237) )
 :effect 
	(and 
		
			(not (done--237)) (done--238) )
)

(:action simulate--positive--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(> 
						(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
					(> 
						0.021 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrdc1) 6.33 ) ) (done--238) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_y_wrdc1) 
					(* 
						(delta) 0.021 ) ) 
			(not (done--238)) (done--239) )
)

(:action simulate--negative--flowrun_green__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(> 
							(copy--occupancy__oldwa_c_wrec1) 0.0 ) 
						(> 
							0.021 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrdc1) 6.33 ) )) (done--238) )
 :effect 
	(and 
		
			(not (done--238)) (done--239) )
)

(:action simulate--positive--flowrun_green__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(> 
						(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--239) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_y_wrfc1) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--239)) (done--240) )
)

(:action simulate--negative--flowrun_green__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(> 
							(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--239) )
 :effect 
	(and 
		
			(not (done--239)) (done--240) )
)

(:action simulate--positive--flowrun_green__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(> 
						(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--240) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__oldwa_d_wrec1) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--240)) (done--241) )
)

(:action simulate--negative--flowrun_green__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(> 
							(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--240) )
 :effect 
	(and 
		
			(not (done--240)) (done--241) )
)

(:action simulate--positive--flowrun_green__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage3) 
					(> 
						(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--241) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_y_wrfc1) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--241)) (done--242) )
)

(:action simulate--negative--flowrun_green__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage3) 
						(> 
							(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--241) )
 :effect 
	(and 
		
			(not (done--241)) (done--242) )
)

(:action simulate--positive--flowrun_green__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(> 
						(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--242) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_y_wrfc1) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--242)) (done--243) )
)

(:action simulate--negative--flowrun_green__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(> 
							(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--242) )
 :effect 
	(and 
		
			(not (done--242)) (done--243) )
)

(:action simulate--positive--flowrun_green__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(> 
						(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--243) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__oldwa_d_wrec1) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--243)) (done--244) )
)

(:action simulate--negative--flowrun_green__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(> 
							(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--243) )
 :effect 
	(and 
		
			(not (done--243)) (done--244) )
)

(:action simulate--positive--flowrun_green__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(> 
						(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
					(> 
						0.353 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrfc1) 13.0 ) ) (done--244) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_y_wrfc1) 
					(* 
						(delta) 0.353 ) ) 
			(not (done--244)) (done--245) )
)

(:action simulate--negative--flowrun_green__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(> 
							(copy--occupancy__oldwa_d_wrec1) 0.0 ) 
						(> 
							0.353 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrfc1) 13.0 ) )) (done--244) )
 :effect 
	(and 
		
			(not (done--244)) (done--245) )
)

(:action simulate--positive--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.774 0.0 ) 
					(< 
						(copy--occupancy__wrec1_z_wrdc1) 28.67 ) ) (done--245) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_z_wrdc1) 
					(* 
						(delta) 0.774 ) ) 
			(not (done--245)) (done--246) )
)

(:action simulate--negative--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.774 0.0 ) 
						(< 
							(copy--occupancy__wrec1_z_wrdc1) 28.67 ) )) (done--245) )
 :effect 
	(and 
		
			(not (done--245)) (done--246) )
)

(:action simulate--positive--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.774 0.0 ) 
					(< 
						(copy--occupancy__wrec1_z_wrdc1) 28.67 ) ) (done--246) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrfc1_a_wrec1) 
					(* 
						(delta) 0.774 ) ) 
			(not (done--246)) (done--247) )
)

(:action simulate--negative--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.774 0.0 ) 
						(< 
							(copy--occupancy__wrec1_z_wrdc1) 28.67 ) )) (done--246) )
 :effect 
	(and 
		
			(not (done--246)) (done--247) )
)

(:action simulate--positive--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.774 0.0 ) 
					(< 
						(copy--occupancy__wrec1_z_wrdc1) 28.67 ) ) (done--247) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_z_wrdc1) 
					(* 
						(delta) 0.774 ) ) 
			(not (done--247)) (done--248) )
)

(:action simulate--negative--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.774 0.0 ) 
						(< 
							(copy--occupancy__wrec1_z_wrdc1) 28.67 ) )) (done--247) )
 :effect 
	(and 
		
			(not (done--247)) (done--248) )
)

(:action simulate--positive--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.049 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrdc1) 6.33 ) ) (done--248) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrec1_y_wrdc1) 
					(* 
						(delta) 0.049 ) ) 
			(not (done--248)) (done--249) )
)

(:action simulate--negative--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.049 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrdc1) 6.33 ) )) (done--248) )
 :effect 
	(and 
		
			(not (done--248)) (done--249) )
)

(:action simulate--positive--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.049 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrdc1) 6.33 ) ) (done--249) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrfc1_a_wrec1) 
					(* 
						(delta) 0.049 ) ) 
			(not (done--249)) (done--250) )
)

(:action simulate--negative--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.049 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrdc1) 6.33 ) )) (done--249) )
 :effect 
	(and 
		
			(not (done--249)) (done--250) )
)

(:action simulate--positive--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrec1_stage4) 
					(> 
						(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
					(> 
						0.049 0.0 ) 
					(< 
						(copy--occupancy__wrec1_y_wrdc1) 6.33 ) ) (done--250) )
 :effect 
	(and 
		
			(increase 
				(counter__wrec1_y_wrdc1) 
					(* 
						(delta) 0.049 ) ) 
			(not (done--250)) (done--251) )
)

(:action simulate--negative--flowrun_green__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrec1_stage4) 
						(> 
							(copy--occupancy__wrfc1_a_wrec1) 0.0 ) 
						(> 
							0.049 0.0 ) 
						(< 
							(copy--occupancy__wrec1_y_wrdc1) 6.33 ) )) (done--250) )
 :effect 
	(and 
		
			(not (done--250)) (done--251) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.249 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_r_broad) 100000.0 ) ) (done--251) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrfc1_r_broad) 
					(* 
						(delta) 0.249 ) ) 
			(not (done--251)) (done--252) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.249 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_r_broad) 100000.0 ) )) (done--251) )
 :effect 
	(and 
		
			(not (done--251)) (done--252) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.249 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_r_broad) 100000.0 ) ) (done--252) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrec1_y_wrfc1) 
					(* 
						(delta) 0.249 ) ) 
			(not (done--252)) (done--253) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.249 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_r_broad) 100000.0 ) )) (done--252) )
 :effect 
	(and 
		
			(not (done--252)) (done--253) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.249 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_r_broad) 100000.0 ) ) (done--253) )
 :effect 
	(and 
		
			(increase 
				(counter__wrfc1_r_broad) 
					(* 
						(delta) 0.249 ) ) 
			(not (done--253)) (done--254) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.249 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_r_broad) 100000.0 ) )) (done--253) )
 :effect 
	(and 
		
			(not (done--253)) (done--254) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.575 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_t_wakef) 100000.0 ) ) (done--254) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrfc1_t_wakef) 
					(* 
						(delta) 0.575 ) ) 
			(not (done--254)) (done--255) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.575 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_t_wakef) 100000.0 ) )) (done--254) )
 :effect 
	(and 
		
			(not (done--254)) (done--255) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.575 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_t_wakef) 100000.0 ) ) (done--255) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrec1_y_wrfc1) 
					(* 
						(delta) 0.575 ) ) 
			(not (done--255)) (done--256) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.575 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_t_wakef) 100000.0 ) )) (done--255) )
 :effect 
	(and 
		
			(not (done--255)) (done--256) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.575 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_t_wakef) 100000.0 ) ) (done--256) )
 :effect 
	(and 
		
			(increase 
				(counter__wrfc1_t_wakef) 
					(* 
						(delta) 0.575 ) ) 
			(not (done--256)) (done--257) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.575 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_t_wakef) 100000.0 ) )) (done--256) )
 :effect 
	(and 
		
			(not (done--256)) (done--257) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(> 
						(copy--occupancy__wakef_z_wrfc1) 0.0 ) 
					(> 
						0.765 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_a_wrec1) 17.4 ) ) (done--257) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrfc1_a_wrec1) 
					(* 
						(delta) 0.765 ) ) 
			(not (done--257)) (done--258) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(> 
							(copy--occupancy__wakef_z_wrfc1) 0.0 ) 
						(> 
							0.765 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_a_wrec1) 17.4 ) )) (done--257) )
 :effect 
	(and 
		
			(not (done--257)) (done--258) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(> 
						(copy--occupancy__wakef_z_wrfc1) 0.0 ) 
					(> 
						0.765 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_a_wrec1) 17.4 ) ) (done--258) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wakef_z_wrfc1) 
					(* 
						(delta) 0.765 ) ) 
			(not (done--258)) (done--259) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(> 
							(copy--occupancy__wakef_z_wrfc1) 0.0 ) 
						(> 
							0.765 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_a_wrec1) 17.4 ) )) (done--258) )
 :effect 
	(and 
		
			(not (done--258)) (done--259) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage1) 
					(> 
						(copy--occupancy__wakef_z_wrfc1) 0.0 ) 
					(> 
						0.765 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_a_wrec1) 17.4 ) ) (done--259) )
 :effect 
	(and 
		
			(increase 
				(counter__wrfc1_a_wrec1) 
					(* 
						(delta) 0.765 ) ) 
			(not (done--259)) (done--260) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage1) 
						(> 
							(copy--occupancy__wakef_z_wrfc1) 0.0 ) 
						(> 
							0.765 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_a_wrec1) 17.4 ) )) (done--259) )
 :effect 
	(and 
		
			(not (done--259)) (done--260) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage2) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.249 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_r_broad) 100000.0 ) ) (done--260) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrfc1_r_broad) 
					(* 
						(delta) 0.249 ) ) 
			(not (done--260)) (done--261) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage2) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.249 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_r_broad) 100000.0 ) )) (done--260) )
 :effect 
	(and 
		
			(not (done--260)) (done--261) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage2) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.249 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_r_broad) 100000.0 ) ) (done--261) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrec1_y_wrfc1) 
					(* 
						(delta) 0.249 ) ) 
			(not (done--261)) (done--262) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage2) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.249 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_r_broad) 100000.0 ) )) (done--261) )
 :effect 
	(and 
		
			(not (done--261)) (done--262) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage2) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.249 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_r_broad) 100000.0 ) ) (done--262) )
 :effect 
	(and 
		
			(increase 
				(counter__wrfc1_r_broad) 
					(* 
						(delta) 0.249 ) ) 
			(not (done--262)) (done--263) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage2) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.249 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_r_broad) 100000.0 ) )) (done--262) )
 :effect 
	(and 
		
			(not (done--262)) (done--263) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage2) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.575 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_t_wakef) 100000.0 ) ) (done--263) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrfc1_t_wakef) 
					(* 
						(delta) 0.575 ) ) 
			(not (done--263)) (done--264) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage2) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.575 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_t_wakef) 100000.0 ) )) (done--263) )
 :effect 
	(and 
		
			(not (done--263)) (done--264) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage2) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.575 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_t_wakef) 100000.0 ) ) (done--264) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__wrec1_y_wrfc1) 
					(* 
						(delta) 0.575 ) ) 
			(not (done--264)) (done--265) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage2) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.575 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_t_wakef) 100000.0 ) )) (done--264) )
 :effect 
	(and 
		
			(not (done--264)) (done--265) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage2) 
					(> 
						(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
					(> 
						0.575 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_t_wakef) 100000.0 ) ) (done--265) )
 :effect 
	(and 
		
			(increase 
				(counter__wrfc1_t_wakef) 
					(* 
						(delta) 0.575 ) ) 
			(not (done--265)) (done--266) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage2) 
						(> 
							(copy--occupancy__wrec1_y_wrfc1) 0.0 ) 
						(> 
							0.575 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_t_wakef) 100000.0 ) )) (done--265) )
 :effect 
	(and 
		
			(not (done--265)) (done--266) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage3) 
					(> 
						(copy--occupancy__broad_x_wrfc1) 0.0 ) 
					(> 
						0.586 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_a_wrec1) 17.4 ) ) (done--266) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrfc1_a_wrec1) 
					(* 
						(delta) 0.586 ) ) 
			(not (done--266)) (done--267) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage3) 
						(> 
							(copy--occupancy__broad_x_wrfc1) 0.0 ) 
						(> 
							0.586 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_a_wrec1) 17.4 ) )) (done--266) )
 :effect 
	(and 
		
			(not (done--266)) (done--267) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage3) 
					(> 
						(copy--occupancy__broad_x_wrfc1) 0.0 ) 
					(> 
						0.586 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_a_wrec1) 17.4 ) ) (done--267) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__broad_x_wrfc1) 
					(* 
						(delta) 0.586 ) ) 
			(not (done--267)) (done--268) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage3) 
						(> 
							(copy--occupancy__broad_x_wrfc1) 0.0 ) 
						(> 
							0.586 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_a_wrec1) 17.4 ) )) (done--267) )
 :effect 
	(and 
		
			(not (done--267)) (done--268) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage3) 
					(> 
						(copy--occupancy__broad_x_wrfc1) 0.0 ) 
					(> 
						0.586 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_a_wrec1) 17.4 ) ) (done--268) )
 :effect 
	(and 
		
			(increase 
				(counter__wrfc1_a_wrec1) 
					(* 
						(delta) 0.586 ) ) 
			(not (done--268)) (done--269) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage3) 
						(> 
							(copy--occupancy__broad_x_wrfc1) 0.0 ) 
						(> 
							0.586 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_a_wrec1) 17.4 ) )) (done--268) )
 :effect 
	(and 
		
			(not (done--268)) (done--269) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage3) 
					(> 
						(copy--occupancy__broad_x_wrfc1) 0.0 ) 
					(> 
						0.002 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_t_wakef) 100000.0 ) ) (done--269) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wrfc1_t_wakef) 
					(* 
						(delta) 0.002 ) ) 
			(not (done--269)) (done--270) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage3) 
						(> 
							(copy--occupancy__broad_x_wrfc1) 0.0 ) 
						(> 
							0.002 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_t_wakef) 100000.0 ) )) (done--269) )
 :effect 
	(and 
		
			(not (done--269)) (done--270) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage3) 
					(> 
						(copy--occupancy__broad_x_wrfc1) 0.0 ) 
					(> 
						0.002 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_t_wakef) 100000.0 ) ) (done--270) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__broad_x_wrfc1) 
					(* 
						(delta) 0.002 ) ) 
			(not (done--270)) (done--271) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage3) 
						(> 
							(copy--occupancy__broad_x_wrfc1) 0.0 ) 
						(> 
							0.002 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_t_wakef) 100000.0 ) )) (done--270) )
 :effect 
	(and 
		
			(not (done--270)) (done--271) )
)

(:action simulate--positive--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__wrfc1_stage3) 
					(> 
						(copy--occupancy__broad_x_wrfc1) 0.0 ) 
					(> 
						0.002 0.0 ) 
					(< 
						(copy--occupancy__wrfc1_t_wakef) 100000.0 ) ) (done--271) )
 :effect 
	(and 
		
			(increase 
				(counter__wrfc1_t_wakef) 
					(* 
						(delta) 0.002 ) ) 
			(not (done--271)) (done--272) )
)

(:action simulate--negative--flowrun_green__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__wrfc1_stage3) 
						(> 
							(copy--occupancy__broad_x_wrfc1) 0.0 ) 
						(> 
							0.002 0.0 ) 
						(< 
							(copy--occupancy__wrfc1_t_wakef) 100000.0 ) )) (done--271) )
 :effect 
	(and 
		
			(not (done--271)) (done--272) )
)

(:action simulate--positive--flowrun_green__fake__outside__hsac3_c_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.3376 0.0 ) 
					(< 
						(copy--occupancy__hsac3_c_wrac1) 100000.0 ) ) (done--272) )
 :effect 
	(and 
		
			(increase 
				(occupancy__hsac3_c_wrac1) 
					(* 
						(delta) 0.3376 ) ) 
			(not (done--272)) (done--273) )
)

(:action simulate--negative--flowrun_green__fake__outside__hsac3_c_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.3376 0.0 ) 
						(< 
							(copy--occupancy__hsac3_c_wrac1) 100000.0 ) )) (done--272) )
 :effect 
	(and 
		
			(not (done--272)) (done--273) )
)

(:action simulate--positive--flowrun_green__fake__outside__hsac3_c_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.3376 0.0 ) 
					(< 
						(copy--occupancy__hsac3_c_wrac1) 100000.0 ) ) (done--273) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.3376 ) ) 
			(not (done--273)) (done--274) )
)

(:action simulate--negative--flowrun_green__fake__outside__hsac3_c_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.3376 0.0 ) 
						(< 
							(copy--occupancy__hsac3_c_wrac1) 100000.0 ) )) (done--273) )
 :effect 
	(and 
		
			(not (done--273)) (done--274) )
)

(:action simulate--positive--flowrun_green__fake__outside__hsac3_c_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.3376 0.0 ) 
					(< 
						(copy--occupancy__hsac3_c_wrac1) 100000.0 ) ) (done--274) )
 :effect 
	(and 
		
			(increase 
				(counter__hsac3_c_wrac1) 
					(* 
						(delta) 0.3376 ) ) 
			(not (done--274)) (done--275) )
)

(:action simulate--negative--flowrun_green__fake__outside__hsac3_c_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.3376 0.0 ) 
						(< 
							(copy--occupancy__hsac3_c_wrac1) 100000.0 ) )) (done--274) )
 :effect 
	(and 
		
			(not (done--274)) (done--275) )
)

(:action simulate--positive--flowrun_green__fake__outside__stand_f_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.2258 0.0 ) 
					(< 
						(copy--occupancy__stand_f_wrac1) 100000.0 ) ) (done--275) )
 :effect 
	(and 
		
			(increase 
				(occupancy__stand_f_wrac1) 
					(* 
						(delta) 0.2258 ) ) 
			(not (done--275)) (done--276) )
)

(:action simulate--negative--flowrun_green__fake__outside__stand_f_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.2258 0.0 ) 
						(< 
							(copy--occupancy__stand_f_wrac1) 100000.0 ) )) (done--275) )
 :effect 
	(and 
		
			(not (done--275)) (done--276) )
)

(:action simulate--positive--flowrun_green__fake__outside__stand_f_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.2258 0.0 ) 
					(< 
						(copy--occupancy__stand_f_wrac1) 100000.0 ) ) (done--276) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.2258 ) ) 
			(not (done--276)) (done--277) )
)

(:action simulate--negative--flowrun_green__fake__outside__stand_f_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.2258 0.0 ) 
						(< 
							(copy--occupancy__stand_f_wrac1) 100000.0 ) )) (done--276) )
 :effect 
	(and 
		
			(not (done--276)) (done--277) )
)

(:action simulate--positive--flowrun_green__fake__outside__stand_f_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.2258 0.0 ) 
					(< 
						(copy--occupancy__stand_f_wrac1) 100000.0 ) ) (done--277) )
 :effect 
	(and 
		
			(increase 
				(counter__stand_f_wrac1) 
					(* 
						(delta) 0.2258 ) ) 
			(not (done--277)) (done--278) )
)

(:action simulate--negative--flowrun_green__fake__outside__stand_f_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.2258 0.0 ) 
						(< 
							(copy--occupancy__stand_f_wrac1) 100000.0 ) )) (done--277) )
 :effect 
	(and 
		
			(not (done--277)) (done--278) )
)

(:action simulate--positive--flowrun_green__fake__outside__firth_d_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0686 0.0 ) 
					(< 
						(copy--occupancy__firth_d_wrac1) 100000.0 ) ) (done--278) )
 :effect 
	(and 
		
			(increase 
				(occupancy__firth_d_wrac1) 
					(* 
						(delta) 0.0686 ) ) 
			(not (done--278)) (done--279) )
)

(:action simulate--negative--flowrun_green__fake__outside__firth_d_wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0686 0.0 ) 
						(< 
							(copy--occupancy__firth_d_wrac1) 100000.0 ) )) (done--278) )
 :effect 
	(and 
		
			(not (done--278)) (done--279) )
)

(:action simulate--positive--flowrun_green__fake__outside__firth_d_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0686 0.0 ) 
					(< 
						(copy--occupancy__firth_d_wrac1) 100000.0 ) ) (done--279) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.0686 ) ) 
			(not (done--279)) (done--280) )
)

(:action simulate--negative--flowrun_green__fake__outside__firth_d_wrac1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0686 0.0 ) 
						(< 
							(copy--occupancy__firth_d_wrac1) 100000.0 ) )) (done--279) )
 :effect 
	(and 
		
			(not (done--279)) (done--280) )
)

(:action simulate--positive--flowrun_green__fake__outside__firth_d_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0686 0.0 ) 
					(< 
						(copy--occupancy__firth_d_wrac1) 100000.0 ) ) (done--280) )
 :effect 
	(and 
		
			(increase 
				(counter__firth_d_wrac1) 
					(* 
						(delta) 0.0686 ) ) 
			(not (done--280)) (done--281) )
)

(:action simulate--negative--flowrun_green__fake__outside__firth_d_wrac1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0686 0.0 ) 
						(< 
							(copy--occupancy__firth_d_wrac1) 100000.0 ) )) (done--280) )
 :effect 
	(and 
		
			(not (done--280)) (done--281) )
)

(:action simulate--positive--flowrun_green__fake__outside__silve_w_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0546 0.0 ) 
					(< 
						(copy--occupancy__silve_w_wrbc1) 100000.0 ) ) (done--281) )
 :effect 
	(and 
		
			(increase 
				(occupancy__silve_w_wrbc1) 
					(* 
						(delta) 0.0546 ) ) 
			(not (done--281)) (done--282) )
)

(:action simulate--negative--flowrun_green__fake__outside__silve_w_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0546 0.0 ) 
						(< 
							(copy--occupancy__silve_w_wrbc1) 100000.0 ) )) (done--281) )
 :effect 
	(and 
		
			(not (done--281)) (done--282) )
)

(:action simulate--positive--flowrun_green__fake__outside__silve_w_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0546 0.0 ) 
					(< 
						(copy--occupancy__silve_w_wrbc1) 100000.0 ) ) (done--282) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.0546 ) ) 
			(not (done--282)) (done--283) )
)

(:action simulate--negative--flowrun_green__fake__outside__silve_w_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0546 0.0 ) 
						(< 
							(copy--occupancy__silve_w_wrbc1) 100000.0 ) )) (done--282) )
 :effect 
	(and 
		
			(not (done--282)) (done--283) )
)

(:action simulate--positive--flowrun_green__fake__outside__silve_w_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0546 0.0 ) 
					(< 
						(copy--occupancy__silve_w_wrbc1) 100000.0 ) ) (done--283) )
 :effect 
	(and 
		
			(increase 
				(counter__silve_w_wrbc1) 
					(* 
						(delta) 0.0546 ) ) 
			(not (done--283)) (done--284) )
)

(:action simulate--negative--flowrun_green__fake__outside__silve_w_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0546 0.0 ) 
						(< 
							(copy--occupancy__silve_w_wrbc1) 100000.0 ) )) (done--283) )
 :effect 
	(and 
		
			(not (done--283)) (done--284) )
)

(:action simulate--positive--flowrun_green__fake__outside__somer_v_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0774 0.0 ) 
					(< 
						(copy--occupancy__somer_v_wrbc1) 100000.0 ) ) (done--284) )
 :effect 
	(and 
		
			(increase 
				(occupancy__somer_v_wrbc1) 
					(* 
						(delta) 0.0774 ) ) 
			(not (done--284)) (done--285) )
)

(:action simulate--negative--flowrun_green__fake__outside__somer_v_wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0774 0.0 ) 
						(< 
							(copy--occupancy__somer_v_wrbc1) 100000.0 ) )) (done--284) )
 :effect 
	(and 
		
			(not (done--284)) (done--285) )
)

(:action simulate--positive--flowrun_green__fake__outside__somer_v_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0774 0.0 ) 
					(< 
						(copy--occupancy__somer_v_wrbc1) 100000.0 ) ) (done--285) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.0774 ) ) 
			(not (done--285)) (done--286) )
)

(:action simulate--negative--flowrun_green__fake__outside__somer_v_wrbc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0774 0.0 ) 
						(< 
							(copy--occupancy__somer_v_wrbc1) 100000.0 ) )) (done--285) )
 :effect 
	(and 
		
			(not (done--285)) (done--286) )
)

(:action simulate--positive--flowrun_green__fake__outside__somer_v_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0774 0.0 ) 
					(< 
						(copy--occupancy__somer_v_wrbc1) 100000.0 ) ) (done--286) )
 :effect 
	(and 
		
			(increase 
				(counter__somer_v_wrbc1) 
					(* 
						(delta) 0.0774 ) ) 
			(not (done--286)) (done--287) )
)

(:action simulate--negative--flowrun_green__fake__outside__somer_v_wrbc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0774 0.0 ) 
						(< 
							(copy--occupancy__somer_v_wrbc1) 100000.0 ) )) (done--286) )
 :effect 
	(and 
		
			(not (done--286)) (done--287) )
)

(:action simulate--positive--flowrun_green__fake__outside__smith_c_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0685 0.0 ) 
					(< 
						(copy--occupancy__smith_c_wrcc1) 100000.0 ) ) (done--287) )
 :effect 
	(and 
		
			(increase 
				(occupancy__smith_c_wrcc1) 
					(* 
						(delta) 0.0685 ) ) 
			(not (done--287)) (done--288) )
)

(:action simulate--negative--flowrun_green__fake__outside__smith_c_wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0685 0.0 ) 
						(< 
							(copy--occupancy__smith_c_wrcc1) 100000.0 ) )) (done--287) )
 :effect 
	(and 
		
			(not (done--287)) (done--288) )
)

(:action simulate--positive--flowrun_green__fake__outside__smith_c_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0685 0.0 ) 
					(< 
						(copy--occupancy__smith_c_wrcc1) 100000.0 ) ) (done--288) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.0685 ) ) 
			(not (done--288)) (done--289) )
)

(:action simulate--negative--flowrun_green__fake__outside__smith_c_wrcc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0685 0.0 ) 
						(< 
							(copy--occupancy__smith_c_wrcc1) 100000.0 ) )) (done--288) )
 :effect 
	(and 
		
			(not (done--288)) (done--289) )
)

(:action simulate--positive--flowrun_green__fake__outside__smith_c_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0685 0.0 ) 
					(< 
						(copy--occupancy__smith_c_wrcc1) 100000.0 ) ) (done--289) )
 :effect 
	(and 
		
			(increase 
				(counter__smith_c_wrcc1) 
					(* 
						(delta) 0.0685 ) ) 
			(not (done--289)) (done--290) )
)

(:action simulate--negative--flowrun_green__fake__outside__smith_c_wrcc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0685 0.0 ) 
						(< 
							(copy--occupancy__smith_c_wrcc1) 100000.0 ) )) (done--289) )
 :effect 
	(and 
		
			(not (done--289)) (done--290) )
)

(:action simulate--positive--flowrun_green__fake__outside__abnor_v_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0736 0.0 ) 
					(< 
						(copy--occupancy__abnor_v_wrdc1) 100000.0 ) ) (done--290) )
 :effect 
	(and 
		
			(increase 
				(occupancy__abnor_v_wrdc1) 
					(* 
						(delta) 0.0736 ) ) 
			(not (done--290)) (done--291) )
)

(:action simulate--negative--flowrun_green__fake__outside__abnor_v_wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0736 0.0 ) 
						(< 
							(copy--occupancy__abnor_v_wrdc1) 100000.0 ) )) (done--290) )
 :effect 
	(and 
		
			(not (done--290)) (done--291) )
)

(:action simulate--positive--flowrun_green__fake__outside__abnor_v_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0736 0.0 ) 
					(< 
						(copy--occupancy__abnor_v_wrdc1) 100000.0 ) ) (done--291) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.0736 ) ) 
			(not (done--291)) (done--292) )
)

(:action simulate--negative--flowrun_green__fake__outside__abnor_v_wrdc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0736 0.0 ) 
						(< 
							(copy--occupancy__abnor_v_wrdc1) 100000.0 ) )) (done--291) )
 :effect 
	(and 
		
			(not (done--291)) (done--292) )
)

(:action simulate--positive--flowrun_green__fake__outside__abnor_v_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0736 0.0 ) 
					(< 
						(copy--occupancy__abnor_v_wrdc1) 100000.0 ) ) (done--292) )
 :effect 
	(and 
		
			(increase 
				(counter__abnor_v_wrdc1) 
					(* 
						(delta) 0.0736 ) ) 
			(not (done--292)) (done--293) )
)

(:action simulate--negative--flowrun_green__fake__outside__abnor_v_wrdc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0736 0.0 ) 
						(< 
							(copy--occupancy__abnor_v_wrdc1) 100000.0 ) )) (done--292) )
 :effect 
	(and 
		
			(not (done--292)) (done--293) )
)

(:action simulate--positive--flowrun_green__fake__outside__oldwa_c_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0064 0.0 ) 
					(< 
						(copy--occupancy__oldwa_c_wrec1) 100000.0 ) ) (done--293) )
 :effect 
	(and 
		
			(increase 
				(occupancy__oldwa_c_wrec1) 
					(* 
						(delta) 0.0064 ) ) 
			(not (done--293)) (done--294) )
)

(:action simulate--negative--flowrun_green__fake__outside__oldwa_c_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0064 0.0 ) 
						(< 
							(copy--occupancy__oldwa_c_wrec1) 100000.0 ) )) (done--293) )
 :effect 
	(and 
		
			(not (done--293)) (done--294) )
)

(:action simulate--positive--flowrun_green__fake__outside__oldwa_c_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0064 0.0 ) 
					(< 
						(copy--occupancy__oldwa_c_wrec1) 100000.0 ) ) (done--294) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.0064 ) ) 
			(not (done--294)) (done--295) )
)

(:action simulate--negative--flowrun_green__fake__outside__oldwa_c_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0064 0.0 ) 
						(< 
							(copy--occupancy__oldwa_c_wrec1) 100000.0 ) )) (done--294) )
 :effect 
	(and 
		
			(not (done--294)) (done--295) )
)

(:action simulate--positive--flowrun_green__fake__outside__oldwa_c_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.0064 0.0 ) 
					(< 
						(copy--occupancy__oldwa_c_wrec1) 100000.0 ) ) (done--295) )
 :effect 
	(and 
		
			(increase 
				(counter__oldwa_c_wrec1) 
					(* 
						(delta) 0.0064 ) ) 
			(not (done--295)) (done--296) )
)

(:action simulate--negative--flowrun_green__fake__outside__oldwa_c_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.0064 0.0 ) 
						(< 
							(copy--occupancy__oldwa_c_wrec1) 100000.0 ) )) (done--295) )
 :effect 
	(and 
		
			(not (done--295)) (done--296) )
)

(:action simulate--positive--flowrun_green__fake__outside__oldwa_d_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.02 0.0 ) 
					(< 
						(copy--occupancy__oldwa_d_wrec1) 100000.0 ) ) (done--296) )
 :effect 
	(and 
		
			(increase 
				(occupancy__oldwa_d_wrec1) 
					(* 
						(delta) 0.02 ) ) 
			(not (done--296)) (done--297) )
)

(:action simulate--negative--flowrun_green__fake__outside__oldwa_d_wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.02 0.0 ) 
						(< 
							(copy--occupancy__oldwa_d_wrec1) 100000.0 ) )) (done--296) )
 :effect 
	(and 
		
			(not (done--296)) (done--297) )
)

(:action simulate--positive--flowrun_green__fake__outside__oldwa_d_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.02 0.0 ) 
					(< 
						(copy--occupancy__oldwa_d_wrec1) 100000.0 ) ) (done--297) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.02 ) ) 
			(not (done--297)) (done--298) )
)

(:action simulate--negative--flowrun_green__fake__outside__oldwa_d_wrec1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.02 0.0 ) 
						(< 
							(copy--occupancy__oldwa_d_wrec1) 100000.0 ) )) (done--297) )
 :effect 
	(and 
		
			(not (done--297)) (done--298) )
)

(:action simulate--positive--flowrun_green__fake__outside__oldwa_d_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.02 0.0 ) 
					(< 
						(copy--occupancy__oldwa_d_wrec1) 100000.0 ) ) (done--298) )
 :effect 
	(and 
		
			(increase 
				(counter__oldwa_d_wrec1) 
					(* 
						(delta) 0.02 ) ) 
			(not (done--298)) (done--299) )
)

(:action simulate--negative--flowrun_green__fake__outside__oldwa_d_wrec1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.02 0.0 ) 
						(< 
							(copy--occupancy__oldwa_d_wrec1) 100000.0 ) )) (done--298) )
 :effect 
	(and 
		
			(not (done--298)) (done--299) )
)

(:action simulate--positive--flowrun_green__fake__outside__wakef_z_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.2116 0.0 ) 
					(< 
						(copy--occupancy__wakef_z_wrfc1) 100000.0 ) ) (done--299) )
 :effect 
	(and 
		
			(increase 
				(occupancy__wakef_z_wrfc1) 
					(* 
						(delta) 0.2116 ) ) 
			(not (done--299)) (done--300) )
)

(:action simulate--negative--flowrun_green__fake__outside__wakef_z_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.2116 0.0 ) 
						(< 
							(copy--occupancy__wakef_z_wrfc1) 100000.0 ) )) (done--299) )
 :effect 
	(and 
		
			(not (done--299)) (done--300) )
)

(:action simulate--positive--flowrun_green__fake__outside__wakef_z_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.2116 0.0 ) 
					(< 
						(copy--occupancy__wakef_z_wrfc1) 100000.0 ) ) (done--300) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.2116 ) ) 
			(not (done--300)) (done--301) )
)

(:action simulate--negative--flowrun_green__fake__outside__wakef_z_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.2116 0.0 ) 
						(< 
							(copy--occupancy__wakef_z_wrfc1) 100000.0 ) )) (done--300) )
 :effect 
	(and 
		
			(not (done--300)) (done--301) )
)

(:action simulate--positive--flowrun_green__fake__outside__wakef_z_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.2116 0.0 ) 
					(< 
						(copy--occupancy__wakef_z_wrfc1) 100000.0 ) ) (done--301) )
 :effect 
	(and 
		
			(increase 
				(counter__wakef_z_wrfc1) 
					(* 
						(delta) 0.2116 ) ) 
			(not (done--301)) (done--302) )
)

(:action simulate--negative--flowrun_green__fake__outside__wakef_z_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.2116 0.0 ) 
						(< 
							(copy--occupancy__wakef_z_wrfc1) 100000.0 ) )) (done--301) )
 :effect 
	(and 
		
			(not (done--301)) (done--302) )
)

(:action simulate--positive--flowrun_green__fake__outside__broad_x_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.1798 0.0 ) 
					(< 
						(copy--occupancy__broad_x_wrfc1) 100000.0 ) ) (done--302) )
 :effect 
	(and 
		
			(increase 
				(occupancy__broad_x_wrfc1) 
					(* 
						(delta) 0.1798 ) ) 
			(not (done--302)) (done--303) )
)

(:action simulate--negative--flowrun_green__fake__outside__broad_x_wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.1798 0.0 ) 
						(< 
							(copy--occupancy__broad_x_wrfc1) 100000.0 ) )) (done--302) )
 :effect 
	(and 
		
			(not (done--302)) (done--303) )
)

(:action simulate--positive--flowrun_green__fake__outside__broad_x_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.1798 0.0 ) 
					(< 
						(copy--occupancy__broad_x_wrfc1) 100000.0 ) ) (done--303) )
 :effect 
	(and 
		
			(decrease 
				(occupancy__outside) 
					(* 
						(delta) 0.1798 ) ) 
			(not (done--303)) (done--304) )
)

(:action simulate--negative--flowrun_green__fake__outside__broad_x_wrfc1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.1798 0.0 ) 
						(< 
							(copy--occupancy__broad_x_wrfc1) 100000.0 ) )) (done--303) )
 :effect 
	(and 
		
			(not (done--303)) (done--304) )
)

(:action simulate--positive--flowrun_green__fake__outside__broad_x_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(active__fake) 
					(> 
						(copy--occupancy__outside) 0.0 ) 
					(> 
						0.1798 0.0 ) 
					(< 
						(copy--occupancy__broad_x_wrfc1) 100000.0 ) ) (done--304) )
 :effect 
	(and 
		
			(increase 
				(counter__broad_x_wrfc1) 
					(* 
						(delta) 0.1798 ) ) 
			(not (done--304)) (done--305) )
)

(:action simulate--negative--flowrun_green__fake__outside__broad_x_wrfc1--2
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(active__fake) 
						(> 
							(copy--occupancy__outside) 0.0 ) 
						(> 
							0.1798 0.0 ) 
						(< 
							(copy--occupancy__broad_x_wrfc1) 100000.0 ) )) (done--304) )
 :effect 
	(and 
		
			(not (done--304)) (done--305) )
)

(:action simulate--positive--keepinter__wrac1_stage1__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrac1_stage1) 
					(< 
						(copy--intertime__wrac1) 5 ) ) (done--305) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrac1) 
					(* 
						(delta) 1 ) ) 
			(not (done--305)) (done--306) )
)

(:action simulate--negative--keepinter__wrac1_stage1__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrac1_stage1) 
						(< 
							(copy--intertime__wrac1) 5 ) )) (done--305) )
 :effect 
	(and 
		
			(not (done--305)) (done--306) )
)

(:action simulate--positive--keepinter__wrac1_stage2__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrac1_stage2) 
					(< 
						(copy--intertime__wrac1) 6 ) ) (done--306) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrac1) 
					(* 
						(delta) 1 ) ) 
			(not (done--306)) (done--307) )
)

(:action simulate--negative--keepinter__wrac1_stage2__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrac1_stage2) 
						(< 
							(copy--intertime__wrac1) 6 ) )) (done--306) )
 :effect 
	(and 
		
			(not (done--306)) (done--307) )
)

(:action simulate--positive--keepinter__wrac1_stage3__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrac1_stage3) 
					(< 
						(copy--intertime__wrac1) 5 ) ) (done--307) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrac1) 
					(* 
						(delta) 1 ) ) 
			(not (done--307)) (done--308) )
)

(:action simulate--negative--keepinter__wrac1_stage3__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrac1_stage3) 
						(< 
							(copy--intertime__wrac1) 5 ) )) (done--307) )
 :effect 
	(and 
		
			(not (done--307)) (done--308) )
)

(:action simulate--positive--keepinter__wrac1_stage4__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrac1_stage4) 
					(< 
						(copy--intertime__wrac1) 5 ) ) (done--308) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrac1) 
					(* 
						(delta) 1 ) ) 
			(not (done--308)) (done--309) )
)

(:action simulate--negative--keepinter__wrac1_stage4__wrac1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrac1_stage4) 
						(< 
							(copy--intertime__wrac1) 5 ) )) (done--308) )
 :effect 
	(and 
		
			(not (done--308)) (done--309) )
)

(:action simulate--positive--keepinter__wrbc1_stage1__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrbc1_stage1) 
					(< 
						(copy--intertime__wrbc1) 6 ) ) (done--309) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--309)) (done--310) )
)

(:action simulate--negative--keepinter__wrbc1_stage1__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrbc1_stage1) 
						(< 
							(copy--intertime__wrbc1) 6 ) )) (done--309) )
 :effect 
	(and 
		
			(not (done--309)) (done--310) )
)

(:action simulate--positive--keepinter__wrbc1_stage2__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrbc1_stage2) 
					(< 
						(copy--intertime__wrbc1) 12 ) ) (done--310) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--310)) (done--311) )
)

(:action simulate--negative--keepinter__wrbc1_stage2__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrbc1_stage2) 
						(< 
							(copy--intertime__wrbc1) 12 ) )) (done--310) )
 :effect 
	(and 
		
			(not (done--310)) (done--311) )
)

(:action simulate--positive--keepinter__wrbc1_stage3__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrbc1_stage3) 
					(< 
						(copy--intertime__wrbc1) 6 ) ) (done--311) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--311)) (done--312) )
)

(:action simulate--negative--keepinter__wrbc1_stage3__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrbc1_stage3) 
						(< 
							(copy--intertime__wrbc1) 6 ) )) (done--311) )
 :effect 
	(and 
		
			(not (done--311)) (done--312) )
)

(:action simulate--positive--keepinter__wrbc1_stage4__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrbc1_stage4) 
					(< 
						(copy--intertime__wrbc1) 7 ) ) (done--312) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--312)) (done--313) )
)

(:action simulate--negative--keepinter__wrbc1_stage4__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrbc1_stage4) 
						(< 
							(copy--intertime__wrbc1) 7 ) )) (done--312) )
 :effect 
	(and 
		
			(not (done--312)) (done--313) )
)

(:action simulate--positive--keepinter__wrbc1_stage5__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrbc1_stage5) 
					(< 
						(copy--intertime__wrbc1) 12 ) ) (done--313) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrbc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--313)) (done--314) )
)

(:action simulate--negative--keepinter__wrbc1_stage5__wrbc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrbc1_stage5) 
						(< 
							(copy--intertime__wrbc1) 12 ) )) (done--313) )
 :effect 
	(and 
		
			(not (done--313)) (done--314) )
)

(:action simulate--positive--keepinter__wrcc1_stage1__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrcc1_stage1) 
					(< 
						(copy--intertime__wrcc1) 6 ) ) (done--314) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--314)) (done--315) )
)

(:action simulate--negative--keepinter__wrcc1_stage1__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrcc1_stage1) 
						(< 
							(copy--intertime__wrcc1) 6 ) )) (done--314) )
 :effect 
	(and 
		
			(not (done--314)) (done--315) )
)

(:action simulate--positive--keepinter__wrcc1_stage2__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrcc1_stage2) 
					(< 
						(copy--intertime__wrcc1) 8 ) ) (done--315) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--315)) (done--316) )
)

(:action simulate--negative--keepinter__wrcc1_stage2__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrcc1_stage2) 
						(< 
							(copy--intertime__wrcc1) 8 ) )) (done--315) )
 :effect 
	(and 
		
			(not (done--315)) (done--316) )
)

(:action simulate--positive--keepinter__wrcc1_stage3__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrcc1_stage3) 
					(< 
						(copy--intertime__wrcc1) 8 ) ) (done--316) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--316)) (done--317) )
)

(:action simulate--negative--keepinter__wrcc1_stage3__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrcc1_stage3) 
						(< 
							(copy--intertime__wrcc1) 8 ) )) (done--316) )
 :effect 
	(and 
		
			(not (done--316)) (done--317) )
)

(:action simulate--positive--keepinter__wrcc1_stage4__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrcc1_stage4) 
					(< 
						(copy--intertime__wrcc1) 11 ) ) (done--317) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--317)) (done--318) )
)

(:action simulate--negative--keepinter__wrcc1_stage4__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrcc1_stage4) 
						(< 
							(copy--intertime__wrcc1) 11 ) )) (done--317) )
 :effect 
	(and 
		
			(not (done--317)) (done--318) )
)

(:action simulate--positive--keepinter__wrcc1_stage5__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrcc1_stage5) 
					(< 
						(copy--intertime__wrcc1) 3 ) ) (done--318) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--318)) (done--319) )
)

(:action simulate--negative--keepinter__wrcc1_stage5__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrcc1_stage5) 
						(< 
							(copy--intertime__wrcc1) 3 ) )) (done--318) )
 :effect 
	(and 
		
			(not (done--318)) (done--319) )
)

(:action simulate--positive--keepinter__wrcc1_stage6__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrcc1_stage6) 
					(< 
						(copy--intertime__wrcc1) 2 ) ) (done--319) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrcc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--319)) (done--320) )
)

(:action simulate--negative--keepinter__wrcc1_stage6__wrcc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrcc1_stage6) 
						(< 
							(copy--intertime__wrcc1) 2 ) )) (done--319) )
 :effect 
	(and 
		
			(not (done--319)) (done--320) )
)

(:action simulate--positive--keepinter__wrdc1_stage1__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrdc1_stage1) 
					(< 
						(copy--intertime__wrdc1) 8 ) ) (done--320) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrdc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--320)) (done--321) )
)

(:action simulate--negative--keepinter__wrdc1_stage1__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrdc1_stage1) 
						(< 
							(copy--intertime__wrdc1) 8 ) )) (done--320) )
 :effect 
	(and 
		
			(not (done--320)) (done--321) )
)

(:action simulate--positive--keepinter__wrdc1_stage2__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrdc1_stage2) 
					(< 
						(copy--intertime__wrdc1) 8 ) ) (done--321) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrdc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--321)) (done--322) )
)

(:action simulate--negative--keepinter__wrdc1_stage2__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrdc1_stage2) 
						(< 
							(copy--intertime__wrdc1) 8 ) )) (done--321) )
 :effect 
	(and 
		
			(not (done--321)) (done--322) )
)

(:action simulate--positive--keepinter__wrdc1_stage3__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrdc1_stage3) 
					(< 
						(copy--intertime__wrdc1) 6 ) ) (done--322) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrdc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--322)) (done--323) )
)

(:action simulate--negative--keepinter__wrdc1_stage3__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrdc1_stage3) 
						(< 
							(copy--intertime__wrdc1) 6 ) )) (done--322) )
 :effect 
	(and 
		
			(not (done--322)) (done--323) )
)

(:action simulate--positive--keepinter__wrdc1_stage4__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrdc1_stage4) 
					(< 
						(copy--intertime__wrdc1) 7 ) ) (done--323) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrdc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--323)) (done--324) )
)

(:action simulate--negative--keepinter__wrdc1_stage4__wrdc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrdc1_stage4) 
						(< 
							(copy--intertime__wrdc1) 7 ) )) (done--323) )
 :effect 
	(and 
		
			(not (done--323)) (done--324) )
)

(:action simulate--positive--keepinter__wrec1_stage1__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrec1_stage1) 
					(< 
						(copy--intertime__wrec1) 3 ) ) (done--324) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrec1) 
					(* 
						(delta) 1 ) ) 
			(not (done--324)) (done--325) )
)

(:action simulate--negative--keepinter__wrec1_stage1__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrec1_stage1) 
						(< 
							(copy--intertime__wrec1) 3 ) )) (done--324) )
 :effect 
	(and 
		
			(not (done--324)) (done--325) )
)

(:action simulate--positive--keepinter__wrec1_stage2__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrec1_stage2) 
					(< 
						(copy--intertime__wrec1) 7 ) ) (done--325) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrec1) 
					(* 
						(delta) 1 ) ) 
			(not (done--325)) (done--326) )
)

(:action simulate--negative--keepinter__wrec1_stage2__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrec1_stage2) 
						(< 
							(copy--intertime__wrec1) 7 ) )) (done--325) )
 :effect 
	(and 
		
			(not (done--325)) (done--326) )
)

(:action simulate--positive--keepinter__wrec1_stage3__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrec1_stage3) 
					(< 
						(copy--intertime__wrec1) 7 ) ) (done--326) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrec1) 
					(* 
						(delta) 1 ) ) 
			(not (done--326)) (done--327) )
)

(:action simulate--negative--keepinter__wrec1_stage3__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrec1_stage3) 
						(< 
							(copy--intertime__wrec1) 7 ) )) (done--326) )
 :effect 
	(and 
		
			(not (done--326)) (done--327) )
)

(:action simulate--positive--keepinter__wrec1_stage4__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrec1_stage4) 
					(< 
						(copy--intertime__wrec1) 7 ) ) (done--327) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrec1) 
					(* 
						(delta) 1 ) ) 
			(not (done--327)) (done--328) )
)

(:action simulate--negative--keepinter__wrec1_stage4__wrec1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrec1_stage4) 
						(< 
							(copy--intertime__wrec1) 7 ) )) (done--327) )
 :effect 
	(and 
		
			(not (done--327)) (done--328) )
)

(:action simulate--positive--keepinter__wrfc1_stage1__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrfc1_stage1) 
					(< 
						(copy--intertime__wrfc1) 5 ) ) (done--328) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrfc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--328)) (done--329) )
)

(:action simulate--negative--keepinter__wrfc1_stage1__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrfc1_stage1) 
						(< 
							(copy--intertime__wrfc1) 5 ) )) (done--328) )
 :effect 
	(and 
		
			(not (done--328)) (done--329) )
)

(:action simulate--positive--keepinter__wrfc1_stage2__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrfc1_stage2) 
					(< 
						(copy--intertime__wrfc1) 9 ) ) (done--329) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrfc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--329)) (done--330) )
)

(:action simulate--negative--keepinter__wrfc1_stage2__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrfc1_stage2) 
						(< 
							(copy--intertime__wrfc1) 9 ) )) (done--329) )
 :effect 
	(and 
		
			(not (done--329)) (done--330) )
)

(:action simulate--positive--keepinter__wrfc1_stage3__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				(inter__wrfc1_stage3) 
					(< 
						(copy--intertime__wrfc1) 8 ) ) (done--330) )
 :effect 
	(and 
		
			(increase 
				(intertime__wrfc1) 
					(* 
						(delta) 1 ) ) 
			(not (done--330)) (done--331) )
)

(:action simulate--negative--keepinter__wrfc1_stage3__wrfc1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					(inter__wrfc1_stage3) 
						(< 
							(copy--intertime__wrfc1) 8 ) )) (done--330) )
 :effect 
	(and 
		
			(not (done--330)) (done--331) )
)

(:action end-simulate-processes
 :parameters()
 :precondition 
	(and 
		(pause) (done--331) )
 :effect 
	(and 
		
			(not (pause)) 
			(not (done--331)) (force-events) )
)

)