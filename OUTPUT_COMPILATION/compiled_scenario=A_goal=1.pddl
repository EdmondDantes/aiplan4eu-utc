(define (problem grounded-huddersfield)
(:domain grounded-urbantraffic)
(:init
	
	(controllable__wrac1) (controllable__wrbc1) (controllable__wrcc1) (controllable__wrdc1) (controllable__wrec1) (controllable__wrfc1) 
		(= 
			(capacity__outside) 100000.0 ) 
		(= 
			(capacity__hsac3_c_wrac1) 100000.0 ) 
		(= 
			(capacity__wrac1_z_hsac1) 100000.0 ) 
		(= 
			(capacity__wrac1_y_wrbc1) 55.5 ) 
		(= 
			(capacity__wrac1_m_stand) 100000.0 ) 
		(= 
			(capacity__stand_f_wrac1) 100000.0 ) 
		(= 
			(capacity__wrac1_n_firth) 100000.0 ) 
		(= 
			(capacity__firth_d_wrac1) 100000.0 ) 
		(= 
			(capacity__wrac1_x_wrbc1) 24.5 ) 
		(= 
			(capacity__wrbc1_a_wrac1) 85.0 ) 
		(= 
			(capacity__wrbc1_b_wrcc1) 30.66 ) 
		(= 
			(capacity__wrbc1_r_silve) 100000.0 ) 
		(= 
			(capacity__silve_w_wrbc1) 100000.0 ) 
		(= 
			(capacity__wrbc1_s_somer) 100000.0 ) 
		(= 
			(capacity__somer_v_wrbc1) 100000.0 ) 
		(= 
			(capacity__wrbc1_b_wrac1) 12.0 ) 
		(= 
			(capacity__wrcc1_z_wrbc1) 56.0 ) 
		(= 
			(capacity__wrcc1_x_wrdc1) 93.3 ) 
		(= 
			(capacity__smith_c_wrcc1) 100000.0 ) 
		(= 
			(capacity__wrdc1_a_wrcc1) 42.0 ) 
		(= 
			(capacity__wrdc1_b_wrec1) 26.67 ) 
		(= 
			(capacity__wrdc1_q_abnor) 100000.0 ) 
		(= 
			(capacity__wrcc1_w_wrdc1) 5.0 ) 
		(= 
			(capacity__wrdc1_l_absou) 100000.0 ) 
		(= 
			(capacity__abnor_v_wrdc1) 100000.0 ) 
		(= 
			(capacity__wrec1_z_wrdc1) 28.67 ) 
		(= 
			(capacity__wrec1_y_wrfc1) 13.0 ) 
		(= 
			(capacity__oldwa_c_wrec1) 100000.0 ) 
		(= 
			(capacity__wrec1_y_wrdc1) 6.33 ) 
		(= 
			(capacity__oldwa_d_wrec1) 100000.0 ) 
		(= 
			(capacity__wakef_z_wrfc1) 100000.0 ) 
		(= 
			(capacity__wrfc1_a_wrec1) 17.4 ) 
		(= 
			(capacity__wrfc1_r_broad) 100000.0 ) 
		(= 
			(capacity__broad_x_wrfc1) 100000.0 ) 
		(= 
			(capacity__wrfc1_t_wakef) 100000.0 ) (contains__wrac1__wrac1_stage1) (contains__wrac1__wrac1_stage2) (contains__wrac1__wrac1_stage3) (contains__wrac1__wrac1_stage4) (contains__wrbc1__wrbc1_stage1) (contains__wrbc1__wrbc1_stage2) (contains__wrbc1__wrbc1_stage3) (contains__wrbc1__wrbc1_stage4) (contains__wrbc1__wrbc1_stage5) (contains__wrcc1__wrcc1_stage1) (contains__wrcc1__wrcc1_stage2) (contains__wrcc1__wrcc1_stage3) (contains__wrcc1__wrcc1_stage4) (contains__wrcc1__wrcc1_stage5) (contains__wrcc1__wrcc1_stage6) (contains__wrdc1__wrdc1_stage1) (contains__wrdc1__wrdc1_stage2) (contains__wrdc1__wrdc1_stage3) (contains__wrdc1__wrdc1_stage4) (contains__wrec1__wrec1_stage1) (contains__wrec1__wrec1_stage2) (contains__wrec1__wrec1_stage3) (contains__wrec1__wrec1_stage4) (contains__wrfc1__wrfc1_stage1) (contains__wrfc1__wrfc1_stage2) (contains__wrfc1__wrfc1_stage3) 
		(= 
			(interlimit__wrac1_stage1) 5 ) 
		(= 
			(interlimit__wrac1_stage2) 6 ) 
		(= 
			(interlimit__wrac1_stage3) 5 ) 
		(= 
			(interlimit__wrac1_stage4) 5 ) 
		(= 
			(interlimit__wrbc1_stage1) 6 ) 
		(= 
			(interlimit__wrbc1_stage2) 12 ) 
		(= 
			(interlimit__wrbc1_stage3) 6 ) 
		(= 
			(interlimit__wrbc1_stage4) 7 ) 
		(= 
			(interlimit__wrbc1_stage5) 12 ) 
		(= 
			(interlimit__wrcc1_stage1) 6 ) 
		(= 
			(interlimit__wrcc1_stage2) 8 ) 
		(= 
			(interlimit__wrcc1_stage3) 8 ) 
		(= 
			(interlimit__wrcc1_stage4) 11 ) 
		(= 
			(interlimit__wrcc1_stage5) 3 ) 
		(= 
			(interlimit__wrcc1_stage6) 2 ) 
		(= 
			(interlimit__wrdc1_stage1) 8 ) 
		(= 
			(interlimit__wrdc1_stage2) 8 ) 
		(= 
			(interlimit__wrdc1_stage3) 6 ) 
		(= 
			(interlimit__wrdc1_stage4) 7 ) 
		(= 
			(interlimit__wrec1_stage1) 3 ) 
		(= 
			(interlimit__wrec1_stage2) 7 ) 
		(= 
			(interlimit__wrec1_stage3) 7 ) 
		(= 
			(interlimit__wrec1_stage4) 7 ) 
		(= 
			(interlimit__wrfc1_stage1) 5 ) 
		(= 
			(interlimit__wrfc1_stage2) 9 ) 
		(= 
			(interlimit__wrfc1_stage3) 8 ) (next__wrac1_stage1__wrac1_stage2) (next__wrac1_stage2__wrac1_stage3) (next__wrac1_stage3__wrac1_stage4) (next__wrac1_stage4__wrac1_stage1) (next__wrbc1_stage1__wrbc1_stage2) (next__wrbc1_stage2__wrbc1_stage3) (next__wrbc1_stage3__wrbc1_stage4) (next__wrbc1_stage4__wrbc1_stage5) (next__wrbc1_stage5__wrbc1_stage1) (next__wrcc1_stage1__wrcc1_stage2) (next__wrcc1_stage2__wrcc1_stage3) (next__wrcc1_stage3__wrcc1_stage4) (next__wrcc1_stage4__wrcc1_stage5) (next__wrcc1_stage5__wrcc1_stage6) (next__wrcc1_stage6__wrcc1_stage1) (next__wrdc1_stage1__wrdc1_stage2) (next__wrdc1_stage2__wrdc1_stage3) (next__wrdc1_stage3__wrdc1_stage4) (next__wrdc1_stage4__wrdc1_stage1) (next__wrec1_stage1__wrec1_stage2) (next__wrec1_stage2__wrec1_stage3) (next__wrec1_stage3__wrec1_stage4) (next__wrec1_stage4__wrec1_stage1) (next__wrfc1_stage1__wrfc1_stage2) (next__wrfc1_stage2__wrfc1_stage3) (next__wrfc1_stage3__wrfc1_stage1) 
		(= 
			(mingreentime__wrac1_stage1) 10 ) 
		(= 
			(maxgreentime__wrac1_stage1) 120 ) 
		(= 
			(mingreentime__wrac1_stage2) 9 ) 
		(= 
			(maxgreentime__wrac1_stage2) 120 ) 
		(= 
			(mingreentime__wrac1_stage3) 8 ) 
		(= 
			(maxgreentime__wrac1_stage3) 120 ) 
		(= 
			(mingreentime__wrac1_stage4) 3 ) 
		(= 
			(maxgreentime__wrac1_stage4) 120 ) 
		(= 
			(mingreentime__wrbc1_stage1) 7 ) 
		(= 
			(maxgreentime__wrbc1_stage1) 120 ) 
		(= 
			(mingreentime__wrbc1_stage2) 5 ) 
		(= 
			(maxgreentime__wrbc1_stage2) 120 ) 
		(= 
			(mingreentime__wrbc1_stage3) 10 ) 
		(= 
			(maxgreentime__wrbc1_stage3) 120 ) 
		(= 
			(mingreentime__wrbc1_stage4) 5 ) 
		(= 
			(maxgreentime__wrbc1_stage4) 120 ) 
		(= 
			(mingreentime__wrbc1_stage5) 2 ) 
		(= 
			(maxgreentime__wrbc1_stage5) 120 ) 
		(= 
			(mingreentime__wrcc1_stage1) 7 ) 
		(= 
			(maxgreentime__wrcc1_stage1) 120 ) 
		(= 
			(mingreentime__wrcc1_stage2) 5 ) 
		(= 
			(maxgreentime__wrcc1_stage2) 120 ) 
		(= 
			(mingreentime__wrcc1_stage3) 5 ) 
		(= 
			(maxgreentime__wrcc1_stage3) 120 ) 
		(= 
			(mingreentime__wrcc1_stage4) 4 ) 
		(= 
			(maxgreentime__wrcc1_stage4) 120 ) 
		(= 
			(mingreentime__wrcc1_stage5) 7 ) 
		(= 
			(maxgreentime__wrcc1_stage5) 120 ) 
		(= 
			(mingreentime__wrcc1_stage6) 2 ) 
		(= 
			(maxgreentime__wrcc1_stage6) 120 ) 
		(= 
			(mingreentime__wrdc1_stage1) 7 ) 
		(= 
			(maxgreentime__wrdc1_stage1) 120 ) 
		(= 
			(mingreentime__wrdc1_stage2) 7 ) 
		(= 
			(maxgreentime__wrdc1_stage2) 120 ) 
		(= 
			(mingreentime__wrdc1_stage3) 7 ) 
		(= 
			(maxgreentime__wrdc1_stage3) 120 ) 
		(= 
			(mingreentime__wrdc1_stage4) 7 ) 
		(= 
			(maxgreentime__wrdc1_stage4) 120 ) 
		(= 
			(mingreentime__wrec1_stage1) 10 ) 
		(= 
			(maxgreentime__wrec1_stage1) 120 ) 
		(= 
			(mingreentime__wrec1_stage2) 2 ) 
		(= 
			(maxgreentime__wrec1_stage2) 120 ) 
		(= 
			(mingreentime__wrec1_stage3) 6 ) 
		(= 
			(maxgreentime__wrec1_stage3) 120 ) 
		(= 
			(mingreentime__wrec1_stage4) 6 ) 
		(= 
			(maxgreentime__wrec1_stage4) 120 ) 
		(= 
			(mingreentime__wrfc1_stage1) 10 ) 
		(= 
			(maxgreentime__wrfc1_stage1) 120 ) 
		(= 
			(mingreentime__wrfc1_stage2) 4 ) 
		(= 
			(maxgreentime__wrfc1_stage2) 120 ) 
		(= 
			(mingreentime__wrfc1_stage3) 7 ) 
		(= 
			(maxgreentime__wrfc1_stage3) 120 ) 
		(= 
			(defaultgreentime__wrac1_stage1) 45 ) 
		(= 
			(defaultgreentime__wrac1_stage2) 14 ) 
		(= 
			(defaultgreentime__wrac1_stage3) 19 ) 
		(= 
			(defaultgreentime__wrac1_stage4) 10 ) 
		(= 
			(defaultgreentime__wrbc1_stage1) 47 ) 
		(= 
			(defaultgreentime__wrbc1_stage2) 15 ) 
		(= 
			(defaultgreentime__wrbc1_stage3) 26 ) 
		(= 
			(defaultgreentime__wrbc1_stage4) 7 ) 
		(= 
			(defaultgreentime__wrbc1_stage5) 7 ) 
		(= 
			(defaultgreentime__wrcc1_stage1) 50 ) 
		(= 
			(defaultgreentime__wrcc1_stage2) 7 ) 
		(= 
			(defaultgreentime__wrcc1_stage3) 20 ) 
		(= 
			(defaultgreentime__wrcc1_stage4) 7 ) 
		(= 
			(defaultgreentime__wrcc1_stage5) 18 ) 
		(= 
			(defaultgreentime__wrcc1_stage6) 7 ) 
		(= 
			(defaultgreentime__wrdc1_stage1) 41 ) 
		(= 
			(defaultgreentime__wrdc1_stage2) 17 ) 
		(= 
			(defaultgreentime__wrdc1_stage3) 15 ) 
		(= 
			(defaultgreentime__wrdc1_stage4) 15 ) 
		(= 
			(defaultgreentime__wrec1_stage1) 72 ) 
		(= 
			(defaultgreentime__wrec1_stage2) 7 ) 
		(= 
			(defaultgreentime__wrec1_stage3) 16 ) 
		(= 
			(defaultgreentime__wrec1_stage4) 7 ) 
		(= 
			(defaultgreentime__wrfc1_stage1) 67 ) 
		(= 
			(defaultgreentime__wrfc1_stage2) 9 ) 
		(= 
			(defaultgreentime__wrfc1_stage3) 12 ) 
		(= 
			(counter__abnor_v_wrdc1) 0.0 ) 
		(= 
			(counter__broad_x_wrfc1) 0.0 ) 
		(= 
			(counter__firth_d_wrac1) 0.0 ) 
		(= 
			(counter__hsac3_c_wrac1) 0.0 ) 
		(= 
			(counter__oldwa_c_wrec1) 0.0 ) 
		(= 
			(counter__oldwa_d_wrec1) 0.0 ) 
		(= 
			(counter__silve_w_wrbc1) 0.0 ) 
		(= 
			(counter__smith_c_wrcc1) 0.0 ) 
		(= 
			(counter__somer_v_wrbc1) 0.0 ) 
		(= 
			(counter__stand_f_wrac1) 0.0 ) 
		(= 
			(counter__wakef_z_wrfc1) 0.0 ) 
		(= 
			(counter__wrac1_m_stand) 0.0 ) 
		(= 
			(counter__wrac1_n_firth) 0.0 ) 
		(= 
			(counter__wrac1_x_wrbc1) 0.0 ) 
		(= 
			(counter__wrac1_y_wrbc1) 0.0 ) 
		(= 
			(counter__wrac1_z_hsac1) 0.0 ) 
		(= 
			(counter__wrbc1_a_wrac1) 0.0 ) 
		(= 
			(counter__wrbc1_b_wrac1) 0.0 ) 
		(= 
			(counter__wrbc1_b_wrcc1) 0.0 ) 
		(= 
			(counter__wrbc1_r_silve) 0.0 ) 
		(= 
			(counter__wrbc1_s_somer) 0.0 ) 
		(= 
			(counter__wrcc1_w_wrdc1) 0.0 ) 
		(= 
			(counter__wrcc1_x_wrdc1) 0.0 ) 
		(= 
			(counter__wrcc1_z_wrbc1) 0.0 ) 
		(= 
			(counter__wrdc1_a_wrcc1) 0.0 ) 
		(= 
			(counter__wrdc1_b_wrec1) 0.0 ) 
		(= 
			(counter__wrdc1_l_absou) 0.0 ) 
		(= 
			(counter__wrdc1_q_abnor) 0.0 ) 
		(= 
			(counter__wrec1_y_wrdc1) 0.0 ) 
		(= 
			(counter__wrec1_y_wrfc1) 0.0 ) 
		(= 
			(counter__wrec1_z_wrdc1) 0.0 ) 
		(= 
			(counter__wrfc1_a_wrec1) 0.0 ) 
		(= 
			(counter__wrfc1_r_broad) 0.0 ) 
		(= 
			(counter__wrfc1_t_wakef) 0.0 ) 
		(= 
			(cycletime__wrac1) 88 ) 
		(= 
			(cycletime__wrbc1) 102 ) 
		(= 
			(cycletime__wrcc1) 109 ) 
		(= 
			(cycletime__wrdc1) 88 ) 
		(= 
			(cycletime__wrec1) 102 ) 
		(= 
			(cycletime__wrfc1) 102 ) 
		(= 
			(granularity) 10.0 ) 
		(= 
			(maxcycletime__wrac1) 200 ) 
		(= 
			(maxcycletime__wrbc1) 200 ) 
		(= 
			(maxcycletime__wrcc1) 200 ) 
		(= 
			(maxcycletime__wrdc1) 200 ) 
		(= 
			(maxcycletime__wrec1) 200 ) 
		(= 
			(maxcycletime__wrfc1) 200 ) 
		(= 
			(mincycletime__wrac1) 50 ) 
		(= 
			(mincycletime__wrbc1) 50 ) 
		(= 
			(mincycletime__wrcc1) 50 ) 
		(= 
			(mincycletime__wrdc1) 50 ) 
		(= 
			(mincycletime__wrec1) 50 ) 
		(= 
			(mincycletime__wrfc1) 50 ) 
		(= 
			(time) 0 ) 
		(= 
			(turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1) 0.294 ) 
		(= 
			(turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand) 0.178 ) 
		(= 
			(turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1) 0.704 ) 
		(= 
			(turnrate__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1) 0.844 ) 
		(= 
			(turnrate__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth) 0.097 ) 
		(= 
			(turnrate__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1) 0.844 ) 
		(= 
			(turnrate__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth) 0.097 ) 
		(= 
			(turnrate__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand) 0.412 ) 
		(= 
			(turnrate__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1) 0.592 ) 
		(= 
			(turnrate__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve) 0.114 ) 
		(= 
			(turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1) 0.904 ) 
		(= 
			(turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1) 0.22 ) 
		(= 
			(turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve) 0.026 ) 
		(= 
			(turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer) 0.027 ) 
		(= 
			(turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1) 0.182 ) 
		(= 
			(turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1) 0.068 ) 
		(= 
			(turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1) 0.093 ) 
		(= 
			(turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer) 0.07 ) 
		(= 
			(turnrate__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1) 0.592 ) 
		(= 
			(turnrate__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve) 0.114 ) 
		(= 
			(turnrate__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer) 0.412 ) 
		(= 
			(turnrate__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1) 0.478 ) 
		(= 
			(turnrate__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1) 0.111 ) 
		(= 
			(turnrate__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer) 0.412 ) 
		(= 
			(turnrate__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1) 0.478 ) 
		(= 
			(turnrate__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1) 0.111 ) 
		(= 
			(turnrate__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1) 0.592 ) 
		(= 
			(turnrate__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve) 0.114 ) 
		(= 
			(turnrate__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer) 0.412 ) 
		(= 
			(turnrate__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1) 0.467 ) 
		(= 
			(turnrate__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1) 0.003 ) 
		(= 
			(turnrate__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1) 0.824 ) 
		(= 
			(turnrate__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1) 0.824 ) 
		(= 
			(turnrate__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1) 0.467 ) 
		(= 
			(turnrate__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1) 0.003 ) 
		(= 
			(turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1) 0.001 ) 
		(= 
			(turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1) 0.468 ) 
		(= 
			(turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1) 0.001 ) 
		(= 
			(turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1) 0.001 ) 
		(= 
			(turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1) 0.468 ) 
		(= 
			(turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1) 0.001 ) 
		(= 
			(turnrate__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1) 0.821 ) 
		(= 
			(turnrate__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou) 0.002 ) 
		(= 
			(turnrate__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1) 0.867 ) 
		(= 
			(turnrate__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor) 0.133 ) 
		(= 
			(turnrate__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1) 0.867 ) 
		(= 
			(turnrate__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor) 0.133 ) 
		(= 
			(turnrate__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou) 0.353 ) 
		(= 
			(turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1) 0.283 ) 
		(= 
			(turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1) 0.215 ) 
		(= 
			(turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou) 0.03 ) 
		(= 
			(turnrate__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1) 0.821 ) 
		(= 
			(turnrate__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou) 0.002 ) 
		(= 
			(turnrate__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor) 0.353 ) 
		(= 
			(turnrate__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1) 0.774 ) 
		(= 
			(turnrate__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1) 0.049 ) 
		(= 
			(turnrate__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1) 0.824 ) 
		(= 
			(turnrate__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1) 0.824 ) 
		(= 
			(turnrate__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1) 0.332 ) 
		(= 
			(turnrate__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1) 0.021 ) 
		(= 
			(turnrate__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1) 0.353 ) 
		(= 
			(turnrate__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1) 0.774 ) 
		(= 
			(turnrate__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1) 0.049 ) 
		(= 
			(turnrate__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1) 0.353 ) 
		(= 
			(turnrate__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1) 0.765 ) 
		(= 
			(turnrate__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad) 0.249 ) 
		(= 
			(turnrate__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef) 0.575 ) 
		(= 
			(turnrate__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad) 0.249 ) 
		(= 
			(turnrate__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef) 0.575 ) 
		(= 
			(turnrate__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1) 0.586 ) 
		(= 
			(turnrate__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef) 0.002 ) 
		(= 
			(turnrate__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1) 0.118 ) 
		(= 
			(turnrate__wrac1_stage3__firth_d_wrac1__wrac1_m_stand) 0.294 ) 
		(= 
			(turnrate__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1) 0.057 ) 
		(= 
			(turnrate__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1) 0.165 ) 
		(= 
			(turnrate__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1) 0.049 ) 
		(= 
			(turnrate__wrac1_stage4__firth_d_wrac1__wrac1_m_stand) 0.141 ) 
		(= 
			(turnrate__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1) 0.372 ) 
		(= 
			(turnrate__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1) 0.154 ) 
		(= 
			(turnrate__wrac1_stage3__stand_f_wrac1__wrac1_n_firth) 0.339 ) 
		(= 
			(turnrate__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1) 0.159 ) 
		(= 
			(turnrate__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1) 0.089 ) 
		(= 
			(turnrate__fake__outside__abnor_v_wrdc1) 0.0736 ) 
		(= 
			(turnrate__fake__outside__broad_x_wrfc1) 0.1798 ) 
		(= 
			(turnrate__fake__outside__firth_d_wrac1) 0.0686 ) 
		(= 
			(turnrate__fake__outside__hsac3_c_wrac1) 0.3376 ) 
		(= 
			(turnrate__fake__outside__oldwa_c_wrec1) 0.0064 ) 
		(= 
			(turnrate__fake__outside__oldwa_d_wrec1) 0.02 ) 
		(= 
			(turnrate__fake__outside__silve_w_wrbc1) 0.0546 ) 
		(= 
			(turnrate__fake__outside__smith_c_wrcc1) 0.0685 ) 
		(= 
			(turnrate__fake__outside__somer_v_wrbc1) 0.0774 ) 
		(= 
			(turnrate__fake__outside__stand_f_wrac1) 0.2258 ) 
		(= 
			(turnrate__fake__outside__wakef_z_wrfc1) 0.2116 ) 
		(= 
			(min_occ__wrac1_y_wrbc1) 15.866 ) 
		(= 
			(min_occ__wrac1_x_wrbc1) 2.318 ) 
		(= 
			(min_occ__wrbc1_a_wrac1) 23.0795 ) 
		(= 
			(min_occ__wrbc1_b_wrcc1) 7.4091 ) 
		(= 
			(min_occ__wrbc1_b_wrac1) 0.8363 ) 
		(= 
			(min_occ__wrcc1_z_wrbc1) 14.741 ) 
		(= 
			(min_occ__wrcc1_x_wrdc1) 32.5227 ) 
		(= 
			(min_occ__wrcc1_w_wrdc1) 0.1893 ) 
		(= 
			(min_occ__wrdc1_a_wrcc1) 12.7607 ) 
		(= 
			(min_occ__wrdc1_b_wrec1) 9.3493 ) 
		(= 
			(min_occ__wrec1_z_wrdc1) 8.7141 ) 
		(= 
			(min_occ__wrec1_y_wrfc1) 4.0929 ) 
		(= 
			(min_occ__wrec1_y_wrdc1) 0.062 ) 
		(= 
			(min_occ__wrfc1_a_wrec1) 0.1 ) 
		(= 
			(min_occ__hsac3_c_wrac1) 14.8544 ) 
		(= 
			(min_occ__stand_f_wrac1) 9.7546 ) 
		(= 
			(min_occ__firth_d_wrac1) 1.5185 ) 
		(= 
			(min_occ__silve_w_wrbc1) 0.5971 ) 
		(= 
			(min_occ__somer_v_wrbc1) 3.4366 ) 
		(= 
			(min_occ__smith_c_wrcc1) 0.7743 ) 
		(= 
			(min_occ__abnor_v_wrdc1) 8.7337 ) 
		(= 
			(min_occ__oldwa_c_wrec1) 0.0631 ) 
		(= 
			(min_occ__oldwa_d_wrec1) 0.2133 ) 
		(= 
			(min_occ__wakef_z_wrfc1) 4.0627 ) 
		(= 
			(min_occ__broad_x_wrfc1) 1.9663 ) 
		(= 
			(min_occ__outside) 1.0 ) 
		(= 
			(occupancy__hsac3_c_wrac1) 20.59 ) 
		(= 
			(occupancy__wrac1_y_wrbc1) 15.87 ) 
		(= 
			(occupancy__stand_f_wrac1) 16.56 ) 
		(= 
			(occupancy__firth_d_wrac1) 5.09 ) 
		(= 
			(occupancy__wrac1_x_wrbc1) 6.59 ) 
		(= 
			(occupancy__wrbc1_a_wrac1) 23.28 ) 
		(= 
			(occupancy__wrbc1_b_wrcc1) 9.78 ) 
		(= 
			(occupancy__silve_w_wrbc1) 5.02 ) 
		(= 
			(occupancy__somer_v_wrbc1) 6.76 ) 
		(= 
			(occupancy__wrbc1_b_wrac1) 2.75 ) 
		(= 
			(occupancy__wrcc1_z_wrbc1) 23.28 ) 
		(= 
			(occupancy__wrcc1_x_wrdc1) 80.01 ) 
		(= 
			(occupancy__smith_c_wrcc1) 5.12 ) 
		(= 
			(occupancy__wrcc1_w_wrdc1) 0.25 ) 
		(= 
			(occupancy__wrdc1_a_wrcc1) 12.76 ) 
		(= 
			(occupancy__wrdc1_b_wrec1) 26.67 ) 
		(= 
			(occupancy__abnor_v_wrdc1) 12.19 ) 
		(= 
			(occupancy__wrec1_z_wrdc1) 8.71 ) 
		(= 
			(occupancy__wrec1_y_wrfc1) 4.09 ) 
		(= 
			(occupancy__oldwa_c_wrec1) 0.15 ) 
		(= 
			(occupancy__wrec1_y_wrdc1) 0.42 ) 
		(= 
			(occupancy__oldwa_d_wrec1) 0.49 ) 
		(= 
			(occupancy__wakef_z_wrfc1) 4.06 ) 
		(= 
			(occupancy__wrfc1_a_wrec1) 0.88 ) 
		(= 
			(occupancy__broad_x_wrfc1) 9.09 ) 
		(= 
			(occupancy__wrac1_z_hsac1) 0.0 ) 
		(= 
			(occupancy__wrac1_m_stand) 0.0 ) 
		(= 
			(occupancy__wrac1_n_firth) 0.0 ) 
		(= 
			(occupancy__wrbc1_r_silve) 0.0 ) 
		(= 
			(occupancy__wrbc1_s_somer) 0.0 ) 
		(= 
			(occupancy__wrdc1_q_abnor) 0.0 ) 
		(= 
			(occupancy__wrdc1_l_absou) 0.0 ) 
		(= 
			(occupancy__wrfc1_r_broad) 0.0 ) 
		(= 
			(occupancy__wrfc1_t_wakef) 0.0 ) 
		(= 
			(occupancy__outside) 50000.0 ) (active__wrac1_stage2) 
		(= 
			(greentime__wrac1) 13 ) 
		(= 
			(intertime__wrac1) 0 ) 
		(= 
			(greentime__wrbc1) 0 ) 
		(= 
			(intertime__wrbc1) 1 ) (inter__wrbc1_stage1) (active__wrcc1_stage1) 
		(= 
			(greentime__wrcc1) 39 ) 
		(= 
			(intertime__wrcc1) 0 ) (active__wrdc1_stage1) 
		(= 
			(greentime__wrdc1) 15 ) 
		(= 
			(intertime__wrdc1) 0 ) (active__wrec1_stage1) 
		(= 
			(greentime__wrec1) 15 ) 
		(= 
			(intertime__wrec1) 0 ) (active__wrfc1_stage1) 
		(= 
			(greentime__wrfc1) 27 ) 
		(= 
			(intertime__wrfc1) 0 ) (active__fake) 
		(= 
			(delta) 1 ) 
		(= 
			(copy--capacity__outside) 100000.0 ) 
		(= 
			(copy--capacity__hsac3_c_wrac1) 100000.0 ) 
		(= 
			(copy--capacity__wrac1_z_hsac1) 100000.0 ) 
		(= 
			(copy--capacity__wrac1_y_wrbc1) 55.5 ) 
		(= 
			(copy--capacity__wrac1_m_stand) 100000.0 ) 
		(= 
			(copy--capacity__stand_f_wrac1) 100000.0 ) 
		(= 
			(copy--capacity__wrac1_n_firth) 100000.0 ) 
		(= 
			(copy--capacity__firth_d_wrac1) 100000.0 ) 
		(= 
			(copy--capacity__wrac1_x_wrbc1) 24.5 ) 
		(= 
			(copy--capacity__wrbc1_a_wrac1) 85.0 ) 
		(= 
			(copy--capacity__wrbc1_b_wrcc1) 30.66 ) 
		(= 
			(copy--capacity__wrbc1_r_silve) 100000.0 ) 
		(= 
			(copy--capacity__silve_w_wrbc1) 100000.0 ) 
		(= 
			(copy--capacity__wrbc1_s_somer) 100000.0 ) 
		(= 
			(copy--capacity__somer_v_wrbc1) 100000.0 ) 
		(= 
			(copy--capacity__wrbc1_b_wrac1) 12.0 ) 
		(= 
			(copy--capacity__wrcc1_z_wrbc1) 56.0 ) 
		(= 
			(copy--capacity__wrcc1_x_wrdc1) 93.3 ) 
		(= 
			(copy--capacity__smith_c_wrcc1) 100000.0 ) 
		(= 
			(copy--capacity__wrdc1_a_wrcc1) 42.0 ) 
		(= 
			(copy--capacity__wrdc1_b_wrec1) 26.67 ) 
		(= 
			(copy--capacity__wrdc1_q_abnor) 100000.0 ) 
		(= 
			(copy--capacity__wrcc1_w_wrdc1) 5.0 ) 
		(= 
			(copy--capacity__wrdc1_l_absou) 100000.0 ) 
		(= 
			(copy--capacity__abnor_v_wrdc1) 100000.0 ) 
		(= 
			(copy--capacity__wrec1_z_wrdc1) 28.67 ) 
		(= 
			(copy--capacity__wrec1_y_wrfc1) 13.0 ) 
		(= 
			(copy--capacity__oldwa_c_wrec1) 100000.0 ) 
		(= 
			(copy--capacity__wrec1_y_wrdc1) 6.33 ) 
		(= 
			(copy--capacity__oldwa_d_wrec1) 100000.0 ) 
		(= 
			(copy--capacity__wakef_z_wrfc1) 100000.0 ) 
		(= 
			(copy--capacity__wrfc1_a_wrec1) 17.4 ) 
		(= 
			(copy--capacity__wrfc1_r_broad) 100000.0 ) 
		(= 
			(copy--capacity__broad_x_wrfc1) 100000.0 ) 
		(= 
			(copy--capacity__wrfc1_t_wakef) 100000.0 ) 
		(= 
			(copy--interlimit__wrac1_stage1) 5 ) 
		(= 
			(copy--interlimit__wrac1_stage2) 6 ) 
		(= 
			(copy--interlimit__wrac1_stage3) 5 ) 
		(= 
			(copy--interlimit__wrac1_stage4) 5 ) 
		(= 
			(copy--interlimit__wrbc1_stage1) 6 ) 
		(= 
			(copy--interlimit__wrbc1_stage2) 12 ) 
		(= 
			(copy--interlimit__wrbc1_stage3) 6 ) 
		(= 
			(copy--interlimit__wrbc1_stage4) 7 ) 
		(= 
			(copy--interlimit__wrbc1_stage5) 12 ) 
		(= 
			(copy--interlimit__wrcc1_stage1) 6 ) 
		(= 
			(copy--interlimit__wrcc1_stage2) 8 ) 
		(= 
			(copy--interlimit__wrcc1_stage3) 8 ) 
		(= 
			(copy--interlimit__wrcc1_stage4) 11 ) 
		(= 
			(copy--interlimit__wrcc1_stage5) 3 ) 
		(= 
			(copy--interlimit__wrcc1_stage6) 2 ) 
		(= 
			(copy--interlimit__wrdc1_stage1) 8 ) 
		(= 
			(copy--interlimit__wrdc1_stage2) 8 ) 
		(= 
			(copy--interlimit__wrdc1_stage3) 6 ) 
		(= 
			(copy--interlimit__wrdc1_stage4) 7 ) 
		(= 
			(copy--interlimit__wrec1_stage1) 3 ) 
		(= 
			(copy--interlimit__wrec1_stage2) 7 ) 
		(= 
			(copy--interlimit__wrec1_stage3) 7 ) 
		(= 
			(copy--interlimit__wrec1_stage4) 7 ) 
		(= 
			(copy--interlimit__wrfc1_stage1) 5 ) 
		(= 
			(copy--interlimit__wrfc1_stage2) 9 ) 
		(= 
			(copy--interlimit__wrfc1_stage3) 8 ) 
		(= 
			(copy--mingreentime__wrac1_stage1) 10 ) 
		(= 
			(copy--maxgreentime__wrac1_stage1) 120 ) 
		(= 
			(copy--mingreentime__wrac1_stage2) 9 ) 
		(= 
			(copy--maxgreentime__wrac1_stage2) 120 ) 
		(= 
			(copy--mingreentime__wrac1_stage3) 8 ) 
		(= 
			(copy--maxgreentime__wrac1_stage3) 120 ) 
		(= 
			(copy--mingreentime__wrac1_stage4) 3 ) 
		(= 
			(copy--maxgreentime__wrac1_stage4) 120 ) 
		(= 
			(copy--mingreentime__wrbc1_stage1) 7 ) 
		(= 
			(copy--maxgreentime__wrbc1_stage1) 120 ) 
		(= 
			(copy--mingreentime__wrbc1_stage2) 5 ) 
		(= 
			(copy--maxgreentime__wrbc1_stage2) 120 ) 
		(= 
			(copy--mingreentime__wrbc1_stage3) 10 ) 
		(= 
			(copy--maxgreentime__wrbc1_stage3) 120 ) 
		(= 
			(copy--mingreentime__wrbc1_stage4) 5 ) 
		(= 
			(copy--maxgreentime__wrbc1_stage4) 120 ) 
		(= 
			(copy--mingreentime__wrbc1_stage5) 2 ) 
		(= 
			(copy--maxgreentime__wrbc1_stage5) 120 ) 
		(= 
			(copy--mingreentime__wrcc1_stage1) 7 ) 
		(= 
			(copy--maxgreentime__wrcc1_stage1) 120 ) 
		(= 
			(copy--mingreentime__wrcc1_stage2) 5 ) 
		(= 
			(copy--maxgreentime__wrcc1_stage2) 120 ) 
		(= 
			(copy--mingreentime__wrcc1_stage3) 5 ) 
		(= 
			(copy--maxgreentime__wrcc1_stage3) 120 ) 
		(= 
			(copy--mingreentime__wrcc1_stage4) 4 ) 
		(= 
			(copy--maxgreentime__wrcc1_stage4) 120 ) 
		(= 
			(copy--mingreentime__wrcc1_stage5) 7 ) 
		(= 
			(copy--maxgreentime__wrcc1_stage5) 120 ) 
		(= 
			(copy--mingreentime__wrcc1_stage6) 2 ) 
		(= 
			(copy--maxgreentime__wrcc1_stage6) 120 ) 
		(= 
			(copy--mingreentime__wrdc1_stage1) 7 ) 
		(= 
			(copy--maxgreentime__wrdc1_stage1) 120 ) 
		(= 
			(copy--mingreentime__wrdc1_stage2) 7 ) 
		(= 
			(copy--maxgreentime__wrdc1_stage2) 120 ) 
		(= 
			(copy--mingreentime__wrdc1_stage3) 7 ) 
		(= 
			(copy--maxgreentime__wrdc1_stage3) 120 ) 
		(= 
			(copy--mingreentime__wrdc1_stage4) 7 ) 
		(= 
			(copy--maxgreentime__wrdc1_stage4) 120 ) 
		(= 
			(copy--mingreentime__wrec1_stage1) 10 ) 
		(= 
			(copy--maxgreentime__wrec1_stage1) 120 ) 
		(= 
			(copy--mingreentime__wrec1_stage2) 2 ) 
		(= 
			(copy--maxgreentime__wrec1_stage2) 120 ) 
		(= 
			(copy--mingreentime__wrec1_stage3) 6 ) 
		(= 
			(copy--maxgreentime__wrec1_stage3) 120 ) 
		(= 
			(copy--mingreentime__wrec1_stage4) 6 ) 
		(= 
			(copy--maxgreentime__wrec1_stage4) 120 ) 
		(= 
			(copy--mingreentime__wrfc1_stage1) 10 ) 
		(= 
			(copy--maxgreentime__wrfc1_stage1) 120 ) 
		(= 
			(copy--mingreentime__wrfc1_stage2) 4 ) 
		(= 
			(copy--maxgreentime__wrfc1_stage2) 120 ) 
		(= 
			(copy--mingreentime__wrfc1_stage3) 7 ) 
		(= 
			(copy--maxgreentime__wrfc1_stage3) 120 ) 
		(= 
			(copy--defaultgreentime__wrac1_stage1) 45 ) 
		(= 
			(copy--defaultgreentime__wrac1_stage2) 14 ) 
		(= 
			(copy--defaultgreentime__wrac1_stage3) 19 ) 
		(= 
			(copy--defaultgreentime__wrac1_stage4) 10 ) 
		(= 
			(copy--defaultgreentime__wrbc1_stage1) 47 ) 
		(= 
			(copy--defaultgreentime__wrbc1_stage2) 15 ) 
		(= 
			(copy--defaultgreentime__wrbc1_stage3) 26 ) 
		(= 
			(copy--defaultgreentime__wrbc1_stage4) 7 ) 
		(= 
			(copy--defaultgreentime__wrbc1_stage5) 7 ) 
		(= 
			(copy--defaultgreentime__wrcc1_stage1) 50 ) 
		(= 
			(copy--defaultgreentime__wrcc1_stage2) 7 ) 
		(= 
			(copy--defaultgreentime__wrcc1_stage3) 20 ) 
		(= 
			(copy--defaultgreentime__wrcc1_stage4) 7 ) 
		(= 
			(copy--defaultgreentime__wrcc1_stage5) 18 ) 
		(= 
			(copy--defaultgreentime__wrcc1_stage6) 7 ) 
		(= 
			(copy--defaultgreentime__wrdc1_stage1) 41 ) 
		(= 
			(copy--defaultgreentime__wrdc1_stage2) 17 ) 
		(= 
			(copy--defaultgreentime__wrdc1_stage3) 15 ) 
		(= 
			(copy--defaultgreentime__wrdc1_stage4) 15 ) 
		(= 
			(copy--defaultgreentime__wrec1_stage1) 72 ) 
		(= 
			(copy--defaultgreentime__wrec1_stage2) 7 ) 
		(= 
			(copy--defaultgreentime__wrec1_stage3) 16 ) 
		(= 
			(copy--defaultgreentime__wrec1_stage4) 7 ) 
		(= 
			(copy--defaultgreentime__wrfc1_stage1) 67 ) 
		(= 
			(copy--defaultgreentime__wrfc1_stage2) 9 ) 
		(= 
			(copy--defaultgreentime__wrfc1_stage3) 12 ) 
		(= 
			(copy--counter__abnor_v_wrdc1) 0.0 ) 
		(= 
			(copy--counter__broad_x_wrfc1) 0.0 ) 
		(= 
			(copy--counter__firth_d_wrac1) 0.0 ) 
		(= 
			(copy--counter__hsac3_c_wrac1) 0.0 ) 
		(= 
			(copy--counter__oldwa_c_wrec1) 0.0 ) 
		(= 
			(copy--counter__oldwa_d_wrec1) 0.0 ) 
		(= 
			(copy--counter__silve_w_wrbc1) 0.0 ) 
		(= 
			(copy--counter__smith_c_wrcc1) 0.0 ) 
		(= 
			(copy--counter__somer_v_wrbc1) 0.0 ) 
		(= 
			(copy--counter__stand_f_wrac1) 0.0 ) 
		(= 
			(copy--counter__wakef_z_wrfc1) 0.0 ) 
		(= 
			(copy--counter__wrac1_m_stand) 0.0 ) 
		(= 
			(copy--counter__wrac1_n_firth) 0.0 ) 
		(= 
			(copy--counter__wrac1_x_wrbc1) 0.0 ) 
		(= 
			(copy--counter__wrac1_y_wrbc1) 0.0 ) 
		(= 
			(copy--counter__wrac1_z_hsac1) 0.0 ) 
		(= 
			(copy--counter__wrbc1_a_wrac1) 0.0 ) 
		(= 
			(copy--counter__wrbc1_b_wrac1) 0.0 ) 
		(= 
			(copy--counter__wrbc1_b_wrcc1) 0.0 ) 
		(= 
			(copy--counter__wrbc1_r_silve) 0.0 ) 
		(= 
			(copy--counter__wrbc1_s_somer) 0.0 ) 
		(= 
			(copy--counter__wrcc1_w_wrdc1) 0.0 ) 
		(= 
			(copy--counter__wrcc1_x_wrdc1) 0.0 ) 
		(= 
			(copy--counter__wrcc1_z_wrbc1) 0.0 ) 
		(= 
			(copy--counter__wrdc1_a_wrcc1) 0.0 ) 
		(= 
			(copy--counter__wrdc1_b_wrec1) 0.0 ) 
		(= 
			(copy--counter__wrdc1_l_absou) 0.0 ) 
		(= 
			(copy--counter__wrdc1_q_abnor) 0.0 ) 
		(= 
			(copy--counter__wrec1_y_wrdc1) 0.0 ) 
		(= 
			(copy--counter__wrec1_y_wrfc1) 0.0 ) 
		(= 
			(copy--counter__wrec1_z_wrdc1) 0.0 ) 
		(= 
			(copy--counter__wrfc1_a_wrec1) 0.0 ) 
		(= 
			(copy--counter__wrfc1_r_broad) 0.0 ) 
		(= 
			(copy--counter__wrfc1_t_wakef) 0.0 ) 
		(= 
			(copy--cycletime__wrac1) 88 ) 
		(= 
			(copy--cycletime__wrbc1) 102 ) 
		(= 
			(copy--cycletime__wrcc1) 109 ) 
		(= 
			(copy--cycletime__wrdc1) 88 ) 
		(= 
			(copy--cycletime__wrec1) 102 ) 
		(= 
			(copy--cycletime__wrfc1) 102 ) 
		(= 
			(copy--granularity) 10.0 ) 
		(= 
			(copy--maxcycletime__wrac1) 200 ) 
		(= 
			(copy--maxcycletime__wrbc1) 200 ) 
		(= 
			(copy--maxcycletime__wrcc1) 200 ) 
		(= 
			(copy--maxcycletime__wrdc1) 200 ) 
		(= 
			(copy--maxcycletime__wrec1) 200 ) 
		(= 
			(copy--maxcycletime__wrfc1) 200 ) 
		(= 
			(copy--mincycletime__wrac1) 50 ) 
		(= 
			(copy--mincycletime__wrbc1) 50 ) 
		(= 
			(copy--mincycletime__wrcc1) 50 ) 
		(= 
			(copy--mincycletime__wrdc1) 50 ) 
		(= 
			(copy--mincycletime__wrec1) 50 ) 
		(= 
			(copy--mincycletime__wrfc1) 50 ) 
		(= 
			(copy--time) 0 ) 
		(= 
			(copy--turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_x_wrbc1) 0.294 ) 
		(= 
			(copy--turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_m_stand) 0.178 ) 
		(= 
			(copy--turnrate__wrac1_stage1__hsac3_c_wrac1__wrac1_y_wrbc1) 0.704 ) 
		(= 
			(copy--turnrate__wrac1_stage1__wrbc1_a_wrac1__wrac1_z_hsac1) 0.844 ) 
		(= 
			(copy--turnrate__wrac1_stage1__wrbc1_a_wrac1__wrac1_n_firth) 0.097 ) 
		(= 
			(copy--turnrate__wrac1_stage2__wrbc1_a_wrac1__wrac1_z_hsac1) 0.844 ) 
		(= 
			(copy--turnrate__wrac1_stage2__wrbc1_a_wrac1__wrac1_n_firth) 0.097 ) 
		(= 
			(copy--turnrate__wrac1_stage2__wrbc1_b_wrac1__wrac1_m_stand) 0.412 ) 
		(= 
			(copy--turnrate__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_b_wrcc1) 0.592 ) 
		(= 
			(copy--turnrate__wrbc1_stage1__wrac1_y_wrbc1__wrbc1_r_silve) 0.114 ) 
		(= 
			(copy--turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_a_wrac1) 0.904 ) 
		(= 
			(copy--turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_b_wrac1) 0.22 ) 
		(= 
			(copy--turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_r_silve) 0.026 ) 
		(= 
			(copy--turnrate__wrbc1_stage1__wrcc1_z_wrbc1__wrbc1_s_somer) 0.027 ) 
		(= 
			(copy--turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_a_wrac1) 0.182 ) 
		(= 
			(copy--turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrac1) 0.068 ) 
		(= 
			(copy--turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_b_wrcc1) 0.093 ) 
		(= 
			(copy--turnrate__wrbc1_stage2__silve_w_wrbc1__wrbc1_s_somer) 0.07 ) 
		(= 
			(copy--turnrate__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_b_wrcc1) 0.592 ) 
		(= 
			(copy--turnrate__wrbc1_stage3__wrac1_y_wrbc1__wrbc1_r_silve) 0.114 ) 
		(= 
			(copy--turnrate__wrbc1_stage3__wrac1_x_wrbc1__wrbc1_s_somer) 0.412 ) 
		(= 
			(copy--turnrate__wrbc1_stage3__somer_v_wrbc1__wrbc1_a_wrac1) 0.478 ) 
		(= 
			(copy--turnrate__wrbc1_stage3__somer_v_wrbc1__wrbc1_b_wrac1) 0.111 ) 
		(= 
			(copy--turnrate__wrbc1_stage4__wrac1_x_wrbc1__wrbc1_s_somer) 0.412 ) 
		(= 
			(copy--turnrate__wrbc1_stage4__somer_v_wrbc1__wrbc1_a_wrac1) 0.478 ) 
		(= 
			(copy--turnrate__wrbc1_stage4__somer_v_wrbc1__wrbc1_b_wrac1) 0.111 ) 
		(= 
			(copy--turnrate__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_b_wrcc1) 0.592 ) 
		(= 
			(copy--turnrate__wrbc1_stage5__wrac1_y_wrbc1__wrbc1_r_silve) 0.114 ) 
		(= 
			(copy--turnrate__wrbc1_stage5__wrac1_x_wrbc1__wrbc1_s_somer) 0.412 ) 
		(= 
			(copy--turnrate__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_x_wrdc1) 0.467 ) 
		(= 
			(copy--turnrate__wrcc1_stage1__wrbc1_b_wrcc1__wrcc1_w_wrdc1) 0.003 ) 
		(= 
			(copy--turnrate__wrcc1_stage1__wrdc1_a_wrcc1__wrcc1_z_wrbc1) 0.824 ) 
		(= 
			(copy--turnrate__wrcc1_stage2__wrdc1_a_wrcc1__wrcc1_z_wrbc1) 0.824 ) 
		(= 
			(copy--turnrate__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_x_wrdc1) 0.467 ) 
		(= 
			(copy--turnrate__wrcc1_stage3__wrbc1_b_wrcc1__wrcc1_w_wrdc1) 0.003 ) 
		(= 
			(copy--turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_z_wrbc1) 0.001 ) 
		(= 
			(copy--turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_x_wrdc1) 0.468 ) 
		(= 
			(copy--turnrate__wrcc1_stage4__smith_c_wrcc1__wrcc1_w_wrdc1) 0.001 ) 
		(= 
			(copy--turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_z_wrbc1) 0.001 ) 
		(= 
			(copy--turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_x_wrdc1) 0.468 ) 
		(= 
			(copy--turnrate__wrcc1_stage5__smith_c_wrcc1__wrcc1_w_wrdc1) 0.001 ) 
		(= 
			(copy--turnrate__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_b_wrec1) 0.821 ) 
		(= 
			(copy--turnrate__wrdc1_stage1__wrcc1_x_wrdc1__wrdc1_l_absou) 0.002 ) 
		(= 
			(copy--turnrate__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_a_wrcc1) 0.867 ) 
		(= 
			(copy--turnrate__wrdc1_stage1__wrec1_z_wrdc1__wrdc1_q_abnor) 0.133 ) 
		(= 
			(copy--turnrate__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_a_wrcc1) 0.867 ) 
		(= 
			(copy--turnrate__wrdc1_stage2__wrec1_z_wrdc1__wrdc1_q_abnor) 0.133 ) 
		(= 
			(copy--turnrate__wrdc1_stage2__wrec1_y_wrdc1__wrdc1_l_absou) 0.353 ) 
		(= 
			(copy--turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_a_wrcc1) 0.283 ) 
		(= 
			(copy--turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_b_wrec1) 0.215 ) 
		(= 
			(copy--turnrate__wrdc1_stage3__abnor_v_wrdc1__wrdc1_l_absou) 0.03 ) 
		(= 
			(copy--turnrate__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_b_wrec1) 0.821 ) 
		(= 
			(copy--turnrate__wrdc1_stage4__wrcc1_x_wrdc1__wrdc1_l_absou) 0.002 ) 
		(= 
			(copy--turnrate__wrdc1_stage4__wrcc1_w_wrdc1__wrdc1_q_abnor) 0.353 ) 
		(= 
			(copy--turnrate__wrec1_stage1__wrfc1_a_wrec1__wrec1_z_wrdc1) 0.774 ) 
		(= 
			(copy--turnrate__wrec1_stage1__wrfc1_a_wrec1__wrec1_y_wrdc1) 0.049 ) 
		(= 
			(copy--turnrate__wrec1_stage1__wrdc1_b_wrec1__wrec1_y_wrfc1) 0.824 ) 
		(= 
			(copy--turnrate__wrec1_stage2__wrdc1_b_wrec1__wrec1_y_wrfc1) 0.824 ) 
		(= 
			(copy--turnrate__wrec1_stage3__oldwa_c_wrec1__wrec1_z_wrdc1) 0.332 ) 
		(= 
			(copy--turnrate__wrec1_stage3__oldwa_c_wrec1__wrec1_y_wrdc1) 0.021 ) 
		(= 
			(copy--turnrate__wrec1_stage3__oldwa_d_wrec1__wrec1_y_wrfc1) 0.353 ) 
		(= 
			(copy--turnrate__wrec1_stage4__wrfc1_a_wrec1__wrec1_z_wrdc1) 0.774 ) 
		(= 
			(copy--turnrate__wrec1_stage4__wrfc1_a_wrec1__wrec1_y_wrdc1) 0.049 ) 
		(= 
			(copy--turnrate__wrec1_stage4__oldwa_d_wrec1__wrec1_y_wrfc1) 0.353 ) 
		(= 
			(copy--turnrate__wrfc1_stage1__wakef_z_wrfc1__wrfc1_a_wrec1) 0.765 ) 
		(= 
			(copy--turnrate__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_r_broad) 0.249 ) 
		(= 
			(copy--turnrate__wrfc1_stage1__wrec1_y_wrfc1__wrfc1_t_wakef) 0.575 ) 
		(= 
			(copy--turnrate__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_r_broad) 0.249 ) 
		(= 
			(copy--turnrate__wrfc1_stage2__wrec1_y_wrfc1__wrfc1_t_wakef) 0.575 ) 
		(= 
			(copy--turnrate__wrfc1_stage3__broad_x_wrfc1__wrfc1_a_wrec1) 0.586 ) 
		(= 
			(copy--turnrate__wrfc1_stage3__broad_x_wrfc1__wrfc1_t_wakef) 0.002 ) 
		(= 
			(copy--turnrate__wrac1_stage3__firth_d_wrac1__wrac1_z_hsac1) 0.118 ) 
		(= 
			(copy--turnrate__wrac1_stage3__firth_d_wrac1__wrac1_m_stand) 0.294 ) 
		(= 
			(copy--turnrate__wrac1_stage4__firth_d_wrac1__wrac1_z_hsac1) 0.057 ) 
		(= 
			(copy--turnrate__wrac1_stage4__firth_d_wrac1__wrac1_y_wrbc1) 0.165 ) 
		(= 
			(copy--turnrate__wrac1_stage4__firth_d_wrac1__wrac1_x_wrbc1) 0.049 ) 
		(= 
			(copy--turnrate__wrac1_stage4__firth_d_wrac1__wrac1_m_stand) 0.141 ) 
		(= 
			(copy--turnrate__wrac1_stage2__stand_f_wrac1__wrac1_y_wrbc1) 0.372 ) 
		(= 
			(copy--turnrate__wrac1_stage2__stand_f_wrac1__wrac1_x_wrbc1) 0.154 ) 
		(= 
			(copy--turnrate__wrac1_stage3__stand_f_wrac1__wrac1_n_firth) 0.339 ) 
		(= 
			(copy--turnrate__wrac1_stage3__stand_f_wrac1__wrac1_y_wrbc1) 0.159 ) 
		(= 
			(copy--turnrate__wrac1_stage3__stand_f_wrac1__wrac1_x_wrbc1) 0.089 ) 
		(= 
			(copy--turnrate__fake__outside__abnor_v_wrdc1) 0.0736 ) 
		(= 
			(copy--turnrate__fake__outside__broad_x_wrfc1) 0.1798 ) 
		(= 
			(copy--turnrate__fake__outside__firth_d_wrac1) 0.0686 ) 
		(= 
			(copy--turnrate__fake__outside__hsac3_c_wrac1) 0.3376 ) 
		(= 
			(copy--turnrate__fake__outside__oldwa_c_wrec1) 0.0064 ) 
		(= 
			(copy--turnrate__fake__outside__oldwa_d_wrec1) 0.02 ) 
		(= 
			(copy--turnrate__fake__outside__silve_w_wrbc1) 0.0546 ) 
		(= 
			(copy--turnrate__fake__outside__smith_c_wrcc1) 0.0685 ) 
		(= 
			(copy--turnrate__fake__outside__somer_v_wrbc1) 0.0774 ) 
		(= 
			(copy--turnrate__fake__outside__stand_f_wrac1) 0.2258 ) 
		(= 
			(copy--turnrate__fake__outside__wakef_z_wrfc1) 0.2116 ) 
		(= 
			(copy--min_occ__wrac1_y_wrbc1) 15.866 ) 
		(= 
			(copy--min_occ__wrac1_x_wrbc1) 2.318 ) 
		(= 
			(copy--min_occ__wrbc1_a_wrac1) 23.0795 ) 
		(= 
			(copy--min_occ__wrbc1_b_wrcc1) 7.4091 ) 
		(= 
			(copy--min_occ__wrbc1_b_wrac1) 0.8363 ) 
		(= 
			(copy--min_occ__wrcc1_z_wrbc1) 14.741 ) 
		(= 
			(copy--min_occ__wrcc1_x_wrdc1) 32.5227 ) 
		(= 
			(copy--min_occ__wrcc1_w_wrdc1) 0.1893 ) 
		(= 
			(copy--min_occ__wrdc1_a_wrcc1) 12.7607 ) 
		(= 
			(copy--min_occ__wrdc1_b_wrec1) 9.3493 ) 
		(= 
			(copy--min_occ__wrec1_z_wrdc1) 8.7141 ) 
		(= 
			(copy--min_occ__wrec1_y_wrfc1) 4.0929 ) 
		(= 
			(copy--min_occ__wrec1_y_wrdc1) 0.062 ) 
		(= 
			(copy--min_occ__wrfc1_a_wrec1) 0.1 ) 
		(= 
			(copy--min_occ__hsac3_c_wrac1) 14.8544 ) 
		(= 
			(copy--min_occ__stand_f_wrac1) 9.7546 ) 
		(= 
			(copy--min_occ__firth_d_wrac1) 1.5185 ) 
		(= 
			(copy--min_occ__silve_w_wrbc1) 0.5971 ) 
		(= 
			(copy--min_occ__somer_v_wrbc1) 3.4366 ) 
		(= 
			(copy--min_occ__smith_c_wrcc1) 0.7743 ) 
		(= 
			(copy--min_occ__abnor_v_wrdc1) 8.7337 ) 
		(= 
			(copy--min_occ__oldwa_c_wrec1) 0.0631 ) 
		(= 
			(copy--min_occ__oldwa_d_wrec1) 0.2133 ) 
		(= 
			(copy--min_occ__wakef_z_wrfc1) 4.0627 ) 
		(= 
			(copy--min_occ__broad_x_wrfc1) 1.9663 ) 
		(= 
			(copy--min_occ__outside) 1.0 ) 
		(= 
			(copy--occupancy__hsac3_c_wrac1) 20.59 ) 
		(= 
			(copy--occupancy__wrac1_y_wrbc1) 15.87 ) 
		(= 
			(copy--occupancy__stand_f_wrac1) 16.56 ) 
		(= 
			(copy--occupancy__firth_d_wrac1) 5.09 ) 
		(= 
			(copy--occupancy__wrac1_x_wrbc1) 6.59 ) 
		(= 
			(copy--occupancy__wrbc1_a_wrac1) 23.28 ) 
		(= 
			(copy--occupancy__wrbc1_b_wrcc1) 9.78 ) 
		(= 
			(copy--occupancy__silve_w_wrbc1) 5.02 ) 
		(= 
			(copy--occupancy__somer_v_wrbc1) 6.76 ) 
		(= 
			(copy--occupancy__wrbc1_b_wrac1) 2.75 ) 
		(= 
			(copy--occupancy__wrcc1_z_wrbc1) 23.28 ) 
		(= 
			(copy--occupancy__wrcc1_x_wrdc1) 80.01 ) 
		(= 
			(copy--occupancy__smith_c_wrcc1) 5.12 ) 
		(= 
			(copy--occupancy__wrcc1_w_wrdc1) 0.25 ) 
		(= 
			(copy--occupancy__wrdc1_a_wrcc1) 12.76 ) 
		(= 
			(copy--occupancy__wrdc1_b_wrec1) 26.67 ) 
		(= 
			(copy--occupancy__abnor_v_wrdc1) 12.19 ) 
		(= 
			(copy--occupancy__wrec1_z_wrdc1) 8.71 ) 
		(= 
			(copy--occupancy__wrec1_y_wrfc1) 4.09 ) 
		(= 
			(copy--occupancy__oldwa_c_wrec1) 0.15 ) 
		(= 
			(copy--occupancy__wrec1_y_wrdc1) 0.42 ) 
		(= 
			(copy--occupancy__oldwa_d_wrec1) 0.49 ) 
		(= 
			(copy--occupancy__wakef_z_wrfc1) 4.06 ) 
		(= 
			(copy--occupancy__wrfc1_a_wrec1) 0.88 ) 
		(= 
			(copy--occupancy__broad_x_wrfc1) 9.09 ) 
		(= 
			(copy--occupancy__wrac1_z_hsac1) 0.0 ) 
		(= 
			(copy--occupancy__wrac1_m_stand) 0.0 ) 
		(= 
			(copy--occupancy__wrac1_n_firth) 0.0 ) 
		(= 
			(copy--occupancy__wrbc1_r_silve) 0.0 ) 
		(= 
			(copy--occupancy__wrbc1_s_somer) 0.0 ) 
		(= 
			(copy--occupancy__wrdc1_q_abnor) 0.0 ) 
		(= 
			(copy--occupancy__wrdc1_l_absou) 0.0 ) 
		(= 
			(copy--occupancy__wrfc1_r_broad) 0.0 ) 
		(= 
			(copy--occupancy__wrfc1_t_wakef) 0.0 ) 
		(= 
			(copy--occupancy__outside) 50000.0 ) 
		(= 
			(copy--greentime__wrac1) 13 ) 
		(= 
			(copy--intertime__wrac1) 0 ) 
		(= 
			(copy--greentime__wrbc1) 0 ) 
		(= 
			(copy--intertime__wrbc1) 1 ) 
		(= 
			(copy--greentime__wrcc1) 39 ) 
		(= 
			(copy--intertime__wrcc1) 0 ) 
		(= 
			(copy--greentime__wrdc1) 15 ) 
		(= 
			(copy--intertime__wrdc1) 0 ) 
		(= 
			(copy--greentime__wrec1) 15 ) 
		(= 
			(copy--intertime__wrec1) 0 ) 
		(= 
			(copy--greentime__wrfc1) 27 ) 
		(= 
			(copy--intertime__wrfc1) 0 ) 
		(= 
			(copy--delta) 1 ) 
		(= 
			(defaultgreentime__fake) 0 ) 
		(= 
			(copy--defaultgreentime__fake) 0 ) (force-events) 
		 
)
(:goal
	
	(and 
		
			(>= 
				(counter__wrbc1_b_wrcc1) 500 ) 
			(>= 
				(counter__wrac1_y_wrbc1) 500 ) 
			(<= 
				(time) 10000000000 ) 
			(not (pause)) 
			(not (force-events)) )
)

)