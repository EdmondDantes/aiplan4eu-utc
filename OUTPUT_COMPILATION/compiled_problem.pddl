(define (problem grounded-trafficlights)
(:domain grounded-urbantraffic)
(:init
	
	
		(= 
			(queue__r1) 60 ) 
		(= 
			(queue__r2) 20 ) 
		(= 
			(queue__r3) 50 ) 
		(= 
			(queue__r4) 30 ) 
		(= 
			(queue__r5) 10 ) 
		(= 
			(queue__r6) 30 ) 
		(= 
			(queue__r7s) 20 ) 
		(= 
			(queue__r7n) 20 ) 
		(= 
			(queue__r8) 25 ) 
		(= 
			(queue__r9) 0 ) 
		(= 
			(queue__r10) 0 ) (bufferconnected__buf0__r1) 
		(= 
			(max_queue__r1) 1000 ) 
		(= 
			(max_queue__r2) 1000 ) 
		(= 
			(max_queue__r3) 1000 ) 
		(= 
			(max_queue__r4) 80 ) 
		(= 
			(max_queue__r5) 70 ) 
		(= 
			(max_queue__r6) 1000 ) 
		(= 
			(max_queue__r7n) 50 ) 
		(= 
			(max_queue__r7s) 50 ) 
		(= 
			(max_queue__r8) 1000 ) 
		(= 
			(max_queue__r9) 10000 ) 
		(= 
			(max_queue__r10) 10000 ) 
		(= 
			(saturated_queue__r1) 20 ) 
		(= 
			(saturated_queue__r2) 20 ) 
		(= 
			(saturated_queue__r3) 20 ) 
		(= 
			(greentime__ter1) 0 ) 
		(= 
			(token__ter1) 10 ) 
		(= 
			(maxtoken__ter1) 30 ) 
		(= 
			(tokenvalue__r1__ter1) 10 ) 
		(= 
			(tokenvalue__r2__ter1) 20 ) 
		(= 
			(tokenvalue__r3__ter1) 30 ) 
		(= 
			(maxgreentime__ter1) 25 ) 
		(= 
			(mingreentime__ter1) 5 ) 
		(= 
			(flow__r2__r5__ter1) 2 ) 
		(= 
			(flow__r1__r5__ter1) 2 ) 
		(= 
			(flow__r3__r5__ter1) 2 ) (availableflow__r2__r5__ter1) (availableflow__r3__r5__ter1) (availableflow__r1__r5__ter1) (active__ter1__r1) (active__ter1__r2) (active__ter1__r3) 
		(= 
			(maxgreentime__ter3) 25 ) 
		(= 
			(mingreentime__ter3) 5 ) 
		(= 
			(greentime__ter3) 0 ) 
		(= 
			(token__ter3) 10 ) 
		(= 
			(maxtoken__ter3) 20 ) 
		(= 
			(tokenvalue__r7s__ter3) 10 ) 
		(= 
			(tokenvalue__r8__ter3) 20 ) 
		(= 
			(flow__r8__r10__ter3) 2 ) 
		(= 
			(flow__r8__r7n__ter3) 2 ) 
		(= 
			(flow__r7s__r10__ter3) 2 ) (availableflow__r8__r10__ter3) (availableflow__r8__r7n__ter3) (availableflow__r7s__r10__ter3) (active__ter3__r8) (active__ter3__r7s) 
		(= 
			(greentime__ter2) 0 ) 
		(= 
			(token__ter2) 10 ) 
		(= 
			(maxtoken__ter2) 30 ) 
		(= 
			(tokenvalue__r5__ter2) 10 ) 
		(= 
			(tokenvalue__r6__ter2) 20 ) 
		(= 
			(tokenvalue__r7n__ter2) 30 ) 
		(= 
			(maxgreentime__ter2) 25 ) 
		(= 
			(mingreentime__ter2) 5 ) 
		(= 
			(flow__r5__r9__ter2) 2 ) 
		(= 
			(flow__r5__r7s__ter2) 2 ) 
		(= 
			(flow__r7n__r9__ter2) 2 ) 
		(= 
			(flow__r6__r9__ter2) 2 ) 
		(= 
			(flow__r6__r7s__ter2) 2 ) (availableflow__r5__r9__ter2) (availableflow__r5__r7s__ter2) (availableflow__r6__r9__ter2) (availableflow__r6__r7s__ter2) (availableflow__r7n__r9__ter2) (active__ter2__r5) (active__ter2__r6) (active__ter2__r7n) 
		(= 
			(delta) 1 ) 
		(= 
			(copy--queue__r1) 60 ) 
		(= 
			(copy--queue__r2) 20 ) 
		(= 
			(copy--queue__r3) 50 ) 
		(= 
			(copy--queue__r4) 30 ) 
		(= 
			(copy--queue__r5) 10 ) 
		(= 
			(copy--queue__r6) 30 ) 
		(= 
			(copy--queue__r7s) 20 ) 
		(= 
			(copy--queue__r7n) 20 ) 
		(= 
			(copy--queue__r8) 25 ) 
		(= 
			(copy--queue__r9) 0 ) 
		(= 
			(copy--queue__r10) 0 ) 
		(= 
			(copy--max_queue__r1) 1000 ) 
		(= 
			(copy--max_queue__r2) 1000 ) 
		(= 
			(copy--max_queue__r3) 1000 ) 
		(= 
			(copy--max_queue__r4) 80 ) 
		(= 
			(copy--max_queue__r5) 70 ) 
		(= 
			(copy--max_queue__r6) 1000 ) 
		(= 
			(copy--max_queue__r7n) 50 ) 
		(= 
			(copy--max_queue__r7s) 50 ) 
		(= 
			(copy--max_queue__r8) 1000 ) 
		(= 
			(copy--max_queue__r9) 10000 ) 
		(= 
			(copy--max_queue__r10) 10000 ) 
		(= 
			(copy--saturated_queue__r1) 20 ) 
		(= 
			(copy--saturated_queue__r2) 20 ) 
		(= 
			(copy--saturated_queue__r3) 20 ) 
		(= 
			(copy--greentime__ter1) 0 ) 
		(= 
			(copy--token__ter1) 10 ) 
		(= 
			(copy--maxtoken__ter1) 30 ) 
		(= 
			(copy--tokenvalue__r1__ter1) 10 ) 
		(= 
			(copy--tokenvalue__r2__ter1) 20 ) 
		(= 
			(copy--tokenvalue__r3__ter1) 30 ) 
		(= 
			(copy--maxgreentime__ter1) 25 ) 
		(= 
			(copy--mingreentime__ter1) 5 ) 
		(= 
			(copy--flow__r2__r5__ter1) 2 ) 
		(= 
			(copy--flow__r1__r5__ter1) 2 ) 
		(= 
			(copy--flow__r3__r5__ter1) 2 ) 
		(= 
			(copy--maxgreentime__ter3) 25 ) 
		(= 
			(copy--mingreentime__ter3) 5 ) 
		(= 
			(copy--greentime__ter3) 0 ) 
		(= 
			(copy--token__ter3) 10 ) 
		(= 
			(copy--maxtoken__ter3) 20 ) 
		(= 
			(copy--tokenvalue__r7s__ter3) 10 ) 
		(= 
			(copy--tokenvalue__r8__ter3) 20 ) 
		(= 
			(copy--flow__r8__r10__ter3) 2 ) 
		(= 
			(copy--flow__r8__r7n__ter3) 2 ) 
		(= 
			(copy--flow__r7s__r10__ter3) 2 ) 
		(= 
			(copy--greentime__ter2) 0 ) 
		(= 
			(copy--token__ter2) 10 ) 
		(= 
			(copy--maxtoken__ter2) 30 ) 
		(= 
			(copy--tokenvalue__r5__ter2) 10 ) 
		(= 
			(copy--tokenvalue__r6__ter2) 20 ) 
		(= 
			(copy--tokenvalue__r7n__ter2) 30 ) 
		(= 
			(copy--maxgreentime__ter2) 25 ) 
		(= 
			(copy--mingreentime__ter2) 5 ) 
		(= 
			(copy--flow__r5__r9__ter2) 2 ) 
		(= 
			(copy--flow__r5__r7s__ter2) 2 ) 
		(= 
			(copy--flow__r7n__r9__ter2) 2 ) 
		(= 
			(copy--flow__r6__r9__ter2) 2 ) 
		(= 
			(copy--flow__r6__r7s__ter2) 2 ) 
		(= 
			(copy--delta) 1 ) 
		(= 
			(carsinbuffer__buf0) 0 ) 
		(= 
			(copy--carsinbuffer__buf0) 0 ) (force-events) 
		 
)
(:goal
	
	(and 
		
			(< 
				(queue__r1) (saturated_queue__r1) ) 
			(< 
				(queue__r2) (saturated_queue__r2) ) 
			(< 
				(queue__r3) (saturated_queue__r3) ) 
			(not (pause)) 
			(not (force-events)) )
)

)