(define (problem grounded-trafficlights)
(:domain grounded-urbantraffic)
(:init
	
	
		(= 
			(queue__r1) 3000 ) 
		(= 
			(queue__r2) 3000 ) 
		(= 
			(queue__r3) 3000 ) 
		(= 
			(queue__r4) 3000 ) 
		(= 
			(queue__r5) 200 ) 
		(= 
			(queue__r6w) 200 ) 
		(= 
			(queue__r6e) 200 ) 
		(= 
			(queue__r7w) 200 ) 
		(= 
			(queue__r7e) 200 ) 
		(= 
			(queue__r8) 200 ) 
		(= 
			(queue__r9) 300 ) 
		(= 
			(queue__r10) 300 ) 
		(= 
			(queue__r11w) 50 ) 
		(= 
			(queue__r11e) 50 ) 
		(= 
			(queue__r12) 300 ) 
		(= 
			(queue__r13) 100 ) 
		(= 
			(queue__r14) 0 ) 
		(= 
			(queue__r15w) 50 ) 
		(= 
			(queue__r15e) 50 ) 
		(= 
			(queue__r16w) 50 ) 
		(= 
			(queue__r16e) 50 ) 
		(= 
			(queue__r17) 0 ) (bufferconnected__buf0__r1) 
		(= 
			(max_queue__r1) 5000 ) 
		(= 
			(max_queue__r2) 5000 ) 
		(= 
			(max_queue__r3) 5000 ) 
		(= 
			(max_queue__r4) 5000 ) 
		(= 
			(max_queue__r5) 500 ) 
		(= 
			(max_queue__r6w) 500 ) 
		(= 
			(max_queue__r7w) 500 ) 
		(= 
			(max_queue__r6e) 500 ) 
		(= 
			(max_queue__r7e) 500 ) 
		(= 
			(max_queue__r8) 300 ) 
		(= 
			(max_queue__r9) 500 ) 
		(= 
			(max_queue__r10) 500 ) 
		(= 
			(max_queue__r11w) 150 ) 
		(= 
			(max_queue__r11e) 150 ) 
		(= 
			(max_queue__r12) 700 ) 
		(= 
			(max_queue__r13) 500 ) 
		(= 
			(max_queue__r14) 10000 ) 
		(= 
			(max_queue__r15w) 200 ) 
		(= 
			(max_queue__r15e) 200 ) 
		(= 
			(max_queue__r16w) 200 ) 
		(= 
			(max_queue__r16e) 200 ) 
		(= 
			(max_queue__r17) 10000 ) 
		(= 
			(saturated_queue__r1) 500 ) 
		(= 
			(saturated_queue__r2) 500 ) 
		(= 
			(saturated_queue__r3) 500 ) 
		(= 
			(saturated_queue__r4) 500 ) 
		(= 
			(saturated_queue__r5) 300 ) 
		(= 
			(saturated_queue__r6w) 300 ) 
		(= 
			(saturated_queue__r7w) 300 ) 
		(= 
			(saturated_queue__r6e) 300 ) 
		(= 
			(saturated_queue__r7e) 300 ) 
		(= 
			(saturated_queue__r8) 200 ) 
		(= 
			(saturated_queue__r9) 300 ) 
		(= 
			(saturated_queue__r10) 400 ) 
		(= 
			(saturated_queue__r11w) 100 ) 
		(= 
			(saturated_queue__r11e) 100 ) 
		(= 
			(saturated_queue__r12) 500 ) 
		(= 
			(saturated_queue__r13) 300 ) 
		(= 
			(saturated_queue__r14) 10000 ) 
		(= 
			(saturated_queue__r15w) 100 ) 
		(= 
			(saturated_queue__r15e) 100 ) 
		(= 
			(saturated_queue__r16w) 100 ) 
		(= 
			(saturated_queue__r16e) 100 ) 
		(= 
			(saturated_queue__r17) 10000 ) 
		(= 
			(greentime__int1) 0 ) 
		(= 
			(token__int1) 10 ) 
		(= 
			(maxtoken__int1) 20 ) 
		(= 
			(tokenvalue__r4__int1) 10 ) 
		(= 
			(tokenvalue__r6w__int1) 20 ) 
		(= 
			(maxgreentime__int1) 40 ) 
		(= 
			(mingreentime__int1) 5 ) 
		(= 
			(flow__r4__r6e__int1) 3 ) 
		(= 
			(flow__r4__r5__int1) 5 ) 
		(= 
			(flow__r6w__r5__int1) 2 ) (availableflow__r4__r6e__int1) (availableflow__r4__r5__int1) (availableflow__r6w__r5__int1) (active__int1__r4) (active__int1__r6w) 
		(= 
			(greentime__int2) 0 ) 
		(= 
			(token__int2) 10 ) 
		(= 
			(maxtoken__int2) 20 ) 
		(= 
			(tokenvalue__r7w__int2) 10 ) 
		(= 
			(tokenvalue__r6e__int2) 20 ) 
		(= 
			(maxgreentime__int2) 40 ) 
		(= 
			(mingreentime__int2) 5 ) 
		(= 
			(flow__r7w__r6w__int2) 2 ) 
		(= 
			(flow__r6e__r7e__int2) 2 ) 
		(= 
			(flow__r6e__r8__int2) 1 ) 
		(= 
			(flow__r7w__r8__int2) 1 ) (availableflow__r7w__r6w__int2) (availableflow__r6e__r7e__int2) (availableflow__r6e__r8__int2) (availableflow__r7w__r8__int2) (active__int2__r6e) (active__int2__r7w) 
		(= 
			(greentime__int3) 0 ) 
		(= 
			(token__int3) 10 ) 
		(= 
			(maxtoken__int3) 20 ) 
		(= 
			(tokenvalue__r10__int3) 10 ) 
		(= 
			(tokenvalue__r7e__int3) 20 ) 
		(= 
			(maxgreentime__int3) 40 ) 
		(= 
			(mingreentime__int3) 5 ) 
		(= 
			(flow__r10__r7w__int3) 1 ) 
		(= 
			(flow__r10__r9__int3) 2 ) 
		(= 
			(flow__r7e__r9__int3) 1 ) (availableflow__r10__r7w__int3) (availableflow__r10__r9__int3) (availableflow__r7e__r9__int3) (active__int3__r10) (active__int3__r7e) 
		(= 
			(greentime__int4) 0 ) 
		(= 
			(token__int4) 10 ) 
		(= 
			(maxtoken__int4) 20 ) 
		(= 
			(tokenvalue__r3__int4) 10 ) 
		(= 
			(tokenvalue__r11w__int4) 20 ) 
		(= 
			(maxgreentime__int4) 40 ) 
		(= 
			(mingreentime__int4) 5 ) 
		(= 
			(flow__r3__r11e__int4) 3 ) 
		(= 
			(flow__r3__r10__int4) 5 ) 
		(= 
			(flow__r11w__r10__int4) 1 ) (availableflow__r3__r11e__int4) (availableflow__r3__r10__int4) (availableflow__r11w__r10__int4) (active__int4__r3) (active__int4__r11w) 
		(= 
			(greentime__int5) 0 ) 
		(= 
			(token__int5) 10 ) 
		(= 
			(maxtoken__int5) 20 ) 
		(= 
			(tokenvalue__r2__int5) 10 ) 
		(= 
			(tokenvalue__r11e__int5) 20 ) 
		(= 
			(maxgreentime__int5) 40 ) 
		(= 
			(mingreentime__int5) 5 ) 
		(= 
			(flow__r2__r11w__int5) 3 ) 
		(= 
			(flow__r2__r12__int5) 5 ) 
		(= 
			(flow__r11e__r12__int5) 1 ) (availableflow__r2__r11w__int5) (availableflow__r2__r12__int5) (availableflow__r11e__r12__int5) (active__int5__r2) (active__int5__r11e) 
		(= 
			(greentime__int6) 0 ) 
		(= 
			(token__int6) 10 ) 
		(= 
			(maxtoken__int6) 20 ) 
		(= 
			(tokenvalue__r1__int6) 10 ) 
		(= 
			(tokenvalue__r12__int6) 20 ) 
		(= 
			(maxgreentime__int6) 40 ) 
		(= 
			(mingreentime__int6) 5 ) 
		(= 
			(flow__r12__r13__int6) 2 ) 
		(= 
			(flow__r1__r13__int6) 5 ) (availableflow__r12__r13__int6) (availableflow__r1__r13__int6) (active__int6__r1) (active__int6__r12) 
		(= 
			(greentime__int7) 0 ) 
		(= 
			(token__int7) 10 ) 
		(= 
			(maxtoken__int7) 20 ) 
		(= 
			(tokenvalue__r15e__int7) 10 ) 
		(= 
			(tokenvalue__r13__int7) 10 ) 
		(= 
			(tokenvalue__r9__int7) 20 ) 
		(= 
			(maxgreentime__int7) 40 ) 
		(= 
			(mingreentime__int7) 5 ) 
		(= 
			(flow__r15e__r14__int7) 1 ) 
		(= 
			(flow__r13__r14__int7) 1 ) 
		(= 
			(flow__r9__r14__int7) 2 ) (availableflow__r15e__r14__int7) (availableflow__r13__r14__int7) (availableflow__r9__r14__int7) (active__int7__r15e) (active__int7__r13) (active__int7__r9) 
		(= 
			(greentime__int8) 0 ) 
		(= 
			(token__int8) 10 ) 
		(= 
			(maxtoken__int8) 20 ) 
		(= 
			(tokenvalue__r15w__int8) 10 ) 
		(= 
			(tokenvalue__r16e__int8) 10 ) 
		(= 
			(tokenvalue__r8__int8) 20 ) 
		(= 
			(maxgreentime__int8) 20 ) 
		(= 
			(mingreentime__int8) 4 ) 
		(= 
			(flow__r15w__r16w__int8) 2 ) 
		(= 
			(flow__r16e__r15e__int8) 2 ) 
		(= 
			(flow__r8__r16e__int8) 1 ) 
		(= 
			(flow__r8__r15e__int8) 1 ) (availableflow__r15w__r16w__int8) (availableflow__r16e__r15e__int8) (availableflow__r8__r16w__int8) (availableflow__r8__r15e__int8) (active__int8__r16e) (active__int8__r15w) (active__int8__r8) 
		(= 
			(greentime__int9) 0 ) 
		(= 
			(token__int9) 10 ) 
		(= 
			(maxtoken__int9) 20 ) 
		(= 
			(tokenvalue__r5__int9) 10 ) 
		(= 
			(tokenvalue__r16w__int9) 20 ) 
		(= 
			(maxgreentime__int9) 20 ) 
		(= 
			(mingreentime__int9) 4 ) 
		(= 
			(flow__r5__r16e__int9) 3 ) 
		(= 
			(flow__r5__r17__int9) 2 ) 
		(= 
			(flow__r16w__r17__int9) 2 ) (availableflow__r5__r16e__int9) (availableflow__r5__r17__int9) (availableflow__r16w__r17__int9) (active__int9__r5) (active__int9__r16w) 
		(= 
			(delta) 1 ) 
		(= 
			(copy--queue__r1) 3000 ) 
		(= 
			(copy--queue__r2) 3000 ) 
		(= 
			(copy--queue__r3) 3000 ) 
		(= 
			(copy--queue__r4) 3000 ) 
		(= 
			(copy--queue__r5) 200 ) 
		(= 
			(copy--queue__r6w) 200 ) 
		(= 
			(copy--queue__r6e) 200 ) 
		(= 
			(copy--queue__r7w) 200 ) 
		(= 
			(copy--queue__r7e) 200 ) 
		(= 
			(copy--queue__r8) 200 ) 
		(= 
			(copy--queue__r9) 300 ) 
		(= 
			(copy--queue__r10) 300 ) 
		(= 
			(copy--queue__r11w) 50 ) 
		(= 
			(copy--queue__r11e) 50 ) 
		(= 
			(copy--queue__r12) 300 ) 
		(= 
			(copy--queue__r13) 100 ) 
		(= 
			(copy--queue__r14) 0 ) 
		(= 
			(copy--queue__r15w) 50 ) 
		(= 
			(copy--queue__r15e) 50 ) 
		(= 
			(copy--queue__r16w) 50 ) 
		(= 
			(copy--queue__r16e) 50 ) 
		(= 
			(copy--queue__r17) 0 ) 
		(= 
			(copy--max_queue__r1) 5000 ) 
		(= 
			(copy--max_queue__r2) 5000 ) 
		(= 
			(copy--max_queue__r3) 5000 ) 
		(= 
			(copy--max_queue__r4) 5000 ) 
		(= 
			(copy--max_queue__r5) 500 ) 
		(= 
			(copy--max_queue__r6w) 500 ) 
		(= 
			(copy--max_queue__r7w) 500 ) 
		(= 
			(copy--max_queue__r6e) 500 ) 
		(= 
			(copy--max_queue__r7e) 500 ) 
		(= 
			(copy--max_queue__r8) 300 ) 
		(= 
			(copy--max_queue__r9) 500 ) 
		(= 
			(copy--max_queue__r10) 500 ) 
		(= 
			(copy--max_queue__r11w) 150 ) 
		(= 
			(copy--max_queue__r11e) 150 ) 
		(= 
			(copy--max_queue__r12) 700 ) 
		(= 
			(copy--max_queue__r13) 500 ) 
		(= 
			(copy--max_queue__r14) 10000 ) 
		(= 
			(copy--max_queue__r15w) 200 ) 
		(= 
			(copy--max_queue__r15e) 200 ) 
		(= 
			(copy--max_queue__r16w) 200 ) 
		(= 
			(copy--max_queue__r16e) 200 ) 
		(= 
			(copy--max_queue__r17) 10000 ) 
		(= 
			(copy--saturated_queue__r1) 500 ) 
		(= 
			(copy--saturated_queue__r2) 500 ) 
		(= 
			(copy--saturated_queue__r3) 500 ) 
		(= 
			(copy--saturated_queue__r4) 500 ) 
		(= 
			(copy--saturated_queue__r5) 300 ) 
		(= 
			(copy--saturated_queue__r6w) 300 ) 
		(= 
			(copy--saturated_queue__r7w) 300 ) 
		(= 
			(copy--saturated_queue__r6e) 300 ) 
		(= 
			(copy--saturated_queue__r7e) 300 ) 
		(= 
			(copy--saturated_queue__r8) 200 ) 
		(= 
			(copy--saturated_queue__r9) 300 ) 
		(= 
			(copy--saturated_queue__r10) 400 ) 
		(= 
			(copy--saturated_queue__r11w) 100 ) 
		(= 
			(copy--saturated_queue__r11e) 100 ) 
		(= 
			(copy--saturated_queue__r12) 500 ) 
		(= 
			(copy--saturated_queue__r13) 300 ) 
		(= 
			(copy--saturated_queue__r14) 10000 ) 
		(= 
			(copy--saturated_queue__r15w) 100 ) 
		(= 
			(copy--saturated_queue__r15e) 100 ) 
		(= 
			(copy--saturated_queue__r16w) 100 ) 
		(= 
			(copy--saturated_queue__r16e) 100 ) 
		(= 
			(copy--saturated_queue__r17) 10000 ) 
		(= 
			(copy--greentime__int1) 0 ) 
		(= 
			(copy--token__int1) 10 ) 
		(= 
			(copy--maxtoken__int1) 20 ) 
		(= 
			(copy--tokenvalue__r4__int1) 10 ) 
		(= 
			(copy--tokenvalue__r6w__int1) 20 ) 
		(= 
			(copy--maxgreentime__int1) 40 ) 
		(= 
			(copy--mingreentime__int1) 5 ) 
		(= 
			(copy--flow__r4__r6e__int1) 3 ) 
		(= 
			(copy--flow__r4__r5__int1) 5 ) 
		(= 
			(copy--flow__r6w__r5__int1) 2 ) 
		(= 
			(copy--greentime__int2) 0 ) 
		(= 
			(copy--token__int2) 10 ) 
		(= 
			(copy--maxtoken__int2) 20 ) 
		(= 
			(copy--tokenvalue__r7w__int2) 10 ) 
		(= 
			(copy--tokenvalue__r6e__int2) 20 ) 
		(= 
			(copy--maxgreentime__int2) 40 ) 
		(= 
			(copy--mingreentime__int2) 5 ) 
		(= 
			(copy--flow__r7w__r6w__int2) 2 ) 
		(= 
			(copy--flow__r6e__r7e__int2) 2 ) 
		(= 
			(copy--flow__r6e__r8__int2) 1 ) 
		(= 
			(copy--flow__r7w__r8__int2) 1 ) 
		(= 
			(copy--greentime__int3) 0 ) 
		(= 
			(copy--token__int3) 10 ) 
		(= 
			(copy--maxtoken__int3) 20 ) 
		(= 
			(copy--tokenvalue__r10__int3) 10 ) 
		(= 
			(copy--tokenvalue__r7e__int3) 20 ) 
		(= 
			(copy--maxgreentime__int3) 40 ) 
		(= 
			(copy--mingreentime__int3) 5 ) 
		(= 
			(copy--flow__r10__r7w__int3) 1 ) 
		(= 
			(copy--flow__r10__r9__int3) 2 ) 
		(= 
			(copy--flow__r7e__r9__int3) 1 ) 
		(= 
			(copy--greentime__int4) 0 ) 
		(= 
			(copy--token__int4) 10 ) 
		(= 
			(copy--maxtoken__int4) 20 ) 
		(= 
			(copy--tokenvalue__r3__int4) 10 ) 
		(= 
			(copy--tokenvalue__r11w__int4) 20 ) 
		(= 
			(copy--maxgreentime__int4) 40 ) 
		(= 
			(copy--mingreentime__int4) 5 ) 
		(= 
			(copy--flow__r3__r11e__int4) 3 ) 
		(= 
			(copy--flow__r3__r10__int4) 5 ) 
		(= 
			(copy--flow__r11w__r10__int4) 1 ) 
		(= 
			(copy--greentime__int5) 0 ) 
		(= 
			(copy--token__int5) 10 ) 
		(= 
			(copy--maxtoken__int5) 20 ) 
		(= 
			(copy--tokenvalue__r2__int5) 10 ) 
		(= 
			(copy--tokenvalue__r11e__int5) 20 ) 
		(= 
			(copy--maxgreentime__int5) 40 ) 
		(= 
			(copy--mingreentime__int5) 5 ) 
		(= 
			(copy--flow__r2__r11w__int5) 3 ) 
		(= 
			(copy--flow__r2__r12__int5) 5 ) 
		(= 
			(copy--flow__r11e__r12__int5) 1 ) 
		(= 
			(copy--greentime__int6) 0 ) 
		(= 
			(copy--token__int6) 10 ) 
		(= 
			(copy--maxtoken__int6) 20 ) 
		(= 
			(copy--tokenvalue__r1__int6) 10 ) 
		(= 
			(copy--tokenvalue__r12__int6) 20 ) 
		(= 
			(copy--maxgreentime__int6) 40 ) 
		(= 
			(copy--mingreentime__int6) 5 ) 
		(= 
			(copy--flow__r12__r13__int6) 2 ) 
		(= 
			(copy--flow__r1__r13__int6) 5 ) 
		(= 
			(copy--greentime__int7) 0 ) 
		(= 
			(copy--token__int7) 10 ) 
		(= 
			(copy--maxtoken__int7) 20 ) 
		(= 
			(copy--tokenvalue__r15e__int7) 10 ) 
		(= 
			(copy--tokenvalue__r13__int7) 10 ) 
		(= 
			(copy--tokenvalue__r9__int7) 20 ) 
		(= 
			(copy--maxgreentime__int7) 40 ) 
		(= 
			(copy--mingreentime__int7) 5 ) 
		(= 
			(copy--flow__r15e__r14__int7) 1 ) 
		(= 
			(copy--flow__r13__r14__int7) 1 ) 
		(= 
			(copy--flow__r9__r14__int7) 2 ) 
		(= 
			(copy--greentime__int8) 0 ) 
		(= 
			(copy--token__int8) 10 ) 
		(= 
			(copy--maxtoken__int8) 20 ) 
		(= 
			(copy--tokenvalue__r15w__int8) 10 ) 
		(= 
			(copy--tokenvalue__r16e__int8) 10 ) 
		(= 
			(copy--tokenvalue__r8__int8) 20 ) 
		(= 
			(copy--maxgreentime__int8) 20 ) 
		(= 
			(copy--mingreentime__int8) 4 ) 
		(= 
			(copy--flow__r15w__r16w__int8) 2 ) 
		(= 
			(copy--flow__r16e__r15e__int8) 2 ) 
		(= 
			(copy--flow__r8__r16e__int8) 1 ) 
		(= 
			(copy--flow__r8__r15e__int8) 1 ) 
		(= 
			(copy--greentime__int9) 0 ) 
		(= 
			(copy--token__int9) 10 ) 
		(= 
			(copy--maxtoken__int9) 20 ) 
		(= 
			(copy--tokenvalue__r5__int9) 10 ) 
		(= 
			(copy--tokenvalue__r16w__int9) 20 ) 
		(= 
			(copy--maxgreentime__int9) 20 ) 
		(= 
			(copy--mingreentime__int9) 4 ) 
		(= 
			(copy--flow__r5__r16e__int9) 3 ) 
		(= 
			(copy--flow__r5__r17__int9) 2 ) 
		(= 
			(copy--flow__r16w__r17__int9) 2 ) 
		(= 
			(copy--delta) 1 ) 
		(= 
			(carsinbuffer__buf0) 0 ) 
		(= 
			(copy--carsinbuffer__buf0) 0 ) (force-events) 
		 
)
(:goal
	
	(and 
		
			(< 
				(queue__r4) (saturated_queue__r4) ) 
			(< 
				(queue__r3) (saturated_queue__r3) ) 
			(< 
				(queue__r2) (saturated_queue__r2) ) 
			(< 
				(queue__r1) (saturated_queue__r1) ) 
			(not (pause)) 
			(not (force-events)) )
)

)