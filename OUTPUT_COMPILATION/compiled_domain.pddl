(define (domain grounded-urbantraffic)
(:requirements :fluents :adl :typing)
(:predicates
	(bufferconnected__buf0__r1)
	(availableflow__r2__r5__ter1)
	(availableflow__r3__r5__ter1)
	(availableflow__r1__r5__ter1)
	(active__ter1__r1)
	(active__ter1__r2)
	(active__ter1__r3)
	(availableflow__r8__r10__ter3)
	(availableflow__r8__r7n__ter3)
	(availableflow__r7s__r10__ter3)
	(active__ter3__r8)
	(active__ter3__r7s)
	(availableflow__r5__r9__ter2)
	(availableflow__r5__r7s__ter2)
	(availableflow__r6__r9__ter2)
	(availableflow__r6__r7s__ter2)
	(availableflow__r7n__r9__ter2)
	(active__ter2__r5)
	(active__ter2__r6)
	(active__ter2__r7n)
	(gotcars__buf0)
	(force-events)
	(fired--restartoken__ter1)
	(fired--restartoken__ter2)
	(fired--restartoken__ter3)
	(fired--maxgreenreachedANDmaxtoken__ter1)
	(fired--maxgreenreachedANDmaxtoken__ter2)
	(fired--maxgreenreachedANDmaxtoken__ter3)
	(fired--maxgreenreachedANDNOTmaxtoken__ter1)
	(fired--maxgreenreachedANDNOTmaxtoken__ter2)
	(fired--maxgreenreachedANDNOTmaxtoken__ter3)
	(fired--releasecar__buf0__r1)
	(pause)
	(done--0)
	(done--1)
	(done--2)
	(done--3)
	(done--4)
	(done--5)
	(done--6)
	(done--7)
	(done--8)
	(done--9)
	(done--10)
	(done--11)
	(done--12)
	(done--13)
	(done--14)
	(done--15)
	(done--16)
	(done--17)
	(done--18)
	(done--19)
	(done--20)
	(done--21)
	(done--22)
	(done--23)
	(done--24)
	(done--25)
	(done--26)
	(done--27)
	(done--28)
	(done--29)
	(done--30)
)

(:functions
	(queue__r1)
	(queue__r2)
	(queue__r3)
	(queue__r4)
	(queue__r5)
	(queue__r6)
	(queue__r7s)
	(queue__r7n)
	(queue__r8)
	(queue__r9)
	(queue__r10)
	(max_queue__r1)
	(max_queue__r2)
	(max_queue__r3)
	(max_queue__r4)
	(max_queue__r5)
	(max_queue__r6)
	(max_queue__r7n)
	(max_queue__r7s)
	(max_queue__r8)
	(max_queue__r9)
	(max_queue__r10)
	(saturated_queue__r1)
	(saturated_queue__r2)
	(saturated_queue__r3)
	(greentime__ter1)
	(token__ter1)
	(maxtoken__ter1)
	(tokenvalue__r1__ter1)
	(tokenvalue__r2__ter1)
	(tokenvalue__r3__ter1)
	(maxgreentime__ter1)
	(mingreentime__ter1)
	(flow__r2__r5__ter1)
	(flow__r1__r5__ter1)
	(flow__r3__r5__ter1)
	(maxgreentime__ter3)
	(mingreentime__ter3)
	(greentime__ter3)
	(token__ter3)
	(maxtoken__ter3)
	(tokenvalue__r7s__ter3)
	(tokenvalue__r8__ter3)
	(flow__r8__r10__ter3)
	(flow__r8__r7n__ter3)
	(flow__r7s__r10__ter3)
	(greentime__ter2)
	(token__ter2)
	(maxtoken__ter2)
	(tokenvalue__r5__ter2)
	(tokenvalue__r6__ter2)
	(tokenvalue__r7n__ter2)
	(maxgreentime__ter2)
	(mingreentime__ter2)
	(flow__r5__r9__ter2)
	(flow__r5__r7s__ter2)
	(flow__r7n__r9__ter2)
	(flow__r6__r9__ter2)
	(flow__r6__r7s__ter2)
	(carsinbuffer__buf0)
	(delta)
	(copy--queue__r1)
	(copy--queue__r2)
	(copy--queue__r3)
	(copy--queue__r4)
	(copy--queue__r5)
	(copy--queue__r6)
	(copy--queue__r7s)
	(copy--queue__r7n)
	(copy--queue__r8)
	(copy--queue__r9)
	(copy--queue__r10)
	(copy--max_queue__r1)
	(copy--max_queue__r2)
	(copy--max_queue__r3)
	(copy--max_queue__r4)
	(copy--max_queue__r5)
	(copy--max_queue__r6)
	(copy--max_queue__r7n)
	(copy--max_queue__r7s)
	(copy--max_queue__r8)
	(copy--max_queue__r9)
	(copy--max_queue__r10)
	(copy--saturated_queue__r1)
	(copy--saturated_queue__r2)
	(copy--saturated_queue__r3)
	(copy--greentime__ter1)
	(copy--token__ter1)
	(copy--maxtoken__ter1)
	(copy--tokenvalue__r1__ter1)
	(copy--tokenvalue__r2__ter1)
	(copy--tokenvalue__r3__ter1)
	(copy--maxgreentime__ter1)
	(copy--mingreentime__ter1)
	(copy--flow__r2__r5__ter1)
	(copy--flow__r1__r5__ter1)
	(copy--flow__r3__r5__ter1)
	(copy--maxgreentime__ter3)
	(copy--mingreentime__ter3)
	(copy--greentime__ter3)
	(copy--token__ter3)
	(copy--maxtoken__ter3)
	(copy--tokenvalue__r7s__ter3)
	(copy--tokenvalue__r8__ter3)
	(copy--flow__r8__r10__ter3)
	(copy--flow__r8__r7n__ter3)
	(copy--flow__r7s__r10__ter3)
	(copy--greentime__ter2)
	(copy--token__ter2)
	(copy--maxtoken__ter2)
	(copy--tokenvalue__r5__ter2)
	(copy--tokenvalue__r6__ter2)
	(copy--tokenvalue__r7n__ter2)
	(copy--maxgreentime__ter2)
	(copy--mingreentime__ter2)
	(copy--flow__r5__r9__ter2)
	(copy--flow__r5__r7s__ter2)
	(copy--flow__r7n__r9__ter2)
	(copy--flow__r6__r9__ter2)
	(copy--flow__r6__r7s__ter2)
	(copy--carsinbuffer__buf0)
	(copy--delta)
)

(:action switchTrafficSignal__ter1
 :parameters()
 :precondition 
	(and 
		
			(>= 
				(greentime__ter1) 5 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(token__ter1) 10 ) 
			(assign 
				(greentime__ter1) 0 ) (force-events) )
)

(:action switchTrafficSignal__ter2
 :parameters()
 :precondition 
	(and 
		
			(>= 
				(greentime__ter2) 5 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(token__ter2) 10 ) 
			(assign 
				(greentime__ter2) 0 ) (force-events) )
)

(:action switchTrafficSignal__ter3
 :parameters()
 :precondition 
	(and 
		
			(>= 
				(greentime__ter3) 5 ) 
			(not (force-events)) 
			(not (pause)) )
 :effect 
	(and 
		
			(increase 
				(token__ter3) 10 ) 
			(assign 
				(greentime__ter3) 0 ) (force-events) )
)

(:action simulate-events
 :parameters()
 :precondition 
	(and (force-events))
 :effect 
	(and 
		
			(not (force-events)) 
			
				(when 
					
						(and 
							
								(<= 
									(greentime__ter1) 
										(- 
											25 10 ) ) 
								(>= 
									(token__ter1) 
										(+ 
											30 10 ) ) ) 
						(and 
							
								(assign 
									(greentime__ter1) 0 ) 
								(assign 
									(token__ter1) 10 ) ) ) 
				(when 
					
						(and 
							
								(<= 
									(greentime__ter2) 
										(- 
											25 10 ) ) 
								(>= 
									(token__ter2) 
										(+ 
											30 10 ) ) ) 
						(and 
							
								(assign 
									(greentime__ter2) 0 ) 
								(assign 
									(token__ter2) 10 ) ) ) 
				(when 
					
						(and 
							
								(<= 
									(greentime__ter3) 
										(- 
											25 10 ) ) 
								(>= 
									(token__ter3) 
										(+ 
											20 10 ) ) ) 
						(and 
							
								(assign 
									(greentime__ter3) 0 ) 
								(assign 
									(token__ter3) 10 ) ) ) 
				(when 
					
						(and 
							
								(>= 
									(greentime__ter1) 25 ) 
								(= 
									(token__ter1) 30 ) ) 
						(and 
							
								(assign 
									(greentime__ter1) 0 ) 
								(assign 
									(token__ter1) 10 ) ) ) 
				(when 
					
						(and 
							
								(>= 
									(greentime__ter2) 25 ) 
								(= 
									(token__ter2) 30 ) ) 
						(and 
							
								(assign 
									(greentime__ter2) 0 ) 
								(assign 
									(token__ter2) 10 ) ) ) 
				(when 
					
						(and 
							
								(>= 
									(greentime__ter3) 25 ) 
								(= 
									(token__ter3) 20 ) ) 
						(and 
							
								(assign 
									(greentime__ter3) 0 ) 
								(assign 
									(token__ter3) 10 ) ) ) 
				(when 
					
						(and 
							
								(>= 
									(greentime__ter1) 25 ) 
								(< 
									(token__ter1) 30 ) ) 
						(and 
							
								(assign 
									(greentime__ter1) 0 ) 
								(increase 
									(token__ter1) 10 ) ) ) 
				(when 
					
						(and 
							
								(>= 
									(greentime__ter2) 25 ) 
								(< 
									(token__ter2) 30 ) ) 
						(and 
							
								(assign 
									(greentime__ter2) 0 ) 
								(increase 
									(token__ter2) 10 ) ) ) 
				(when 
					
						(and 
							
								(>= 
									(greentime__ter3) 25 ) 
								(< 
									(token__ter3) 20 ) ) 
						(and 
							
								(assign 
									(greentime__ter3) 0 ) 
								(increase 
									(token__ter3) 10 ) ) ) 
				(when 
					
						(and 
							(gotcars__buf0) 
								(>= 
									(carsinbuffer__buf0) 1 ) ) 
						(and 
							
								(not 
									(gotcars__buf0) ) 
								(increase 
									(queue__r1) (carsinbuffer__buf0) ) 
								(assign 
									(carsinbuffer__buf0) 0 ) ) )  )
)

(:action start-simulate-processes
 :parameters()
 :precondition 
	(and 
		
			(not (pause)) 
			(not (force-events)) )
 :effect 
	(and 
		
			(assign 
				(copy--queue__r1) (queue__r1) ) 
			(assign 
				(copy--queue__r2) (queue__r2) ) 
			(assign 
				(copy--queue__r3) (queue__r3) ) 
			(assign 
				(copy--queue__r5) (queue__r5) ) 
			(assign 
				(copy--queue__r6) (queue__r6) ) 
			(assign 
				(copy--queue__r7s) (queue__r7s) ) 
			(assign 
				(copy--queue__r7n) (queue__r7n) ) 
			(assign 
				(copy--queue__r8) (queue__r8) ) 
			(assign 
				(copy--queue__r9) (queue__r9) ) 
			(assign 
				(copy--queue__r10) (queue__r10) ) 
			(assign 
				(copy--greentime__ter2) (greentime__ter2) ) 
			(assign 
				(copy--token__ter2) (token__ter2) ) 
			(assign 
				(copy--greentime__ter3) (greentime__ter3) ) 
			(assign 
				(copy--token__ter3) (token__ter3) ) 
			(assign 
				(copy--greentime__ter1) (greentime__ter1) ) 
			(assign 
				(copy--token__ter1) (token__ter1) ) (pause) (done--0) 
			 )
)

(:action simulate--positive--keepgreen__r1__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter1) 10 ) 
					(< 
						(copy--greentime__ter1) 25 ) ) (done--0) )
 :effect 
	(and 
		
			(increase 
				(greentime__ter1) 
					(* 
						(delta) 1 ) ) 
			(not (done--0)) (done--1) )
)

(:action simulate--negative--keepgreen__r1__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter1) 10 ) 
						(< 
							(copy--greentime__ter1) 25 ) )) (done--0) )
 :effect 
	(and 
		
			(not (done--0)) (done--1) )
)

(:action simulate--positive--keepgreen__r2__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter1) 20 ) 
					(< 
						(copy--greentime__ter1) 25 ) ) (done--1) )
 :effect 
	(and 
		
			(increase 
				(greentime__ter1) 
					(* 
						(delta) 1 ) ) 
			(not (done--1)) (done--2) )
)

(:action simulate--negative--keepgreen__r2__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter1) 20 ) 
						(< 
							(copy--greentime__ter1) 25 ) )) (done--1) )
 :effect 
	(and 
		
			(not (done--1)) (done--2) )
)

(:action simulate--positive--keepgreen__r3__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter1) 30 ) 
					(< 
						(copy--greentime__ter1) 25 ) ) (done--2) )
 :effect 
	(and 
		
			(increase 
				(greentime__ter1) 
					(* 
						(delta) 1 ) ) 
			(not (done--2)) (done--3) )
)

(:action simulate--negative--keepgreen__r3__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter1) 30 ) 
						(< 
							(copy--greentime__ter1) 25 ) )) (done--2) )
 :effect 
	(and 
		
			(not (done--2)) (done--3) )
)

(:action simulate--positive--keepgreen__r5__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 10 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--3) )
 :effect 
	(and 
		
			(increase 
				(greentime__ter2) 
					(* 
						(delta) 1 ) ) 
			(not (done--3)) (done--4) )
)

(:action simulate--negative--keepgreen__r5__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 10 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--3) )
 :effect 
	(and 
		
			(not (done--3)) (done--4) )
)

(:action simulate--positive--keepgreen__r6__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 20 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--4) )
 :effect 
	(and 
		
			(increase 
				(greentime__ter2) 
					(* 
						(delta) 1 ) ) 
			(not (done--4)) (done--5) )
)

(:action simulate--negative--keepgreen__r6__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 20 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--4) )
 :effect 
	(and 
		
			(not (done--4)) (done--5) )
)

(:action simulate--positive--keepgreen__r7n__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 30 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--5) )
 :effect 
	(and 
		
			(increase 
				(greentime__ter2) 
					(* 
						(delta) 1 ) ) 
			(not (done--5)) (done--6) )
)

(:action simulate--negative--keepgreen__r7n__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 30 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--5) )
 :effect 
	(and 
		
			(not (done--5)) (done--6) )
)

(:action simulate--positive--keepgreen__r7s__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter3) 10 ) 
					(< 
						(copy--greentime__ter3) 25 ) ) (done--6) )
 :effect 
	(and 
		
			(increase 
				(greentime__ter3) 
					(* 
						(delta) 1 ) ) 
			(not (done--6)) (done--7) )
)

(:action simulate--negative--keepgreen__r7s__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter3) 10 ) 
						(< 
							(copy--greentime__ter3) 25 ) )) (done--6) )
 :effect 
	(and 
		
			(not (done--6)) (done--7) )
)

(:action simulate--positive--keepgreen__r8__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter3) 20 ) 
					(< 
						(copy--greentime__ter3) 25 ) ) (done--7) )
 :effect 
	(and 
		
			(increase 
				(greentime__ter3) 
					(* 
						(delta) 1 ) ) 
			(not (done--7)) (done--8) )
)

(:action simulate--negative--keepgreen__r8__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter3) 20 ) 
						(< 
							(copy--greentime__ter3) 25 ) )) (done--7) )
 :effect 
	(and 
		
			(not (done--7)) (done--8) )
)

(:action simulate--positive--flowrun_green__r1__r5__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter1) 10 ) 
					(> 
						(copy--queue__r1) 0 ) 
					(< 
						(copy--queue__r5) 70 ) 
					(< 
						(copy--greentime__ter1) 25 ) ) (done--8) )
 :effect 
	(and 
		
			(increase 
				(queue__r5) 
					(* 
						(delta) 2 ) ) 
			(not (done--8)) (done--9) )
)

(:action simulate--negative--flowrun_green__r1__r5__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter1) 10 ) 
						(> 
							(copy--queue__r1) 0 ) 
						(< 
							(copy--queue__r5) 70 ) 
						(< 
							(copy--greentime__ter1) 25 ) )) (done--8) )
 :effect 
	(and 
		
			(not (done--8)) (done--9) )
)

(:action simulate--positive--flowrun_green__r1__r5__ter1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter1) 10 ) 
					(> 
						(copy--queue__r1) 0 ) 
					(< 
						(copy--queue__r5) 70 ) 
					(< 
						(copy--greentime__ter1) 25 ) ) (done--9) )
 :effect 
	(and 
		
			(decrease 
				(queue__r1) 
					(* 
						(delta) 2 ) ) 
			(not (done--9)) (done--10) )
)

(:action simulate--negative--flowrun_green__r1__r5__ter1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter1) 10 ) 
						(> 
							(copy--queue__r1) 0 ) 
						(< 
							(copy--queue__r5) 70 ) 
						(< 
							(copy--greentime__ter1) 25 ) )) (done--9) )
 :effect 
	(and 
		
			(not (done--9)) (done--10) )
)

(:action simulate--positive--flowrun_green__r2__r5__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter1) 20 ) 
					(> 
						(copy--queue__r2) 0 ) 
					(< 
						(copy--queue__r5) 70 ) 
					(< 
						(copy--greentime__ter1) 25 ) ) (done--10) )
 :effect 
	(and 
		
			(increase 
				(queue__r5) 
					(* 
						(delta) 2 ) ) 
			(not (done--10)) (done--11) )
)

(:action simulate--negative--flowrun_green__r2__r5__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter1) 20 ) 
						(> 
							(copy--queue__r2) 0 ) 
						(< 
							(copy--queue__r5) 70 ) 
						(< 
							(copy--greentime__ter1) 25 ) )) (done--10) )
 :effect 
	(and 
		
			(not (done--10)) (done--11) )
)

(:action simulate--positive--flowrun_green__r2__r5__ter1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter1) 20 ) 
					(> 
						(copy--queue__r2) 0 ) 
					(< 
						(copy--queue__r5) 70 ) 
					(< 
						(copy--greentime__ter1) 25 ) ) (done--11) )
 :effect 
	(and 
		
			(decrease 
				(queue__r2) 
					(* 
						(delta) 2 ) ) 
			(not (done--11)) (done--12) )
)

(:action simulate--negative--flowrun_green__r2__r5__ter1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter1) 20 ) 
						(> 
							(copy--queue__r2) 0 ) 
						(< 
							(copy--queue__r5) 70 ) 
						(< 
							(copy--greentime__ter1) 25 ) )) (done--11) )
 :effect 
	(and 
		
			(not (done--11)) (done--12) )
)

(:action simulate--positive--flowrun_green__r3__r5__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter1) 30 ) 
					(> 
						(copy--queue__r3) 0 ) 
					(< 
						(copy--queue__r5) 70 ) 
					(< 
						(copy--greentime__ter1) 25 ) ) (done--12) )
 :effect 
	(and 
		
			(increase 
				(queue__r5) 
					(* 
						(delta) 2 ) ) 
			(not (done--12)) (done--13) )
)

(:action simulate--negative--flowrun_green__r3__r5__ter1--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter1) 30 ) 
						(> 
							(copy--queue__r3) 0 ) 
						(< 
							(copy--queue__r5) 70 ) 
						(< 
							(copy--greentime__ter1) 25 ) )) (done--12) )
 :effect 
	(and 
		
			(not (done--12)) (done--13) )
)

(:action simulate--positive--flowrun_green__r3__r5__ter1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter1) 30 ) 
					(> 
						(copy--queue__r3) 0 ) 
					(< 
						(copy--queue__r5) 70 ) 
					(< 
						(copy--greentime__ter1) 25 ) ) (done--13) )
 :effect 
	(and 
		
			(decrease 
				(queue__r3) 
					(* 
						(delta) 2 ) ) 
			(not (done--13)) (done--14) )
)

(:action simulate--negative--flowrun_green__r3__r5__ter1--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter1) 30 ) 
						(> 
							(copy--queue__r3) 0 ) 
						(< 
							(copy--queue__r5) 70 ) 
						(< 
							(copy--greentime__ter1) 25 ) )) (done--13) )
 :effect 
	(and 
		
			(not (done--13)) (done--14) )
)

(:action simulate--positive--flowrun_green__r5__r7s__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 10 ) 
					(> 
						(copy--queue__r5) 0 ) 
					(< 
						(copy--queue__r7s) 50 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--14) )
 :effect 
	(and 
		
			(increase 
				(queue__r7s) 
					(* 
						(delta) 2 ) ) 
			(not (done--14)) (done--15) )
)

(:action simulate--negative--flowrun_green__r5__r7s__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 10 ) 
						(> 
							(copy--queue__r5) 0 ) 
						(< 
							(copy--queue__r7s) 50 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--14) )
 :effect 
	(and 
		
			(not (done--14)) (done--15) )
)

(:action simulate--positive--flowrun_green__r5__r7s__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 10 ) 
					(> 
						(copy--queue__r5) 0 ) 
					(< 
						(copy--queue__r7s) 50 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--15) )
 :effect 
	(and 
		
			(decrease 
				(queue__r5) 
					(* 
						(delta) 2 ) ) 
			(not (done--15)) (done--16) )
)

(:action simulate--negative--flowrun_green__r5__r7s__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 10 ) 
						(> 
							(copy--queue__r5) 0 ) 
						(< 
							(copy--queue__r7s) 50 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--15) )
 :effect 
	(and 
		
			(not (done--15)) (done--16) )
)

(:action simulate--positive--flowrun_green__r5__r9__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 10 ) 
					(> 
						(copy--queue__r5) 0 ) 
					(< 
						(copy--queue__r9) 10000 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--16) )
 :effect 
	(and 
		
			(increase 
				(queue__r9) 
					(* 
						(delta) 2 ) ) 
			(not (done--16)) (done--17) )
)

(:action simulate--negative--flowrun_green__r5__r9__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 10 ) 
						(> 
							(copy--queue__r5) 0 ) 
						(< 
							(copy--queue__r9) 10000 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--16) )
 :effect 
	(and 
		
			(not (done--16)) (done--17) )
)

(:action simulate--positive--flowrun_green__r5__r9__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 10 ) 
					(> 
						(copy--queue__r5) 0 ) 
					(< 
						(copy--queue__r9) 10000 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--17) )
 :effect 
	(and 
		
			(decrease 
				(queue__r5) 
					(* 
						(delta) 2 ) ) 
			(not (done--17)) (done--18) )
)

(:action simulate--negative--flowrun_green__r5__r9__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 10 ) 
						(> 
							(copy--queue__r5) 0 ) 
						(< 
							(copy--queue__r9) 10000 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--17) )
 :effect 
	(and 
		
			(not (done--17)) (done--18) )
)

(:action simulate--positive--flowrun_green__r6__r7s__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 20 ) 
					(> 
						(copy--queue__r6) 0 ) 
					(< 
						(copy--queue__r7s) 50 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--18) )
 :effect 
	(and 
		
			(increase 
				(queue__r7s) 
					(* 
						(delta) 2 ) ) 
			(not (done--18)) (done--19) )
)

(:action simulate--negative--flowrun_green__r6__r7s__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 20 ) 
						(> 
							(copy--queue__r6) 0 ) 
						(< 
							(copy--queue__r7s) 50 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--18) )
 :effect 
	(and 
		
			(not (done--18)) (done--19) )
)

(:action simulate--positive--flowrun_green__r6__r7s__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 20 ) 
					(> 
						(copy--queue__r6) 0 ) 
					(< 
						(copy--queue__r7s) 50 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--19) )
 :effect 
	(and 
		
			(decrease 
				(queue__r6) 
					(* 
						(delta) 2 ) ) 
			(not (done--19)) (done--20) )
)

(:action simulate--negative--flowrun_green__r6__r7s__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 20 ) 
						(> 
							(copy--queue__r6) 0 ) 
						(< 
							(copy--queue__r7s) 50 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--19) )
 :effect 
	(and 
		
			(not (done--19)) (done--20) )
)

(:action simulate--positive--flowrun_green__r6__r9__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 20 ) 
					(> 
						(copy--queue__r6) 0 ) 
					(< 
						(copy--queue__r9) 10000 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--20) )
 :effect 
	(and 
		
			(increase 
				(queue__r9) 
					(* 
						(delta) 2 ) ) 
			(not (done--20)) (done--21) )
)

(:action simulate--negative--flowrun_green__r6__r9__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 20 ) 
						(> 
							(copy--queue__r6) 0 ) 
						(< 
							(copy--queue__r9) 10000 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--20) )
 :effect 
	(and 
		
			(not (done--20)) (done--21) )
)

(:action simulate--positive--flowrun_green__r6__r9__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 20 ) 
					(> 
						(copy--queue__r6) 0 ) 
					(< 
						(copy--queue__r9) 10000 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--21) )
 :effect 
	(and 
		
			(decrease 
				(queue__r6) 
					(* 
						(delta) 2 ) ) 
			(not (done--21)) (done--22) )
)

(:action simulate--negative--flowrun_green__r6__r9__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 20 ) 
						(> 
							(copy--queue__r6) 0 ) 
						(< 
							(copy--queue__r9) 10000 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--21) )
 :effect 
	(and 
		
			(not (done--21)) (done--22) )
)

(:action simulate--positive--flowrun_green__r7n__r9__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 30 ) 
					(> 
						(copy--queue__r7n) 0 ) 
					(< 
						(copy--queue__r9) 10000 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--22) )
 :effect 
	(and 
		
			(increase 
				(queue__r9) 
					(* 
						(delta) 2 ) ) 
			(not (done--22)) (done--23) )
)

(:action simulate--negative--flowrun_green__r7n__r9__ter2--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 30 ) 
						(> 
							(copy--queue__r7n) 0 ) 
						(< 
							(copy--queue__r9) 10000 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--22) )
 :effect 
	(and 
		
			(not (done--22)) (done--23) )
)

(:action simulate--positive--flowrun_green__r7n__r9__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter2) 30 ) 
					(> 
						(copy--queue__r7n) 0 ) 
					(< 
						(copy--queue__r9) 10000 ) 
					(< 
						(copy--greentime__ter2) 25 ) ) (done--23) )
 :effect 
	(and 
		
			(decrease 
				(queue__r7n) 
					(* 
						(delta) 2 ) ) 
			(not (done--23)) (done--24) )
)

(:action simulate--negative--flowrun_green__r7n__r9__ter2--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter2) 30 ) 
						(> 
							(copy--queue__r7n) 0 ) 
						(< 
							(copy--queue__r9) 10000 ) 
						(< 
							(copy--greentime__ter2) 25 ) )) (done--23) )
 :effect 
	(and 
		
			(not (done--23)) (done--24) )
)

(:action simulate--positive--flowrun_green__r7s__r10__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter3) 10 ) 
					(> 
						(copy--queue__r7s) 0 ) 
					(< 
						(copy--queue__r10) 10000 ) 
					(< 
						(copy--greentime__ter3) 25 ) ) (done--24) )
 :effect 
	(and 
		
			(increase 
				(queue__r10) 
					(* 
						(delta) 2 ) ) 
			(not (done--24)) (done--25) )
)

(:action simulate--negative--flowrun_green__r7s__r10__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter3) 10 ) 
						(> 
							(copy--queue__r7s) 0 ) 
						(< 
							(copy--queue__r10) 10000 ) 
						(< 
							(copy--greentime__ter3) 25 ) )) (done--24) )
 :effect 
	(and 
		
			(not (done--24)) (done--25) )
)

(:action simulate--positive--flowrun_green__r7s__r10__ter3--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter3) 10 ) 
					(> 
						(copy--queue__r7s) 0 ) 
					(< 
						(copy--queue__r10) 10000 ) 
					(< 
						(copy--greentime__ter3) 25 ) ) (done--25) )
 :effect 
	(and 
		
			(decrease 
				(queue__r7s) 
					(* 
						(delta) 2 ) ) 
			(not (done--25)) (done--26) )
)

(:action simulate--negative--flowrun_green__r7s__r10__ter3--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter3) 10 ) 
						(> 
							(copy--queue__r7s) 0 ) 
						(< 
							(copy--queue__r10) 10000 ) 
						(< 
							(copy--greentime__ter3) 25 ) )) (done--25) )
 :effect 
	(and 
		
			(not (done--25)) (done--26) )
)

(:action simulate--positive--flowrun_green__r8__r7n__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter3) 20 ) 
					(> 
						(copy--queue__r8) 0 ) 
					(< 
						(copy--queue__r7n) 50 ) 
					(< 
						(copy--greentime__ter3) 25 ) ) (done--26) )
 :effect 
	(and 
		
			(increase 
				(queue__r7n) 
					(* 
						(delta) 2 ) ) 
			(not (done--26)) (done--27) )
)

(:action simulate--negative--flowrun_green__r8__r7n__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter3) 20 ) 
						(> 
							(copy--queue__r8) 0 ) 
						(< 
							(copy--queue__r7n) 50 ) 
						(< 
							(copy--greentime__ter3) 25 ) )) (done--26) )
 :effect 
	(and 
		
			(not (done--26)) (done--27) )
)

(:action simulate--positive--flowrun_green__r8__r7n__ter3--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter3) 20 ) 
					(> 
						(copy--queue__r8) 0 ) 
					(< 
						(copy--queue__r7n) 50 ) 
					(< 
						(copy--greentime__ter3) 25 ) ) (done--27) )
 :effect 
	(and 
		
			(decrease 
				(queue__r8) 
					(* 
						(delta) 2 ) ) 
			(not (done--27)) (done--28) )
)

(:action simulate--negative--flowrun_green__r8__r7n__ter3--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter3) 20 ) 
						(> 
							(copy--queue__r8) 0 ) 
						(< 
							(copy--queue__r7n) 50 ) 
						(< 
							(copy--greentime__ter3) 25 ) )) (done--27) )
 :effect 
	(and 
		
			(not (done--27)) (done--28) )
)

(:action simulate--positive--flowrun_green__r8__r10__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter3) 20 ) 
					(> 
						(copy--queue__r8) 0 ) 
					(< 
						(copy--queue__r10) 10000 ) 
					(< 
						(copy--greentime__ter3) 25 ) ) (done--28) )
 :effect 
	(and 
		
			(increase 
				(queue__r10) 
					(* 
						(delta) 2 ) ) 
			(not (done--28)) (done--29) )
)

(:action simulate--negative--flowrun_green__r8__r10__ter3--0
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter3) 20 ) 
						(> 
							(copy--queue__r8) 0 ) 
						(< 
							(copy--queue__r10) 10000 ) 
						(< 
							(copy--greentime__ter3) 25 ) )) (done--28) )
 :effect 
	(and 
		
			(not (done--28)) (done--29) )
)

(:action simulate--positive--flowrun_green__r8__r10__ter3--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(and 
				
					(= 
						(copy--token__ter3) 20 ) 
					(> 
						(copy--queue__r8) 0 ) 
					(< 
						(copy--queue__r10) 10000 ) 
					(< 
						(copy--greentime__ter3) 25 ) ) (done--29) )
 :effect 
	(and 
		
			(decrease 
				(queue__r8) 
					(* 
						(delta) 2 ) ) 
			(not (done--29)) (done--30) )
)

(:action simulate--negative--flowrun_green__r8__r10__ter3--1
 :parameters()
 :precondition 
	(and 
		(pause) 
			(not 
				(and 
					
						(= 
							(copy--token__ter3) 20 ) 
						(> 
							(copy--queue__r8) 0 ) 
						(< 
							(copy--queue__r10) 10000 ) 
						(< 
							(copy--greentime__ter3) 25 ) )) (done--29) )
 :effect 
	(and 
		
			(not (done--29)) (done--30) )
)

(:action end-simulate-processes
 :parameters()
 :precondition 
	(and 
		(pause) (done--30) )
 :effect 
	(and 
		
			(not (pause)) 
			(not (done--30)) (force-events) )
)

)