import os
import nltk
import math
import click
import datetime
import matplotlib.pyplot as plt

from nltk.corpus import treebank
from networkx    import *
from os.path     import join, exists
from copy        import deepcopy

CURRENT_PATH = os.getcwd()

DOMAIN_KW       = 'domain'
TYPES_KW        = ':types'
PREDICATES_KW   = ':predicates'
FUNCTIONS_KW    = ':functions'
PROCESS_KW      = ':process'
PARAMETERS_KW   = ':parameters'
PRECONDITION_KW = ':precondition'
EFFECT_KW       = ':effect'
ACTION_KW       = ':action'
EVENTS_KW       = ':event'
PROCESSES_KW    = ':process'
INIT_KW         = ':init'
GOAL_KW         = ':goal'

METRIC = '(:metric minimize (total-time))\n'

# Domain strings
DOMAIN_NAME = 'domain_name'
TYPES       = 'types'
PREDICATES  = 'predicates'
FUNCTIONS   = 'functions'
PROCESSES   = 'processes'
EVENTS      = 'events'
ACTIONS     = 'actions'


# Global map strings
GROUNDED_VARIABLES  = 'grounded_variable'
GROUNDED_ACTIONS    = 'grounded_actions'
GROUNDED_PREDICATES = 'grounded_predicates'
GROUNDED_FUNCTIONS  = 'grounded_functions'
# Copyall map
DONE_LIST      = 'done_list'
COPY_INDX_DICT = 'copy_indx_dict'

G_A     = 'g_a'
G_E     = 'g_e'
G_P     = 'g_p'
G_PRED  = 'g_pred'
G_FUNCT = 'g_funct'

# Problem strings
PROBLEM_NAME = 'problem_name'
OBJECTS      = 'objects'
INIT_STATE   = 'init_state'
GOAL_STATE   = 'goal_state'

PARSED_EMPTY_PRECONDITION = '', []


WHEN      = 'when'
NOT       = 'not'
AND       = 'and'
OR        = 'or'
LESS      = '<'
LESSEQ    = '<='
GREATER   = '>'
GREATEREQ = '>='
EQUAL     = '='
ASSIGN    = 'assign'
INCREASE  = 'increase'
DECREASE  = 'decrease'
MINUS     = '-'
TIMES     = '*'
POWER     = '^'
DIVISION  = '/'
PLUS      = '+'
NAN       = None

NUMERIC_OPERATORS = [LESS, LESSEQ, GREATER, GREATEREQ, EQUAL, NOT, ASSIGN, DECREASE, INCREASE]
LOGICAL_OPERATORS = [AND, OR]
OPERANDS_SYMBOLS  = [MINUS, TIMES, POWER, DIVISION, PLUS]

PROPOSITIONAL = 'prop'
NUMERICAL     = 'num'

LVALUE = 0
RVALUE = 1

CONTINOUS_TIME = '#t'

FALSE   = False  # truth value
TRUE    = True
NEG     = False  # neg = del, pos = add
POS     = True
DISCARD = None  # used to discard grounded actions with pre(a) = False
 
NO_CASCADING_E  = False
YES_CASCADING_E = True

INVARIANT     = True
INVARIANTS    = 'invariant'
UNDEFINED_PAR = None
NO_PLAN       = None


NOT_FOUND = -1

''' new compiled predicates '''
UNDEFINED         = 'undefined'                   # invalid
FORCE_EVENTS      = 'force-events'                # invalid/copyall
FIRED             = 'fired--{}'                   # invalid/copyall
# DONE              = 'done--{}--{}'              # copyall
DONE              = 'done--{}'                    # copyall
COPY              = 'copy--{}'                    # copyall
PAUSE             = 'pause'                       # copyall
SIMULATE          = 'simulate--{}--{}'            # copyall
SIMULATE_POSITIVE = 'simulate--positive--{}--{}'  # copyall
SIMULATE_NEGATIVE = 'simulate--negative--{}--{}'  # copyall
BLOCKPRED         = 'blockpred'


''' new compiled actions '''
SIMULATE_EVENTS    = 'simulate-events'
SIMULATE_PROCESSES = 'simulate-processes'
START_SIMULATE     = 'start-simulate-processes'
END_SIMULATE       = 'end-simulate-processes'
UNBLOCK_ACTION     = 'unblock-action'

''' new functions '''
DELTA        = 'delta'
MINUS_DELTA  = 'minus-delta'
TOTAL_TIMEs  = 'total-time'
TOTAL_TIME   = '(total-time)'
NOVEL_TIME   = 'novel-time'


''' compilation methods '''
INVALID     = 'invalid'     # POLY-minus
INVALIDplus = 'invalidplus' # POLY-ominus
COPYALL     = 'copyall'     # POLY
EXP         = 'exp'         # EXP
POLYBEST    = 'polybest'    # POLY*
COPYALLplus = 'copyallplus' # ad hoc POLY combined with Z1

''' correct compilation names '''
POLY      = 'POLY'
EXPbold   = 'EXP'
POLYMINUS = 'POLY-'
EXPLOCAL  = 'EXPL'


''' compilation methods for validation '''
Z0           = 'z0'
Z1           = 'z1'
Z2           = 'z2'
Z3           = 'z3'
Z0_PLAIN     = 'z0+plain'
Z0_COPYALL   = 'z0+copyall'
Z0_EXP       = 'z0+exp'
Z1_PLAIN     = 'z1+plain'
Z1_COPYALL   = 'z1+copyall'
Z1_EXP       = 'z1+exp'
Z2_COPYALL_2 = 'z2+copyallplus' # ad hoc version of Z1_COPYALL for POLY
Z2_PLAIN     = 'z2+plain'
Z2_COPYALL   = 'z2+copyall'
Z3_PLAIN     = 'z3+plain'
Z3_COPYALL   = 'z3+copyall'

''' paper names '''
PLAIN        = 'PLAIN'
Z0p          = 'V0'
Z1p          = 'VU'
Z2p          = 'VUD'
#
Z0_PLAINp    = '{}+{}'.format(Z0p, PLAIN)
Z0_POLYp     = '{}+{}'.format(Z0p, POLY)
# Z0_EXPbold = '{}+{}'.format(Z0p, EXPbold)
#
Z1_PLAINp    = '{}+{}'.format(Z1p, PLAIN)
Z1_POLYp     = '{}+{}'.format(Z1p, POLY)
# Z1_EXPbold = '{}+{}'.format(Z1p, EXPbold)
#
Z2_PLAINp    = '{}+{}'.format(Z2p, PLAIN)
Z2_POLYp     = '{}+{}'.format(Z2p, POLY)
Z2_POLYVp    = 'POLYV'
# Z2_EXPbold = '{}+{}'.format(Z2p, EXPbold)

''' relaxed translation '''
R0               = 'r0'
R0_PLAIN         = 'r0+plain'
R0_PLAINp        = 'R0+PLAIN'
R0_COPYALL       = 'r0+copyall'
R0_COPYALLp      = 'R0+POLY'
R1               = 'r1'
R1_PLAIN         = 'r1+plain'
R1_PLAINp        = 'R1+PLAIN'
RD0              = 'rd0'
RD1              = 'rd1'
RD0_PLAIN        = 'rd0+plain'
RD0_PLAINp       = 'RD0+PLAIN'
RD1_PLAIN        = 'rd1+plain'
RD1_PLAINp       = 'RD1+PLAIN'
R0_LESS          = 'r0l'
R0_LESS_PLAIN    = 'r0l+plain'
R0_LESS_PLAINp   = 'R0L+PLAIN'
R1_LESS          = 'r1l'
R1_LESS_PLAIN    = 'r1l+plain'
R1_LESS_PLAINp   = 'R1L+PLAIN'
R0_CONSTR        = 'r0c'
R0_CONSTR_PLAINp = 'R0C+PLAIN'
R0_CONSTR_PLAIN  = 'r0c+plain'
R1_CONSTR        = 'r1c'
R1_CONSTR_PLAINp = 'R1C+PLAIN'
R1_CONSTR_PLAIN  = 'r1c+plain'
RSW              = 'rsw'
RSW_PLAIN        = 'rsw+plain'
RSW_PLAINp       = 'RSW+PLAIN'


ALLOWED_SCHEME = [
    COPYALL, EXP, INVALID, INVALIDplus]
ALLOWED_SCHEME_EXTERNAL = [
    POLY, EXPbold, POLYMINUS, EXPLOCAL]

COMPOSED_SCHEME = [
    Z0_PLAIN, Z0_COPYALL, 
    Z1_PLAIN, Z1_COPYALL,
    Z2_PLAIN, Z2_COPYALL, Z2_COPYALL_2, 
    R0_PLAIN, R0_LESS_PLAIN, R0_CONSTR_PLAIN, R0_COPYALL,
    R1_PLAIN, R1_LESS_PLAIN, R1_CONSTR_PLAIN,
    RSW_PLAIN,
    RD0_PLAIN, 
    RD1_PLAIN]
    # Z3_PLAIN, Z3_COPYALL]

# ALLOWED_SCHEME = [
#     COPYALL, EXP, INVALID, INVALIDplus, 
#     POLY, EXPbold, POLYMINUS, EXPLOCAL,
#     Z0_PLAIN, Z0_COPYALL, Z0_EXP, Z1_PLAIN, Z1_COPYALL, Z1_EXP, Z2_COPYALL_2, Z2_PLAIN, Z2_COPYALL]


''' Validation translation '''
TRANSL_per_VAL   = True
TRANSLATION      = False
SIMULATE_TIME    = 'simulate_novel_time'
PROP_TRUE        = 'prop-true'
PROP_FALSE       = 'prop-false'
MONO_EVENT_DONE  = 'mono-event-done--{}'         # Used by Z1
EVENT_DONE       = 'event-done--{}'              # Used by Z0, Z1
SIMULATE_ACTION  = 'simulate_action-{}-{}'
START_SIMULATE_t = 'start-simulate-processes-{}'
TIMED_EVENT      = 'TIMED_EVENTS_{}'             # Used by Z1
BOUND_EVENT      = 'time_bound_event'


# TEMPORARY [TROVARE DEFINIZIONE DI CASCADING EVENTS]
baxter_cascading = None

'''
Configuration 
'''
USE_CONDEFF_PROC = False

DNAME   = 'DNAME'
PNAME   = 'PNAME'
PR      = 'PR'
TW      = 'TW'
BYDICT  = 'bydict'
BYAVGIN = 'avg_input'

INFOS = {
    DNAME: None,
    PNAME: None,
    PR   : False
}

def readfile(path):
    with open(path, 'r') as fin:
        return fin.read()


def writefile(path, string):
    # if exists(path):
    if False:
        print ('The {} path is not free'.format(path))
    else:
        with open(path, 'w') as fout:
            fout.write(string)


def write_temporary_file(string):
    temp_name = 'temp_file_{}'.format(str(datetime.datetime.now()).replace(' ', '-'))
    temp_name = join(CURRENT_PATH, temp_name)
    with open(temp_name, 'w') as fout:
        fout.write(string)
    return temp_name


def merge_list_of_lists(lists):
    new_list = []
    for element in lists:
        for t in element:
            new_list.append(t)
    return new_list


def not_variable(value):
    return value in LOGICAL_OPERATORS + NUMERIC_OPERATORS + OPERANDS_SYMBOLS


def extract_leaves(expr, leaves_set):
    '''
    This is a recursive function that, given a tree expression 'expr',
    populates the set 'leaves_set'.
    '''
    if type(expr) == int:    # variable indx
        leaves_set.add(expr)
    elif type(expr) == str:  # #t or costant
        pass
    elif type(expr) == tuple and len(expr) == 2:
        lvalue = expr[LVALUE]
        if lvalue in [ASSIGN, DECREASE, INCREASE]:
            extract_leaves (expr[RVALUE][LVALUE], leaves_set)
        if lvalue in [NOT]:
            # print (expr[RVALUE].__class__.__name__)
            extract_leaves (expr[RVALUE], leaves_set)
        else: 
            #type(expr) == tuple:
            rvalue = expr[RVALUE]
            [extract_leaves(el, leaves_set) for el in rvalue]
    elif type(expr) == list:
        [extract_leaves(el, leaves_set) for el in expr]
    else:
        print ('ERROR in extract_leaves')
        print (type(expr), expr)
        assert False


def extract_leaves_full(expr, leaves_set):
    '''
    This is a recursive function that, given a tree expression 'expr',
    populates the set 'leaves_set'.
    '''
    if type(expr) == int:    # variable indx
        leaves_set.add(expr)
    elif type(expr) == str:  # #t or costant
        pass
    elif type(expr) == tuple and len(expr) == 2:
        lvalue = expr[LVALUE]
        if lvalue in [ASSIGN, DECREASE, INCREASE]:
            extract_leaves_full (expr[RVALUE][RVALUE], leaves_set)
            extract_leaves_full (expr[RVALUE][LVALUE], leaves_set)
        elif lvalue == NOT:
            extract_leaves_full (expr[RVALUE], leaves_set)
        else: 
            #type(expr) == tuple:
            rvalue = expr[RVALUE]
            [extract_leaves_full(el, leaves_set) for el in rvalue]
    elif type(expr) == list:
        [extract_leaves_full(el, leaves_set) for el in expr]
    else:
        print (expr)
        print (type(expr))
        assert False



def extract_lifted_leaves(expr, leaves_set):
    if type(expr) == str:  # #t or costant
        leaves_set.add(expr)
    elif type(expr) == list:
        [extract_lifted_leaves(el, leaves_set) for el in expr]
    elif type(expr) == tuple and len(expr) == 2:
        rvalue = expr[LVALUE]
        lvalue = expr[RVALUE]
        extract_lifted_leaves(rvalue, leaves_set)
        extract_lifted_leaves(lvalue, leaves_set)
    else:
        assert False


def is_tree(t):
    return type(t) == nltk.Tree


def get_last_name(path):
    return path.split('/')[-1]


def is_number(string):
    string = string.replace('.', '').replace('-', '')
    return string.isnumeric()


# Function to generate all binary strings  
# from: https://gist.github.com/kevinwuhoo/2424597
def generate_binary(n):

    # 2^(n-1)  2^n - 1 inclusive
    bin_arr = range(0, int(math.pow(2,n)))
    bin_arr = [bin(i)[2:] for i in bin_arr]

    # Prepending 0's to binary strings
    max_len = len(max(bin_arr, key=len))
    bin_arr = [i.zfill(max_len) for i in bin_arr]

    return bin_arr


def set_baxter_cascading(domain_str):
    global baxter_cascading
    # baxter_cascading = '(domain paco3d)' in domain_str or '(domain car_linear_mt_sc)' in domain_str
    baxter_cascading = '(define (domain urbantraffic)' in domain_str



def get_baxter_cascading():
    global baxter_cascading
    return baxter_cascading


def print_header(string):
    print(len(string)*'-' + '\n' + string + '\n' + len(string)*'-')


AVGD_NOISE = {
    "Baxter": {
        "P4_i1": 2,
        "P4_i2": 2,
        "P4_i3": 2,
        "P4_i4": 2,
        "P4_i5": 5,
        "P5_i1": 6,
        "P5_i2": 1,
        "P5_i3": 4,
        "P5_i4": 1,
        "P6_i1": 4,
        "P6_i2": 1,
        "P6_i3": 1,
        "P6_i4": 2,
        "P6_i5": 3,
        "P7_i1": 1,
        "P7_i2": 2,
        "P7_i4": 1,
        "P4_i1.pddl": 2,
        "P4_i2.pddl": 2,
        "P4_i3.pddl": 2,
        "P4_i4.pddl": 2,
        "P4_i5.pddl": 5,
        "P5_i1.pddl": 6,
        "P5_i2.pddl": 1,
        "P5_i3.pddl": 4,
        "P5_i4.pddl": 1,
        "P6_i1.pddl": 4,
        "P6_i2.pddl": 1,
        "P6_i3.pddl": 1,
        "P6_i4.pddl": 2,
        "P6_i5.pddl": 3,
        "P7_i1.pddl": 1,
        "P7_i2.pddl": 2,
        "P7_i4.pddl": 1
    },
    "Descent": {
        "prob_earth01.pddl": 1,
        "prob_earth02.pddl": 2,
        "prob_earth03.pddl": 2,
        "prob_earth04.pddl": 2,
        "prob_earth05.pddl": 2,
        "prob_earth06.pddl": 3,
        "prob_earth07.pddl": 4,
        "prob_earth08.pddl": 7,
        "prob_earth09.pddl": 4,
        "prob_earth10.pddl": 4,
        "prob_earth11.pddl": 8,
        "prob_earth12.pddl": 3,
        "prob_earth13.pddl": 5,
        "prob_earth14.pddl": 5,
        "prob_earth15.pddl": 5,
        "prob_earth16.pddl": 3,
        "prob_earth17.pddl": 5,
        "prob_earth18.pddl": 4,
        "prob_earth19.pddl": 5,
        "prob_earth20.pddl": 10
    },
    "HVAC": {
        "instance_1_1.pddl": 2,
        "instance_1_10.pddl": 2,
        "instance_1_11.pddl": 2,
        "instance_1_12.pddl": 2,
        "instance_1_13.pddl": 2,
        "instance_1_14.pddl": 2,
        "instance_1_15.pddl": 2,
        "instance_1_16.pddl": 2,
        "instance_1_2.pddl": 2,
        "instance_1_3.pddl": 2,
        "instance_1_4.pddl": 2,
        "instance_1_5.pddl": 2,
        "instance_1_6.pddl": 2,
        "instance_1_7.pddl": 2,
        "instance_1_8.pddl": 2,
        "instance_1_9.pddl": 2
    },
    "HVAC[unconstrained]": {
        "instance_1_1.pddl": 2,
        "instance_1_10.pddl": 2,
        "instance_1_11.pddl": 2,
        "instance_1_12.pddl": 2,
        "instance_1_13.pddl": 2,
        "instance_1_14.pddl": 2,
        "instance_1_15.pddl": 2,
        "instance_1_16.pddl": 2,
        "instance_1_2.pddl": 2,
        "instance_1_3.pddl": 2,
        "instance_1_4.pddl": 2,
        "instance_1_5.pddl": 2,
        "instance_1_6.pddl": 2,
        "instance_1_7.pddl": 2,
        "instance_1_8.pddl": 2,
        "instance_1_9.pddl": 2
    },
    "Linear-Car": {
        "instance_10_30.0_0.1_10.0.pddl": 1,
        "instance_1_30.0_0.1_10.0.pddl": 1,
        "instance_2_30.0_0.1_10.0.pddl": 1,
        "instance_3_30.0_0.1_10.0.pddl": 1,
        "instance_4_30.0_0.1_10.0.pddl": 1,
        "instance_5_30.0_0.1_10.0.pddl": 1,
        "instance_6_30.0_0.1_10.0.pddl": 1,
        "instance_7_30.0_0.1_10.0.pddl": 1,
        "instance_8_30.0_0.1_10.0.pddl": 1,
        "instance_9_30.0_0.1_10.0.pddl": 1
    },
    "Linear-Car[ver2]": {
        "p01.pddl": 7,
        "p02.pddl": 3,
        "p03.pddl": 2,
        "p04.pddl": 2,
        "p05.pddl": 1,
        "p06.pddl": 1,
        "p07.pddl": 1,
        "p08.pddl": 1,
        "p09.pddl": 1,
        "p10.pddl": 1,
        "p11.pddl": 1,
        "p12.pddl": 1,
        "p13.pddl": 10,
        "p14.pddl": 4,
        "p15.pddl": 2,
        "p16.pddl": 2,
        "p17.pddl": 2,
        "p18.pddl": 2,
        "p19.pddl": 1,
        "p20.pddl": 1,
        "p21.pddl": 1,
        "p22.pddl": 1,
        "p23.pddl": 1,
        "p24.pddl": 1
    },
    "Linear-Generator": {
        "instance_1.pddl": 500,
        "instance_10.pddl": 500,
        "instance_2.pddl": 500,
        "instance_3.pddl": 500,
        "instance_4.pddl": 500,
        "instance_5.pddl": 500,
        "instance_6.pddl": 500,
        "instance_7.pddl": 500,
        "instance_8.pddl": 500,
        "instance_9.pddl": 500
    },
    "OT-Car": {
        "instance_01.pddl": 1,
        "instance_02.pddl": 1,
        "instance_03.pddl": 1,
        "instance_04.pddl": 1,
        "instance_05.pddl": 1,
        "instance_06.pddl": 1,
        "instance_07.pddl": 1,
        "instance_08.pddl": 1,
        "instance_09.pddl": 1,
        "instance_10.pddl": 1,
        "instance_11.pddl": 1,
        "instance_12.pddl": 1,
        "instance_14.pddl": 1,
        "instance_15.pddl": 1,
        "instance_16.pddl": 1,
        "instance_17.pddl": 1,
        "instance_18.pddl": 1,
        "instance_19.pddl": 1,
        "instance_20.pddl": 1
    },
    "Solar-Rover": {
        "prob01.pddl": 6,
        "prob02.pddl": 12,
        "prob03.pddl": 19,
        "prob04.pddl": 25,
        "prob05.pddl": 31,
        "prob06.pddl": 75,
        "prob07.pddl": 88,
        "prob08.pddl": 100,
        "prob09.pddl": 112,
        "prob10.pddl": 125,
        "prob11.pddl": 138,
        "prob12.pddl": 150,
        "prob13.pddl": 162,
        "prob14.pddl": 175,
        "prob15.pddl": 188,
        "prob16.pddl": 200,
        "prob17.pddl": 212,
        "prob18.pddl": 225,
        "prob19.pddl": 238,
        "prob20.pddl": 250
    },
    "UTC": {
        "problem.pddl": 2,
        "problem1_medium_revised.pddl": 2,
        "problem_2.pddl": 2,
        "problem_3.pddl": 2,
        "problem_4.pddl": 2,
        "problem_cfr.pddl": 4,
        "problem_easy_revised.pddl": 3
    },
    "UTC2": {
        "problem_scoot_res_26_eve_p01[count=100].pddl": 28,
        "problem_scoot_res_26_eve_p02[count=200].pddl": 137,
        "problem_scoot_res_26_eve_p03[count=300].pddl": 61,
        "problem_scoot_res_26_eve_p04[count=400].pddl": 50,
        "problem_scoot_res_26_eve_p05[count=500].pddl": 75,
        "problem_scoot_res_26_morn_p01[count=100].pddl": 37,
        "problem_scoot_res_26_morn_p02[count=200].pddl": 440,
        "problem_scoot_res_26_morn_p03[count=300].pddl": 86,
        "problem_scoot_res_26_morn_p04[count=400].pddl": 71,
        "problem_scoot_res_26_morn_p05[count=500].pddl": 95,
        "problem_scoot_res_26_noon_p01[count=200].pddl": 106,
        "problem_scoot_res_26_noon_p02[count=300].pddl": 625,
        "problem_scoot_res_26_noon_p03[count=400].pddl": 96,
        "problem_scoot_res_26_noon_p04[count=500].pddl": 83,
        "problem_scoot_res_26_noon_p05[count=600].pddl": 88,
        "problem_scoot_res_30_eve_p01[count=300].pddl": 223,
        "problem_scoot_res_30_eve_p02[count=400].pddl": 1096,
        "problem_scoot_res_30_eve_p03[count=500].pddl": 173,
        "problem_scoot_res_30_eve_p04[count=600].pddl": 119,
        "problem_scoot_res_30_eve_p05[count=700].pddl": 150
    },
    "Non-Linear-Car[unconstrained]": {
        "instance_1_30.0_0.1_10.0.pddl": 2,
        "instance_2_30.0_0.1_10.0.pddl": 1,
        "instance_3_30.0_0.1_10.0.pddl": 1,
        "instance_4_30.0_0.1_10.0.pddl": 1,
        "instance_5_30.0_0.1_10.0.pddl": 1,
        "instance_6_30.0_0.1_10.0.pddl": 1,
        "instance_7_30.0_0.1_10.0.pddl": 1,
        "instance_8_30.0_0.1_10.0.pddl": 1
    },
    "Non-Linear-Car[unconstrained+ver2]": {
        "p01.pddl": 2,
        "p02.pddl": 2,
        "p03.pddl": 2,
        "p04.pddl": 1,
        "p05.pddl": 1,
        "p06.pddl": 1,
        "p07.pddl": 2,
        "p08.pddl": 1,
        "p09.pddl": 1,
        "p10.pddl": 2,
        "p11.pddl": 2,
        "p12.pddl": 2,
        "p13.pddl": 1,
        "p14.pddl": 2,
        "p15.pddl": 2,
        "p16.pddl": 1,
        "p17.pddl": 2,
        "p18.pddl": 3,
        "p19.pddl": 2,
        "p20.pddl": 2
    },
    "Intercept[unconstrained]": {
        "instance_1.pddl": 1.1,
        "instance_10.pddl": 4.04,
        "instance_2.pddl": 1.83,
        "instance_3.pddl": 2.57,
        "instance_4.pddl": 3.3,
        "instance_5.pddl": 4.04,
        "instance_6.pddl": 1.1,
        "instance_7.pddl": 2.21,
        "instance_8.pddl": 2.58,
        "instance_9.pddl": 3.68
    },
    "Intercept[unconstrained+easy]": {
        "instance_1.pddl": 1.1,
        "instance_10.pddl": 2.7,
        "instance_2.pddl": 1.8,
        "instance_3.pddl": 2.6,
        "instance_4.pddl": 3.3,
        "instance_5.pddl": 4.0,
        "instance_6.pddl": 1.1,
        "instance_7.pddl": 2.2,
        "instance_8.pddl": 2.6,
        "instance_9.pddl": 3.7
    }
}