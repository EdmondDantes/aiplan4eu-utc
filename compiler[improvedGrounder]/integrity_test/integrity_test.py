'''
This script is used to test whether errors are introduced as the compiler progresses.
'''

from os.path   import join
from os        import listdir
from itertools import product

import os

ICAPS_BENCHMARK = '../ICAPS2021_benchmarks'

PDDLPLUS_COMPILER_TEMPLATE = 'python3.8 ../pddlplus_compiler.py --domain_path #DOMAIN-PATH#  --problem_path #PROBLEM-PATH# --schema #SCHEMA# --no_event_opt_cascade #CASCADE-OPT# --no_event_opt_action #EVENT-OPT# --output_path #OUTPUT-PATH# >  #LOG#'

WALLTIME = '60s'

METRICFF_template = 'timeout {} ./metric-ff -p \'\' -o {} -f {} > test_folder/metricff_log'

LEGAL_PLAN_MARK = 'ff: found legal plan as follows'

PAR_list = ['--schema', '--no_event_opt_cascade', '--no_event_opt_action']

# FULL
PARAMETERS_dict = {
    '--schema': ['copyall', 'invalid', 'exp'],
    '--no_event_opt_cascade': ['True', 'False'],
    '--no_event_opt_action': ['True', 'False'],
}

# RESTRICTED
PARAMETERS_dict = {
    '--schema'              : ['exp'],
    '--no_event_opt_cascade': ['False'],
    '--no_event_opt_action' : ['False'],
}


ALIAS_dict = {
    '--schema': '#SCHEMA#',
    '--no_event_opt_cascade': '#CASCADE-OPT#',
    '--no_event_opt_action': '#EVENT-OPT#',   
}

FAILED_COMPILATION_dict = {}
SOLVED_dict = {}

''' set if we test the metric-ff execution '''
TEST_EXECUTION  = True
''' set if we test just the first instance of each domain '''
FIRST_INSTANCES = False

def main():
    clean_test_folder()
    DOMAINS = (listdir(ICAPS_BENCHMARK))
    DOMAINS.remove('UTC')
    DOMAINS.remove('Baxter')


    CONFIGURATION_LIST = gen_configs_list()
    for c in CONFIGURATION_LIST:
        kconf = get_kconf_from_c(c)
        SOLVED_dict[kconf] = 0
        FAILED_COMPILATION_dict[kconf] = {}
        for d in DOMAINS:
            FAILED_COMPILATION_dict[kconf][d] = 0

    for c in CONFIGURATION_LIST:

        for d in DOMAINS:
            test_d(c, d)


def get_kconf_from_c(c):
    kconf = ''
    for k in PAR_list:
        kconf += c[k] + '###'
    return kconf


def gen_configs_list():
    sets  = []
    kpars = []
    for kpar, kset in PARAMETERS_dict.items():
        sets.append(kset)
        kpars.append(kpar)
    configs_list = (list(product(*sets)))
    print ('\t|Configuration| = {}'.format(len(configs_list)))

    configs_dict_list = []
    for c in configs_list:
        single_c = {}
        for indx_k, k in enumerate(kpars):
            single_c[k] = c[indx_k]
        configs_dict_list.append(single_c)

    return configs_dict_list

def test_d(c, d):
    domains, instances = get_instances_and_domain(d)
    kconf = get_kconf_from_c(c)

    for indx_i, i in enumerate(instances):
        test_d_i(c, d, domains[indx_i], i, kconf)

def test_d_i(c, d, dname, iname, kconf):
    # COMPILATION #
    print ('Testing ')
    print ('Domain  : {}'.format(d))
    print ('Instance: {}'.format(iname))
    print ('Conf    : {}'.format(kconf))
    print ()
    problem_path = (join(ICAPS_BENCHMARK, d, iname))
    domain_path  = (join(ICAPS_BENCHMARK, d, dname))
    compiler_command = PDDLPLUS_COMPILER_TEMPLATE
    compiler_command = compiler_command.replace('#DOMAIN-PATH#', domain_path)
    compiler_command = compiler_command.replace('#PROBLEM-PATH#', problem_path)
    for k, par in c.items():
        kalias = ALIAS_dict[k]
        compiler_command = compiler_command.replace(kalias, par)
    compiler_command = compiler_command.replace('#OUTPUT-PATH#', 'test_folder')
    compiler_command = compiler_command.replace('#LOG#', 'test_folder/log')
    print (compiler_command)
    os.system(compiler_command)

    passed = test_compilation(d, kconf)

    # EXECUTE-metric-ff
    if TEST_EXECUTION is True:
        comp_dom = join('test_folder', 'compiled_domain.pddl')
        comp_prob = join('test_folder', 'compiled_{}'.format(iname))
        metricff_command = METRICFF_template.format(WALLTIME, comp_dom, comp_prob)
        os.system(metricff_command)
        solved = test_solved(kconf)

    print (SOLVED_dict)
    print (FAILED_COMPILATION_dict)
    print ('*****************************')
    print ('*****************************')
    print ('*****************************')


    clean_test_folder()


def clean_test_folder():
    os.system('rm test_folder/*')


def test_solved(kconf):
    with open('test_folder/metricff_log') as fout:
        txt = fout.read()
    if LEGAL_PLAN_MARK in txt:
        SOLVED_dict[kconf] += 1
        return True
    else:
        return False
    exit(1)

def test_compilation(d, kconf):
    n = len(listdir('test_folder'))
    if n != 4:
        FAILED_COMPILATION_dict[kconf][d] += 1
        return False
    return True

def get_instances_and_domain(d):
    path_d    = join(ICAPS_BENCHMARK, d)
    instances = listdir(path_d)
    domains   = [i for i in instances if i == 'domain.pddl' or 'domain_' in i]
    try:
        domains.remove('domain_UTC.pddl')
    except:
        pass
    assert len(domains) == 1
    instances = [f for f in instances if f not in domains]
    try:
        instances.remove('domain_UTC.pddl')
    except:
        pass
    domains   = [domains[0] for i in range(0, len(instances))]
    domains   = natural_sort(domains)
    instances = natural_sort(instances)

    if FIRST_INSTANCES is True:
        return [domains[0]], [instances[0]]  # test just the first instance
    return domains, instances    

import re

def natural_sort(l): 
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key=alphanum_key)

if __name__ == "__main__":
    main()