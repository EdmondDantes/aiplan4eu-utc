with open('full_plan', 'r') as fout:
	plan = fout.read()

plan = plan.replace('ENHSP returned: ', '')
plan = plan.replace('[', '').replace(']', '')
plan = plan.replace(' ', '')

actions      = plan.split(',')

kept_actions = []

for a in actions:
	if 'simulate--positive' in a or 'simulate--negative' in a or 'end-simulate-processes' in a:
		pass
	elif 'start-simulate-processes' in a:
		kept_actions.append('simulate-time')
	else:
	    kept_actions.append(a)

string_plan = '['

for a in kept_actions:
	if 'switch' in a:
		a = '\\textbf{a}'
	string_plan += a +', '

print (string_plan)