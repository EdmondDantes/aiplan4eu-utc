from utils                  import *
from global_map             import *
from numeric_postconditions import *


def compile_plan_into_domain(g_domain_dict, plan_data, makespan, mode, delta, schema, val_schema):
    global GLOBAL_MAP

    if mode == TRANSLATION:
        return g_domain_dict

    add_novel_predicates_val(g_domain_dict, plan_data, val_schema, schema)

    indx_false = generate_new_predicate(PROP_FALSE)
    g_domain_dict[G_PRED].add(indx_false)
    g_domain_dict = add_bot_to_everything(g_domain_dict)

    if schema == 'plain':
        '''
        ENHSP does not accept total-time in its precondition since
        total-time is a keyword. So we use novel-time.
        '''
        if val_schema not in [R0, R1]: # R0, R1 do not use novel-time
            indx_novel_time = generate_new_function(NOVEL_TIME)
            g_domain_dict[G_FUNCT].add(indx_novel_time)
            g_domain_dict[G_P].append(generate_time_process(delta, makespan))

    if val_schema in [R0, R0_LESS, R1_LESS, R0_CONSTR, R1_CONSTR, RSW, R1, Z0, Z1, Z2, Z3]:
        g_domain_dict[G_A] = compile_action_into_event(g_domain_dict, plan_data, schema, val_schema, '', None, makespan)
    elif val_schema in [RD0, RD1]:
        partA = compile_action_into_event(g_domain_dict, plan_data, schema, R0, '_relaxed', val_schema, None)
        partB = compile_action_into_event(g_domain_dict, plan_data, schema, Z0, '_unrelaxed', val_schema, None)
        g_domain_dict[G_A] = partA + partB

        if val_schema in [RD1]:
            g_domain_dict[G_A].append(generate_unblock_action())


    if val_schema in [Z1, Z2, Z3]:
        g_domain_dict[G_P] = compile_process_bound(g_domain_dict, schema, makespan)

    if val_schema == Z2: 
        if schema != COPYALLplus:
            g_domain_dict[G_E] += add_bot_events(g_domain_dict, plan_data, makespan, delta, schema)

    elif val_schema == Z3:
        g_domain_dict[G_P] = split_processes(g_domain_dict[G_P], plan_data, makespan, schema)

    ''' Removed, it is not necessary '''
    # g_domain_dict[G_E] = add_time_bound_event(g_domain_dict, makespan, schema)

    return g_domain_dict


def add_time_bound_event(g_domain_dict, makespan, schema):
    ''' TEMPORARY FUNCTION '''
    if schema == 'plain':
        time2add = get_predicate_indx(NOVEL_TIME)
    else:
        time2add = TOTAL_TIME

    indx_bound = update_grounded_action(BOUND_EVENT, [])
    pre = AND, [(NOT, get_predicate_indx(PROP_FALSE)), (GREATER, [time2add, str(float(makespan))])]
    eff = AND, [get_predicate_indx(PROP_FALSE)]
    return [(indx_bound, pre, eff)]


def generate_unblock_action():
    '''
    Action used by RD1
    '''
    indx_action = update_grounded_action(UNBLOCK_ACTION, [])
    block_pred  = get_predicate_indx(BLOCKPRED)
    pre         = AND, [block_pred]
    eff         = AND, [(NOT, block_pred)]
    return indx_action, pre, eff


def compile_process_bound(g_domain_dict, schema, makespan):

    if schema == 'plain':
        time2add = get_predicate_indx(NOVEL_TIME)
    else:
        time2add = TOTAL_TIME

    for indx_list, p in enumerate(g_domain_dict[G_P]):
        indx, pre, eff = p
        op, pre_list = pre
        if op == AND:
            if schema == 'plain':
                pre_list.append((LESS, [time2add, makespan]))
            else:
                assert schema in [COPYALL, COPYALLplus]
                pre_list.append((LESSEQ, [time2add, makespan]))
        elif pre == PARSED_EMPTY_PRECONDITION:
            if schema == 'plain':
                pre = AND, [(LESS, [time2add, makespan])]
            else:
                assert schema in [COPYALL, COPYALLplus]
                pre = AND, [(LESSEQ, [time2add, makespan])]

            g_domain_dict[G_P][indx_list] = indx, pre, eff
        else:
            print ('Extend Code. Function: compile_process_bound')
            exit(1)
    return g_domain_dict[G_P]


def split_processes(g_p, plan_data, makespan, val_schema):
    new_gp = []
    timed_actions, seen_timestamp = get_timed_actions(plan_data, makespan)

    if val_schema == 'plain':
        indx_time = get_predicate_indx(NOVEL_TIME)
    else:
        indx_time = TOTAL_TIME

    for indx_ts, ts in enumerate(seen_timestamp):
        if indx_ts == len(seen_timestamp) - 1:
            break

        ts_action = timed_actions[indx_ts]
        pre_list  = [get_predicate_indx(MONO_EVENT_DONE.format(i)) for i in ts_action]

        if val_schema == 'plain':
            pre_list.append((GREATEREQ, [indx_time, str(ts)]))
        else:
            pre_list.append((GREATER, [indx_time, str(ts)]))

        if indx_ts < len(seen_timestamp) - 1:
            if val_schema == 'plain':
                pre_list.append((LESS, [indx_time, str(seen_timestamp[indx_ts + 1])]))
            else:
                pre_list.append((LESSEQ, [indx_time, str(seen_timestamp[indx_ts + 1])]))

        for indx_p, pre_p, eff_p in g_p:
            new_name  = get_something_name(indx_p, None) + '--step--{}'.format(indx_ts)
            new_pre_p = AND, pre_p[RVALUE] + pre_list
            new_eff_p = eff_p
            new_indx  = update_grounded_action(new_name, [])
            new_gp.append((new_indx, new_pre_p, new_eff_p))
    
    return new_gp


def add_bot_to_everything(g_domain_dict):
    ''' actions '''
    for indx_s, a_tup in enumerate(g_domain_dict[G_A]):
        indx_a, pre_a, eff_a = a_tup
        if pre_a[LVALUE] == AND:
            pre_a[RVALUE].append((NOT, [get_predicate_indx(PROP_FALSE)]))
        elif pre_a == PARSED_EMPTY_PRECONDITION:
            g_domain_dict[G_A][indx_s] = indx_a, (AND, [(NOT, [get_predicate_indx(PROP_FALSE)])]), eff_a
        else:
            print (pre_a)
            print ('Please extend code in \'add_bot_to_everything - action\'')
            exit(1)
    ''' processes '''
    for indx_s, p_tup in enumerate(g_domain_dict[G_P]):
        indx_p, pre_p, eff_p = p_tup
        if pre_p[LVALUE] == AND:
            pre_p[RVALUE].append((NOT, get_predicate_indx(PROP_FALSE)))
        elif pre_p == PARSED_EMPTY_PRECONDITION:
            g_domain_dict[G_P][indx_s] = indx_p, (AND, [(NOT, [get_predicate_indx(PROP_FALSE)])]), eff_p
        else:
            print (pre_p)
            print ('Please extend code in \'add_bot_to_everything\' - process')
            exit(1)

    ''' events '''
    # for indx_s, e_tup in enumerate(g_domain_dict[G_E]):
    #     indx_e, pre_e, eff_e = e_tup
    #     if pre_p[LVALUE] == AND:
    #         pre_e[RVALUE].append((NOT, get_predicate_indx(PROP_FALSE)))
    #     elif pre_p == PARSED_EMPTY_PRECONDITION:
    #         g_domain_dict[G_A][indx_s] = indx_e, (AND, (NOT, get_predicate_indx(PROP_FALSE))), eff_e
    #     else:
    #         print (pre_p)
    #         print ('Please extend code in \'add_bot_to_everything\' - process')
    #         exit(1)

    return g_domain_dict


def add_bot_events(g_domain_dict, plan_data, makespan, delta, schema):

    if schema == 'plain':
        time2add = get_predicate_indx(NOVEL_TIME)
    else:
        time2add = TOTAL_TIME

    timed_actions, seen_timestamp = get_timed_actions(plan_data, makespan)
    timed_events = []
    eff_e        = AND, [get_predicate_indx(PROP_FALSE)]
    not_false    = NOT, [get_predicate_indx(PROP_FALSE)]

    for indx_ts, ts in enumerate(seen_timestamp, start=0):
        if len(timed_actions[indx_ts]) > 0:
            indx_bot     = update_grounded_action(TIMED_EVENT.format(str(len(timed_events))), [])
            greater_time = GREATER, [time2add, str(float(ts))]
            indx_a       = timed_actions[indx_ts][-1]
            not_done     = NOT, [get_predicate_indx(MONO_EVENT_DONE.format(indx_a))]
            pre_ebot     = AND, [greater_time, not_done, not_false]
            timed_events.append((indx_bot, pre_ebot, eff_e))
    return timed_events


def add_novel_predicates_val(g_domain_dict, plan_data, val_schema, schema):
    indx_prop_true  = generate_new_predicate(PROP_TRUE)
    g_domain_dict[G_PRED].add(indx_prop_true)

    for indx in range(0, len(plan_data) + 1):
        indx_test = generate_new_predicate(EVENT_DONE.format(str(indx)))
        g_domain_dict[G_PRED].add(indx_test)

        if val_schema in [Z2, Z3]:
            indx_test = generate_new_predicate(MONO_EVENT_DONE.format(str(indx)))
            g_domain_dict[G_PRED].add(indx_test)

    if val_schema in [RD1]:
        indx_block = generate_new_predicate(BLOCKPRED)
        g_domain_dict[G_PRED].add(indx_block)



def compile_plan_into_problem(g_problem_dict, plan_data, makespan, mode, delta, schema, val_schema):

    if schema == 'plain':
        time2add = get_predicate_indx(NOVEL_TIME)
    else:
        time2add = TOTAL_TIME

    g_problem_dict[INIT_STATE].append(get_predicate_indx(PROP_TRUE))

    if val_schema not in [R1, R1_LESS, R1_CONSTR, RSW]:
        g_problem_dict[INIT_STATE].append(get_predicate_indx(EVENT_DONE.format(str(0))))

    if schema == 'plain':
        if val_schema not in [R0, R1]:
            g_problem_dict[INIT_STATE].append((EQUAL, [get_predicate_indx(NOVEL_TIME), '0.0']))

    goal_state = g_problem_dict[GOAL_STATE]
    lvalue = goal_state[LVALUE]
    rvalue = goal_state[RVALUE]

    if val_schema not in [R1, R1_LESS, R1_CONSTR, RSW]:
        rvalue.append(get_predicate_indx(EVENT_DONE.format(str(len(plan_data)))))
    elif val_schema in [R1, R1_LESS, R1_CONSTR, RSW]:
        ''' Execute all the actions '''
        for i in range(len(plan_data)):
           rvalue.append(get_predicate_indx(EVENT_DONE.format(str(i))))
    elif val_schema in [Z0, Z1, Z2, Z3]:
        rvalue.append((EQUAL, [time2add, makespan]))

    if val_schema in [R0_LESS, R1_LESS]:
        t_e = plan_data[-1][0] * 1.2
        rvalue.append((LESSEQ, [time2add, str(t_e)]))

    return g_problem_dict


def generate_time_process(delta, makespan):
    indx_novel_time = get_predicate_indx(NOVEL_TIME)

    indx = update_grounded_action(SIMULATE_TIME, [])
    # pre_list = [(LESS, [indx_novel_time, makespan]), (NOT, get_predicate_indx(PROP_FALSE))]
    pre_list = [(NOT, get_predicate_indx(PROP_FALSE))]


    pre  = AND, pre_list
    eff_list = []
    eff_list.append((INCREASE, [indx_novel_time, (TIMES, [CONTINOUS_TIME, str(1)])]))
    eff  = AND, eff_list

    return (indx, pre, eff)


def compile_action_into_event(g_domain_dict, plan_data, schema, val_schema, appendix, original_val_schema, makespan):

    '''
    Important function to refactor
    '''

    global GLOBAL_MAP
    actions = g_domain_dict[G_A]
    
    if schema == 'plain':
        time2use = get_predicate_indx(NOVEL_TIME)
    else:
        time2use = TOTAL_TIME

    actions_from_plan = []

    if val_schema in [R0_CONSTR, R1_CONSTR, RSW]:
        actions_constrains = generate_actions_contraints(plan_data, makespan)
        if val_schema in [RSW]:
            bool_done = generate_bool_done(actions_constrains)

    for indx, (timestamp, action) in enumerate(plan_data, start=0):
        
        indx_action = get_action_from_lifted_name(action)

        indx_action, g_pre, g_eff = actions[indx_action]

        andstr, g_pre = g_pre
        andstr, g_eff = g_eff

        done       = get_predicate_indx(EVENT_DONE.format(str(indx)))
        pre_list = g_pre
        if val_schema not in [R0, R1, R0_LESS, R1_LESS, R0_CONSTR, R1_CONSTR, RSW]: # Do not add time-stamp
            time_equal = EQUAL, [time2use, str(timestamp)]
            pre_list = pre_list + [time_equal]
        else:
            pass

        if val_schema in [R0_CONSTR, R1_CONSTR, RSW]:
            ''' inf and sup '''
            pre_list = pre_list + [(GREATEREQ, [time2use, str(actions_constrains[indx][0])])] + [(LESSEQ, [time2use, str(actions_constrains[indx][1])])]

        if val_schema not in [R1, R1_LESS, R1_CONSTR, RSW]:
            pre_list = pre_list + [done]
        else:
            pre_list = pre_list + [(NOT, done)]
            if val_schema in [RSW]:
                for j in bool_done[indx]:
                    doneprev = get_predicate_indx(EVENT_DONE.format(str(j)))
                    pre_list.append(doneprev)

        if original_val_schema == RD1:
            not_block = get_predicate_indx(BLOCKPRED)
            pre_list = pre_list + [(NOT, not_block)]

        pre_action = AND, pre_list

        done_prime = get_predicate_indx(EVENT_DONE.format(str(indx + 1)))

        if val_schema in [R1, R1_LESS, R1_CONSTR, RSW]:
            eff_list = [done] + g_eff
        else:
            eff_list = [(NOT, done), done_prime] + g_eff

        if val_schema in [Z2, Z3]:
            mono_done_indx = get_predicate_indx(MONO_EVENT_DONE.format(indx))
            eff_list.append(mono_done_indx)

        if appendix == '_relaxed' and original_val_schema == RD1:
            not_block = get_predicate_indx(BLOCKPRED)
            eff_list = eff_list + [not_block]

        eff_action = AND, eff_list 

        event_name = SIMULATE_ACTION.format(action.replace(' ', '--'), str(indx))
        indx_event = update_grounded_action(event_name + appendix, [])
        # events.append((indx_event, pre_action, eff_action))

        actions_from_plan.append((indx_event, pre_action, eff_action))
    # return events
    return actions_from_plan


def generate_bool_done(actions_constrains):
    # addDone = [False]
    # for j in range(1, len(actions_constrains)):
    #     if j == 0:
    #         addDone.append(False)
    #     else:
    #         if actions_constrains[j][0] > actions_constrains[j - 1][1]:
    #             addDone.append(True)
    #         else:
    #             addDone.append(False)
    # addDone = [[] for i in range(0, len(actions_constrains))]
    addDone = [[] for i in range(0, len(actions_constrains))]
    for j in range(1, len(actions_constrains)):
        if actions_constrains[j][0] > actions_constrains[j - 1][1]:
            for z in range(0, j):
                addDone[j].append(z)
        else:
            addDone[j] = addDone[j-1]
    return addDone


def generate_actions_contraints(plan_data, te):
    te = float(te)
    ts_list = [0.0] + [a[0] for a in plan_data] + [te]
    ts_list = list(set(ts_list))
    ts_list.sort()
    ts_list = [a[0] for a in plan_data]
    constr = []
    avg_d = compute_avg_d(ts_list)
    for indx, ts in enumerate(ts_list):

        inf = max(ts - avg_d, 0)
        sup = min(ts + avg_d, te)

        constr.append((inf, sup))

    print ('Constraints on actions:')
    for indx, ts in enumerate(ts_list):
        print ('{}: ({},{})'.format(ts, constr[indx][0], constr[indx][1]))
    print ('Window: {}'.format(avg_d * 2))
    print ('\n')
    return constr


def compute_avg_d(ts_list):
    if INFOS[TW] == BYDICT:
        return AVGD_NOISE[INFOS[DNAME]][INFOS[PNAME]]
    elif INFOS[TW] == BYAVGIN:

        ts_set  = set(ts_list)
        ts_list = list(ts_set)
        ts_list.sort()
        deltas = []
        for i in range(0, len(ts_list) - 1):
            delta = ts_list[i+1] - ts_list[i]
            deltas.append(delta)
        return sum(deltas) / len(deltas)
