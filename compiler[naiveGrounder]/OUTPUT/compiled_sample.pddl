(define (problem grounded-descent_prob)
(:domain grounded-descent)
(:init
	
	
		(= 
			(d_final) 100 ) 
		(= 
			(d_margin) 10 ) 
		(= 
			(v_margin) 10 ) 
		(= 
			(v) 0 ) 
		(= 
			(d) 0 ) 
		(= 
			(g) 9.8 ) 
		(= 
			(M) 10000 ) 
		(= 
			(M_min) 5000 ) 
		(= 
			(q) 50 ) 
		(= 
			(ISP) 311 ) (stop) 
		(= 
			(delta) 1 ) 
		(= 
			(minus-delta) -1.0 ) (force-events) 
		(= 
			(total-time) 0.0 ) 
)
(:goal
	
	(and 
		(landed) 
			(not 
				(block) ) 
			(not (pause)) 
			(not (force-events)) )
)
(:metric minimize (total-time))
)