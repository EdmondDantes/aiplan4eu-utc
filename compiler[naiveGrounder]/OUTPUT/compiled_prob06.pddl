(define (problem grounded-run-generator2)
(:domain grounded-generator2)
(:init
	
	
		(= 
			(roverenergy) 0 ) (night) (datatosend) (gboff__GB) 
		(= 
			(SoC__b1) 40 ) (off__b1) (off__b2) 
		(= 
			(SoC__b2) 80 ) (off__b3) 
		(= 
			(SoC__b3) 100 ) 
		(= 
			(sunexposure_time) 300.0 ) 
		(= 
			(time) 0.0 ) (prop-true) (event-done--0) 
)
(:goal
	
	(and 
		(datasent) (event-done--5) )
)
(:metric minimize (total-time))
)