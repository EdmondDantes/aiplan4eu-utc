(define (domain grounded-descent)
(:requirements :fluents :adl :typing :time)
(:predicates
	(stop)
	(landed)
	(block)
	(landing)
	(thrusting)
	(force-events)
	(fired--anti-crash)
	(pause)
	(undefined)
)

(:functions
	(d_final)
	(d_margin)
	(v_margin)
	(v)
	(d)
	(g)
	(M)
	(M_min)
	(q)
	(ISP)
	(falling-time)
	(thrust-duration)
	(delta)
	(minus-delta)
)

(:action start_descent
 :parameters()
 :precondition 
	(and 
		(stop) 
			(not 
				(block) ) 
			(not (force-events)) )
 :effect 
	(and 
		(landing) 
			(assign 
				(falling-time) 0 ) 
			(not 
				(stop) ) )
)

(:action land
 :parameters()
 :precondition 
	(and 
		
			(not 
				(block) ) (landing) 
			(< 
				(v) 10 ) 
			(< 
				(d) 100 ) 
			(> 
				(d) 
					(- 
						100 10 ) ) 
			(not (force-events)) )
 :effect 
	(and 
		(landed) 
			(not 
				(landing) ) )
)

(:action start-thrust
 :parameters()
 :precondition 
	(and 
		
			(not 
				(thrusting) ) (landing) 
			(not 
				(block) ) 
			(not (force-events)) )
 :effect 
	(and 
		(thrusting) 
			(assign 
				(thrust-duration) 0 ) )
)

(:action stop-thrust
 :parameters()
 :precondition 
	(and 
		(thrusting) 
			(not 
				(block) ) 
			(not (force-events)) )
 :effect 
	(and 
		
			(not 
				(thrusting) ) )
)

(:action simulate-events
 :parameters()
 :precondition 
	(and (force-events))
 :effect 
	(and 
		
			(not (force-events)) 
			
				(when 
					
						(and 
							
								(not 
									
										(and 
											
												(< 
													(d) 100 ) 
												(> 
													(M) 5000 ) ) ) 
								(not 
									(block) ) ) 
						(and 
							(block) ) )  )
)

(:action simulate_processes_00
 :parameters()
 :precondition 
	(and 
		
			(not 
				(and 
					
						(< 
							(falling-time) 40 ) (landing) 
						(< 
							(d) 100 ) 
						(not 
							(block) ) )) 
			(not 
				(and 
					
						(< 
							(thrust-duration) 
								(/ 
									
										(- 
											(M) 5000 ) q ) ) (landing) (thrusting) 
						(not 
							(block) ) )) 
			(not (force-events)) )
 :effect 
	(and 
		
			(increase 
				(total-time) (delta) ) (force-events) )
)

(:action simulate_processes_01
 :parameters()
 :precondition 
	(and 
		
			(not 
				(and 
					
						(< 
							(falling-time) 40 ) (landing) 
						(< 
							(d) 100 ) 
						(not 
							(block) ) )) 
			(and 
				
					(< 
						(thrust-duration) 
							(/ 
								
									(- 
										(M) 5000 ) q ) ) (landing) (thrusting) 
					(not 
						(block) ) ) 
			(not (force-events)) )
 :effect 
	(and 
		
			(decrease 
				(v) 
					(* 
						(delta) 
							(* 
								
									(* 
										(ISP) (g) ) 
									(/ 
										(q) (M) ) ) ) ) 
			(decrease 
				(M) 
					(* 
						(delta) (q) ) ) 
			(increase 
				(thrust-duration) 
					(* 
						(delta) 1.0 ) ) 
			(increase 
				(total-time) (delta) ) (force-events) )
)

(:action simulate_processes_10
 :parameters()
 :precondition 
	(and 
		
			(and 
				
					(< 
						(falling-time) 40 ) (landing) 
					(< 
						(d) 100 ) 
					(not 
						(block) ) ) 
			(not 
				(and 
					
						(< 
							(thrust-duration) 
								(/ 
									
										(- 
											(M) 5000 ) q ) ) (landing) (thrusting) 
						(not 
							(block) ) )) 
			(not (force-events)) )
 :effect 
	(and 
		
			(increase 
				(d) 
					(* 
						(delta) 
							(* 
								0.5 (v) ) ) ) 
			(increase 
				(v) 
					(* 
						(delta) (g) ) ) 
			(increase 
				(falling-time) 
					(* 
						(delta) 1.0 ) ) 
			(increase 
				(total-time) (delta) ) (force-events) )
)

(:action simulate_processes_11
 :parameters()
 :precondition 
	(and 
		
			(and 
				
					(< 
						(falling-time) 40 ) (landing) 
					(< 
						(d) 100 ) 
					(not 
						(block) ) ) 
			(and 
				
					(< 
						(thrust-duration) 
							(/ 
								
									(- 
										(M) 5000 ) q ) ) (landing) (thrusting) 
					(not 
						(block) ) ) 
			(not (force-events)) )
 :effect 
	(and 
		
			(increase 
				(d) 
					(* 
						(delta) 
							(* 
								0.5 (v) ) ) ) 
			(increase 
				(v) 
					(+ 
						
							(* 
								(delta) (g) ) 
							(* 
								(minus-delta) 
									(* 
										
											(* 
												(ISP) (g) ) 
											(/ 
												(q) (M) ) ) ) ) ) 
			(increase 
				(falling-time) 
					(* 
						(delta) 1.0 ) ) 
			(decrease 
				(M) 
					(* 
						(delta) (q) ) ) 
			(increase 
				(thrust-duration) 
					(* 
						(delta) 1.0 ) ) 
			(increase 
				(total-time) (delta) ) (force-events) )
)

)