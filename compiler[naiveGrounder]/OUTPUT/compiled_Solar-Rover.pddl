(define (domain grounded-generator2)
(:requirements :fluents :adl :typing :time)(:predicates
	(night)
	(datatosend)
	(gboff__GB)
	(off__b1)
	(off__b2)
	(off__b3)
	(datasent)
	(gbon__GB)
	(roversafe)
	(on__b1)
	(on__b2)
	(on__b3)
	(sunexposure)
	(prop-true)
	(event-done--0)
	(event-done--1)
	(event-done--2)
	(event-done--3)
	(event-done--4)
	(event-done--5)
	(prop-false)
)

(:functions
	(roverenergy)
	(SoC__b1)
	(SoC__b2)
	(SoC__b3)
	(sunexposure_time)
	(time)
)

(:action simulate_action-start_useBattery__b1-0
 :parameters()
 :precondition 
	(and 
		(off__b1) 
			(not 
				(prop-false) ) (event-done--0) )
 :effect 
	(and 
		
			(not (event-done--0)) (event-done--1) 
			(increase 
				(roverenergy) 10 ) (on__b1) 
			(not 
				(off__b1) ) )
)

(:action simulate_action-start_useBattery__b2-1
 :parameters()
 :precondition 
	(and 
		(off__b2) 
			(not 
				(prop-false) ) (event-done--1) )
 :effect 
	(and 
		
			(not (event-done--1)) (event-done--2) 
			(increase 
				(roverenergy) 10 ) (on__b2) 
			(not 
				(off__b2) ) )
)

(:action simulate_action-start_useBattery__b3-2
 :parameters()
 :precondition 
	(and 
		(off__b3) 
			(not 
				(prop-false) ) (event-done--2) )
 :effect 
	(and 
		
			(not (event-done--2)) (event-done--3) 
			(increase 
				(roverenergy) 10 ) (on__b3) 
			(not 
				(off__b3) ) )
)

(:action simulate_action-switchGenBatteryOn__GB-3
 :parameters()
 :precondition 
	(and 
		(gboff__GB) 
			(not 
				(prop-false) ) (event-done--3) )
 :effect 
	(and 
		
			(not (event-done--3)) (event-done--4) (gbon__GB) 
			(not 
				(gboff__GB) ) (roversafe) 
			(increase 
				(roverenergy) 100 ) )
)

(:action simulate_action-sendData-4
 :parameters()
 :precondition 
	(and 
		(datatosend) (roversafe) 
			(>= 
				(roverenergy) 500 ) 
			(not 
				(prop-false) ) (event-done--4) )
 :effect 
	(and 
		
			(not (event-done--4)) (event-done--5) (datasent) 
			(not 
				(datatosend) ) )
)

(:event end_useBattery__b1
 :parameters()
 :precondition 
	(and 
		(on__b1) 
			(or 
				
					(not 
						(roversafe) ) 
					(not 
						
							(> 
								(SoC__b1) 0 ) ) ) )
 :effect 
	(and 
		
			(not 
				(on__b1) ) )
)

(:event end_useBattery__b2
 :parameters()
 :precondition 
	(and 
		(on__b2) 
			(or 
				
					(not 
						(roversafe) ) 
					(not 
						
							(> 
								(SoC__b2) 0 ) ) ) )
 :effect 
	(and 
		
			(not 
				(on__b2) ) )
)

(:event end_useBattery__b3
 :parameters()
 :precondition 
	(and 
		(on__b3) 
			(or 
				
					(not 
						(roversafe) ) 
					(not 
						
							(> 
								(SoC__b3) 0 ) ) ) )
 :effect 
	(and 
		
			(not 
				(on__b3) ) )
)

(:event sunshine
 :parameters()
 :precondition 
	(and 
		(night) (sunexposure) )
 :effect 
	(and 
		
			(not 
				(night) ) 
			(increase 
				(roverenergy) 400 ) )
)

(:event sunexposure_event
 :parameters()
 :precondition 
	(and 
		
			(>= 
				(time) 300.0 ) 
			(not 
				(sunexposure) ) )
 :effect 
	(and 
		(sunexposure) )
)

(:process useBattery__b1
 :parameters()
 :precondition 
	(and 
		(on__b1) (roversafe) 
			(> 
				(SoC__b1) 0 ) 
			(not (prop-false)) )
 :effect 
	(and 
		
			(decrease 
				(SoC__b1) 
					(* 
						#t 1 ) ) )
)

(:process useBattery__b2
 :parameters()
 :precondition 
	(and 
		(on__b2) (roversafe) 
			(> 
				(SoC__b2) 0 ) 
			(not (prop-false)) )
 :effect 
	(and 
		
			(decrease 
				(SoC__b2) 
					(* 
						#t 1 ) ) )
)

(:process useBattery__b3
 :parameters()
 :precondition 
	(and 
		(on__b3) (roversafe) 
			(> 
				(SoC__b3) 0 ) 
			(not (prop-false)) )
 :effect 
	(and 
		
			(decrease 
				(SoC__b3) 
					(* 
						#t 1 ) ) )
)

(:process passingTime
 :parameters()
 :precondition 
	(and 
		
			(not (prop-false)) )
 :effect 
	(and 
		
			(increase 
				(time) 
					(* 
						#t 1 ) ) )
)

)